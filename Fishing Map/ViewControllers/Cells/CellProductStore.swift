//
//  CellProductStore.swift
//  Fishing Map
//
//  Created by macOS on 08/11/20.
//

import UIKit

class CellProductStore: UICollectionViewCell {
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnShopNow: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func cellConfiguration(_ objProduct: Products) {
        imgProduct.sd_setImage(with: URL(string: objProduct.productImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblName.text = objProduct.productName
        lblPrice.text = "‎¥‎ \(objProduct.productPrice ?? "0")"
        lblDescription.text = objProduct.productDescription
    }

}
