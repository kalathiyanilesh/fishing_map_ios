//
//  CellPost.swift
//  Fishing Map
//
//  Created by iMac on 04/11/20.
//

import UIKit

class CellPost: UICollectionViewCell {

    @IBOutlet var viewContent: UIView!
    @IBOutlet var imgPost: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTotalLikes: UILabel!
    @IBOutlet var lblTotalComments: UILabel!
    @IBOutlet var btnLikes: UIButton!
    @IBOutlet var btnComments: UIButton!
    @IBOutlet var viewLike: UIView!
    @IBOutlet var viewComment: UIView!
    @IBOutlet var heightViewLike: NSLayoutConstraint!
    @IBOutlet var heightViewComment: NSLayoutConstraint!
    @IBOutlet var topOfLike: NSLayoutConstraint!
    @IBOutlet var heightImgFish: NSLayoutConstraint!
    @IBOutlet var topOfComment: NSLayoutConstraint!
    @IBOutlet var viewSlots: UIView!
    @IBOutlet var lblParkingCost: UILabel!
    @IBOutlet var imgLike: UIImageView!
    @IBOutlet var lblTotalSlots: UILabel!
    @IBOutlet var yImageName: NSLayoutConstraint!
    @IBOutlet var heightImageName: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
