//
//  TextVC.swift
//  Fishing Map
//
//  Created by iMac on 12/01/21.
//

import UIKit

class TextVC: UIViewController {

    @IBOutlet var txtText: UITextView!
    var sText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtText.text = sText
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
