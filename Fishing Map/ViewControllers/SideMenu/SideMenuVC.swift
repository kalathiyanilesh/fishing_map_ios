//
//  SideMenuVC.swift
//  Fishing Map
//
//  Created by macOS on 03/11/20.
//

import UIKit

class CellUpcomingEvents: UITableViewCell {
    @IBOutlet weak var viewEventDate: UIView!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
}

class CellOptions: UITableViewCell {
    @IBOutlet weak var lblOptionName: UILabel!
    @IBOutlet weak var imgOption: UIImageView!
}

class SideMenuVC: UIViewController {
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblTotalLike: UILabel!
    @IBOutlet var lblTotalImage: UILabel!
    
    @IBOutlet var tblSideMenuOptionList: UITableView!
    @IBOutlet var viewBackground: UIView!
    @IBOutlet var leadingContentView: NSLayoutConstraint!
    @IBOutlet var leadingTop: NSLayoutConstraint!
    @IBOutlet var yTop: NSLayoutConstraint!
    
    var arrSideMenuOptions = ["スポットを追加", "通知", "プライバシーポリシー", "コンタクト"]
    var arrImgOptions = ["icn_plus_s0de", "icn_notification", "icn_privacy", "icn_contact"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.isHidden = true
        tblSideMenuOptionList.register(UINib(nibName: "SideMenuBottomView", bundle: nil), forHeaderFooterViewReuseIdentifier: "SideMenuTableFooterView")
        leadingContentView.constant = SCREENWIDTH()
        leadingTop.constant = SCREENWIDTH()
        yTop.constant = topSafeAreaHeight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUserInfo()
        apiGetUserInfo { (isSuccess) in
            self.setUserInfo()
            self.tblSideMenuOptionList.reloadData()
        }
        runAfterTime(time: 0.2) {
            UIView.animate(withDuration: 0.2) {
                self.leadingContentView.constant = 0
                self.leadingTop.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setUserInfo() {
        imgUser.sd_setImage(with: URL(string: APP_DELEGATE.objUser?.userAvatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblUserName.text = APP_DELEGATE.objUser?.userName
        lblTotalLike.text = APP_DELEGATE.objUser?.likesCount
        lblTotalImage.text = APP_DELEGATE.objUser?.imagesCount
        if lblTotalLike.text?.count ?? 0 <= 0 { lblTotalLike.text = "0" }
        if lblTotalImage.text?.count ?? 0 <= 0 { lblTotalImage.text = "0" }
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnHideMenu(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.leadingContentView.constant = SCREENWIDTH()
            self.leadingTop.constant = SCREENWIDTH()
            self.view.layoutIfNeeded()
        }) { (finished) in
            runAfterTime(time: 0.1) {
                self.dismiss(animated: false)
            }
        }
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
        vc.isFromSideMenu = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: "サインアウトしますか?", preferredStyle: .alert)
        let actionYes = UIAlertAction(title: "はい", style: .default) { (action:UIAlertAction) in
            Logoutuser()
        }
        let actionNo = UIAlertAction(title: "いいえ", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK:- UITABLEVIEW DELEGATE METHOD
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return APP_DELEGATE.objUser?.upcomingEvents?.count ?? 0
        case 1:
            return arrSideMenuOptions.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: CellUpcomingEvents = tableView.dequeueReusableCell(withIdentifier: "CellUpcomingEvents", for: indexPath) as! CellUpcomingEvents
            if let objEvent: UpcomingEvents = APP_DELEGATE.objUser?.upcomingEvents?[indexPath.row] {
                cell.lblEventName.text = objEvent.eventTitle
                cell.lblEventDate.text = intToDate(miliseconds: Int(objEvent.eventDate ?? "0") ?? 0, shortMonthName: true)
            }
            if indexPath.row%2 == 0 {
                cell.lblEventDate.backgroundColor = Color_Hex(hex: "FF7646")
            } else {
                cell.lblEventDate.backgroundColor = Color_Hex(hex: "006677")
            }
            return cell
        case 1:
            let cell: CellOptions = tableView.dequeueReusableCell(withIdentifier: "CellOptions", for: indexPath) as! CellOptions
            cell.imgOption.image = UIImage(named: arrImgOptions[indexPath.row])
            cell.lblOptionName.text = arrSideMenuOptions[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if let objEvent: UpcomingEvents = APP_DELEGATE.objUser?.upcomingEvents?[indexPath.row] {
                apiGetEventDetails(objEvent.eventId ?? "") { (objEvent) in
                    let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventsDetailsVC") as! EventsDetailsVC
                    vc.event = objEvent
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case 1:
            //var arrSideMenuOptions = ["スポットを追加", "Notifications", "Privacy Policy", "コンタクト"]
            let sOptionName = arrSideMenuOptions[indexPath.row]
            switch sOptionName {
            case "スポットを追加":
                let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "AddLocationVC")
                self.navigationController?.pushViewController(vc, animated: true)
            case "通知":
                let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "NotificationVC")
                self.navigationController?.pushViewController(vc, animated: true)
            case "プライバシーポリシー":
                let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "PrivacyPolicyVC") as! PrivacyPolicyVC
                vc.isPrivacy = true
                self.navigationController?.pushViewController(vc, animated: true)
            case "コンタクト":
                let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "PrivacyPolicyVC") as! PrivacyPolicyVC
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 72
        case 1:
            return 55
        default:
            return 0
        }
    }
}
