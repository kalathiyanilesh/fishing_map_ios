//
//  AddLocationVC.swift
//  Fishing Map
//
//  Created by iMac on 04/11/20.
//

import UIKit
import IQKeyboardManagerSwift

class CellAddLocation: UITableViewCell {
    @IBOutlet var btnCellLoc: UIButton!
    @IBOutlet var imgLocationType: UIImageView!
    @IBOutlet var lblLocationType: UILabel!
    @IBOutlet var lblPriceTitle: UILabel!
    @IBOutlet var txtPrice: UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var lblImageName: UILabel!
    @IBOutlet var txtDescription: IQTextView!
    @IBOutlet var btnUseCurrentLoc: UIButton!
}

class AddLocationVC: UIViewController {

    @IBOutlet var tblAddLocation: UITableView!
    
    var arrLocation = ["locationtype", "addnameoflocation","addlocation", "uploadimage", "adddescription"]
    var dictDetails = NSMutableDictionary()
    var arrCategory: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnUseCurrentLocationTapped()
        for catName in getArrCatName() {
            arrCategory.append("\(catName) ロケーション")
        }
    }
    
    @IBAction func btnSendForApproval(_ sender: Any) {
        self.view.endEditing(true)
        var selectedImage = UIImage()
        let dictParam: NSMutableDictionary = NSMutableDictionary()
        if let indexSelected = dictDetails.object(forKey: "category_id") as? Int {
            dictParam.setObject(getArrCatIDs()[indexSelected], forKey: "category_id" as NSCopying)
        } else {
            showMessage("スポットの詳細を選択してください")
            return
        }
        
        if let name = dictDetails.object(forKey: "name") as? String {
            dictParam.setObject(name, forKey: "name" as NSCopying)
        } else {
            showMessage("スポットの名前を入力してください")
            return
        }
        
        if let address = dictDetails.object(forKey: "map_address") as? String {
            dictParam.setObject(address, forKey: "map_address" as NSCopying)
            dictParam.setObject("\(dictDetails.object(forKey: "lat") as? Double ?? 0)", forKey: "lat" as NSCopying)
            dictParam.setObject("\(dictDetails.object(forKey: "lng") as? Double ?? 0)", forKey: "lng" as NSCopying)
        } else {
            showMessage("場所を入力してください")
            return
        }
        
        if let image = dictDetails.object(forKey: "image") as? UIImage {
            selectedImage = image
        } else {
            showMessage("写真をアップロードしてください")
            return
        }
        
        let cateID = dictDetails.object(forKey: "category_id") as? Int ?? 0
        if cateID != 0 {
            if let price = dictDetails.object(forKey: "price") as? String {
                dictParam.setObject(price, forKey: "price" as NSCopying)
            } else {
                showMessage("価格を入力してください")
                return
            }
        }
        
        if let description = dictDetails.object(forKey: "description") as? String {
            dictParam.setObject(description, forKey: "description" as NSCopying)
        } else {
            showMessage("情報を入力してください")
            return
        }
        print(dictParam)
        
        showLoaderHUD(strMessage: "")
        UploadImageToCloud(selectedImage) { (imgurl) in
            dictParam.setObject(imgurl, forKey: "image_url" as NSCopying)
            self.apiAddLocation(dictParam)
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension AddLocationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellAddLocation = tableView.dequeueReusableCell(withIdentifier: arrLocation[indexPath.row], for: indexPath) as! CellAddLocation
        if cell.btnCellLoc != nil {
            cell.btnCellLoc.addTarget(self, action: #selector(btnCellLoc(_:)), for: .touchUpInside)
        }
        let sPostType = arrLocation[indexPath.row]
        switch sPostType {
        case "locationtype":
            cell.lblLocationType.text = "選択"
            cell.lblLocationType.textColor = Color_Hex(hex: "797A7B")
            if let index = dictDetails.object(forKey: "category_id") as? Int {
                cell.lblLocationType.text = arrCategory[index]
                cell.lblLocationType.textColor = UIColor.black
            }
            if cell.lblLocationType.text == "選択" {
                cell.imgLocationType.image = UIImage(named: "Select_cat")
            } else {
                let catName = cell.lblLocationType.text?.replacingOccurrences(of: " ロケーション", with: "") ?? ""
                let imageURL = getCatImage(catName)
                cell.imgLocationType.sd_setImage(with: imageURL, placeholderImage: PLACE_HOLDER_CATEGORY, options: .progressiveLoad, context: nil)
            }
        case "addlocation":
            if let address = dictDetails.object(forKey: "map_address") as? String {
                cell.txtAddress.text = address
            }
            cell.btnUseCurrentLoc.addTarget(self, action: #selector(btnUseCurrentLocationTapped), for: .touchUpInside)
        case "addnameoflocation":
            cell.txtName.tag = 1
            cell.txtName.delegate = self
            if let name = dictDetails.object(forKey: "name") as? String {
                cell.txtName.text = name
            }
        case "uploadimage":
            cell.lblImageName.text = "アップロード"
            if let sImageName = dictDetails.object(forKey: "imageName") as? String {
                cell.lblImageName.text = sImageName
            }
        case "price":
            cell.txtPrice.tag = 2
            cell.txtPrice.delegate = self
            cell.txtPrice.text = ""
            if let price = dictDetails.object(forKey: "price") as? String {
                cell.txtPrice.text = price
            }
            let index = dictDetails.object(forKey: "category_id") as? Int ?? 0
            if index == 1 {
                cell.lblPriceTitle.text = "価格 ( ２人 )"
            } else {
                cell.lblPriceTitle.text = "価格（1日）"
            }
        case "adddescription":
            cell.txtDescription.delegate = self
            if let description = dictDetails.object(forKey: "description") as? String {
                cell.txtDescription.text = description
            }
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //["locationtype", "addnameoflocation","addlocation", "uploadimage", "adddescription"]
        let sPostType = arrLocation[indexPath.row]
        switch sPostType {
        case "addlocation":
            return UITableView.automaticDimension
        case "locationtype","addnameoflocation":
            return 111
        case "adddescription":
            return 210
        default:
            return 69
        }
    }
    
    @objc func btnCellLoc(_ sender: UIButton) {
        //["locationtype", "addnameoflocation","addlocation", "uploadimage", "adddescription"]
        self.view.endEditing(true)
        let buttonPosition = sender.convert(CGPoint.zero, to: tblAddLocation)
        if let indexPath = tblAddLocation.indexPathForRow(at:buttonPosition) {
            let sPostType = arrLocation[indexPath.row]
            switch sPostType {
            case "locationtype":
                addLocationType()
            case "uploadimage":
                ImagePickerManager().pickImage(self, false) { (img, sPath) in
                    self.dictDetails.setObject(sPath, forKey: "imageName" as NSCopying)
                    self.dictDetails.setObject(img, forKey: "image" as NSCopying)
                    self.tblAddLocation.reloadData()
                }
            default:
                break
            }
        }
    }
    
    @objc func btnUseCurrentLocationTapped() {
        self.view.endEditing(true)
        getAddress(lat_currnt, long_currnt) { (address) in
            self.dictDetails.setObject(address, forKey: "map_address" as NSCopying)
            self.dictDetails.setObject(lat_currnt, forKey: "lat" as NSCopying)
            self.dictDetails.setObject(long_currnt, forKey: "lng" as NSCopying)
            self.tblAddLocation.reloadData()
        }
    }
}

//MARK:- API CALLING
extension AddLocationVC {
    func apiAddLocation(_ dictParam: NSDictionary) {
        self.view.endEditing(true)
        let dictLocation: NSDictionary = ["location":dictParam]
        print("Location Parameters: ", dictLocation)
        let sURL = SERVER_URL + API_LOCATION
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOCATION, parameters: dictLocation, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            hideLoaderHUD()
            if error == nil {
                print(responseObject ?? "")
                if responseObject?["success"] as? String == "1" {
                    showMessage("スポットが作成されました")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(responseObject?["message"] as? String ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}

//MARK:- UIBUTTON CELLS ACTION
extension AddLocationVC {
    func addLocationType() {
        var indexSelected = 0
        let alert = UIAlertController(style: .actionSheet, title: "スポットの種類を選択", message: nil)
        alert.addPickerView(values: [arrCategory], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    indexSelected = index.row
                }
            }
        }
        alert.addAction(title: "完了", style: .cancel) { (action) in
            self.arrLocation = ["locationtype", "addnameoflocation","addlocation", "uploadimage", "adddescription"]
            if indexSelected != 0 {
                self.arrLocation = ["locationtype", "addnameoflocation","addlocation", "uploadimage", "price", "adddescription"]
                if (self.dictDetails.object(forKey: "price") as? String) != nil {
                    self.dictDetails.removeObject(forKey: "price")
                }
            }
            self.dictDetails.setObject(indexSelected, forKey: "category_id" as NSCopying)
            self.tblAddLocation.reloadData()
        }
        alert.show(self)
    }
}

// MARK:- UITEXTVIEW DELEGATE METHOD
extension AddLocationVC: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if TRIM(string: textView.text ?? "").count > 0 {
            dictDetails.setObject(TRIM(string: textView.text ?? ""), forKey: "description" as NSCopying)
        }
    }
}

// MARK:- UITEXTFIELD DELEGATE METHOD
extension AddLocationVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            if textField.tag == 1 {
                dictDetails.setObject(TRIM(string: textField.text ?? ""), forKey: "name" as NSCopying)
            } else if textField.tag == 2 {
                dictDetails.setObject(TRIM(string: textField.text ?? ""), forKey: "price" as NSCopying)
            }
        }
    }
}
