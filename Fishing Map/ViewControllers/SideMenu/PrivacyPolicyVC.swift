//
//  PrivacyPolicyVC.swift
//  Fishing Map
//
//  Created by iMac on 02/02/21.
//

import UIKit

class CellPrivacyPolicy: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
}

class PrivacyPolicyVC: UIViewController {
    @IBOutlet var lblTitle: UILabel!
    var isPrivacy: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = isPrivacy ? "プライバシーポリシー" : "コンタクト"
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension PrivacyPolicyVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isPrivacy ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellPrivacyPolicy = tableView.dequeueReusableCell(withIdentifier: "CellPrivacyPolicy", for: indexPath) as! CellPrivacyPolicy
        if isPrivacy {
            switch indexPath.row {
            case 0:
                cell.lblTitle.text = "当社は、以下のいずれかに該当する場合を除き、 個人情報を第三者へ開示また提供しません。"
                cell.lblDesc.text = "・ご本人の同意がある場合\n\n・個人情報の取扱に関する業務の全部または一部を委託する場合,（但しこの場合、当社は委託先との間で個人情報保護に関する契約を締結する等、委託先の適切な監督に努めます。）\n\n・統計的なデータなどご本人を識別することができない状態で開示・提供する場合\n\n・法令に基づき開示・提供を求められた場合\n\n・国または地方公共団体等が公的な事務を実施するうえで、協力する必要がある場合であって、ご本人の同意を得ることによ,り当該事務の遂行に支障を及ぼすおそれがある場合保有個人データに関して、皆様がご自身の情報の開示をご希望される場合,には、お申し出いただいた方がご本人であることを確認したうえで、合理的な期間および範囲で回答します。"
            default:
                cell.lblTitle.text = "但し、以下に記載する場合は非開示とさせていただきます。"
                cell.lblDesc.text = "・非開示を決定した場合は、その旨理由を付して通知させていただきます。\n\n・保有個人データのご本人であることが確認できない場合。\n\n・代理人による申請に際して、代理権が確認できない場合\n\n・申込書面に不備があった場合。\n\n・開示の求めの対象が保有個人データに該当しない場合。\n\n・本人または第三者の生命、身体、財産その他の権利利益を害するおそれがある場合。\n\n・当社の業務の適正な実施に著しい支障を及ぼすおそれがある場合。\n\n・他の法令に違反することとなる場合。\n\n・その他、個人情報保護法に基づき開示する義務を負わない場合。"
            }
        } else {
            cell.lblTitle.text = "WildScene"
            cell.lblDesc.text = "住所：〒437-0045 静岡県袋井市彦島6-2\n\neMail：support@wildscene.info\n\n営業日：9:00~17:00\n\n定休日：土、日、祝日"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
