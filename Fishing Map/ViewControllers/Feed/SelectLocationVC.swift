//
//  SelectLocationVC.swift
//  Fishing Map
//
//  Created by iMac on 30/01/21.
//

import UIKit

class CellLocation: UICollectionViewCell {
    @IBOutlet var viewMain: UIView!
    @IBOutlet var imgLoc: UIImageView!
    @IBOutlet var lblLoc: UILabel!
}

protocol delegateSelectLoc {
    func selectedLoc(_ objLoc: NewLocation?)
}

class SelectLocationVC: UIViewController {

    @IBOutlet var txtSearchLoc: UITextField!
    @IBOutlet var collLocations: UICollectionView!
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var heightViewSearch: NSLayoutConstraint!
    @IBOutlet var viewSubmit: UIView!
    @IBOutlet var heightViewSubmit: NSLayoutConstraint!
    
    var delegateSelectLoc: delegateSelectLoc?
    
    var arrLocations = NSMutableArray()
    var sImageType = ""
    
    var selectedLat: Double = 0
    var selectedLong: Double = 0
    var objSelectedLoc: NewLocation?
    
    var isFromLocate: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedLat = lat_currnt
        selectedLong = long_currnt
        txtSearchLoc.delegate = self
        apiGetLocations()
        if isFromLocate {
            viewSearch.alpha = 0
            viewSubmit.alpha = 0
            heightViewSearch.constant = 0
            heightViewSubmit.constant = 0
        }
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClearText(_ sender: Any) {
        self.view.endEditing(true)
        txtSearchLoc.text = ""
        apiGetLocations()
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        print("btnSubmit")
        if objSelectedLoc != nil {
            self.delegateSelectLoc?.selectedLoc(objSelectedLoc)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension SelectLocationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLocations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellLocation = collectionView.dequeueReusableCell(withReuseIdentifier: "CellLocation", for: indexPath) as! CellLocation
        let objLoc: NewLocation = arrLocations.object(at: indexPath.row) as! NewLocation
        if objLoc.locationId == objSelectedLoc?.locationId {
            cell.viewMain.layer.borderColor = Color_Hex(hex: "056989").cgColor
        } else {
            cell.viewMain.layer.borderColor = Color_Hex(hex: "E8E8E8").cgColor
        }
        
        cell.lblLoc.text = objLoc.locationName
        cell.imgLoc.sd_setImage(with: URL(string: objLoc.locationImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        objSelectedLoc = arrLocations.object(at: indexPath.row) as? NewLocation
        if isFromLocate {
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
            vc.imgType = getPushImageType(sImageType)
            vc.sLocationID = objSelectedLoc?.locationId
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            collLocations.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(SCREENWIDTH()-30)/2, height:170)
    }
}

//MARK:- API CALLING
extension SelectLocationVC {
    func apiGetLocations() {
        objSelectedLoc = nil
        var sURL = SERVER_URL + API_LOCATION
        var dictParam: NSDictionary = ["image_type": sImageType, "lat": "\(selectedLat)", "lng": "\(selectedLong)"]
        if TRIM(string: txtSearchLoc.text ?? "").count > 0 {
            sURL = (SERVER_URL + API_SEARCH_LOCATION + "?term=\(TRIM(string: txtSearchLoc.text ?? ""))").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            dictParam = NSDictionary()
        }
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOCATION, parameters: dictParam, method: .get, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                self.arrLocations = NSMutableArray()
                let objLoc: FMNewLocation = FMNewLocation.init(object: responseObject ?? "")
                if objLoc.success == "1" {
                    for loc in objLoc.locations ?? [] {
                        self.arrLocations.add(loc)
                    }
                } else {
                    showMessage(objLoc.message ?? ErrorMSG)
                }
                self.collLocations.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension SelectLocationVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        apiGetLocations()
    }
}
