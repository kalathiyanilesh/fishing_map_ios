//
//  FeedSearchVC.swift
//  Fishing Map
//
//  Created by macOS on 02/11/20.
//

import UIKit

class CellUsedTags: UITableViewCell {
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblUsedTag: UILabel!
}

class CellLatestPosts: UITableViewCell {
    @IBOutlet weak var heightHeader: NSLayoutConstraint!
    @IBOutlet weak var topOfImage: NSLayoutConstraint!
    @IBOutlet weak var imgLatestPost: UIImageView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostDate: UILabel!
    @IBOutlet weak var imgPostCategory: UIImageView!
    
    func configureCell(post: Posts) {
        imgLatestPost.sd_setImage(with: URL(string: post.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        imgUserProfile.sd_setImage(with: URL(string: post.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblUserName.text = post.userName
        lblPostDate.text = intToDate(miliseconds: post.postDate ?? 0, shortMonthName: false)
        imgPostCategory.sd_setImage(with: URL(string: post.postCategoryIconUrl ?? ""))
    }
}

class CellAddedLocations: UITableViewCell {
    @IBOutlet weak var collAddedLocation: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collAddedLocation.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 25, bottom: 20, right: 25)
        layout.itemSize = CGSize(width: 148, height: 170)
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        collAddedLocation!.collectionViewLayout = layout
    }
}

extension CellAddedLocations {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collAddedLocation.delegate = dataSourceDelegate
        collAddedLocation.dataSource = dataSourceDelegate
        collAddedLocation.tag = row
        collAddedLocation.setContentOffset(collAddedLocation.contentOffset, animated:false)
        collAddedLocation.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collAddedLocation.contentOffset.x = newValue }
        get { return collAddedLocation.contentOffset.x }
    }
}

class FeedSearchVC: UIViewController {
    
    @IBOutlet weak var tblSearchFeeds: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    
    var isForLocation = false
    var isForProduct = false
    var arrUsedTags = ["人気のタグ", "#Wildscene", "#Fishing", "#Mackarel", "#Wildscene", "#Fishing", "#Mackarel"]
    var arrUsedLocations = ["Top Searched Prefectures", "Tokyo", "Ibaraki", "Tochigi", "Gunma", "Miyagi", "Akita"]
    var arrTopProducts = ["人気の商品", "Fishing Rod", "Assist Hook", "High Grip Gloves", "Metal Jig"]
    var storedOffsets = [Int: CGFloat]()
    
    var arrSearchTags : [Tags] = []
    var arrSearchPosts : [Posts] = []
    var arrSearchLoc : [NewLocation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrUsedLocations.removeAll()
        arrTopProducts.removeAll()
        setupUI()
        if isForLocation {
            apiGetNewLocation()
        } else {
            getInitialSearches()
        }
    }
    
    private func setupUI() {
        if isForLocation {
            tfSearch.placeholder = "場所の検索"
        } else if isForProduct {
            tfSearch.placeholder = "釣具の検索"
        } else {
            tfSearch.placeholder = "フィードの検索"
        }
    }
    
    func getInitialSearches() {
        showLoaderHUD(strMessage: "")
        
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: SERVER_URL + API_GET_SEARCH_INITIAL, httpHeader: getAuthToken()) { (response, error)  in
            
            hideLoaderHUD()
            
            let objInitialSearch: FMInitialSearches = FMInitialSearches.init(object: response ?? "")
            
            self.arrSearchTags = objInitialSearch.tags ?? []
            self.arrSearchPosts = objInitialSearch.posts ?? []
            
            self.tblSearchFeeds.reloadData()
        }
    }
    
    func apiGetNewLocation() {
        let sURL = SERVER_URL + API_NEW_LOCATIONS
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_NEW_LOCATIONS, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: true) { (error, response) in
            print(response ?? "")
            if error == nil {
                let objLoc: FMNewLocation = FMNewLocation.init(object: response ?? "")
                if objLoc.success == "1" {
                    self.arrSearchLoc.removeAll()
                    self.arrSearchLoc = objLoc.locations ?? []
                    self.tblSearchFeeds.reloadData()
                }
            }
        }
    }
    
    @IBAction func btnCloseSearch(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK:- UITABLEVIEW DELEGATE
extension FeedSearchVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return isForProduct ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isForLocation {
                return arrUsedLocations.count
            }
            return isForProduct ? arrTopProducts.count : arrSearchTags.count + 1
        case 1:
            return isForLocation ? 1 : arrSearchPosts.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: CellUsedTags = tableView.dequeueReusableCell(withIdentifier: "CellUsedTags", for: indexPath) as! CellUsedTags
            if indexPath.row == 0 {
                cell.lblHeader.isHidden = false
                cell.lblUsedTag.isHidden = true
            } else {
                cell.lblHeader.isHidden = true
                cell.lblUsedTag.isHidden = false
            }
            if isForLocation {
                cell.lblHeader.text = arrUsedLocations[indexPath.row]
                cell.lblUsedTag.text = arrUsedLocations[indexPath.row]
            } else if isForProduct {
                cell.lblHeader.text = arrTopProducts[indexPath.row]
                cell.lblUsedTag.text = arrTopProducts[indexPath.row]
            } else {
                cell.lblHeader.text = "人気のタグ"
                guard let tags = indexPath.row == 0 ? "" : arrSearchTags[indexPath.row - 1].tag else {
                    return cell
                }
                cell.lblUsedTag.text = indexPath.row == 0 ? "" : "#\(tags)"
            }
            return cell
        case 1:
            if isForLocation {
                let cell: CellAddedLocations = tableView.dequeueReusableCell(withIdentifier: "CellAddedLocations", for: indexPath) as! CellAddedLocations
                
                return cell
            } else {
                let cell: CellLatestPosts = tableView.dequeueReusableCell(withIdentifier: "CellLatestPosts", for: indexPath) as! CellLatestPosts
                if indexPath.row == 0 {
                    cell.heightHeader.constant = 13
                    cell.topOfImage.constant = 9
                } else {
                    cell.heightHeader.constant = 0
                    cell.topOfImage.constant = -8
                }
                cell.configureCell(post: self.arrSearchPosts[indexPath.row])
                return cell
            }
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            return
        default:
            if isForLocation {
                guard let profileCell = cell as? CellAddedLocations else { return }
                profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
                profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
            } else {
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if isForLocation {
                if indexPath.row == 0 { return } //"Top Searched Prefectures"
                let sLoc = arrUsedLocations[indexPath.row]
                self.gotoLocResult(sLoc)
            } else if isForProduct {
                if indexPath.row == 0 { return } //"Top Products"
                let vc = loadVC(strStoryboardId: SB_Store, strVCId: "ProductSearchVC") as! ProductSearchVC
                vc.sProductName = arrTopProducts[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let tags = arrSearchTags[indexPath.row-1]
                self.gotoFeedResult(tags.tag ?? "")
            }
        case 1:
            if isForLocation {
                
            } else if isForProduct {
                
            } else {
                let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
                vc.post = arrSearchPosts[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        default:
            if isForLocation {
                let page = CGFloat(self.arrSearchLoc.count)/2
                let totalPage = Int(page.rounded(.up))
                let totalSpace = totalPage*20
                return CGFloat(totalPage*179) + CGFloat(totalSpace) //179 is collectionview cell height
            }
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            return
        default:
            if isForLocation {
                guard let profileCell = cell as? CellFishImages else { return }
                storedOffsets[indexPath.row] = profileCell.collectionViewOffset
            } else {
                return
            }
        }
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension FeedSearchVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSearchLoc.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.viewComment.isHidden = true
        cell.viewLike.isHidden = true
        cell.heightImgFish.constant = 128
        let objLoc = arrSearchLoc[indexPath.row]
        cell.imgPost.sd_setImage(with: URL(string: objLoc.locationImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        cell.lblTitle.text = objLoc.locationName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objLoc = arrSearchLoc[indexPath.row]
        let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
        if objLoc.locationCategoryId == "1" {
            vc.imgType = .Fish
        } else if objLoc.locationCategoryId == "2" {
            vc.imgType = .Restaurant
        } else if objLoc.locationCategoryId == "3" {
            vc.imgType = .Hotels
        }
        vc.sLocationID = objLoc.locationId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-78)/2, height:179)
    }
}

//MARK:- UITEXTFEILD DELEGATE
extension FeedSearchVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            if isForProduct {
                let vc = loadVC(strStoryboardId: SB_Store, strVCId: "ProductSearchVC") as! ProductSearchVC
                vc.sProductName = TRIM(string: textField.text ?? "")
                self.navigationController?.pushViewController(vc, animated: true)
            } else if isForLocation {
                self.gotoLocResult(TRIM(string: textField.text ?? ""))
            } else {
                self.gotoFeedResult(TRIM(string: textField.text ?? ""))
            }
        }
    }
    
    func gotoLocResult(_ sTag: String) {
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "LocateSearchVC") as! LocateSearchVC
        vc.sTag = sTag
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoFeedResult(_ sTag: String) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "FeedSearchResultVC") as! FeedSearchResultVC
        vc.sTag = sTag
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

