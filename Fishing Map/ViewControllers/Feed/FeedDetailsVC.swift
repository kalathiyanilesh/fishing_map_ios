//
//  FeedDetailsVC.swift
//  Fishing Map
//
//  Created by macOS on 28/10/20.
//

import UIKit
import SDWebImage
import GoogleMaps

protocol delegateFeedDetails {
    func deleteFeed(_ indexPath: IndexPath)
}

class FeedDetailsVC: UIViewController {

    @IBOutlet var tblDetails: UITableView!
    
    var post: Posts!
    var postComments: [PostComments]!
    
    var indexPath: IndexPath!
    var delegateFeedDetails: delegateFeedDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postComments = post.postComments ?? []
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnAddPost(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        vc.addImgType = .Post
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK:- UITABLEVIEW DELEGATE
extension FeedDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3//4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            if post.postCommentsCount ?? 0 > 5 {
                return 6
            } else {
                return postComments.count
            }
        case 3:
            return 0//1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            
            let cell: CellHomeFeedDetails = tableView.dequeueReusableCell(withIdentifier: "CellHomeFeedDetails", for: indexPath) as! CellHomeFeedDetails
            cell.btnBack.addTarget(self, action: #selector(btnBack), for: .touchUpInside)
            cell.btnLike.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
            //cell.btnMore.isHidden = self.post.isViewerAuthor ?? false
            cell.btnMore.addTarget(self, action: #selector(btnMore(_:)), for: .touchUpInside)
            cell.configureCell(post: self.post)
            
            let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
            cell.imgUser.isUserInteractionEnabled = true
            cell.imgUser.addGestureRecognizer(imgG)
            
            let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
            cell.lblUserName.isUserInteractionEnabled = true
            cell.lblUserName.addGestureRecognizer(lblG)
            
            return cell
            
        case 1:
            
            let cell: CellAddComment = tableView.dequeueReusableCell(withIdentifier: "CellAddComment", for: indexPath) as! CellAddComment
            cell.txtComment.delegate = self
            return cell
            
        case 2:
            let cell: CellComments = tableView.dequeueReusableCell(withIdentifier: "CellComments", for: indexPath) as! CellComments
            if post.postCommentsCount ?? 0 > 5 && indexPath.row == 5 {
                cell.viewAllComment.isHidden = false
                cell.lblViewCountComment.text = "全てのコメントを見る (\((post.postCommentsCount ?? 0)-5))" // View all 12 comments
            } else {
                cell.viewAllComment.isHidden = true
                cell.configureCell(postComment: self.postComments[indexPath.row])
                
                let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
                cell.imgCommentorUser.isUserInteractionEnabled = true
                cell.imgCommentorUser.addGestureRecognizer(imgG)
                
                let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
                cell.lblCommentorUser.isUserInteractionEnabled = true
                cell.lblCommentorUser.addGestureRecognizer(lblG)
            }
                        
            return cell
            
        case 3:
            let cell: CellMap = tableView.dequeueReusableCell(withIdentifier: "CellMap", for: indexPath) as! CellMap
            
            cell.configureCell(post: self.post)
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            if post.postCommentsCount ?? 0 > 5 && indexPath.row == 5 {
                let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AllCommentsVC") as! AllCommentsVC
                vc.post = post
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return 103
        case 2:
            if post.postCommentsCount ?? 0 > 5 && indexPath.row == 5 {
                return 54
            }
            return UITableView.automaticDimension
        case 3:
            return 390
        default:
            return 0
        }
    }
    
    @objc func btnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnLike(_ sender: UIButton) {
        if post.postIsLiked ?? false {
            apiUnLikePost(post.postId ?? 0, isUnlikeEvent: false)
            post.postIsLiked = false
            
            post.postLikesCount = (post.postLikesCount ?? 0) - 1
            if post.postLikesCount ?? 0 < 0 {
                post.postLikesCount = 0
            }
        } else {
            apiLikePost(post.postId ?? 0, isLikeEvent: false)
            post.postIsLiked = true
            post.postLikesCount = (post.postLikesCount ?? 0) + 1
        }
        tblDetails.reloadData()
    }
    
    @objc func btnMore(_ sender: UIButton) {
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if post?.isViewerAuthor ?? false {
            let deleteActionButton = UIAlertAction(title: "削除", style: .default)
            { _ in
                print("Delete")
                self.deleteFeedPopup()
            }
            actionSheet.addAction(deleteActionButton)
        } else {
            let reportActionButton = UIAlertAction(title: "報告書", style: .default)
            { _ in
                let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "BlockReasonVC") as! BlockReasonVC
                vc.sType = "feed_posts"
                vc.sID = "\(self.post.postId ?? 0)"
                vc.modalPresentationStyle = .overCurrentContext
                mostTopViewController?.present(vc, animated: false, completion: nil)
                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                }, completion: nil)
            }
            actionSheet.addAction(reportActionButton)
        }
        
        let cancelActionButton = UIAlertAction(title: "キャンセル", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheet.addAction(cancelActionButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func deleteFeedPopup() {
        let alertController = UIAlertController(title: nil, message: "この投稿を削除してもよろしいですか？", preferredStyle: .alert)
        let deleteActionButton = UIAlertAction(title: "削除", style: .default) { (action:UIAlertAction) in
            apiDeleteFeed(self.post.postId ?? 0) { (_) in
                self.delegateFeedDetails?.deleteFeed(self.indexPath)
                self.navigationController?.popViewController(animated: true)
            }
        }
        let cancelActionButton = UIAlertAction(title: "キャンセル", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(deleteActionButton)
        alertController.addAction(cancelActionButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: tblDetails)
        if let indexPath = tblDetails.indexPathForRow(at: location) {
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
            if indexPath.section == 0 {
                vc.sUserID = "\(self.post.userId ?? 0)"
            } else if indexPath.section == 2 {
                print(indexPath.row)
                let objComment = postComments[indexPath.row]
                vc.sUserID = "\(objComment.userId ?? 0)"
            }
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- UITextFieldDelegate
extension FeedDetailsVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        addComment()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addComment()
        return true
    }

    func addComment() {
        let indexPath = IndexPath(row: 0, section: 1)
        if let cell = tblDetails.cellForRow(at: indexPath) as? CellAddComment {
            if TRIM(string: cell.txtComment.text ?? "").count > 0 {
                apiAddComment(cell.txtComment.text ?? "")
                cell.txtComment.text = ""
            }
        }
    }
}

//MARK:- API CALLING
extension FeedDetailsVC {
    func apiAddComment(_ text: String) {
        let dictComment: NSDictionary = ["comment":TRIM(string: text)]
        let dictParam: NSDictionary = ["comment":dictComment]
        
        let sURL = SERVER_URL + "feed_posts/\(post.postId ?? 0)/" + API_ADD_COMMENT
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_ADD_COMMENT, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            print(responseObject ?? "")
            if error == nil {
                if let dictComment = responseObject?["comment"] as? NSDictionary {
                    let comment: PostComments = PostComments.init(object: dictComment)
                    self.postComments.insert(comment, at: 0)
                    self.post.postCommentsCount = (self.post.postCommentsCount ?? 0) + 1
                    self.tblDetails.reloadData()
                }
            } else {
                print(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}



// MARK: - UITABLEVIEW CELL

class CellHomeFeedDetails: UITableViewCell {
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgPost: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblPostDate: UILabel!
    @IBOutlet var lblPostDescription: UILabel!
    @IBOutlet var lblLikesCount: UILabel!
    @IBOutlet var lblCommentsCount: UILabel!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var imgLike: UIImageView!
    @IBOutlet var imgType: UIImageView!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var heightImage: NSLayoutConstraint!
    @IBOutlet var btnMore: UIButton!
    
    func configureCell(post: Posts) {
        imgUser.sd_setImage(with: URL(string: post.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        
        //imgPost.sd_setImage(with: URL(string: post.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        heightImage.constant = 240
        imgPost.sd_setImage(with: URL(string: post.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad) { (theImage, error, cache, url) in
            if let image = theImage {
                self.heightImage.constant = getAspectRatioAccordingToiPhones(cellImageFrame: CGSize(width: SCREENWIDTH()-24, height: 140),downloadedImage: image)
            }
        }
        
        imgType.sd_setImage(with: URL(string: post.postCategoryIconUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblUserName.text = post.userName
        lblPostDate.text = "オン \(intToDate(miliseconds: post.postDate ?? 0, shortMonthName: false))"
        lblPostDescription.attributedText = convertHashtags(text: post.postDescription ?? "")
        lblLikesCount.text = "\(post.postLikesCount ?? 0)"
        lblCommentsCount.text = "\(post.postCommentsCount ?? 0)"
        lblCategory.text = getCategoryName("\(post.postCategoryId ?? 0)")
        if post.postIsLiked ?? false {
            imgLike.image = UIImage(named: "ic_like")
        } else {
            imgLike.image = UIImage(named: "ic_like_gray")
        }
    }
}

class CellAddComment: UITableViewCell {
    @IBOutlet var txtComment: UITextField!
}

class CellComments: UITableViewCell {
    @IBOutlet weak var imgCommentorUser: UIImageView!
    @IBOutlet weak var lblCommentorUser: UILabel!
    @IBOutlet weak var lblCommentText: UILabel!
    @IBOutlet var viewAllComment: UIView!
    @IBOutlet var lblViewCountComment: UILabel!
    
    func configureCell(postComment: PostComments) {
        imgCommentorUser.sd_setImage(with: URL(string: postComment.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblCommentorUser.text = "\(postComment.userName ?? "")"
        lblCommentText.text = postComment.comment
    }
}

class CellMap: UITableViewCell {
    @IBOutlet weak var lblFeedMapLocation: UILabel!
    @IBOutlet var mapView: GMSMapView!
    
    func configureCell(post: Posts) {
        lblFeedMapLocation.text = post.postMapAddress
        
        let lat = Double(post.postLat ?? "0") ?? 0
        let lng = Double(post.postLng ?? "0") ?? 0
        /*
        if post.postMapAddress?.count ?? 0 <= 0 {
            getAddress(lat, lng) { (sAddress) in
                self.lblFeedMapLocation.text = sAddress
            }
        }*/
        mapView.isUserInteractionEnabled = false
        mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 6.0)
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = mapView
    }
}
