
//
//  AddPostVC.swift
//  Fishing Map
//
//  Created by iMac on 31/10/20.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

protocol delegateCreatePost {
    func getNewPost(_ post: Posts)
}

class CellAddPost: UITableViewCell {
    @IBOutlet var btnCellPost: UIButton!
    @IBOutlet var lblAddLocationTitle: UILabel!
    @IBOutlet var btnUseCurrentLocation: UIButton!
    @IBOutlet var txtAddDescription: IQTextView!
    @IBOutlet var lblNameSport: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblImageName: UILabel!
    @IBOutlet var imgRating: UIImageView!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var imgCategory: UIImageView!
    @IBOutlet var lblAddCategoryTitle: UILabel!
    @IBOutlet var txtAddComment: IQTextView!
    @IBOutlet var txtNameTheSpot: UITextField!
    @IBOutlet var txtPrice: UITextField!
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var lblTotalParkingSlot: UILabel!
    @IBOutlet var imgBeachParking: UIImageView!
    @IBOutlet var txtAddEquipment: UITextField!
}

class AddPostVC: UIViewController {
    
    @IBOutlet var tblPost: UITableView!
    @IBOutlet weak var lblNavigationTitle: UILabel!
    @IBOutlet weak var btnPost: UIButton!
    
    var delegateCreatePost: delegateCreatePost?
    var arrPost = ["addlocation", "addsubtitle", "uploadimage", "adddate", "adddescription", "addrating", "addcategory"]
    var arrCategory: [String] = ["Fish", "Terrain", "Scene", "Parking"]
    var arrEquipment: [String] = ["Fishing Rod", "Assist Hook", "High Grip Gloves", "Metal Jig", "Micro Jig", "Face Cover", "Minnow", "Uncategorized"]
    var arrImageType = ["Fish", "Terrain", "Scene", "Parking", "Restaurant", "Hotel"]
    var arrJapaneseImageType = ["魚", "地形", "シーン", "パーキング", "レストラン", "ホテル"]
    var addImgType = LocateImageType.Post
    
    var dictDetails = NSMutableDictionary()
    var objLocation: Locations?
    var sLocationID: String?
    var sLocationName: String = ""
    var sProductId: String = ""
    var objSelectedLoc: NewLocation?
    var isLocationUpload: Bool = false
    var sImageType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        btnUseCurrentLocationTapped()
    }
    
    private func setupUI() {
        self.dictDetails.setObject(false, forKey: "is_liked" as NSCopying)
        switch addImgType {
        case .Post:
            //"addrating"
            self.lblNavigationTitle.text = "フィード" //"フィード役職"
            self.btnPost.setTitle("投稿", for: .normal)
            //"addlocation"
            self.arrPost = ["uploadimage", "adddate", "uploadtolocation", "adddescription", "addcategory"]
            self.arrCategory = getArrCatName()
        case .TopScene:
            self.lblNavigationTitle.text = "風景の写真を追加"
            self.btnPost.setTitle("申請する", for: .normal)
            self.dictDetails.setObject(arrCategory[2], forKey: "category_name" as NSCopying)
            self.arrPost = ["addlocation", "namethepost", "uploadimage", "adddate", "addcomment", "addrating", "addcategory"]
            self.tblPost.reloadData()
        case .TopTerrain:
            self.lblNavigationTitle.text = "地形の写真を追加"
            self.btnPost.setTitle("申請する", for: .normal)
            self.arrPost = ["addlocation", "namethepost", "uploadimage", "adddate", "addcomment", "addrating", "addcategory"]
            self.dictDetails.setObject(arrCategory[1], forKey: "category_name" as NSCopying)
            self.tblPost.reloadData()
        case .Fish:
            self.lblNavigationTitle.text = "魚の写真"
            self.btnPost.setTitle("申請する", for: .normal)
            self.arrPost = ["addlocation", "namethepost", "uploadimage", "adddate", "addcomment", "addrating", "addcategory"]
            self.dictDetails.setObject(arrCategory[0], forKey: "category_name" as NSCopying)
            self.tblPost.reloadData()
        case .ParkingSpots:
            self.dictDetails.setObject(false, forKey: "beach_parking" as NSCopying)
            self.lblNavigationTitle.text = "駐車場を設定"
            self.btnPost.setTitle("申請する", for: .normal)
            self.dictDetails.setObject(arrCategory[3], forKey: "category_name" as NSCopying)
            self.dictDetails.setObject(false, forKey: "beach_parking" as NSCopying)
            self.arrPost = ["addlocation", "namethepost", "uploadimage", "adddate", "availableparkingslots", "availablebeachparking", "addprice", "addcomment", "addcategory"]
            self.tblPost.reloadData()
        case .AddImage:
            self.lblNavigationTitle.text = "使用した釣具"
            self.btnPost.setTitle("申請する", for: .normal)
            self.arrPost = ["addlocation", "addequipment", "uploadimage", "adddate"]
        case .Hotels:
            self.lblNavigationTitle.text = "ホテルの写真"
            self.btnPost.setTitle("申請する", for: .normal)
            //"addprice",
            self.arrPost = ["addlocation", "namethepost", "uploadimage", "addcomment", "addrating"]
        case .Restaurant:
            self.lblNavigationTitle.text = "レストランの写真を追加"
            self.btnPost.setTitle("申請する", for: .normal)
            //"addprice", 
            self.arrPost = ["addlocation", "namethepost", "uploadimage", "addcomment", "addrating"]
        default:
            return
        }
        if arrPost.contains("adddate") {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "dd-MM-yyyy"
            self.dictDetails.setObject(dateFormat.string(from: Date()), forKey: "date" as NSCopying)
            self.tblPost.reloadData()
        }
        
        if arrPost.contains("namethepost") && (sLocationID?.count ?? 0 > 0)  && (sLocationName.count <= 0) {
            apiGetLocationName()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPost(_ sender: Any) {
        print(dictDetails)
        getParameters { (dictParam, selectedImage) in
            showLoaderHUD(strMessage: "")
            UploadImageToCloud(selectedImage) { (imgurl) in
                dictParam.setObject(imgurl, forKey: "image_url" as NSCopying)
                if self.addImgType == .Post {
                    self.apiCreateFeedPost(dictParam)
                } else if self.addImgType == .AddImage {
                    self.apiCreateEquipmentImage(dictParam)
                } else {
                    self.apiCreateImages(dictParam)
                }
            }
        }
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension AddPostVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellAddPost = tableView.dequeueReusableCell(withIdentifier: arrPost[indexPath.row], for: indexPath) as! CellAddPost
        let sType = arrPost[indexPath.row]
        switch sType {
        case "addlocation":
            cell.lblLocation.text = "タップして場所を特定"
            if let address = dictDetails.object(forKey: "map_address") as? String {
                cell.lblLocation.text = address
            }
            cell.btnUseCurrentLocation.addTarget(self, action: #selector(btnUseCurrentLocationTapped), for: .touchUpInside)
            cell.lblAddLocationTitle.text = "写真の場所を追加"
            if addImgType == .Post || addImgType == .AddImage {
                cell.lblAddLocationTitle.text = "スポットを追加"
            }
        case "adddate":
            cell.lblDate.text = "日付"
            if let sDate = dictDetails.object(forKey: "date") as? String {
                cell.lblDate.text = sDate
            }
        case "adddescription":
            cell.txtAddDescription.delegate = self
            if let description = dictDetails.object(forKey: "description") as? String {
                cell.txtAddDescription.text = description
            }
        case "addcomment":
            cell.txtAddComment.delegate = self
            if let description = dictDetails.object(forKey: "description") as? String {
                cell.txtAddComment.text = description
            }
        case "uploadimage":
            cell.lblImageName.text = "アップロード"
            if let sImageName = dictDetails.object(forKey: "imageName") as? String {
                cell.lblImageName.text = sImageName
            }
        case "addrating":
            cell.imgRating.image = UIImage(named: "ic_like_gray")
            cell.lblRating.text = "00"
            if let isLike = dictDetails.object(forKey: "is_liked") as? Bool {
                if isLike {
                    cell.imgRating.image = UIImage(named: "ic_like")
                    cell.lblRating.text = "01"
                }
            }
        case "addcategory":
            cell.lblCategory.text = "選択"
            if let sCatName = dictDetails.object(forKey: "category_name") as? String {
                cell.lblCategory.text = sCatName
            }
            if addImgType == .Post {
                let imageURL = getCatImage(cell.lblCategory.text ?? "")
                cell.imgCategory.sd_setImage(with: imageURL, placeholderImage: PLACE_HOLDER_CATEGORY, options: .progressiveLoad, context: nil)
            } else if addImgType == .Fish || addImgType == .TopTerrain || addImgType == .TopScene || addImgType == .ParkingSpots {
                cell.lblAddCategoryTitle.textColor = Color_Hex(hex: "797A7B")
                cell.lblCategory.textColor = Color_Hex(hex: "797A7B")
                let imagename = "\(cell.lblCategory.text ?? "選択")_gray"
                cell.imgCategory.image = UIImage(named: imagename)
            } else {
                let imagename = "\(cell.lblCategory.text ?? "選択")_cat"
                cell.imgCategory.image = UIImage(named: imagename)
            }
        case "namethepost":
            /*
            cell.txtNameTheSpot.tag = 1
            cell.txtNameTheSpot.delegate = self
            if let name = dictDetails.object(forKey: "name") as? String {
                cell.txtNameTheSpot.text = name
            }
            
            cell.lblNameSport.text = "釣りスポットの名前"
            if addImgType == .Restaurant || addImgType == .Hotels {
                cell.lblNameSport.text = "写真名"
            }*/
            cell.txtNameTheSpot.isEnabled = false
            cell.lblNameSport.text = "ロケーション"
            if objLocation != nil {
                cell.txtNameTheSpot.text = objLocation?.locationName
            } else if sLocationName.count > 0 {
                cell.txtNameTheSpot.text = sLocationName
            }
        case "addprice":
            cell.txtPrice.tag = 2
            cell.txtPrice.delegate = self
            cell.txtPrice.text = ""
            if let price = dictDetails.object(forKey: "price") as? String {
                cell.txtPrice.text = price
            }
            
            cell.lblPrice.text = "価格の設定（時間）"
            if addImgType == .Restaurant || sImageType == "Restaurant" {
                cell.lblPrice.text = "価格 ( ２人 )"
            } else if addImgType == .Hotels || sImageType == "Hotel" {
                cell.lblPrice.text = "価格（1日）"
            }
        case "availableparkingslots":
            cell.btnPlus.addTarget(self, action: #selector(btnPlus(_:)), for: .touchUpInside)
            cell.btnMinus.addTarget(self, action: #selector(btnMinus(_:)), for: .touchUpInside)
            if let spotsCounter = dictDetails.object(forKey: "spots_counter") as? Int {
                cell.lblTotalParkingSlot.text = String(format: "%2d", spotsCounter)
            }
        case "availablebeachparking":
            cell.imgBeachParking.image = UIImage(named: "icn_uncheck")
            if let isBeachParking = dictDetails.object(forKey: "beach_parking") as? Bool {
                if isBeachParking {
                    cell.imgBeachParking.image = UIImage(named: "icn_check")
                }
            }
        case "addequipment":
            if let sCatName = dictDetails.object(forKey: "equipment_name") as? String {
                cell.txtAddEquipment.text = sCatName
            }
        case "uploadtolocation":
            cell.imgBeachParking.image = UIImage(named: isLocationUpload ? "icn_check" : "icn_uncheck")
        case "addimagetype":
            cell.lblCategory.text = "選択"
            cell.imgCategory.image = UIImage(named: "選択_cat")
            if sImageType.count > 0 {
                cell.lblCategory.text = getImageTypeNameInJapan(sImageType)
                //["魚", "地形", "シーン", "パーキング", "レストラン", "ホテル"]
                let imagename = "\(sImageType)_cat"
                cell.imgCategory.image = UIImage(named: imagename)
            }
        case "selectlocation":
            print("selectlocation")
            cell.lblLocation.text = objSelectedLoc?.locationName
            if cell.lblLocation.text?.count ?? 0 <= 0 {
                cell.lblLocation.text = "スポットを選択"
            }
        default:
            break
        }
        if cell.btnCellPost != nil {
            cell.btnCellPost.addTarget(self, action: #selector(btnCellPost(_:)), for: .touchUpInside)
        }
        //["addlocation", "namethepost", "uploadimage", "availableparkingslots", "availablebeachparking", "addprice", "addcomment", "addcategory"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sType = arrPost[indexPath.row]
        switch sType {
        case "availablebeachparking":
            if let isBeachParking = dictDetails.object(forKey: "beach_parking") as? Bool {
                dictDetails.setObject(!isBeachParking, forKey: "beach_parking" as NSCopying)
            } else {
                dictDetails.setObject(false, forKey: "beach_parking" as NSCopying)
            }
            tblPost.reloadData()
        case "uploadtolocation":
            isLocationUpload = !isLocationUpload
            if isLocationUpload {
                self.arrPost = ["uploadimage", "adddate", "uploadtolocation", "addimagetype", "selectlocation", "adddescription", "addrating", "addcategory"]
            } else {
                self.arrPost = ["uploadimage", "adddate", "uploadtolocation", "adddescription", "addrating", "addcategory"]
                objSelectedLoc = nil
                sImageType = ""
            }
            tblPost.reloadData()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sPostType = arrPost[indexPath.row]
        switch sPostType {
        case "addlocation", "selectlocation":
            return UITableView.automaticDimension
        case "addsubtitle", "namethepost", "addequipment":
            return 111
        case "adddescription", "addcomment":
            return 210
        default:
            return 69
        }
    }
    
    @objc func btnPlus(_ sender: UIButton) {
        self.view.endEditing(true)
        if let spotsCounter = dictDetails.object(forKey: "spots_counter") as? Int {
            dictDetails.setObject(spotsCounter+1, forKey: "spots_counter" as NSCopying)
        } else {
            dictDetails.setObject(1, forKey: "spots_counter" as NSCopying)
        }
        tblPost.reloadData()
    }
    
    @objc func btnMinus(_ sender: UIButton) {
        self.view.endEditing(true)
        if let spotsCounter = dictDetails.object(forKey: "spots_counter") as? Int {
            if spotsCounter <= 0 { return }
            dictDetails.setObject(spotsCounter-1, forKey: "spots_counter" as NSCopying)
        }
        tblPost.reloadData()
    }
    
    @objc func btnCellPost(_ sender: UIButton) {
        //["addlocation", "addsubtitle", "uploadimage", "adddate", "adddescription", "addrating", "addcategory"]
        let buttonPosition = sender.convert(CGPoint.zero, to: tblPost)
        if let indexPath = tblPost.indexPathForRow(at:buttonPosition) {
            let sPostType = arrPost[indexPath.row]
            switch sPostType {
            case "addlocation":
                addLocation()
            case "selectlocation":
                if sImageType.count > 0 {
                    self.view.endEditing(true)
                    let vc = loadVC(strStoryboardId: SB_Select_Location, strVCId: "SelectLocationVC") as! SelectLocationVC
                    vc.sImageType = sImageType.lowercased()
                    vc.delegateSelectLoc = self
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage("画像タイプを選択")
                }
            case "addsubtitle":
                addSubTitle()
            case "addequipment":
                addEquipment()
            case "uploadimage":
                ImagePickerManager().pickImage(self, false) { (img, sPath) in
                    self.dictDetails.setObject(sPath, forKey: "imageName" as NSCopying)
                    self.dictDetails.setObject(img, forKey: "image" as NSCopying)
                    self.tblPost.reloadData()
                }
            case "adddate":
                addDate()
            case "addrating":
                if let isLike = dictDetails.object(forKey: "is_liked") as? Bool {
                    dictDetails.setObject(!isLike, forKey: "is_liked" as NSCopying)
                } else {
                    dictDetails.setObject(false, forKey: "is_liked" as NSCopying)
                }
                tblPost.reloadData()
            case "addcategory":
                addCategory()
            case "addimagetype":
                addImageType()
            default:
                break
            }
        }
    }
    
    @objc func btnUseCurrentLocationTapped() {
        self.view.endEditing(true)
        getAddress(lat_currnt, long_currnt) { (address) in
            self.dictDetails.setObject(address, forKey: "map_address" as NSCopying)
            self.dictDetails.setObject(lat_currnt, forKey: "lat" as NSCopying)
            self.dictDetails.setObject(long_currnt, forKey: "lng" as NSCopying)
            self.tblPost.reloadData()
        }
    }
}

//MARK:- UIBUTTON CELLS ACTION
extension AddPostVC {

    func addSubTitle() {
        let alert = UIAlertController(style: .actionSheet, title: "サブタイトルを選択", message: nil)
        let subtitles: [[String]] = [["Caught a Fish", "Found a Parking Spot", "Captured a Terrain", "Captured a Scene"]]
        alert.addPickerView(values: subtitles, initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    
                }
            }
        }
        alert.addAction(title: "完了", style: .cancel)
        alert.show(self)
    }
    
    func addEquipment() {
        if arrCategory.count > 0 {
            self.dictDetails.setObject(arrEquipment[0], forKey: "equipment_name" as NSCopying)
            self.tblPost.reloadData()
        }
        let alert = UIAlertController(style: .actionSheet, title: "釣具", message: nil)
        alert.addPickerView(values: [arrEquipment], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    DispatchQueue.main.async {
                        self.dictDetails.setObject(self.arrEquipment[index.row], forKey: "equipment_name" as NSCopying)
                        self.tblPost.reloadData()
                    }
                }
            }
        }
        alert.addAction(title: "完了", style: .cancel)
        alert.show(self)
    }
    
    func addDate() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        var sDate = dateFormat.string(from: Date())
        self.dictDetails.setObject(sDate, forKey: "date" as NSCopying)
        self.tblPost.reloadData()
        
        let alert = UIAlertController(style: .actionSheet, title: "日付を選択")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: Date(), maximumDate: nil) { date in
            sDate = dateFormat.string(from: date)
            self.dictDetails.setObject(sDate, forKey: "date" as NSCopying)
            self.tblPost.reloadData()
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show(self)
    }
    
    func addCategory() {
        if addImgType != .Post { return }
        if arrCategory.count > 0 {
            self.dictDetails.setObject(arrCategory[0], forKey: "category_name" as NSCopying)
            self.tblPost.reloadData()
        }
        let alert = UIAlertController(style: .actionSheet, title: "選択", message: nil)
        alert.addPickerView(values: [arrCategory], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                self.dictDetails.setObject(self.arrCategory[index.row], forKey: "category_name" as NSCopying)
                self.tblPost.reloadData()
            }
        }
        alert.addAction(title: "完了", style: .cancel)
        alert.show(self)
    }
    
    func addImageType() {
        if addImgType != .Post { return }
        objSelectedLoc = nil
        if arrImageType.count > 0 {
            self.sImageType = arrImageType[0]
            self.tblPost.reloadData()
        }
        //var arrImageType = ["Fish", "Terrain", "Scene", "Parking", "Restaurant", "Hotel"]
        //var arrJapaneseImageType = ["魚", "地形", "シーン", "パーキング", "レストラン", "ホテル"]
        let alert = UIAlertController(style: .actionSheet, title: "画像タイプ", message: nil)
        alert.addPickerView(values: [arrJapaneseImageType], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                self.sImageType = self.arrImageType[index.row]
                self.tblPost.reloadData()
            }
        }
        alert.addAction(title: "完了", style: .cancel) { (_) in
            if self.sImageType == "Parking" {
                self.dictDetails.setObject(false, forKey: "beach_parking" as NSCopying)
                self.arrPost = ["uploadimage", "adddate", "uploadtolocation", "addimagetype", "availableparkingslots", "availablebeachparking", "addprice", "selectlocation", "adddescription", "addcategory"]
            } else if self.sImageType == "Restaurant" || self.sImageType == "Hotel" {
                self.arrPost = ["uploadimage", "adddate", "uploadtolocation", "addimagetype", "addprice", "selectlocation", "adddescription", "addcategory"]
            } else {
                self.arrPost = ["uploadimage", "adddate", "uploadtolocation", "addimagetype", "selectlocation", "adddescription", "addcategory"]
            }
            self.tblPost.reloadData()
        }
        alert.show(self)
    }
}

//MARK:- API CALLING
extension AddPostVC {
    func apiCreateFeedPost(_ dictParam: NSDictionary) {
        self.view.endEditing(true)
        var dictPost = NSDictionary()
        if isLocationUpload {
            let dictImage = NSMutableDictionary()
            
            dictImage.setObject(dictParam.object(forKey: "lat") as? Double ?? 0, forKey: "lat" as NSCopying)
            dictImage.setObject(dictParam.object(forKey: "lng") as? Double ?? 0, forKey: "lng" as NSCopying)
            dictImage.setObject("\(dictParam.object(forKey: "image_url") as? String ?? "")", forKey: "image_url" as NSCopying)
            dictImage.setObject("\(dictParam.object(forKey: "date") as? String ?? "")", forKey: "date" as NSCopying)
            dictImage.setObject(Int(objSelectedLoc?.locationId ?? "0") ?? 0, forKey: "location_id" as NSCopying)
            
            if self.sImageType == "Parking" {
                if let spotsCounter = dictDetails.object(forKey: "spots_counter") as? Int {
                    dictImage.setObject(spotsCounter, forKey: "spots_counter" as NSCopying)
                }
                
                if let isBeachParking = dictDetails.object(forKey: "beach_parking") as? Bool {
                    dictImage.setObject(isBeachParking, forKey: "beach_parking" as NSCopying)
                }
                
                if let price = dictDetails.object(forKey: "price") as? String {
                    dictImage.setObject(price.CGFloatValue() ?? 0, forKey: "price_per_hour" as NSCopying)
                } else {
                    dictImage.setObject(0, forKey: "price_per_hour" as NSCopying)
                }
            } else if self.sImageType == "Restaurant" || self.sImageType == "Hotel" {
                if let price = dictDetails.object(forKey: "price") as? String {
                    dictImage.setObject(price.CGFloatValue() ?? 0, forKey: "price_per_hour" as NSCopying)
                } else {
                    dictImage.setObject(0, forKey: "price_per_hour" as NSCopying)
                }
            }
            
            dictPost = ["location_upload": isLocationUpload, "image_type": self.sImageType.lowercased(), "post":dictParam, "image":dictImage]
        } else {
            dictPost = ["location_upload": isLocationUpload, "post":dictParam]
        }
        print("Feed Parameters: ", dictPost)
        let sURL = SERVER_URL + API_GET_FEED_POSTS
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_GET_FEED_POSTS, parameters: dictPost, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            hideLoaderHUD()
            if error == nil {
                print(responseObject ?? "")
                let objFeed: FMCreatFeed = FMCreatFeed.init(object: responseObject ?? "")
                if objFeed.success ?? false {
                    if let newpost = objFeed.post {
                        showMessage("フィードが作成されました")
                        self.delegateCreatePost?.getNewPost(newpost)
                    }
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(error?.localizedDescription ?? ErrorMSG)
                    self.removeImage(dictParam)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
                self.removeImage(dictParam)
            }
        }
    }
    
    func apiCreateImages(_ dictParam: NSDictionary) {
        self.view.endEditing(true)
        let sImageType = getImageTypeString()
        let dictPost: NSDictionary = ["image":dictParam, "image_type": sImageType]
        print("Create Image Parameters: ", dictPost)
        let sURL = SERVER_URL + API_CREATE_IMAGES
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_CREATE_IMAGES, parameters: dictPost, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            hideLoaderHUD()
            if error == nil {
                print(responseObject ?? "")
                if responseObject?["success"] as? String == "1" {
                    showMessage("作成しました。")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(responseObject?["message"] as? String ?? ErrorMSG)
                    self.removeImage(dictParam)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
                self.removeImage(dictParam)
            }
        }
    }
    
    func apiCreateEquipmentImage(_ dictParam: NSDictionary) {
        self.view.endEditing(true)
        let dictImage: NSDictionary = ["image":dictParam]
        print("Create Image Parameters: ", dictImage)
        let sURL = SERVER_URL + API_PRODCTS + "/\(sProductId)/\(API_UPLOAD_IMAGE)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_UPLOAD_IMAGE, parameters: dictImage, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            hideLoaderHUD()
            if error == nil {
                print(responseObject ?? "")
                if responseObject?["success"] as? String == "1" {
                    showMessage("作成しました。")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(responseObject?["message"] as? String ?? ErrorMSG)
                    self.removeImage(dictParam)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
                self.removeImage(dictParam)
            }
        }
    }
    
    func apiGetLocationName() {
        let sImageType = getImageTypeString()
        let sURL = SERVER_URL + API_LOCATION
        let dictParam: NSDictionary = ["image_type": sImageType, "lat":lat_currnt, "lng": long_currnt]
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOCATION, parameters: dictParam, method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            if error == nil {
                let objLoc: FMNewLocation = FMNewLocation.init(object: responseObject ?? "")
                if objLoc.success == "1" {
                    for loc in objLoc.locations ?? [] {
                        if loc.locationId == self.sLocationID {
                            self.sLocationName = loc.locationName ?? ""
                            self.tblPost.reloadData()
                            break
                        }
                    }
                }
            }
        }
    }
    
    func removeImage(_ dictParam: NSDictionary) {
        if let imgURL = dictParam.object(forKey: "image_url") as? String {
            deleteImage(imgURL)
        }
    }
}

// MARK:- UITEXTVIEW DELEGATE METHOD
extension AddPostVC: UITextViewDelegate, UITextFieldDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if TRIM(string: textView.text ?? "").count > 0 {
            dictDetails.setObject(TRIM(string: textView.text ?? ""), forKey: "description" as NSCopying)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            /*
            if textField.tag == 1 {
                dictDetails.setObject(TRIM(string: textField.text ?? ""), forKey: "name" as NSCopying)
            } else*/ if textField.tag == 2 {
                dictDetails.setObject(TRIM(string: textField.text ?? ""), forKey: "price" as NSCopying)
            }
        }
    }
}

// MARK:- PLACE PICKER
extension AddPostVC: GMSAutocompleteViewControllerDelegate {
    
    func addLocation() {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        placePickerController.modalPresentationStyle = .fullScreen
        present(placePickerController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place address: \(place.formattedAddress ?? "")")
        dismiss(animated: true) {
            self.dictDetails.setObject(place.name ?? "", forKey: "map_address" as NSCopying)
            self.dictDetails.setObject(place.coordinate.latitude, forKey: "lat" as NSCopying)
            self.dictDetails.setObject(place.coordinate.longitude, forKey: "lng" as NSCopying)
            /*
            if self.dictDetails.object(forKey: "location_id") != nil {
                self.dictDetails.setObject(place.placeID ?? "0", forKey: "location_id" as NSCopying)
            }*/
            self.tblPost.reloadData()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

//MARK: VALIDATION & CREATE PARAMETER
extension AddPostVC {
    func getParameters(handler:@escaping  (_ dictData: NSMutableDictionary,_ image: UIImage) -> Void) {
        self.view.endEditing(true)
        var selectedImage = UIImage()
        let dictParam: NSMutableDictionary = NSMutableDictionary()
        
        if addImgType == .Post {
            
            /*************** FOR ADD POST ********************
            *******************************************************/
            
            /*
            if let isLike = dictDetails.object(forKey: "is_liked") as? Bool {
                dictParam.setObject(isLike, forKey: "is_liked" as NSCopying)
            } else {
                dictParam.setObject(false, forKey: "is_liked" as NSCopying)
            }*/
            
            let latt: Double = 0
            let longi: Double = 0
            dictParam.setObject(latt, forKey: "lat" as NSCopying)
            dictParam.setObject(longi, forKey: "lng" as NSCopying)
            
            /*
            if let address = dictDetails.object(forKey: "map_address") as? String {
                //dictParam.setObject(address, forKey: "map_address" as NSCopying)
                print(address)
                dictParam.setObject(dictDetails.object(forKey: "lat") as? Double ?? 0, forKey: "lat" as NSCopying)
                dictParam.setObject(dictDetails.object(forKey: "lng") as? Double ?? 0, forKey: "lng" as NSCopying)
            } else {
                showMessage("場所を入力してください")
                return
            }*/
            
            if let image = dictDetails.object(forKey: "image") as? UIImage {
                selectedImage = image
            } else {
                showMessage("写真をアップロードしてください")
                return
            }
            
            if let date = dictDetails.object(forKey: "date") as? String {
                dictParam.setObject("\(convertStringToTimeStamp(date))", forKey: "date" as NSCopying)
            } else {
                showMessage("日付を入力してください")
                return
            }
            
            if isLocationUpload {
                if sImageType.count > 0 {
                    if sImageType == "Parking" {
                        if let spotsCounter = dictDetails.object(forKey: "spots_counter") as? Int {
                            if spotsCounter <= 0 {
                                showMessage("駐車場を入力してください")
                                return
                            }
                        } else {
                            showMessage("駐車場を入力してください")
                            return
                        }
                    } else if sImageType == "Restaurant" || sImageType == "Hotel" {
                        if let price = dictDetails.object(forKey: "price") as? String {
                            if Int(price) == 0 {
                                showMessage("価格を入力してください")
                                return
                            }
                        } else {
                            showMessage("価格を入力してください")
                            return
                        }
                    }
                    
                } else {
                    showMessage("画像タイプを選択")
                    return
                }
                
                if objSelectedLoc == nil {
                    showMessage("画像の場所を選択してください")
                    return
                }
            }
            
            if let description = dictDetails.object(forKey: "description") as? String {
                dictParam.setObject(description, forKey: "description" as NSCopying)
            } else {
                /*
                showMessage("情報を入力してください")
                return*/
                dictParam.setObject("", forKey: "description" as NSCopying)
            }
            
            if let cateName = dictDetails.object(forKey: "category_name") as? String {
                dictParam.setObject(Int(getCategoryID(cateName)) ?? 0, forKey: "category_id" as NSCopying)
            } else {
                showMessage("カテゴリを入力してください")
                return
            }
        } else if addImgType == .AddImage {
            /*************** FOR EQUIPMENT IMAGES ********************
            *******************************************************/
            if let address = dictDetails.object(forKey: "map_address") as? String {
                print(address)
                dictParam.setObject("\(dictDetails.object(forKey: "lat") as? Double ?? 0)", forKey: "lat" as NSCopying)
                dictParam.setObject("\(dictDetails.object(forKey: "lng") as? Double ?? 0)", forKey: "lng" as NSCopying)
            } else {
                showMessage("場所を入力してください")
                return
            }
            
            if let equipName = dictDetails.object(forKey: "equipment_name") as? String {
                print(equipName)
                //dictParam.setObject(getCategoryID(cateName), forKey: "category_id" as NSCopying)
                dictParam.setObject("1", forKey: "product_id" as NSCopying)
            } else {
                showMessage("釣具を選択してください")
                return
            }
            
            if let image = dictDetails.object(forKey: "image") as? UIImage {
                selectedImage = image
            } else {
                showMessage("写真をアップロードしてください")
                return
            }
            
            if arrPost.contains("adddate") {
                if let date = dictDetails.object(forKey: "date") as? String {
                    dictParam.setObject("\(convertStringToTimeStamp(date))", forKey: "date" as NSCopying)
                } else {
                    showMessage("日付を入力してください")
                    return
                }
            }
            
        } else {
            
            /*************** FOR ADD IMAGES ********************
            *******************************************************/
            
            if let isLike = dictDetails.object(forKey: "is_liked") as? Bool {
                dictParam.setObject(isLike, forKey: "is_liked" as NSCopying)
            } else {
                dictParam.setObject(false, forKey: "is_liked" as NSCopying)
            }
            
            if let address = dictDetails.object(forKey: "map_address") as? String {
                print(address)
                dictParam.setObject("\(dictDetails.object(forKey: "lat") as? Double ?? 0)", forKey: "lat" as NSCopying)
                dictParam.setObject("\(dictDetails.object(forKey: "lng") as? Double ?? 0)", forKey: "lng" as NSCopying)
                if objLocation != nil {
                    dictParam.setObject(objLocation?.locationId ?? "0", forKey: "location_id" as NSCopying)
                } else if sLocationID?.count ?? 0 > 0 {
                    dictParam.setObject(sLocationID ?? "0", forKey: "location_id" as NSCopying)
                }
            } else {
                showMessage("場所を入力してください")
                return
            }
            
            /*
            if let name = dictDetails.object(forKey: "name") as? String {
                dictParam.setObject(name, forKey: "name" as NSCopying)
            } else {
                showMessage("名前を入力してください")
                return
            }*/
            
            if let image = dictDetails.object(forKey: "image") as? UIImage {
                selectedImage = image
            } else {
                showMessage("写真をアップロードしてください")
                return
            }
            
            if arrPost.contains("adddate") {
                if let date = dictDetails.object(forKey: "date") as? String {
                    dictParam.setObject("\(convertStringToTimeStamp(date))", forKey: "date" as NSCopying)
                } else {
                    showMessage("日付を入力してください")
                    return
                }
            }
            
            if arrPost.contains("availableparkingslots") {
                if let spotsCounter = dictDetails.object(forKey: "spots_counter") as? Int {
                    if spotsCounter <= 0 {
                        showMessage("駐車場を入力してください")
                        return
                    }
                    dictParam.setObject("\(spotsCounter)", forKey: "spots_counter" as NSCopying)
                } else {
                    showMessage("駐車場を入力してください")
                    return
                }
            }
            
            if arrPost.contains("availablebeachparking") {
                if let isBeachParking = dictDetails.object(forKey: "beach_parking") as? Bool {
                    dictParam.setObject(isBeachParking, forKey: "beach_parking" as NSCopying)
                } else {
                    dictParam.setObject(false, forKey: "beach_parking" as NSCopying)
                }
            }
            
            if arrPost.contains("addprice") {
                if let price = dictDetails.object(forKey: "price") as? String {
                    dictParam.setObject(price, forKey: "price_per_hour" as NSCopying)
                } else {
                    if addImgType == .ParkingSpots {
                        dictParam.setObject("0", forKey: "price_per_hour" as NSCopying)
                    } else {
                        showMessage("価格を入力してください")
                        return
                    }
                }
            }
            
            if let description = dictDetails.object(forKey: "description") as? String {
                dictParam.setObject(description, forKey: "description" as NSCopying)
            } else {
                /*
                showMessage("情報を入力してください")
                return*/
                dictParam.setObject("", forKey: "description" as NSCopying)
            }
            if (dictParam.object(forKey: "date") == nil) {
                dictParam.setObject(0, forKey: "date" as NSCopying)
            }
            if (dictParam.object(forKey: "price_per_hour") == nil) {
                dictParam.setObject(0, forKey: "price_per_hour" as NSCopying)
            }
            if (dictParam.object(forKey: "spots_counter") == nil) {
                dictParam.setObject(0, forKey: "spots_counter" as NSCopying)
            }
            if (dictParam.object(forKey: "beach_parking") == nil) {
                dictParam.setObject(false, forKey: "beach_parking" as NSCopying)
            }
        }
        
        handler(dictParam, selectedImage)
    }
}

extension AddPostVC: delegateSelectLoc {
    func selectedLoc(_ objLoc: NewLocation?) {
        objSelectedLoc = objLoc
        tblPost.reloadData()
    }
}

extension AddPostVC {
    func getImageTypeString() -> String {
        var sImageType = ""
        switch addImgType {
        case .Fish:
            sImageType = "fish"
        case .TopTerrain:
            sImageType = "terrain"
        case .Restaurant:
            sImageType = "restaurant"
        case .Hotels:
            sImageType = "hotel"
        case .TopScene:
            sImageType = "scene"
        case .ParkingSpots:
            sImageType = "parking"
        default:
            break
        }
        return sImageType
    }
}
