//
//  AllCommentsVC.swift
//  Fishing Map
//
//  Created by iMac on 08/12/20.
//

import UIKit

class AllCommentsVC: UIViewController {

    @IBOutlet var tblComments: UITableView!
    
    var isEventComments = false
    
    var post: Posts!
    var event: Events!
    
    var currentPage: Int = 1
    var isSend = false
    var arrComments: [PostComments] = []
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblComments.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        apiGetAllComments()
        tblComments.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.apiGetAllComments()
            }else {
                self.tblComments.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        currentPage = 1
        apiGetAllComments()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UITableViewDelegate
extension AllCommentsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellComments = tableView.dequeueReusableCell(withIdentifier: "CellComments", for: indexPath) as! CellComments
        cell.configureCell(postComment: self.arrComments[indexPath.row])
        let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        cell.imgCommentorUser.isUserInteractionEnabled = true
        cell.imgCommentorUser.addGestureRecognizer(imgG)
        
        let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        cell.lblCommentorUser.isUserInteractionEnabled = true
        cell.lblCommentorUser.addGestureRecognizer(lblG)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: tblComments)
        if let indexPath = tblComments.indexPathForRow(at: location) {
            let objComment = arrComments[indexPath.row]
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
            vc.sUserID = "\(objComment.userId ?? 0)"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// MARK: - API CALLING
extension AllCommentsVC {
    func apiGetAllComments() {
        isSend = true
        if currentPage == 1 { showLoaderHUD(strMessage: "") }
        var sURL = String()
        if isEventComments {
            sURL = SERVER_URL + "events/\(event.eventId ?? 0)/" + API_GET_COMMENT + "?page=\(currentPage)"
        } else {
            sURL = SERVER_URL + "feed_posts/\(post.postId ?? 0)/" + API_GET_COMMENT + "?page=\(currentPage)"
        }
        
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error)  in
            self.refreshControl.endRefreshing()
            if self.currentPage == 1 {
                self.tblComments.showsInfiniteScrolling = true
                self.arrComments.removeAll()
                hideLoaderHUD()
            }
            print(response ?? "")
            print("")
            
            let objComment: FMComments = FMComments.init(object: response ?? "")
            
            for comment in objComment.comments ?? [] {
                self.arrComments.append(comment)
            }
            
            if objComment.pagination?.nextPage != nil {
                self.currentPage = objComment.pagination?.nextPage ?? (self.currentPage+1)
            } else {
                self.tblComments.showsInfiniteScrolling = false
            }
            
            self.tblComments.infiniteScrollingView.stopAnimating()
            if self.arrComments.count <= 0 { self.tblComments.showsInfiniteScrolling = false }
            
            self.tblComments.reloadData()
            self.isSend = false
        }
    }
}
