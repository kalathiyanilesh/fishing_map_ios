//
//  FeedSearchResultVC.swift
//  Fishing Map
//
//  Created by iMac on 14/12/20.
//

import UIKit

class FeedSearchResultVC: UIViewController {

    @IBOutlet var tblResult: UITableView!
    @IBOutlet var txtSearch: UITextField!
    
    var arrSearchPosts : [Posts] = []
    var sTag: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        txtSearch.text = sTag
        apiSearchFeed()
    }

    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UITABLEVEW DELEGATE
extension FeedSearchResultVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellLatestPosts = tableView.dequeueReusableCell(withIdentifier: "CellLatestPosts", for: indexPath) as! CellLatestPosts
        cell.configureCell(post: self.arrSearchPosts[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
        vc.post = arrSearchPosts[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- API CALLING
extension FeedSearchResultVC {
    func apiSearchFeed() {
        let params = [
            "tag": TRIM(string: txtSearch.text ?? "")
        ]
        
        showLoaderHUD(strMessage: "")
        
        HttpRequestManager.sharedInstance.getRequestWithParams(endpointurl: SERVER_URL + API_GET_SEARCH_RESULTS, httpHeader: getAuthToken(), parameters: params) { (response, error) in
            print(response ?? "")
            
            self.arrSearchPosts.removeAll()
            
            hideLoaderHUD()
            
            let objFeedPosts: FMFeedPosts = FMFeedPosts.init(object: response ?? "")
            
            if objFeedPosts.success ?? false {
                if let allpost = objFeedPosts.posts {
                    self.arrSearchPosts = allpost
                }
            } else {
                showMessage(objFeedPosts.message ?? ErrorMSG)
            }
            
            self.tblResult.reloadData()
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension FeedSearchResultVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            self.apiSearchFeed()
        }
    }
}
