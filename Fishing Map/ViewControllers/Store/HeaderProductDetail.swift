//
//  HeaderProductDetail.swift
//  Fishing Map
//
//  Created by iMac on 18/12/20.
//

import UIKit

class HeaderProductDetail: UICollectionReusableView {
        
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnShopNow: UIButton!
}
