//
//  EquipmentDetailsVC.swift
//  Fishing Map
//
//  Created by macOS on 12/11/20.
//

import UIKit
import GoogleMaps
import FloatingPanel

class EquipmentDetailsVC: UIViewController {

    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var lblTitle: UILabel!
    
    var objProductImage: ProductImages?
    var objProduct: Products?
    var fpc: FloatingPanelController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "\(objProduct?.productName ?? "") 写真の詳細"
        SetupFloatingView()
        let lat: Double = Double(objProductImage?.productImageLat ?? "0") ?? 0
        let lng: Double = Double(objProductImage?.productImageLng ?? "0") ?? 0
        mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16.0)
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
        marker.icon = UIImage(named: "ic_equipment_pin")
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = mapView
        runAfterTime(time: 1) {
            self.fpc.move(to: .half, animated: true)
        }
    }

    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- SET FLOATING VIEW
extension EquipmentDetailsVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.white
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.layer.cornerRadius = 15.0
        fpc.surfaceView.clipsToBounds = true
        let vc = loadVC(strStoryboardId: SB_Store, strVCId: "DetailsVC") as! DetailsVC
        vc.objProduct = objProduct
        vc.objProductImage = objProductImage
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return EquipmentFloatingPanelLayout()
    }
}
