//
//  DetailsVC.swift
//  Fishing Map
//
//  Created by iMac on 17/12/20.
//

import UIKit

class DetailsVC: UIViewController {

    @IBOutlet var lblLocationName: UILabel!
    @IBOutlet var imgProductImage: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblUploadedOn: UILabel!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    
    var objProductImage: ProductImages?
    var objProduct: Products?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgProductImage.sd_setImage(with: URL(string: objProductImage?.productImageImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblProductName.text = objProductImage?.productName
        lblUploadedOn.text = "にアップロード \(intToDate(miliseconds: Int(objProductImage?.productImageCreatedAt ?? "0") ?? 0, shortMonthName: false))"
        imgUser.sd_setImage(with: URL(string: objProductImage?.userAvatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblUserName.text = objProductImage?.userName
        
        let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        imgUser.isUserInteractionEnabled = true
        imgUser.addGestureRecognizer(imgG)
        
        let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        lblUserName.isUserInteractionEnabled = true
        lblUserName.addGestureRecognizer(lblG)
    }
    
    //MARK:- UIBUTTON ACTIONS
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        if objProductImage == nil { return }
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
        vc.sUserID = objProductImage?.userId
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnShopNow(_ sender: Any) {
        let sURL = objProductImage?.productShopUrl ?? ""
        let escapedAddress = sURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        UIApplication.shared.open(URL(string: escapedAddress)!, options: [:], completionHandler: nil)
    }
    
    @IBAction func btnViewMore(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_Store, strVCId: "ProductMoreDetailsVC") as! ProductMoreDetailsVC
        vc.objProduct = objProduct
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
