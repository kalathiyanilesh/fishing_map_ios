//
//  ProductMoreDetailsVC.swift
//  Fishing Map
//
//  Created by iMac on 17/12/20.
//

import UIKit

class ProductMoreDetailsVC: UIViewController {

    @IBOutlet var collDetails: UICollectionView!
    
    var arrAllImages = NSMutableArray()
    var objProduct: Products?
    
    override func viewDidLoad() {
        collDetails.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
        super.viewDidLoad()

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
        layout.itemSize = CGSize(width: 148, height: 179)
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        collDetails!.collectionViewLayout = layout
        
        apiGetProductImage()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension ProductMoreDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAllImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.viewLike.isHidden = true
        cell.viewComment.isHidden = true
        cell.heightImgFish.constant = 128
        let objImg = arrAllImages.object(at: indexPath.row) as? ProductImages
        cell.imgPost.sd_setImage(with: URL(string: objImg?.productImageImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        cell.lblTitle.text = objImg?.productName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let viewHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderProductDetail", for: indexPath) as! HeaderProductDetail
            viewHeader.imgProduct.sd_setImage(with: URL(string: objProduct?.productImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            viewHeader.lblProductName.text = objProduct?.productName
            viewHeader.lblPrice.text = "‎¥‎ \(objProduct?.productPrice ?? "0")"
            viewHeader.lblDescription.text = objProduct?.productDescription
            viewHeader.btnShopNow.addTarget(self, action: #selector(btnShopNow(_:)), for: .touchUpInside)
            return viewHeader
            
        default:  fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-78)/2, height: 179)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize(width: SCREENWIDTH(), height: 603)
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objProductImage = arrAllImages.object(at: indexPath.row) as? ProductImages
        let vc = loadVC(strStoryboardId: SB_Store, strVCId: "EquipmentDetailsVC") as! EquipmentDetailsVC
        vc.objProductImage = objProductImage
        vc.objProduct = objProduct
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnShopNow(_ sender: UIButton) {
        let sURL = objProduct?.productShopUrl ?? ""
        let escapedAddress = sURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        UIApplication.shared.open(URL(string: escapedAddress)!, options: [:], completionHandler: nil)
    }
}

//MARK:- API CALLING
extension ProductMoreDetailsVC {
    func apiGetProductImage() {
        self.arrAllImages.removeAllObjects()
        let sURL = SERVER_URL + API_PRODCTS + "/\(objProduct?.productId ?? "0")/\(API_PRODUCT_IMAGE)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_PRODUCT_IMAGE, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                let objStore: FMProductImage = FMProductImage.init(object: responseObject ?? "")
                if objStore.success == "1" {
                    for product in objStore.productImages ?? [] {
                        self.arrAllImages.add(product)
                    }
                } else {
                    showMessage(objStore.message ?? ErrorMSG)
                }
                self.collDetails.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}
