//
//  StoreVC.swift
//  Fishing Map
//
//  Created by macOS on 09/11/20.
//

import UIKit
import UPCarouselFlowLayout

class StoreVC: UIViewController {

    @IBOutlet weak var collEquipmentGallery: UICollectionView!
    @IBOutlet weak var collFeaturedProducts: UICollectionView!
    @IBOutlet var heightCollEquipment: NSLayoutConstraint!
    @IBOutlet var btnPrevious: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    var arrProdcts = NSMutableArray()
    var arrImages = NSMutableArray()
    var objCurrentPoduct: Products?
    var currentPage: Int = 0
    
    fileprivate var pageSize: CGSize {
        let layout = self.collFeaturedProducts.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    fileprivate var orientation: UIDeviceOrientation {
        return UIDevice.current.orientation
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        apiGetProducts()
    }
    
    func setupUI() {
        
        collFeaturedProducts.register(UINib(nibName: "CellProductStore", bundle: nil), forCellWithReuseIdentifier: "CellProductStore")
        collEquipmentGallery.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
        
        setupLayout()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
        layout.itemSize = CGSize(width: 148, height: 179)
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        collEquipmentGallery!.collectionViewLayout = layout
        
        btnPrevious.isHidden = true
        btnNext.isHidden = true
    }
    
    fileprivate func setupLayout() {
        let layout = self.collFeaturedProducts.collectionViewLayout as! UPCarouselFlowLayout
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 24)
    }
    
    @IBAction func btnAddImageTapped(_ sender: UIButton) {
        let addImageVC = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        addImageVC.addImgType = .AddImage
        addImageVC.sProductId = objCurrentPoduct?.productId ?? ""
        self.navigationController?.pushViewController(addImageVC, animated: true)
    }
    
    @IBAction func btnSearchProducts(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "FeedSearchVC") as! FeedSearchVC
        vc.isForProduct = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "navsidemenu")
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        //collFeaturedProducts.scrollToItem(at:IndexPath(item: currentPage-1, section: 0), at: .left, animated: true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        //collFeaturedProducts.scrollToItem(at:IndexPath(item: currentPage+1, section: 0), at: .right, animated: true)
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collFeaturedProducts {
            let layout = self.collFeaturedProducts.collectionViewLayout as! UPCarouselFlowLayout
            let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
            let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
            currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
            
            if arrProdcts.count > currentPage {
                objCurrentPoduct = arrProdcts.object(at: currentPage) as? Products
            }
            print(currentPage)
        }
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension StoreVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProdcts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case collFeaturedProducts:
            let cell: CellProductStore = collectionView.dequeueReusableCell(withReuseIdentifier: "CellProductStore", for: indexPath) as! CellProductStore
            let objProduct = arrProdcts.object(at: indexPath.row) as! Products
            cell.cellConfiguration(objProduct)
            cell.btnShopNow.tag = indexPath.row
            cell.btnShopNow.addTarget(self, action: #selector(btnShopNow(_:)), for: .touchUpInside)
            return cell
        case collEquipmentGallery:
            let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
            cell.viewLike.isHidden = true
            cell.viewComment.isHidden = true
            cell.heightImgFish.constant = 128
            let objProduct = arrProdcts.object(at: indexPath.row) as! Products
            cell.imgPost.sd_setImage(with: URL(string: objProduct.productImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTitle.text = objProduct.productName
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch collectionView {
        case collFeaturedProducts:
            let lr = (SCREENWIDTH()-238)/2
            return UIEdgeInsets(top: 0, left: lr, bottom: 0, right: lr)
        case collEquipmentGallery:
            return UIEdgeInsets(top: 0, left: 26, bottom: 0, right: 26)
        default:
            break
        }
        return UIEdgeInsets()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1 {
            return CGSize(width: 238, height: 412)
        } else {
            return CGSize(width: (SCREENWIDTH()-78)/2, height: 170)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collFeaturedProducts {
            return 0
        } else {
            return 20
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objProduct = arrProdcts.object(at: indexPath.row) as! Products
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
        vc.imageType = .AddImage
        vc.objProduct = objProduct
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnShopNow(_ sender: UIButton) {
        let index = sender.tag
        let objProduct = arrProdcts.object(at: index) as! Products
        let sURL =  objProduct.productShopUrl ?? ""
        let escapedAddress = sURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        UIApplication.shared.open(URL(string: escapedAddress)!, options: [:], completionHandler: nil)
    }
}

//MARK:- API CALLING
extension StoreVC {
    func apiGetProducts() {
        let sURL = SERVER_URL + API_PRODCTS
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_PRODCTS, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                let objStore: FMStore = FMStore.init(object: responseObject ?? "")
                if objStore.success == "1" {
                    self.arrProdcts.removeAllObjects()
                    for product in objStore.products ?? [] {
                        self.arrProdcts.add(product)
                    }
                    self.objCurrentPoduct = nil
                    if self.arrProdcts.count > 0 {
                        self.objCurrentPoduct = self.arrProdcts.object(at: 0) as? Products
                    }
                    let page = CGFloat(self.arrProdcts.count)/2
                    let totalPage = Int(page.rounded(.up))
                    self.heightCollEquipment.constant = CGFloat(totalPage*170) + CGFloat((totalPage-1)*20) //170 is cell height
                    self.view.layoutIfNeeded()
                    //self.apiGetProductImage(true)
                } else {
                    showMessage(objStore.message ?? ErrorMSG)
                }
                self.collFeaturedProducts.reloadData()
                self.collEquipmentGallery.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}
