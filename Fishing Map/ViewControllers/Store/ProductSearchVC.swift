//
//  ProductSearchVC.swift
//  Fishing Map
//
//  Created by iMac on 17/12/20.
//

import UIKit

class cellProduct: UITableViewCell {
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
}

class ProductSearchVC: UIViewController {

    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tblProduct: UITableView!
    @IBOutlet var lblNoRecordsFound: UILabel!
    
    var arrSearchProducts : [Products] = []
    var sProductName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        txtSearch.text = sProductName
        apiSearchProduct()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UITABLEVEW DELEGATE
extension ProductSearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lblNoRecordsFound.alpha = arrSearchProducts.count > 0 ? 0 : 1
        return arrSearchProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: cellProduct = tableView.dequeueReusableCell(withIdentifier: "cellProduct", for: indexPath) as! cellProduct
        let objProduct = arrSearchProducts[indexPath.row]
        cell.imgProduct.sd_setImage(with: URL(string: objProduct.productImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        cell.lblName.text = objProduct.productName
        cell.lblPrice.text = "‎¥‎ \(objProduct.productPrice ?? "0")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objProduct = arrSearchProducts[indexPath.row]
        let vc = loadVC(strStoryboardId: SB_Store, strVCId: "ProductMoreDetailsVC") as! ProductMoreDetailsVC
        vc.objProduct = objProduct
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SCREENWIDTH()
    }
}

//MARK:- API CALLING
extension ProductSearchVC {
    func apiSearchProduct() {
        if TRIM(string: txtSearch.text ?? "").count <= 0 { return }
        let sURL = SERVER_URL + "products/search?term=\(TRIM(string: txtSearch.text ?? ""))"
        let urlString = sURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        showLoaderHUD(strMessage: "")
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: urlString, httpHeader: getAuthToken()) { (response, error) in
            print(response ?? "")
            hideLoaderHUD()
            self.arrSearchProducts.removeAll()
            if let dict = response as? NSDictionary {
                let dictData = dictionaryOfFilteredBy(dict: dict)
                let objProd: FMProductSearch = FMProductSearch.init(object: dictData)
                if objProd.success == "1" {
                    if let allproduct = objProd.products {
                        self.arrSearchProducts = allproduct
                    }
                } else {
                    showMessage(objProd.message ?? ErrorMSG)
                }
            }
            self.tblProduct.reloadData()
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension ProductSearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            self.apiSearchProduct()
        }
    }
}
