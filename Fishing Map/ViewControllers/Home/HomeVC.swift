//
//  HomeVC.swift
//  Fishing Map
//
//  Created by iMac on 27/10/20.
//

import UIKit
import SDWebImage

class CellHomeFeed: UITableViewCell {
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostDate: UILabel!
    @IBOutlet weak var lblPostDescription: UILabel!
    @IBOutlet var imgType: UIImageView!
    @IBOutlet weak var lblLikesCount: UILabel!
    @IBOutlet weak var lblCommentsCount: UILabel!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var imgLike: UIImageView!
    @IBOutlet var heightImage: NSLayoutConstraint!
    
    func configureCell(post: Posts) {
        imgUser.sd_setImage(with: URL(string: post.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        //imgPost.sd_setImage(with: URL(string: post.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        imgType.sd_setImage(with: URL(string: post.postCategoryIconUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblUserName.text = post.userName
        lblPostDate.text = "オン \(intToDate(miliseconds: post.postDate ?? 0, shortMonthName: false))"
        lblPostDescription.attributedText = convertHashtags(text: post.postDescription ?? "")
        lblLikesCount.text = "\(post.postLikesCount ?? 0)"
        lblCommentsCount.text = "\(post.postCommentsCount ?? 0)"
        
        if post.postIsLiked ?? false {
            imgLike.image = UIImage(named: "ic_like")
        } else {
            imgLike.image = UIImage(named: "ic_like_gray")
        }
    }
}

class HomeVC: UIViewController {

    @IBOutlet var tblFeed: UITableView!
    
    var refreshControl = UIRefreshControl()
    var feedPost: FMFeedPosts?
    var arrPosts: [Posts] = []
    var currentPage: Int = 1
    var isSend = false
    var cachedHeight = [IndexPath : CGFloat]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblFeed.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        getFeedPosts()
        tblFeed.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.getFeedPosts()
            }else {
                self.tblFeed.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblFeed.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if APP_DELEGATE.isHandlePush {
            APP_DELEGATE.isHandlePush = false
            handlePush()
        }
    }
    
    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        currentPage = 1
        getFeedPosts()
    }
    
    func getFeedPosts() {
        isSend = true
        if currentPage == 1 { showLoaderHUD(strMessage: "") }
        let sURL = SERVER_URL + API_GET_FEED_POSTS + "?page=\(currentPage)"
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error)  in
            self.refreshControl.endRefreshing()
            if self.currentPage == 1 {
                self.tblFeed.showsInfiniteScrolling = true
                self.arrPosts.removeAll()
                self.cachedHeight.removeAll()
                hideLoaderHUD()
            }
            print(response ?? "")
            let objFeedPosts: FMFeedPosts = FMFeedPosts.init(object: response ?? "")
            print(objFeedPosts.message ?? "")
            
            self.feedPost = objFeedPosts
            
            for post in self.feedPost?.posts ?? [] {
                self.arrPosts.append(post)
            }
            
            if objFeedPosts.pagination?.nextPage != nil {
                self.currentPage = objFeedPosts.pagination?.nextPage ?? (self.currentPage+1)
            } else {
                self.tblFeed.showsInfiniteScrolling = false
            }
            
            self.tblFeed.infiniteScrollingView.stopAnimating()
            if self.arrPosts.count <= 0 { self.tblFeed.showsInfiniteScrolling = false }
            
            self.tblFeed.reloadData()
            self.isSend = false
        }
    }
    
    @IBAction func btnAddPost(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        vc.addImgType = .Post
        vc.delegateCreatePost = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSearchPost(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedSearchVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSideMenu(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "navsidemenu")
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellHomeFeed = tableView.dequeueReusableCell(withIdentifier: "CellHomeFeed", for: indexPath) as! CellHomeFeed
        cell.btnInfo.addTarget(self, action: #selector(btnInfoClicked), for: .touchUpInside)
        cell.btnLike.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
        cell.configureCell(post: self.arrPosts[indexPath.row])
        
        let objPost: Posts = self.arrPosts[indexPath.row]
        if objPost.postImageUrl?.count ?? 0 > 0  {
            if let height = cachedHeight[indexPath] {
                cell.heightImage.constant = height
                cell.contentView.layoutIfNeeded()
                cell.imgPost.sd_setImage(with: URL(string: objPost.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            } else {
                cell.heightImage.constant = 240
                cell.imgPost.sd_setImage(with: URL(string: objPost.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad) { (theImage, error, cache, url) in
                    if let image = theImage {
                        self.cachedHeight[indexPath] = getAspectRatioAccordingToiPhones(cellImageFrame: CGSize(width: SCREENWIDTH()-24, height: 140),downloadedImage: image)
                        cell.heightImage.constant = self.cachedHeight[indexPath]!
                        self.tblFeed.reloadRows(at: [indexPath], with: .none)
                    }
                }
            }
        }
        let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        cell.imgUser.isUserInteractionEnabled = true
        cell.imgUser.addGestureRecognizer(imgG)
        
        let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        cell.lblUserName.isUserInteractionEnabled = true
        cell.lblUserName.addGestureRecognizer(lblG)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
        vc.delegateFeedDetails = self
        vc.indexPath = indexPath
        vc.post = arrPosts[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func btnInfoClicked(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblFeed)
        let indexPath: IndexPath = tblFeed.indexPathForRow(at: buttonPosition)!
        
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
        vc.delegateFeedDetails = self
        vc.indexPath = indexPath
        vc.post = arrPosts[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnLike(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblFeed)
        let indexPath: IndexPath = tblFeed.indexPathForRow(at: buttonPosition)!
        
        let post: Posts = arrPosts[indexPath.row]
        if post.postIsLiked ?? false {
            apiUnLikePost(post.postId ?? 0, isUnlikeEvent: false)
            post.postIsLiked = false
            
            post.postLikesCount = (post.postLikesCount ?? 0) - 1
            if post.postLikesCount ?? 0 < 0 {
                post.postLikesCount = 0
            }
        } else {
            apiLikePost(post.postId ?? 0, isLikeEvent: false)
            post.postIsLiked = true
            post.postLikesCount = (post.postLikesCount ?? 0) + 1
        }
        if let cell = self.tblFeed.cellForRow(at: indexPath) as? CellHomeFeed {
            cell.lblLikesCount.text = "\(post.postLikesCount ?? 0)"
            
            if post.postIsLiked ?? false {
                cell.imgLike.image = UIImage(named: "ic_like")
            } else {
                cell.imgLike.image = UIImage(named: "ic_like_gray")
            }
        } else {
            tblFeed.reloadData()
        }
    }
    
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: tblFeed)
        if let indexPath = tblFeed.indexPathForRow(at: location) {
            let objPost = arrPosts[indexPath.row]
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
            vc.sUserID = "\(objPost.userId ?? 0)"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: DELEGATE
extension HomeVC: delegateCreatePost {
    func getNewPost(_ post: Posts) {
        arrPosts.insert(post, at: 0)
        tblFeed.reloadData()
        tblFeed.setContentOffset(.zero, animated: true)
    }
}

extension HomeVC {
    func handlePush() {
        guard let objNot = APP_DELEGATE.objNotiPush else {
            return
        }
        switch objNot.notifyType {
        case NOTIFICATION_TYPE.GENERIC.rawValue:
            let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "TextVC") as! TextVC
            vc.sText = objNot.notificationText ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case NOTIFICATION_TYPE.EVENT.rawValue:
            apiGetEventDetails(objNot.objId ?? "") { (objEvent) in
                let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventsDetailsVC") as! EventsDetailsVC
                vc.event = objEvent
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case NOTIFICATION_TYPE.POST.rawValue:
            apiGetFeedPost(objNot.objId ?? "") { (objPost) in
                let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
                vc.post = objPost
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case NOTIFICATION_TYPE.LOCATION.rawValue:
            print("location")
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
            vc.imgType = getPushImageType(objNot.objClass ?? "")
            vc.sLocationID = objNot.objId ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        case NOTIFICATION_TYPE.IMAGE.rawValue:
            print("image")
            let imageType = getPushImageType(objNot.objClass ?? "")
            switch imageType {
            case .Fish, .TopTerrain, .TopScene, .ParkingSpots:
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                vc.imageType = imageType
                vc.sImageID = objNot.objId ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            case .Restaurant, .Hotels:
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
                vc.imageType = imageType
                vc.sImageID = objNot.objId ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
        default:
            break
        }
    }
}

extension HomeVC: delegateFeedDetails {
    func deleteFeed(_ indexPath: IndexPath) {
        arrPosts.remove(at: indexPath.row)
        tblFeed.reloadData()
    }
}
