//
//  TabBarVC.swift
//  Fishing Map
//
//  Created by macOS on 28/10/20.
//

import UIKit

class TabBarVC: UITabBarController {
    
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = Color_Hex(hex: "#056989")
        tabBar.isTranslucent = false
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.75), NSAttributedString.Key.font: UIFont(name: "Interstate-Bold", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "Interstate-Regular", size: 10)!], for: .selected)
        if index > 0 {
            selectedIndex = index
        }
    }
}
