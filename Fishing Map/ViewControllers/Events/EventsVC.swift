//
//  EventsVC.swift
//  Fishing Map
//
//  Created by macOS on 10/11/20.
//

import UIKit

class CellEvents: UITableViewCell {
    @IBOutlet var btnInfo: UIButton!
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var imgEvent: UIImageView!
    @IBOutlet var lblEventOrganizerName: UILabel!
    @IBOutlet var lblEventPostDate: UILabel!
    @IBOutlet var lblEventDate: UILabel!
    @IBOutlet var lblLikesCount: UILabel!
    @IBOutlet var lblCommentsCount: UILabel!
    @IBOutlet var lblEventName: UILabel!
    @IBOutlet var lblPrefectureName: UILabel!
    @IBOutlet var lblEventDescription: UILabel!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnLikes: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var lblTotalAttendence: UILabel!
    @IBOutlet var imgU1: UIImageView!
    @IBOutlet var imgU2: UIImageView!
    @IBOutlet var imgU3: UIImageView!
    @IBOutlet var btnMoreAttendence: UIButton!
    @IBOutlet var viewAttendenceUser: UIView!
    @IBOutlet var widthViewAttendence: NSLayoutConstraint!
    @IBOutlet var xTotalAttendence: NSLayoutConstraint!
    @IBOutlet var imgLike: UIImageView!
    
    func configureCell(event: Events) {
        imgUserProfile.sd_setImage(with: URL(string: event.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        imgEvent.sd_setImage(with: URL(string: event.eventImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblEventOrganizerName.text = event.userName
        lblEventPostDate.text = "オン \(intToDate(miliseconds: event.eventDate ?? 0, shortMonthName: false))"
        lblEventDate.text = intToDate(miliseconds: event.eventDate ?? 0, shortMonthName: true)
        lblLikesCount.text = "\(event.eventLikesCount ?? 0)"
        lblCommentsCount.text = "\(event.eventCommentsCount ?? 0)"
        lblEventName.text = event.eventTitle
        lblPrefectureName.text = event.eventPrefectureName
        lblEventDescription.text = event.eventDescription
        lblTotalAttendence.text = "\(event.eventAttendeesCount ?? 0) 参加者"
        if event.eventIsLiked ?? false {
            imgLike.image = UIImage(named: "ic_like")
        } else {
            imgLike.image = UIImage(named: "ic_like_gray")
        }
        if event.eventIsRegistered ?? false {
            btnRegister.setTitle("登録を解除する", for: .normal)
            btnRegister.setTitleColor(Color_Hex(hex: "056989"), for: .normal)
            btnRegister.backgroundColor = .white
            btnRegister.layer.borderColor = Color_Hex(hex: "056989").cgColor
            btnRegister.layer.borderWidth = 1
        } else {
            btnRegister.setTitle("登録", for: .normal)
            btnRegister.setTitleColor(UIColor.white, for: .normal)
            btnRegister.backgroundColor = Color_Hex(hex: "056989")
            btnRegister.layer.borderColor = UIColor.clear.cgColor
            btnRegister.layer.borderWidth = 0
        }
        
        xTotalAttendence.constant = 6
        viewAttendenceUser.isHidden = false
        imgU1.isHidden = false
        imgU2.isHidden = false
        imgU3.isHidden = false
        btnMoreAttendence.isHidden = true
        
        let arrUrls = event.eventAttendessAvatarUrls
        
        if event.eventAttendeesCount ?? 0 > 3 {
            btnMoreAttendence.isHidden = false
            if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            }
            if let obj2: EventAttendessAvatarUrls = arrUrls?[1] {
                imgU2.sd_setImage(with: URL(string: obj2.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            }
            if let obj3: EventAttendessAvatarUrls = arrUrls?[2] {
                imgU3.sd_setImage(with: URL(string: obj3.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            }
        } else {
            if event.eventAttendeesCount == 0 {
                xTotalAttendence.constant = -39
                viewAttendenceUser.isHidden = true
            } else if event.eventAttendeesCount == 1 {
                imgU2.isHidden = true
                imgU3.isHidden = true
                if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                    imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
            } else if event.eventAttendeesCount == 2 {
                imgU3.isHidden = true
                if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                    imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
                if let obj2: EventAttendessAvatarUrls = arrUrls?[1] {
                    imgU2.sd_setImage(with: URL(string: obj2.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
            } else if event.eventAttendeesCount == 3 {
                if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                    imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
                if let obj2: EventAttendessAvatarUrls = arrUrls?[1] {
                    imgU2.sd_setImage(with: URL(string: obj2.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
                if let obj3: EventAttendessAvatarUrls = arrUrls?[2] {
                    imgU3.sd_setImage(with: URL(string: obj3.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
            }
        }
    }
}

class EventsVC: UIViewController {
    
    @IBOutlet weak var tblEvents: UITableView!
    @IBOutlet var lblRedFilter: UILabel!
    
    var refreshControl = UIRefreshControl()
    var events: FMEvents?
    var arrEvents: [Events] = []
    var currentPage: Int = 1
    var isSend = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblEvents.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.pulltorefresh), for: .valueChanged)
        getAllEvents()
        tblEvents.addInfiniteScrolling(actionHandler: ({
            if self.isSend == false{
                self.getAllEvents()
            }else {
                self.tblEvents.infiniteScrollingView.stopAnimating()
            }
        }))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblEvents.reloadData()
        lblRedFilter.isHidden = isShowRedFIlterDot()
    }
    
    //MARK:- Refresh Control Method
    @objc func pulltorefresh() {
        isSend = false
        currentPage = 1
        getAllEvents()
    }
    
    @IBAction func btnAddEvent(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "AddEventVC") as! AddEventVC
        vc.delegateCreateEvent = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSideMenu(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "navsidemenu")
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }
    
    @IBAction func btnEventFilter(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventFilterVC") as! EventFilterVC
        vc.delegateFilterEvent = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }
    
    @IBAction func btnSearchEvents(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventSearchVC") as! EventSearchVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension EventsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellEvents = tableView.dequeueReusableCell(withIdentifier: "CellEvents", for: indexPath) as! CellEvents
        cell.btnInfo.addTarget(self, action: #selector(btnInfoClicked), for: .touchUpInside)
        cell.btnLikes.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
        cell.btnRegister.addTarget(self, action: #selector(btnRegister(_:)), for: .touchUpInside)
        cell.configureCell(event: self.arrEvents[indexPath.row])
        let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        cell.imgUserProfile.isUserInteractionEnabled = true
        cell.imgUserProfile.addGestureRecognizer(imgG)
        
        let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        cell.lblEventOrganizerName.isUserInteractionEnabled = true
        cell.lblEventOrganizerName.addGestureRecognizer(lblG)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventsDetailsVC") as! EventsDetailsVC
        vc.event = arrEvents[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnInfoClicked() {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventsDetailsVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnLike(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblEvents)
        let indexPath: IndexPath = tblEvents.indexPathForRow(at: buttonPosition)!
        let event: Events = arrEvents[indexPath.row]
        if event.eventIsLiked ?? false {
            apiUnLikePost(event.eventId ?? 0, isUnlikeEvent: true)
            event.eventIsLiked = false
            
            event.eventLikesCount = (event.eventLikesCount ?? 0) - 1
            if event.eventLikesCount ?? 0 < 0 {
                event.eventLikesCount = 0
            }
        } else {
            apiLikePost(event.eventId ?? 0, isLikeEvent: true)
            event.eventIsLiked = true
            event.eventLikesCount = (event.eventLikesCount ?? 0) + 1
        }
        tblEvents.reloadData()
    }
    
    @objc func btnRegister(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblEvents)
        let indexPath: IndexPath = tblEvents.indexPathForRow(at: buttonPosition)!
        let event: Events = arrEvents[indexPath.row]
        if event.eventIsRegistered ?? false {
            apiUnRegisterEvent(event.eventId ?? 0) { (_) in
                apiGetEventDetails("\(event.eventId ?? 0)", false) { (objEvent) in
                    guard let obj = objEvent else { return }
                    event.eventAttendeesCount = obj.eventAttendeesCount
                    event.eventAttendessAvatarUrls = obj.eventAttendessAvatarUrls
                    self.tblEvents.reloadData()
                }
            }
            event.eventIsRegistered = false
        } else {
            apiRegisterEvent(event.eventId ?? 0) { (_) in
                apiGetEventDetails("\(event.eventId ?? 0)", false) { (objEvent) in
                    guard let obj = objEvent else { return }
                    event.eventAttendeesCount = obj.eventAttendeesCount
                    event.eventAttendessAvatarUrls = obj.eventAttendessAvatarUrls
                    self.tblEvents.reloadData()
                }
            }
            event.eventIsRegistered = true
        }
        self.tblEvents.reloadData()
    }
    
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: tblEvents)
        if let indexPath = tblEvents.indexPathForRow(at: location) {
            let event: Events = arrEvents[indexPath.row]
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
            vc.sUserID = "\(event.userId ?? 0)"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: DELEGATE
extension EventsVC: delegateCreateEvent, delegateFilterEvent {
    func getNewEvent(_ event: Events) {
        arrEvents.insert(event, at: 0)
        tblEvents.reloadData()
        tblEvents.setContentOffset(.zero, animated: true)
    }
    
    func filterEvent(_ sDate: String,_  sPrefectureID: String) {
        apiFilterEvent(sDate, sPrefectureID)
    }
    
    func removeFilter() {
        UserDefaults.standard.removeObject(forKey: "selectedEventDate")
        UserDefaults.standard.removeObject(forKey: "selectedPrefectreId")
        lblRedFilter.isHidden = isShowRedFIlterDot()
        pulltorefresh()
    }
    
    func isShowRedFIlterDot() -> Bool {
        if UserDefaults.standard.object(forKey: "selectedEventDate") != nil {
            return false
        }
        if UserDefaults.standard.object(forKey: "selectedPrefectreId") != nil {
            return false
        }
        return true
    }
}

//MARK:- API CALLING
extension EventsVC {
    func getAllEvents() {
        isSend = true
        if currentPage == 1 { showLoaderHUD(strMessage: "") }
        let sURL = SERVER_URL + API_GET_EVENTS + "?page=\(currentPage)"
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error) in
            self.refreshControl.endRefreshing()
            if self.currentPage == 1 {
                self.tblEvents.showsInfiniteScrolling = true
                self.arrEvents.removeAll()
                hideLoaderHUD()
            }
            print(response ?? "")
            
            let objEvents: FMEvents = FMEvents.init(object: response ?? "")
            print(objEvents.message ?? "")
            
            self.events = objEvents
            
            for event in self.events?.events ?? [] {
                self.arrEvents.append(event)
            }
            
            if objEvents.pagination?.nextPage != nil {
                self.currentPage = objEvents.pagination?.nextPage ?? (self.currentPage+1)
            } else {
                self.tblEvents.showsInfiniteScrolling = false
            }
            
            self.tblEvents.infiniteScrollingView.stopAnimating()
            if self.arrEvents.count <= 0 { self.tblEvents.showsInfiniteScrolling = false }
            
            self.tblEvents.reloadData()
            self.isSend = false
        }
    }
    
    func apiFilterEvent(_ sDate: String,_  sPrefectureID: String) {
        UserDefaults.standard.setValue(sDate, forKey: "selectedEventDate")
        UserDefaults.standard.setValue(sPrefectureID, forKey: "selectedPrefectreId")
        lblRedFilter.isHidden = isShowRedFIlterDot()
        currentPage = 1
        isSend = false
        
        showLoaderHUD(strMessage: "")
        let sValue = (sPrefectureID == "0") ? "" : "&prefecture_id=\(sPrefectureID)"
        let sURL = SERVER_URL + API_FILTER_EVENT + "?date=\("\(convertStringToTimeStamp(sDate))")\(sValue)"
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error) in
            hideLoaderHUD()
            print(response ?? "")
            let objEvents: FMEvents = FMEvents.init(object: response ?? "")
            self.arrEvents.removeAll()
            if objEvents.success ?? false {
                for event in objEvents.events ?? [] {
                    self.arrEvents.append(event)
                }
            } else {
                showMessage(objEvents.message ?? ErrorMSG)
            }
            self.tblEvents.reloadData()
        }
    }
}
