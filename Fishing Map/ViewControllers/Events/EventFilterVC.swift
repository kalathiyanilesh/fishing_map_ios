//
//  EventFilterVC.swift
//  Fishing Map
//
//  Created by macOS on 11/11/20.
//

import UIKit

class CellSearchEvent: UITableViewCell {
    
    @IBOutlet weak var btnSearchEvent: UIButton!
}

class CellSortByDate: UITableViewCell {
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var tfDate: UITextField!
}

class CellSortByPrefecture: UITableViewCell {
    @IBOutlet weak var collEventPrefecture: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collEventPrefecture.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
    }
}

extension CellSortByPrefecture {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collEventPrefecture.delegate = dataSourceDelegate
        collEventPrefecture.dataSource = dataSourceDelegate
        collEventPrefecture.tag = row
        collEventPrefecture.setContentOffset(collEventPrefecture.contentOffset, animated:false)
        collEventPrefecture.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collEventPrefecture.contentOffset.x = newValue }
        get { return collEventPrefecture.contentOffset.x }
    }
}

protocol delegateFilterEvent {
    func filterEvent(_ sDate: String,_  sPrefectureID: String)
    func removeFilter()
}

class EventFilterVC: UIViewController {
    
    @IBOutlet weak var tblEventFilter: UITableView!
    @IBOutlet weak var viewContentDrawer: UIView!
    @IBOutlet weak var topOfContentDrawer: NSLayoutConstraint!
    
    var storedOffsets = [Int: CGFloat]()
    var toolBar = UIToolbar()
    var selectedDate = String()
    var selectedPrefecture = "0"
    var arrPrefectures = [Prefectures]()
    var delegateFilterEvent: delegateFilterEvent?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topOfContentDrawer.constant = 0 - viewContentDrawer.frame.height
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        selectedDate = "\(dateFormatter.string(from: Date()))"
        
        if UserDefaults.standard.object(forKey: "selectedEventDate") != nil {
            selectedDate = UserDefaults.standard.object(forKey: "selectedEventDate") as? String ?? ""
        }
        if UserDefaults.standard.object(forKey: "selectedPrefectreId") != nil {
            selectedPrefecture = UserDefaults.standard.object(forKey: "selectedPrefectreId") as? String ?? "0"
        }
        
        apiGetPrefectures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewContentDrawer.layer.masksToBounds = true
        viewContentDrawer.layer.cornerRadius = 16
        if #available(iOS 11.0, *) {
            viewContentDrawer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        runAfterTime(time: 0.2) {
            UIView.animate(withDuration: 0.3) {
                self.topOfContentDrawer.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func btnSearchEvent(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventSearchVC") as! EventSearchVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.topOfContentDrawer.constant = 0 - self.viewContentDrawer.frame.height
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.dismiss(animated: false) {
                self.delegateFilterEvent?.removeFilter()
            }
        }
    }
    
    @IBAction func btnApply(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.topOfContentDrawer.constant = 0 - self.viewContentDrawer.frame.height
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.dismiss(animated: false) {
                self.delegateFilterEvent?.filterEvent(self.selectedDate, self.selectedPrefecture)
            }
        }
    }
    
    @objc func showDatePicker(_ sender: UIButton) {
        self.view.endEditing(true)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd/MM/yyyy"
        self.selectedDate = dateFormat.string(from: Date())
        self.tblEventFilter.reloadData()
        
        let alert = UIAlertController(style: .actionSheet, title: "日付を選択")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: nil, maximumDate: Date()) { date in
            self.selectedDate = dateFormat.string(from: date)
            self.tblEventFilter.reloadData()
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show(self)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension EventFilterVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: CellSearchEvent = tableView.dequeueReusableCell(withIdentifier: "CellSearchEvent", for: indexPath) as! CellSearchEvent
            cell.btnSearchEvent.addTarget(self, action: #selector(btnSearchEvent(_:)), for: .touchUpInside)
            return cell
        case 1:
            let cell: CellSortByDate = tableView.dequeueReusableCell(withIdentifier: "CellSortByDate", for: indexPath) as! CellSortByDate
            cell.btnDate.setTitle(self.selectedDate, for: .normal)
            cell.btnDate.addTarget(self, action: #selector(self.showDatePicker(_:)), for: .touchUpInside)
            return cell
        default:
            let cell: CellSortByPrefecture = tableView.dequeueReusableCell(withIdentifier: "CellSortByPrefecture", for: indexPath) as! CellSortByPrefecture
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let profileCell = cell as? CellSortByPrefecture else { return }
        profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 76
        case 1:
            return 60
        default:
            return 300
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let profileCell = cell as? CellSortByPrefecture else { return }
        storedOffsets[indexPath.row] = profileCell.collectionViewOffset
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension EventFilterVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPrefectures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.heightImgFish.constant = 128
        cell.viewLike.isHidden = true
        cell.viewComment.isHidden = true
        let objPre = arrPrefectures[indexPath.row]
        cell.lblTitle.text = objPre.prefectureName
        if "\(objPre.prefectureId ?? 0)" == selectedPrefecture {
            cell.viewContent.layer.borderColor = Color_Hex(hex: "#1DBBD1").cgColor
            cell.viewContent.layer.borderWidth = 2
            cell.viewContent.dropShadow(scale: true, color: Color_Hex(hex: "#1DBBD1"), opacity: 0.14, offset: CGSize(width: 0, height: 4), radius: 8)
        }  else {
            cell.viewContent.layer.borderColor = Color_Hex(hex: "#E8E8E8").cgColor
            cell.viewContent.layer.borderWidth = 1
            cell.viewContent.dropShadow(scale: true, color: .clear, opacity: 0.14, offset: CGSize(width: 0, height: 4), radius: 8)
        }
        
        cell.imgPost.sd_setImage(with: URL(string: objPre.prefectureImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objPre = arrPrefectures[indexPath.row]
        selectedPrefecture = "\(objPre.prefectureId ?? 0)"
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 148, height: 179)
    }
}

//MARK:- API CALLING
extension EventFilterVC {
    func apiGetPrefectures() {
        let sURL = SERVER_URL + API_GET_PREFECTURES
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: BLANK_HEADER) { (response, error) in
            print(response ?? "")
            let objPrefectures: FMPrefecturesList = FMPrefecturesList.init(object: response ?? "")
            self.arrPrefectures = objPrefectures.prefectures ?? []
            self.tblEventFilter.reloadData()
        }
    }
}
