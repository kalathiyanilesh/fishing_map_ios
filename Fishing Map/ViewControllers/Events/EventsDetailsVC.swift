//
//  EventsDetailsVC.swift
//  Fishing Map
//
//  Created by macOS on 11/11/20.
//

import UIKit
import GoogleMaps

class CellEventDetails: UITableViewCell {
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var imgEvent: UIImageView!
    @IBOutlet var lblEventOrganizerName: UILabel!
    @IBOutlet var lblEventPostDate: UILabel!
    @IBOutlet var lblEventDate: UILabel!
    @IBOutlet var lblLikesCount: UILabel!
    @IBOutlet var lblCommentsCount: UILabel!
    @IBOutlet var lblEventName: UILabel!
    @IBOutlet var lblPrefectureName: UILabel!
    @IBOutlet var lblEventDescription: UILabel!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var lblAttendaceCount: UILabel!
    @IBOutlet var imgU1: UIImageView!
    @IBOutlet var imgU2: UIImageView!
    @IBOutlet var imgU3: UIImageView!
    @IBOutlet var btnMoreAttendence: UIButton!
    @IBOutlet var viewAttendenceUser: UIView!
    @IBOutlet var widthViewAttendence: NSLayoutConstraint!
    @IBOutlet var xTotalAttendence: NSLayoutConstraint!
    @IBOutlet var imgLike: UIImageView!
    @IBOutlet var btnMore: UIButton!
    
    func configureCell(event: Events) {
        imgUserProfile.sd_setImage(with: URL(string: event.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        imgEvent.sd_setImage(with: URL(string: event.eventImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblEventOrganizerName.text = event.userName
        lblEventPostDate.text = "オン \(intToDate(miliseconds: event.eventDate ?? 0, shortMonthName: false))"
        lblEventDate.text = intToDate(miliseconds: event.eventDate ?? 0, shortMonthName: true)
        lblLikesCount.text = "\(event.eventLikesCount ?? 0)"
        lblCommentsCount.text = "\(event.eventCommentsCount ?? 0)"
        lblEventName.text = event.eventTitle
        lblPrefectureName.text = event.eventPrefectureName
        lblEventDescription.text = event.eventDescription
        lblAttendaceCount.text = "\(event.eventAttendeesCount ?? 0) 参加者"
        
        if event.eventIsLiked ?? false {
            imgLike.image = UIImage(named: "ic_like")
        } else {
            imgLike.image = UIImage(named: "ic_like_gray")
        }
        
        xTotalAttendence.constant = 6
        viewAttendenceUser.isHidden = false
        imgU1.isHidden = false
        imgU2.isHidden = false
        imgU3.isHidden = false
        btnMoreAttendence.isHidden = true
        
        let arrUrls = event.eventAttendessAvatarUrls
        
        if event.eventAttendeesCount ?? 0 > 3 {
            btnMoreAttendence.isHidden = false
            if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            }
            if let obj2: EventAttendessAvatarUrls = arrUrls?[1] {
                imgU2.sd_setImage(with: URL(string: obj2.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            }
            if let obj3: EventAttendessAvatarUrls = arrUrls?[2] {
                imgU3.sd_setImage(with: URL(string: obj3.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            }
        } else {
            if event.eventAttendeesCount == 0 {
                xTotalAttendence.constant = -39
                viewAttendenceUser.isHidden = true
            } else if event.eventAttendeesCount == 1 {
                imgU2.isHidden = true
                imgU3.isHidden = true
                if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                    imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
            } else if event.eventAttendeesCount == 2 {
                imgU3.isHidden = true
                if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                    imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
                if let obj2: EventAttendessAvatarUrls = arrUrls?[1] {
                    imgU2.sd_setImage(with: URL(string: obj2.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
            } else if event.eventAttendeesCount == 3 {
                if let obj1: EventAttendessAvatarUrls = arrUrls?[0] {
                    imgU1.sd_setImage(with: URL(string: obj1.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
                if let obj2: EventAttendessAvatarUrls = arrUrls?[1] {
                    imgU2.sd_setImage(with: URL(string: obj2.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
                if let obj3: EventAttendessAvatarUrls = arrUrls?[2] {
                    imgU3.sd_setImage(with: URL(string: obj3.avatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                }
            }
        }
    }
}

class CellOrganizerName: UITableViewCell {
    @IBOutlet var imgOrganiser: UIImageView!
    @IBOutlet var lblOrganiserName: UILabel!
    
    func configureCell(event: Events) {
        imgOrganiser.sd_setImage(with: URL(string: event.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblOrganiserName.text = event.userName
    }
}

class CellAddCommentEvent: UITableViewCell {
    @IBOutlet weak var txtComment: UITextField!
}

class CellCommentsEvents: UITableViewCell {
    @IBOutlet weak var viewAllComments: UIView!
    @IBOutlet weak var lblAllCommentsCount: UILabel!
    @IBOutlet weak var imgCommentorUser: UIImageView!
    @IBOutlet weak var lblCommentorName: UILabel!
    @IBOutlet weak var lblCommentTexts: UILabel!
    
    func configureCell(eventComment: EventComments) {
        imgCommentorUser.sd_setImage(with: URL(string: eventComment.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblCommentorName.text = "\(eventComment.userName ?? ""):"
        lblCommentTexts.text = eventComment.comment
    }
}

class CellLocationEvents: UITableViewCell {
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var lblEventLocationText: UILabel!
    
    func configureCell(event: Events) {
        lblEventLocationText.text = event.eventMapAddress
        
        let lat = Double(event.eventLat ?? "0") ?? 0
        let lng = Double(event.eventLng ?? "0") ?? 0
        
        if event.eventMapAddress?.count ?? 0 <= 0 {
            getAddress(lat, lng) { (sAddress) in
                self.lblEventLocationText.text = sAddress
            }
        }
        viewMap.isUserInteractionEnabled = false
        viewMap.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 6.0)
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = viewMap
    }
}

class CellRegisterEvent: UITableViewCell {
    
    @IBOutlet var btnRegister: UIButton!
    
    func configureCell(event: Events) {
        if event.eventIsRegistered ?? false {
            btnRegister.setTitle("登録を解除する", for: .normal)
            btnRegister.setTitleColor(Color_Hex(hex: "056989"), for: .normal)
            btnRegister.backgroundColor = .white
            btnRegister.layer.borderColor = Color_Hex(hex: "056989").cgColor
            btnRegister.layer.borderWidth = 1
        } else {
            btnRegister.setTitle("登録", for: .normal)
            btnRegister.setTitleColor(UIColor.white, for: .normal)
            btnRegister.backgroundColor = Color_Hex(hex: "056989")
            btnRegister.layer.borderColor = UIColor.clear.cgColor
            btnRegister.layer.borderWidth = 0
        }
    }
}

class EventsDetailsVC: UIViewController {
    
    @IBOutlet weak var tblEventDetails: UITableView!
    @IBOutlet weak var btnAddEvent: UIButton!
    
    var event: Events!
    var eventComments: [EventComments]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventComments = event.eventComments ?? []
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnAddEventTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "AddEventVC") as! AddEventVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK:- UITABLEVIEW DELEGATE
extension EventsDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5 // i removed register cell (as per Android UI)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            if event.eventCommentsCount ?? 0 > 5 {
                return 6
            } else {
                return eventComments.count
            }
        case 4:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: CellEventDetails = tableView.dequeueReusableCell(withIdentifier: "CellEventDetails", for: indexPath) as! CellEventDetails
            cell.btnBack.addTarget(self, action: #selector(btnBack), for: .touchUpInside)
            cell.btnLike.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
            cell.btnMore.isHidden = ("\(self.event.userId ?? 0)" == getUserID()) ? true : false
            cell.btnMore.addTarget(self, action: #selector(btnMore(_:)), for: .touchUpInside)
            cell.configureCell(event: self.event)
            
            let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
            cell.imgUserProfile.isUserInteractionEnabled = true
            cell.imgUserProfile.addGestureRecognizer(imgG)
            
            let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
            cell.lblEventOrganizerName.isUserInteractionEnabled = true
            cell.lblEventOrganizerName.addGestureRecognizer(lblG)
            
            return cell
        case 1:
            /*
            let cell: CellOrganizerName = tableView.dequeueReusableCell(withIdentifier: "CellOrganizerName", for: indexPath) as! CellOrganizerName
            cell.configureCell(event: self.event)
            let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
            cell.imgOrganiser.isUserInteractionEnabled = true
            cell.imgOrganiser.addGestureRecognizer(imgG)
            
            let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
            cell.lblOrganiserName.isUserInteractionEnabled = true
            cell.lblOrganiserName.addGestureRecognizer(lblG)
            return cell*/
            let cell: CellRegisterEvent = tableView.dequeueReusableCell(withIdentifier: "CellRegisterEvent", for: indexPath) as! CellRegisterEvent
            cell.btnRegister.addTarget(self, action: #selector(btnRegister(_:)), for: .touchUpInside)
            cell.configureCell(event: self.event)
            return cell
        case 2:
            let cell: CellAddCommentEvent = tableView.dequeueReusableCell(withIdentifier: "CellAddCommentEvent", for: indexPath) as! CellAddCommentEvent
            cell.txtComment.delegate = self
            return cell
        case 3:
            let cell: CellCommentsEvents = tableView.dequeueReusableCell(withIdentifier: "CellCommentsEvents", for: indexPath) as! CellCommentsEvents
            if event.eventCommentsCount ?? 0 > 5 && indexPath.row == 5 {
                cell.viewAllComments.isHidden = false
                cell.lblAllCommentsCount.text = "全てのコメントを見る (\((event.eventCommentsCount ?? 0)-5))" // view all 12 comments
            } else {
                cell.viewAllComments.isHidden = true
                cell.configureCell(eventComment: self.eventComments[indexPath.row])
                
                let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
                cell.imgCommentorUser.isUserInteractionEnabled = true
                cell.imgCommentorUser.addGestureRecognizer(imgG)
                
                let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
                cell.lblCommentorName.isUserInteractionEnabled = true
                cell.lblCommentorName.addGestureRecognizer(lblG)
            }
            
            return cell
        case 4:
            let cell: CellLocationEvents = tableView.dequeueReusableCell(withIdentifier: "CellLocationEvents", for: indexPath) as! CellLocationEvents
            
            cell.configureCell(event: self.event)
            
            return cell
        case 5:
            let cell: CellRegisterEvent = tableView.dequeueReusableCell(withIdentifier: "CellRegisterEvent", for: indexPath) as! CellRegisterEvent
            cell.btnRegister.addTarget(self, action: #selector(btnRegister(_:)), for: .touchUpInside)
            cell.configureCell(event: self.event)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return 130 //110
        case 2:
            return 103
        case 3:
            if event.eventCommentsCount ?? 0 > 5 && indexPath.row == 5 {
                return 54
            }
            return UITableView.automaticDimension
        case 4:
            return 390
        default:
            return 130
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 3:
            if event.eventCommentsCount ?? 0 > 5 && indexPath.row == 5 {
                let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AllCommentsVC") as! AllCommentsVC
                vc.isEventComments = true
                vc.event = event
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
    @objc func btnBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnLike(_ sender: UIButton) {
        if event.eventIsLiked ?? false {
            apiUnLikePost(event.eventId ?? 0, isUnlikeEvent: true)
            event.eventIsLiked = false
            
            event.eventLikesCount = (event.eventLikesCount ?? 0) - 1
            if event.eventLikesCount ?? 0 < 0 {
                event.eventLikesCount = 0
            }
        } else {
            apiLikePost(event.eventId ?? 0, isLikeEvent: false)
            event.eventIsLiked = true
            event.eventLikesCount = (event.eventLikesCount ?? 0) + 1
        }
        tblEventDetails.reloadData()
    }
    
    @objc func btnMore(_ sender: UIButton) {
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let reportActionButton = UIAlertAction(title: "報告書", style: .default)
        { _ in
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "BlockReasonVC") as! BlockReasonVC
            vc.sType = "events"
            vc.sID = "\(self.event.eventId ?? 0)"
            vc.modalPresentationStyle = .overCurrentContext
            mostTopViewController?.present(vc, animated: false, completion: nil)
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            }, completion: nil)
        }
        actionSheet.addAction(reportActionButton)
        
        let cancelActionButton = UIAlertAction(title: "キャンセル", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheet.addAction(cancelActionButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func btnRegister(_ sender: UIButton) {
        if event.eventIsRegistered ?? false {
            apiUnRegisterEvent(event.eventId ?? 0) { (_) in
                apiGetEventDetails("\(self.event.eventId ?? 0)", false) { (objEvent) in
                    guard let obj = objEvent else { return }
                    self.event.eventAttendeesCount = obj.eventAttendeesCount
                    self.event.eventAttendessAvatarUrls = obj.eventAttendessAvatarUrls
                    self.tblEventDetails.reloadData()
                }
            }
            event.eventIsRegistered = false
        } else {
            apiRegisterEvent(event.eventId ?? 0) { (_) in
                apiGetEventDetails("\(self.event.eventId ?? 0)", false) { (objEvent) in
                    guard let obj = objEvent else { return }
                    self.event.eventAttendeesCount = obj.eventAttendeesCount
                    self.event.eventAttendessAvatarUrls = obj.eventAttendessAvatarUrls
                    self.tblEventDetails.reloadData()
                }
            }
            event.eventIsRegistered = true
        }
        tblEventDetails.reloadData()
    }
    
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: tblEventDetails)
        if let indexPath = tblEventDetails.indexPathForRow(at: location) {
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
            if indexPath.section == 0 || indexPath.section == 1 {
                vc.sUserID = "\(self.event.userId ?? 0)"
            } else if indexPath.section == 3 {
                print(indexPath.row)
                let objComment = eventComments[indexPath.row]
                vc.sUserID = "\(objComment.userId ?? 0)"
            }
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- UITextFieldDelegate
extension EventsDetailsVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        addComment()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addComment()
        return true
    }
    
    func addComment() {
        let indexPath = IndexPath(row: 0, section: 2)
        if let cell = tblEventDetails.cellForRow(at: indexPath) as? CellAddCommentEvent {
            if TRIM(string: cell.txtComment.text ?? "").count > 0 {
                apiAddComment(cell.txtComment.text ?? "")
                cell.txtComment.text = ""
            }
        }
    }
}

//MARK:- API CALLING
extension EventsDetailsVC {
    func apiAddComment(_ text: String) {
        let dictComment: NSDictionary = ["comment":TRIM(string: text)]
        let dictParam: NSDictionary = ["comment":dictComment]
        
        let sURL = SERVER_URL + "events/\(event.eventId ?? 0)/" + API_ADD_COMMENT
        
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_ADD_COMMENT, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            print(responseObject ?? "")
            if error == nil {
                if let dictComment = responseObject?["comment"] as? NSDictionary {
                    let comment: EventComments = EventComments.init(object: dictComment)
                    self.eventComments.insert(comment, at: 0)
                    self.event.eventCommentsCount = (self.event.eventCommentsCount ?? 0) + 1
                    self.tblEventDetails.reloadData()
                }
            } else {
                print(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}
