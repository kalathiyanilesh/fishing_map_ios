//
//  EventSearchVC.swift
//  Fishing Map
//
//  Created by macOS on 11/11/20.
//

import UIKit

class CellEventSearchHeader: UITableViewCell {
    
}

class CellSearchedEvent: UITableViewCell {
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblPrefectureName: UILabel!
    
    func configureCell(event: Events) {
        imgEvent.sd_setImage(with: URL(string: event.eventImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblEventDate.text = intToDate(miliseconds: event.eventDate ?? 0, shortMonthName: true)
        lblEventName.text = event.eventTitle
        lblPrefectureName.text = event.eventPrefectureName
    }
}

class EventSearchVC: UIViewController {

    @IBOutlet weak var tblEventSearch: UITableView!
    @IBOutlet var txtSearch: UITextField!
    
    var arrEvents: [Events] = []
    var isSearch: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        apiGetTrendingEvents()
    }

    @IBAction func btnCloseTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension EventSearchVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return isSearch ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return isSearch ? arrEvents.count : 1
        default:
            return arrEvents.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if isSearch {
                let cell: CellSearchedEvent = tableView.dequeueReusableCell(withIdentifier: "CellSearchedEvent", for: indexPath) as! CellSearchedEvent
                cell.configureCell(event: self.arrEvents[indexPath.row])
                return cell
            }
            let cell: CellEventSearchHeader = tableView.dequeueReusableCell(withIdentifier: "CellEventSearchHeader", for: indexPath) as! CellEventSearchHeader
            return cell
        default:
            let cell: CellSearchedEvent = tableView.dequeueReusableCell(withIdentifier: "CellSearchedEvent", for: indexPath) as! CellSearchedEvent
            cell.configureCell(event: self.arrEvents[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && !isSearch { return }
        let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventsDetailsVC") as! EventsDetailsVC
        vc.event = arrEvents[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return isSearch ? 289 : 35
        default:
            return 289
        }
    }
}

//MARK:- API CALLING
extension EventSearchVC {
    func apiGetTrendingEvents() {
        showLoaderHUD(strMessage: "")
        let sURL = SERVER_URL + API_TRENDING_EVENT
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error) in
            hideLoaderHUD()
            print(response ?? "")
            let objEvents: FMEvents = FMEvents.init(object: response ?? "")
            self.arrEvents.removeAll()
            if objEvents.success ?? false {
                for event in objEvents.events ?? [] {
                    self.arrEvents.append(event)
                }
            } else {
                showMessage(objEvents.message ?? ErrorMSG)
            }
            self.tblEventSearch.reloadData()
        }
    }
    
    func apiSearchEvents() {
        isSearch = true
        showLoaderHUD(strMessage: "")
        let sURL = SERVER_URL + API_SEARCH_EVENT + "?term=\(TRIM(string: txtSearch.text ?? ""))"
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error) in
            hideLoaderHUD()
            print(response ?? "")
            let objEvents: FMEvents = FMEvents.init(object: response ?? "")
            self.arrEvents.removeAll()
            if objEvents.success ?? false {
                for event in objEvents.events ?? [] {
                    self.arrEvents.append(event)
                }
            } else {
                showMessage(objEvents.message ?? ErrorMSG)
            }
            self.tblEventSearch.reloadData()
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension EventSearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            self.apiSearchEvents()
        } else {
            isSearch = false
            apiGetTrendingEvents()
        }
    }
}
