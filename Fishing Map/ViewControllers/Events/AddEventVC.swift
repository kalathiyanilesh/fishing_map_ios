//
//  AddEventVC.swift
//  Fishing Map
//
//  Created by macOS on 11/11/20.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift

protocol delegateCreateEvent {
    func getNewEvent(_ event: Events)
}

class CellAddEvent: UITableViewCell {
    @IBOutlet weak var btnCellPost: UIButton!
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgCheckOcean: UIImageView!
    @IBOutlet weak var lblPrefecture: UILabel!
    @IBOutlet weak var btnUseCurrentLocation: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblImgName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtEventTitle: UITextField!
    @IBOutlet weak var txtEnterLink: UITextField!
    @IBOutlet weak var txtvEventAddress: IQTextView!
    @IBOutlet weak var txtvAddDescription: IQTextView!
}

class AddEventVC: UIViewController {

    @IBOutlet weak var tblAddEvent: UITableView!
    
    var delegateCreateEvent: delegateCreateEvent?
    var arrEvent = ["addPrefecture", "addEventTitle", "addEventAddress", "addEventLocation", "uploadImage", "addDate", "addDescription", "addRegistrationLink", "cellOceanCleaning"]
    
    var dictDetails = NSMutableDictionary()
    var prefectures = [Prefectures]()
    var isOceanChecked = false
    var prefectureIndex : Int? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPrefectures()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        dictDetails.setObject(dateFormat.string(from: Date()), forKey: "date" as NSCopying)
        tblAddEvent.reloadData()
        btnUseCurrentLocationTapped()
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnPostTapped(_ sender: UIButton) {
        print(dictDetails)
        getParameters { (dictParam, selectedImage) in
            showLoaderHUD(strMessage: "")
            UploadImageToCloud(selectedImage) { (imgurl) in
                dictParam.setObject(imgurl, forKey: "image_url" as NSCopying)
                self.apiCreateEvent(dictParam)
            }
        }
    }
}

extension AddEventVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEvent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellAddEvent = tableView.dequeueReusableCell(withIdentifier: arrEvent[indexPath.row], for: indexPath) as! CellAddEvent
        
        let sType = arrEvent[indexPath.row]
        switch sType {
        case "addPrefecture":
            if self.prefectureIndex != nil {
                cell.lblPrefecture.text = self.prefectures[prefectureIndex ?? 0].prefectureName
                cell.lblPrefecture.textColor = .black
            } else {
                cell.lblPrefecture.text = "都道府県の入力"
                cell.lblPrefecture.textColor = Color_Hex(hex: "#797A7B")
            }
        case "addEventTitle":
            cell.txtEventTitle.tag = 1
            cell.txtEventTitle.delegate = self
        case "addEventLocation":
            cell.lblLocation.text = "タップして場所を特定"
            if let address = dictDetails.object(forKey: "map_address") as? String {
                cell.lblLocation.text = address
            }
            cell.btnUseCurrentLocation.addTarget(self, action: #selector(btnUseCurrentLocationTapped), for: .touchUpInside)
        case "uploadImage":
            cell.lblImgName.text = "アップロード"
            if let sImageName = dictDetails.object(forKey: "imageName") as? String {
                cell.lblImgName.text = sImageName
            }
        case "addDate":
            cell.lblDate.text = "日付"
            if let sDate = dictDetails.object(forKey: "date") as? String {
                cell.lblDate.text = sDate
            }
        case "addEventAddress":
            cell.txtvEventAddress.tag = 1
            cell.txtvEventAddress.delegate = self
        case "addDescription":
            cell.txtvAddDescription.tag = 2
            cell.txtvAddDescription.delegate = self
        case "addRegistrationLink":
            cell.txtEnterLink.tag = 2
            cell.txtEnterLink.delegate = self
        default:
            break
        }
        if cell.btnCellPost != nil {
            cell.btnCellPost.addTarget(self, action: #selector(btnCellPost(_:)), for: .touchUpInside)
        }
        
        if cell.btnAddEvent != nil {
            cell.btnAddEvent.addTarget(self, action: #selector(btnCellPost(_:)), for: .touchUpInside)
        }
        if cell.btnCheckBox != nil {
            cell.btnCheckBox.addTarget(self, action: #selector(btnCheckBox(_:)), for: .touchUpInside)
            cell.imgCheckOcean.image = UIImage(named: isOceanChecked ? "icn_check" : "icn_uncheck")
            self.dictDetails.setObject(isOceanChecked, forKey: "ocean_cleaning" as NSCopying)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sPostType = arrEvent[indexPath.row]
        switch sPostType {
        case "addPrefecture", "addEventTitle", "addRegistrationLink":
            return 111
        case "addDescription", "addEventAddress":
            return 210
        case "addEventLocation":
            return UITableView.automaticDimension
        default:
            return 69
        }
    }
    
    @objc func btnCellPost(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblAddEvent)
        if let indexPath = tblAddEvent.indexPathForRow(at:buttonPosition) {
            let sPostType = arrEvent[indexPath.row]
            switch sPostType {
            case "addPrefecture":
                addPrefecture()
            case "addEventLocation":
                addEventLocation()
            case "uploadImage":
                ImagePickerManager().pickImage(self, false) { (img, sPath) in
                    self.dictDetails.setObject(sPath, forKey: "imageName" as NSCopying)
                    self.dictDetails.setObject(img, forKey: "image" as NSCopying)
                    self.tblAddEvent.reloadData()
                }
            case "addDate":
                addDate()
            default:
                break
            }
        }
    }
    
    @objc func btnUseCurrentLocationTapped() {
        self.view.endEditing(true)
        getAddress(lat_currnt, long_currnt) { (address) in
            self.dictDetails.setObject(address, forKey: "map_address" as NSCopying)
            self.dictDetails.setObject(lat_currnt, forKey: "lat" as NSCopying)
            self.dictDetails.setObject(long_currnt, forKey: "lng" as NSCopying)
            self.tblAddEvent.reloadData()
        }
    }
    
    @objc func btnCheckBox(_ sender: UIButton) {
        isOceanChecked = !isOceanChecked
        tblAddEvent.reloadData()
    }
}

//MARK:- API CALLING
extension AddEventVC {
    func apiCreateEvent(_ dictParam: NSDictionary) {
        self.view.endEditing(true)
        let dictPost: NSDictionary = ["event":dictParam]
        print("Feed Parameters: ", dictPost)
        let sURL = SERVER_URL + API_GET_EVENTS
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_GET_EVENTS, parameters: dictPost, method: .post, isPassHeaderToken: true, showLoader: false, isObjInString: false) { (error, responseObject) in
            hideLoaderHUD()
            if error == nil {
                print(responseObject ?? "")
                let objEvent: FMCreateEvent = FMCreateEvent.init(object: responseObject ?? "")
                if objEvent.success ?? false {
                    if let newEvent = objEvent.event {
                        showMessage("Event added successfully!")
                        self.delegateCreateEvent?.getNewEvent(newEvent)
                    }
                    self.navigationController?.popViewController(animated: true)
                } else {
                    showMessage(error?.localizedDescription ?? ErrorMSG)
                    self.removeImage(dictParam)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
                self.removeImage(dictParam)
            }
        }
    }
    
    func removeImage(_ dictParam: NSDictionary) {
        if let imgURL = dictParam.object(forKey: "image_url") as? String {
            deleteImage(imgURL)
        }
    }
}

// MARK:- UITEXTVIEW DELEGATE METHOD
extension AddEventVC: UITextFieldDelegate, UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if TRIM(string: textView.text ?? "").count > 0 {
            if textView.tag == 1 {
                dictDetails.setObject(TRIM(string: textView.text ?? ""), forKey: "address" as NSCopying)
            } else if textView.tag == 2 {
                dictDetails.setObject(TRIM(string: textView.text ?? ""), forKey: "description" as NSCopying)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            if textField.tag == 1 {
                dictDetails.setObject(TRIM(string: textField.text ?? ""), forKey: "title" as NSCopying)
            } else if textField.tag == 2 {
                dictDetails.setObject(TRIM(string: textField.text ?? ""), forKey: "registration_link" as NSCopying)
            }
        }
    }
}

extension AddEventVC {
    func getPrefectures() {
        let sURL = SERVER_URL + API_GET_PREFECTURES
        showLoaderHUD(strMessage: "")
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: BLANK_HEADER) { (response, error) in
            hideLoaderHUD()
            let objPrefectures: FMPrefecturesList = FMPrefecturesList.init(object: response ?? "")
            self.prefectures = objPrefectures.prefectures ?? []
        }
    }
    
    func addPrefecture() {
        if self.prefectures.count <= 0 { return }
        let alert = UIAlertController(style: .actionSheet, title: "都道府県の入力", message: nil)
        var arrPrefectures = [String]()
        for prefecture in self.prefectures {
            arrPrefectures.append(prefecture.prefectureName ?? "")
        }
        
        alert.addPickerView(values: [arrPrefectures], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    self.prefectureIndex = index.row
                    self.dictDetails.setObject(self.prefectures[index.row].prefectureId ?? 0, forKey: "prefecture_id" as NSCopying)
                    self.tblAddEvent.reloadData()
                }
            }
        }
        alert.addAction(title: "完了", style: .default) { action in
            if self.prefectureIndex == nil {
                self.prefectureIndex = 0
                self.dictDetails.setObject(self.prefectures[0].prefectureId ?? 0, forKey: "prefecture_id" as NSCopying)
                self.tblAddEvent.reloadData()
            } else {
                return
            }
        }
        alert.show(self)
    }
    
    func addDate() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        var sDate = dateFormat.string(from: Date())
        self.dictDetails.setObject(sDate, forKey: "date" as NSCopying)
        self.tblAddEvent.reloadData()
        
        let alert = UIAlertController(style: .actionSheet, title: "日付を選択")
        alert.addDatePicker(mode: .date, date: Date(), minimumDate: Date(), maximumDate: nil) { date in
            sDate = dateFormat.string(from: date)
            self.dictDetails.setObject(sDate, forKey: "date" as NSCopying)
            self.tblAddEvent.reloadData()
        }
        alert.addAction(title: "OK", style: .cancel)
        alert.show(self)
    }
}

extension AddEventVC: GMSAutocompleteViewControllerDelegate {
    func addEventLocation() {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        placePickerController.modalPresentationStyle = .fullScreen
        present(placePickerController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place address: \(place.formattedAddress ?? "")")
        dismiss(animated: true) {
            self.dictDetails.setObject(place.name ?? "", forKey: "map_address" as NSCopying)
            self.dictDetails.setObject(place.coordinate.latitude, forKey: "lat" as NSCopying)
            self.dictDetails.setObject(place.coordinate.longitude, forKey: "lng" as NSCopying)
            self.tblAddEvent.reloadData()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

//MARK: VALIDATION & CREATE PARAMETER
extension AddEventVC {
    func getParameters(handler:@escaping (_ dictData: NSMutableDictionary,_ image: UIImage) -> Void) {
        self.view.endEditing(true)
        var selectedImage = UIImage()
        let dictParam: NSMutableDictionary = NSMutableDictionary()
        
        if let prefectureId = dictDetails.object(forKey: "prefecture_id") as? Int {
            dictParam.setObject(prefectureId, forKey: "prefecture_id" as NSCopying)
        } else {
            showMessage("Please add prefecture")
            return
        }
        
        if let title = dictDetails.object(forKey: "title") as? String {
            dictParam.setObject(title, forKey: "title" as NSCopying)
        } else {
            showMessage("タイトルを入力してください")
            return
        }
        
        if let address = dictDetails.object(forKey: "address") as? String {
            dictParam.setObject(address, forKey: "address" as NSCopying)
        } else {
            showMessage("Please add address")
            return
        }
        
        if let mapAddress = dictDetails.object(forKey: "map_address") as? String {
            dictParam.setObject(mapAddress, forKey: "map_address" as NSCopying)
            dictParam.setObject("\(dictDetails.object(forKey: "lat") as? Double ?? 0)", forKey: "lat" as NSCopying)
            dictParam.setObject("\(dictDetails.object(forKey: "lng") as? Double ?? 0)", forKey: "lng" as NSCopying)
        } else {
            showMessage("場所を入力してください")
            return
        }
        
        if let image = dictDetails.object(forKey: "image") as? UIImage {
            selectedImage = image
        } else {
            showMessage("写真をアップロードしてください")
            return
        }
        
        if let date = dictDetails.object(forKey: "date") as? String {
            dictParam.setObject("\(convertStringToTimeStamp(date))", forKey: "date" as NSCopying)
        } else {
            showMessage("日付を入力してください")
            return
        }
        
        if let description = dictDetails.object(forKey: "description") as? String {
            dictParam.setObject(description, forKey: "description" as NSCopying)
        } else {
            showMessage("情報を入力してください")
            return
        }
        
        if let registerLink = dictDetails.object(forKey: "registration_link") as? String {
            dictParam.setObject(registerLink, forKey: "registration_link" as NSCopying)
        } else {
            showMessage("URLを入力してください")
            return
        }
        
        if let oceanCleaning = dictDetails.object(forKey: "ocean_cleaning") as? Bool {
            dictParam.setObject(oceanCleaning, forKey: "ocean_cleaning" as NSCopying)
        } else {
            dictParam.setObject(false, forKey: "ocean_cleaning" as NSCopying)
        }
        
        handler(dictParam, selectedImage)
    }
}
