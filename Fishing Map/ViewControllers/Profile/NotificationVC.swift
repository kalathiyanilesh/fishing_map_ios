//
//  NotificationVC.swift
//  Fishing Map
//
//  Created by iMac on 04/11/20.
//

import UIKit

class CellNotification: UITableViewCell {
    @IBOutlet var imgNotification: UIImageView!
    @IBOutlet var lblDesc: UILabel!
}

class NotificationVC: UIViewController {

    @IBOutlet var tblNotification: UITableView!
    
    var dictNotifications = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblNotification.estimatedRowHeight = 72
        tblNotification.rowHeight = UITableView.automaticDimension
        apiGetNotifications()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDictAllKeys() -> NSArray {
        return (dictNotifications.allKeys as NSArray).sorted(by: { ($0 as! String) < ($1 as! String) }) as NSArray
    }
}

//MARK:- UITableViewDelegate METHOD
extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let arrSection = getDictAllKeys()
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 36))
        headerView.backgroundColor = self.view.backgroundColor
        
        let labelDate = UILabel()
        labelDate.frame = CGRect.init(x: 26, y: 8, width: 150, height: 16)
        labelDate.font = FontWithSize("Interstate-Bold", 14)
        labelDate.textColor = UIColor.black
        labelDate.backgroundColor = self.view.backgroundColor
        let sSectionName = getDictAllKeys()[section] as? String
        if sSectionName == "Today" {
            //"Today"
            labelDate.text = "今日"
        } else {
            //"Older"
            labelDate.text = "古い"
        }
        
        headerView.addSubview(labelDate)
        
        /*
        let btnClear = UIButton()
        btnClear.frame = CGRect.init(x: SCREENWIDTH()-(26+40), y: 8, width: 40, height: 16)
        let clearAttributes : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : FontWithSize("Interstate-Regular", 12),
            NSAttributedString.Key.foregroundColor : Color_Hex(hex: "056989"),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Clear", attributes: clearAttributes)
        btnClear.setAttributedTitle(attributeString, for: .normal)
        btnClear.contentHorizontalAlignment = .right
        headerView.addSubview(btnClear)*/
 
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrNotification: NSArray = dictNotifications.object(forKey: getDictAllKeys()[section]) as! NSArray
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellNotification = tableView.dequeueReusableCell(withIdentifier: "CellNotification", for: indexPath) as! CellNotification
        let arrNotification: NSArray = dictNotifications.object(forKey: getDictAllKeys()[indexPath.section]) as! NSArray
        let objNotification = arrNotification[indexPath.row] as! Notifications
        cell.imgNotification.sd_setImage(with: URL(string: objNotification.notificationObjImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        cell.lblDesc.text = "\(objNotification.notificationActorName ?? "") \(objNotification.notificationText ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arrNotification: NSArray = dictNotifications.object(forKey: getDictAllKeys()[indexPath.section]) as! NSArray
        let objNot = arrNotification[indexPath.row] as! Notifications
                
        switch objNot.notificationNotifyType {
        case NOTIFICATION_TYPE.GENERIC.rawValue:
            let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "TextVC") as! TextVC
            vc.sText = objNot.notificationText ?? ""
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case NOTIFICATION_TYPE.EVENT.rawValue:
            apiGetEventDetails(objNot.notificationObjId ?? "") { (objEvent) in
                let vc = loadVC(strStoryboardId: SB_Events, strVCId: "EventsDetailsVC") as! EventsDetailsVC
                vc.event = objEvent
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case NOTIFICATION_TYPE.POST.rawValue:
            apiGetFeedPost(objNot.notificationObjId ?? "") { (objPost) in
                let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
                vc.post = objPost
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case NOTIFICATION_TYPE.LOCATION.rawValue:
            print("location")
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
            vc.imgType = getPushImageType(objNot.notificationObjClass ?? "")
            vc.sLocationID = objNot.notificationObjId ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        case NOTIFICATION_TYPE.IMAGE.rawValue:
            print("image")
            let imageType = getPushImageType(objNot.notificationObjClass ?? "")
            switch imageType {
            case .Fish, .TopTerrain, .TopScene, .ParkingSpots:
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                vc.imageType = imageType
                vc.sImageID = objNot.notificationObjId ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            case .Restaurant, .Hotels:
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
                vc.imageType = imageType
                vc.sImageID = objNot.notificationObjId ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- API CALLING
extension NotificationVC {
    func apiGetNotifications() {
        let sURL = SERVER_URL + "users/\(getUserID())/" + API_NOTIFICATION
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_NOTIFICATION, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: true) { (error, response) in
            print(response ?? "")
            if error == nil {
                let objData: FMNotification = FMNotification.init(object: response ?? "")
                if objData.success == "1" {
                    if objData.notifications?.count ?? 0 > 0 {
                        self.apiReadNotifications()
                    }
                    self.dictNotifications = NSMutableDictionary()
                    for objNotification in objData.notifications ?? [] {
                        var arrNotifications = NSMutableArray()
                        var sDate = "Older"
                        if objNotification.isCurrentDate == "1" {
                            sDate = "Today"
                        }
                        if (self.dictNotifications.object(forKey: sDate) != nil) {
                            arrNotifications = (self.dictNotifications.object(forKey: sDate) as! NSArray).mutableCopy() as! NSMutableArray
                            arrNotifications.add(objNotification)
                        } else {
                            arrNotifications.add(objNotification)
                        }
                        self.dictNotifications.setObject(arrNotifications.copy(), forKey: sDate as NSCopying)
                    }
                    self.tblNotification.reloadData()
                } else {
                    showMessage(objData.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiReadNotifications() {
        let sURL = SERVER_URL + "users/\(getUserID())/" + API_READ_NOTIFICATION
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_READ_NOTIFICATION, parameters: NSDictionary(), method: .post, isPassHeaderToken: true, showLoader: false) { (error, response) in
            print(response ?? "")
        }
    }
}
