//
//  ProfileVC.swift
//  Fishing Map
//
//  Created by macOS on 03/11/20.
//

import UIKit

class CellProfile: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnViewAll: UIButton!
    @IBOutlet var collPosts: UICollectionView!
    @IBOutlet var lblNoRecordsFound: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collPosts.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
    }
}

extension CellProfile {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collPosts.delegate = dataSourceDelegate
        collPosts.dataSource = dataSourceDelegate
        collPosts.tag = row
        collPosts.setContentOffset(collPosts.contentOffset, animated:false)
        collPosts.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collPosts.contentOffset.x = newValue }
        get { return collPosts.contentOffset.x }
    }
}

class ProfileVC: UIViewController {

    @IBOutlet var tblProfile: UITableView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblCountry: UILabel!
    @IBOutlet var btnMore: UIButton!
    @IBOutlet var btnEditProfile: UIButton!
    @IBOutlet var btnNotification: UIButton!
    @IBOutlet var lblNotificationCount: UILabel!
    @IBOutlet var lblTotalPosts: UILabel!
    @IBOutlet var lblTotalLikes: UILabel!
    @IBOutlet var lblTotalImages: UILabel!
    
    var storedOffsets = [Int: CGFloat]()
    
    var isFromSideMenu: Bool = false
    var objOtherUser: User?
    var sUserID: String?
    
    var arrPosts: [Posts] = []
    var arrAllImages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblUserName.text = objOtherUser?.userName
        lblCountry.text = objOtherUser?.userAddress
        btnEditProfile.isHidden = true
        btnNotification.isHidden = true
        lblNotificationCount.isHidden = true
        btnMore.isHidden = true
        
        if isFromSideMenu {
            objOtherUser = APP_DELEGATE.objUser
            sUserID = objOtherUser?.userId
        } else {
            apiUserInfo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFromSideMenu {
            apiGetUserInfo { (isSuccess) in
                self.objOtherUser = APP_DELEGATE.objUser
                self.setUserInfo()
            }
        } else {
            setUserInfo()
        }
    }
    
    func setUserInfo() {
        if objOtherUser == nil { return }
        imgUser.sd_setImage(with: URL(string: objOtherUser?.userAvatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblUserName.text = objOtherUser?.userName
        lblCountry.text = objOtherUser?.userAddress
        
        lblTotalPosts.text = objOtherUser?.postsCount
        lblTotalLikes.text = objOtherUser?.likesCount
        lblTotalImages.text = objOtherUser?.imagesCount
        if lblTotalPosts.text?.count ?? 0 <= 0 { lblTotalPosts.text = "0" }
        if lblTotalLikes.text?.count ?? 0 <= 0 { lblTotalLikes.text = "0" }
        if lblTotalImages.text?.count ?? 0 <= 0 { lblTotalImages.text = "0" }
        
        if objOtherUser?.isCurrentUser == "1" {
            btnEditProfile.isHidden = false
            btnNotification.isHidden = false
            lblNotificationCount.isHidden = false
            btnMore.isHidden = true
        } else {
            btnEditProfile.isHidden = true
            btnNotification.isHidden = true
            lblNotificationCount.isHidden = true
            btnMore.isHidden = false
        }
        
        lblNotificationCount.text = objOtherUser?.unreadNotificationsCount
        if objOtherUser?.hasUnreadNotifications != "1" {
            lblNotificationCount.isHidden = true
        }
        
        apiGetUserPost()
        apiGetUserImages()
    }
    
    //MARK:- UIBUTTON ACTION
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditProfile(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "EditProfileVC") as! EditProfileVC
        vc.delegateEditProfile = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNotification(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "NotificationVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnMore(_ sender: Any) {
        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let blockActionButton = UIAlertAction(title: "ブロック", style: .default)
        { _ in
            print("Block")
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "BlockReasonVC") as! BlockReasonVC
            vc.isBlock = true
            vc.sType = "users"
            vc.sID = self.sUserID ?? "0"
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: false, completion: nil)
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            }, completion: nil)
        }
        actionSheet.addAction(blockActionButton)
        
        
        let reportActionButton = UIAlertAction(title: "報告書", style: .default)
        { _ in
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "BlockReasonVC") as! BlockReasonVC
            vc.sType = "users"
            vc.sID = self.sUserID ?? "0"
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: false, completion: nil)
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            }, completion: nil)
        }
        actionSheet.addAction(reportActionButton)
        
        let cancelActionButton = UIAlertAction(title: "キャンセル", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheet.addAction(cancelActionButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
}

//MARK:- UITableViewDelegate METHOD
extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellProfile = tableView.dequeueReusableCell(withIdentifier: "CellProfile", for: indexPath) as! CellProfile
        switch indexPath.row {
        case 0:
            cell.lblTitle.text = "自分の投稿"
            cell.lblNoRecordsFound.alpha = arrPosts.count > 0 ? 0 : 1
        default:
            cell.lblTitle.text = "アップロード"
            cell.lblNoRecordsFound.alpha = arrAllImages.count > 0 ? 0 : 1
        }
        cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let profileCell = cell as? CellProfile else { return }
            profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 236
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            guard let profileCell = cell as? CellProfile else { return }
            storedOffsets[indexPath.row] = profileCell.collectionViewOffset
        default:
            break
        }
    }
    
    @objc func btnViewAll(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblProfile)
        let indexPath: IndexPath = tblProfile.indexPathForRow(at: buttonPosition)!
        
        switch indexPath.row {
        case 0:
            let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
            vc.isShowPosts = true
            vc.arrPosts = arrPosts
            vc.sUserID = sUserID
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
            vc.isProfileImages = true
            vc.arrAllImages = arrAllImages
            vc.sUserID = sUserID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

//MARK:- UICollectionViewDelegate METHOD
extension ProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return arrPosts.count
        default:
            return arrAllImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.yImageName.constant = 0
        cell.heightImageName.constant = 0
        cell.heightImgFish.constant = 120
        switch collectionView.tag {
        case 0:
            let objPost = arrPosts[indexPath.row]
            cell.imgPost.sd_setImage(with: URL(string: objPost.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTitle.text = ""
            cell.lblTotalLikes.text = "\(objPost.postLikesCount ?? 0)"
            cell.lblTotalComments.text = "\(objPost.postCommentsCount ?? 0)"
            if objPost.postIsLiked ?? false {
                cell.imgLike.image = UIImage(named: "ic_like")
            } else {
                cell.imgLike.image = UIImage(named: "ic_like_gray")
            }
            cell.btnLikes.tag = indexPath.row
            cell.btnLikes.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
            cell.viewComment.isHidden = false
        default:
            let objImg = arrAllImages.object(at: indexPath.row) as? ProfileImages
            cell.imgPost.sd_setImage(with: URL(string: objImg?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTitle.text = ""//objImg?.imageName
            cell.lblTotalLikes.text = objImg?.imageLikesCount
            cell.viewComment.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        switch collectionView.tag {
        case 0:
            let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
            vc.post = arrPosts[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            let objImg = arrAllImages.object(at: indexPath.row) as? ProfileImages
            let sType = objImg?.imageType
            print("imageType", sType ?? "")
            switch sType {
            case "fish","terrain":
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                vc.imageType = (sType == "fish") ? .Fish : .TopTerrain
                vc.sImageID = objImg?.imageId ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
            case "scene":
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                vc.imageType = .TopScene
                vc.sImageID = objImg?.imageId ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
            case "restaurant","hotel":
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
                vc.imageType = (sType == "restaurant") ? .Restaurant : .Hotels
                vc.sImageID = objImg?.imageId ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
            case "parking":
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                vc.imageType = .ParkingSpots
                vc.sImageID = objImg?.imageId ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                return
            }
            
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:148, height:170)
    }
    
    @objc func btnLike(_ sender: UIButton) {
        let index = sender.tag
        let post: Posts = arrPosts[index]
        if post.postIsLiked ?? false {
            apiUnLikePost(post.postId ?? 0, isUnlikeEvent: false)
            post.postIsLiked = false
            
            post.postLikesCount = (post.postLikesCount ?? 0) - 1
            if post.postLikesCount ?? 0 < 0 {
                post.postLikesCount = 0
            }
        } else {
            apiLikePost(post.postId ?? 0, isLikeEvent: false)
            post.postIsLiked = true
            post.postLikesCount = (post.postLikesCount ?? 0) + 1
        }
        tblProfile.reloadData()
    }
}

//MARK:- API CALLING
extension ProfileVC: delegateEditProfile {
    func apiUserInfo() {
        let sURL = SERVER_URL + API_USER + "/\(sUserID ?? "0")"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_USER, parameters: NSDictionary(), method: .get, isPassHeaderToken: true , showLoader: true) { (error, responseObject) in
            print(responseObject ?? "")
            if error == nil {
                let obj: FMUser = FMUser.init(object: responseObject ?? "")
                if obj.success == "1" {
                    self.objOtherUser = obj.user
                    self.setUserInfo()
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiGetUserPost() {
        let sURL = SERVER_URL + API_USER + "/\(sUserID ?? "0")/" + API_USER_POST
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error)  in
            self.arrPosts.removeAll()
            let objFeedPosts: FMFeedPosts = FMFeedPosts.init(object: response ?? "")
            for post in objFeedPosts.posts ?? [] {
                self.arrPosts.append(post)
            }
            self.tblProfile.reloadData()
        }
    }
    
    func apiGetUserImages() {
        let sURL = SERVER_URL + API_USER + "/\(sUserID ?? "0")/" + API_USER_IMAGES
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_USER_IMAGES, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            print(responseObject ?? "")
            if error == nil {
                self.arrAllImages.removeAllObjects()
                let objStore: FMProfileUpload = FMProfileUpload.init(object: responseObject ?? "")
                if objStore.success == "1" {
                    for objProImage in objStore.images ?? [] {
                        self.arrAllImages.add(objProImage)
                    }
                }
                self.tblProfile.reloadData()
            }
        }
    }
    
    func editProfile() {
        apiGetUserInfo { (isSuccess) in
            if isSuccess {
                self.objOtherUser = APP_DELEGATE.objUser
                self.setUserInfo()
            }
        }
    }
}
