//
//  EditProfileVC.swift
//  Fishing Map
//
//  Created by iMac on 04/11/20.
//

import UIKit
import IQKeyboardManagerSwift

protocol delegateEditProfile {
    func editProfile()
}

class EditProfileVC: UIViewController {

    @IBOutlet var imgProPic: UIImageView!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtPrefecture: UITextField!
    @IBOutlet var txtAddress: IQTextView!
    @IBOutlet var txtEmailID: UITextField!
    
    var delegateEditProfile: delegateEditProfile?
    var prefectures = [Prefectures]()
    var prefectureId: Int?
    var isSelectImage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiGetPrefectures()
        
        txtPrefecture.delegate = self

        imgProPic.sd_setImage(with: URL(string: APP_DELEGATE.objUser?.userAvatarUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        txtName.text = APP_DELEGATE.objUser?.userName
        txtPrefecture.text = APP_DELEGATE.objUser?.userPrefectureName
        prefectureId = Int(APP_DELEGATE.objUser?.userPrefectureId ?? "0")
        txtAddress.text = APP_DELEGATE.objUser?.userAddress
        txtEmailID.text = APP_DELEGATE.objUser?.userEmail
        txtEmailID.isEnabled = false
        
        /*
        if txtAddress.text.count <= 0 {
            getAddress(lat_currnt, long_currnt) { (sAddress) in
                self.txtAddress.text = sAddress
            }
        }*/
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        ImagePickerManager().pickImage(self, true) { (image, imgPath) in
            self.isSelectImage = true
            self.imgProPic.image = image
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        // && validateTxtViewLength(txtAddress, withMessage: "住所を入力してください")
        //&& validateTxtFieldLength(txtPrefecture, withMessage: "地域を選択してください")
        if validateTxtFieldLength(txtName, withMessage: "名前を入力してください")  {
            
            self.view.endEditing(true)
            let dictData: NSMutableDictionary = ["name":txtName.text ?? "", "prefecture_id": prefectureId ?? 0, "address": txtAddress.text ?? ""]
            if isSelectImage {
                showLoaderHUD(strMessage: "")
                UploadImageToCloud(imgProPic.image!) { (imgurl) in
                    dictData.setObject(imgurl, forKey: "avatar_url" as NSCopying)
                    let dictUser: NSDictionary = ["user": dictData.copy()]
                    self.apiEditProfile(dictUser, false)
                }
            } else {
                let dictUser: NSDictionary = ["user": dictData.copy()]
                apiEditProfile(dictUser, true)
            }
        }
    }
}

//MARK:- API CALLING
extension EditProfileVC {
    func apiGetPrefectures() {
        let sURL = SERVER_URL + API_GET_PREFECTURES
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: BLANK_HEADER) { (response, error) in
            print(response ?? "")
            let objPrefectures: FMPrefecturesList = FMPrefecturesList.init(object: response ?? "")
            self.prefectures = objPrefectures.prefectures ?? []
        }
    }
    
    func apiEditProfile(_ dictParam: NSDictionary, _ showLoader: Bool) {
        let sURL = SERVER_URL + API_USER + "/\(getUserID())/" + API_EDIT_PROFILE
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_EDIT_PROFILE, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: showLoader) { (error, responseObject) in
            print(responseObject ?? "")
            hideLoaderHUD()
            if error == nil {
                self.delegateEditProfile?.editProfile()
                self.navigationController?.popViewController(animated: true)
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension EditProfileVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        addPrefecture()
        return false
    }
    
    func addPrefecture() {
        if prefectures.count <= 0 { return }
        var indexSelected = 0
        let alert = UIAlertController(style: .actionSheet, title: "都道府県の入力", message: nil)
        var arrPrefectures = [String]()
        for prefecture in self.prefectures {
            arrPrefectures.append(prefecture.prefectureName ?? "")
        }
        
        alert.addPickerView(values: [arrPrefectures], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    indexSelected = index.row
                }
            }
        }
        alert.addAction(title: "完了", style: .default) { action in
            self.txtPrefecture.text = self.prefectures[indexSelected].prefectureName
            self.prefectureId = self.prefectures[indexSelected].prefectureId
        }
        alert.show(self)
    }
}
