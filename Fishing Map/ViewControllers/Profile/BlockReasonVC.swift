//
//  BlockReasonVC.swift
//  Fishing Map
//
//  Created by iMac on 02/04/21.
//

import UIKit
import IQKeyboardManagerSwift

class BlockReasonVC: UIViewController {

    @IBOutlet var txtReason: IQTextView!
    var isBlock: Bool = false
    var sType = ""
    var sID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
        }, completion: nil)
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        if validateTxtViewLength(txtReason, withMessage: "理由を入力してください") {
            apiReport()
        }
        /*
        self.dismiss(animated: false)
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
        }, completion: nil)*/
    }
    
    func apiReport() {
        let dictParam: NSDictionary = ["reason": TRIM(string: txtReason.text ?? "")]
        let sURL = SERVER_URL + "\(sType)/\(sID)/\(API_Report)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_Report, parameters: dictParam, method: .post, isPassHeaderToken: true , showLoader: true) { (error, responseObject) in
            print(responseObject ?? "")
            if error == nil {
                showMessage(self.isBlock ? "ブロックされた" : "報告")
                var index = 0
                if self.sType == "locations" {
                    index = 1
                } else if self.sType == "events" {
                    index = 3
                }
                gotoTabBar(index)
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}
