//
//  CompleteProfileVC.swift
//  Fishing Map
//
//  Created by iMac on 23/12/20.
//

import UIKit
import IQKeyboardManagerSwift

class CompleteProfileVC: UIViewController {

    @IBOutlet var imgProPic: UIImageView!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtPrefecture: UITextField!
    @IBOutlet var txtAddress: IQTextView!
    @IBOutlet var txtEmailID: UITextField!
    
    var prefectures = [Prefectures]()
    var prefectureId: Int?
    var isSelectImage: Bool = false
    var isNormalSignIn: Bool = false
    
    var dictParam = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        apiGetPrefectures()
        txtPrefecture.delegate = self
        /*
        if txtAddress.text.count <= 0 {
            getAddress(lat_currnt, long_currnt) { (sAddress) in
                self.txtAddress.text = sAddress
            }
        }*/
        print("Param :", dictParam)
        
        if let sName = dictParam.object(forKey: "name") as? String {
            txtName.text = sName
        }
        if let sEmail = dictParam.object(forKey: "email") as? String {
            txtEmailID.text = sEmail
        }
        if let sAvatarUrl = dictParam.object(forKey: "avatar_url") as? String {
            imgProPic.sd_setImage(with: URL(string: sAvatarUrl), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        }
        if TRIM(string: txtEmailID.text ?? "").count > 0 {
            txtEmailID.isEnabled = false
        }
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        ImagePickerManager().pickImage(self, true) { (image, imgPath) in
            self.isSelectImage = true
            self.imgProPic.image = image
        }
    }
    
    @IBAction func btnSignIn(_ sender: Any) {
        //&& validateTxtViewLength(txtAddress, withMessage: "住所を入力してください")
        //&& validateTxtFieldLength(txtPrefecture, withMessage: "地域を選択してください")
        if validateTxtFieldLength(txtName, withMessage: "名前を入力してください") && validateTxtFieldLength(txtEmailID, withMessage: "メールアドレスを入力してください") && validateEmailAddress(txtEmailID, withMessage: "有効なメールアドレスを入力してください") {
            self.view.endEditing(true)
            dictParam.setObject(txtName.text ?? "", forKey: "name" as NSCopying)
            dictParam.setObject(txtEmailID.text ?? "", forKey: "email" as NSCopying)
            dictParam.setObject(prefectureId ?? 0, forKey: "prefecture_id" as NSCopying)
            dictParam.setObject(txtAddress.text ?? "", forKey: "address" as NSCopying)
            
            let dictToken: NSDictionary = ["token":getDeviceToken(), "device_type":DeviceType]
            let arrToken: NSArray = NSArray.init(object: dictToken)
            dictParam.setObject(arrToken, forKey: "user_device_tokens_attributes" as NSCopying)

            if isSelectImage {
                showLoaderHUD(strMessage: "")
                UploadImageToCloud(imgProPic.image!) { (imgurl) in
                    self.dictParam.setObject(imgurl, forKey: "avatar_url" as NSCopying)
                    let dictUser: NSDictionary = ["user": self.dictParam.copy()]
                    if self.isNormalSignIn {
                        self.apiSingIn(dictUser, false)
                    } else {
                        self.apiSocial(dictUser, false)
                    }
                }
            } else {
                if isNormalSignIn {
                    self.dictParam.setObject("", forKey: "avatar_url" as NSCopying)
                }
                let dictUser: NSDictionary = ["user": self.dictParam.copy()]
                if self.isNormalSignIn {
                    self.apiSingIn(dictUser, true)
                } else {
                    self.apiSocial(dictUser, true)
                }
            }
        }
    }
}

//MARK:- API CALLING
extension CompleteProfileVC {
    func apiSocial(_ dictParam: NSDictionary, _ showLoader: Bool) {
        self.view.endEditing(true)
        let sURL = SERVER_URL + API_SOCIAL
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_SOCIAL, parameters: dictParam, method: .post, isPassHeaderToken: false, showLoader: showLoader) { (error, responseObject) in
            hideLoaderHUD()
            print(responseObject ?? "")
            if error == nil {
                let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                if obj.success == "1" {
                    UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                    if let dictUser = responseObject?["user"] as? NSDictionary {
                        saveUserData(obj.user, dictUser)
                    }
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "SignInConfirmVC")
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiSingIn(_ dictParam: NSDictionary, _ showLoader: Bool) {
        self.view.endEditing(true)
        let sURL = SERVER_URL + API_SIGNIN
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_SIGNIN, parameters: dictParam, method: .post, isPassHeaderToken: false, showLoader: showLoader) { (error, responseObject) in
            hideLoaderHUD()
            print(responseObject ?? "")
            if error == nil {
                let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                if obj.success == "1" {
                    UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                    if let dictUser = responseObject?["user"] as? NSDictionary {
                        saveUserData(obj.user, dictUser)
                    }
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "SignInConfirmVC")
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiGetPrefectures() {
        let sURL = SERVER_URL + API_GET_PREFECTURES
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: BLANK_HEADER) { (response, error) in
            print(response ?? "")
            let objPrefectures: FMPrefecturesList = FMPrefecturesList.init(object: response ?? "")
            self.prefectures = objPrefectures.prefectures ?? []
            if self.prefectures.count > 0 {
                self.txtPrefecture.text = self.prefectures[0].prefectureName
                self.prefectureId = self.prefectures[0].prefectureId
            }
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension CompleteProfileVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        addPrefecture()
        return false
    }
    
    func addPrefecture() {
        if prefectures.count <= 0 { return }
        var indexSelected = 0
        let alert = UIAlertController(style: .actionSheet, title: "都道府県の入力", message: nil)
        var arrPrefectures = [String]()
        for prefecture in self.prefectures {
            arrPrefectures.append(prefecture.prefectureName ?? "")
        }
        
        alert.addPickerView(values: [arrPrefectures], initialSelection: (column: 0, row: 0)) { vc, picker, index, values in
            DispatchQueue.main.async {
                UIView.animate(withDuration: 1) {
                    indexSelected = index.row
                }
            }
        }
        alert.addAction(title: "完了", style: .default) { action in
            self.txtPrefecture.text = self.prefectures[indexSelected].prefectureName
            self.prefectureId = self.prefectures[indexSelected].prefectureId
        }
        alert.show(self)
    }
}
