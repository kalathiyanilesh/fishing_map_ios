//
//  LoginVC.swift
//  Fishing Map
//
//  Created by iMac on 18/12/20.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class LoginVC: UIViewController {

    @IBOutlet var txtEmailAdd: UITextField!
    @IBOutlet var txtPassword: UITextField!
    var prefectureId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiGetPrefectures()
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnLoginWithFB(_ sender: Any) {
        FacebookLogin()
    }
    
    @IBAction func btnLoginWithGoogle(_ sender: Any) {
        GoogleLogin()
    }
    
    @available(iOS 13.0, *)
    @IBAction func btnLoginWithApple(_ sender: Any) {
        AppleLogin()
    }
    
    @IBAction func btnSignIn(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "SignInVC") as! SignInVC
        vc.prefectureId = prefectureId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        if validateTxtFieldLength(txtEmailAdd, withMessage: "メールアドレスを入力してください") && validateEmailAddress(txtEmailAdd, withMessage: "有効なメールアドレスを入力してください") && validateTxtFieldLength(txtPassword, withMessage: "パスワードを入力してください") && validatePasswordLength(txtPassword, withMessage: "パスワードは6以上の文字を使ってください") {
            apiLogin()
        }
    }
    
    @IBAction func btnTerms(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "TermsVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API CALLING
extension LoginVC {
    func apiLogin() {
        self.view.endEditing(true)
        let dictToken: NSDictionary = ["token":getDeviceToken(), "device_type":DeviceType]
        let arrToken: NSArray = NSArray.init(object: dictToken)
        let dictParam: NSDictionary = ["email":txtEmailAdd.text ?? "", "password": txtPassword.text ?? "", "user_device_tokens_attributes":arrToken]
        let dictUser: NSDictionary = ["user": dictParam]
        let sURL = SERVER_URL + API_LOGIN
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOGIN, parameters: dictUser, method: .post, isPassHeaderToken: false, showLoader: true) { (error, responseObject) in
            print(responseObject ?? "")
            if error == nil {
                let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                if obj.success == "1" {
                    UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                    if let dictUser = responseObject?["user"] as? NSDictionary {
                        saveUserData(obj.user, dictUser)
                    }
                    let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}


//MARK:- FACEBOOK LOGIN
extension LoginVC {
    func FacebookLogin() {
        self.view.endEditing(true)
        if AccessToken.current == nil {
            let fbLoginManager : LoginManager = LoginManager()
            fbLoginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { (loginResult) -> Void in
                switch loginResult {
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print("\(grantedPermissions) \(declinedPermissions) \(accessToken)")
                    self.getFBUserInfo()
                case .failed(let error):
                    showMessage(error.localizedDescription)
                case .cancelled:
                    break
                }
            }
        } else {
            self.getFBUserInfo()
        }
    }
    
    func getFBUserInfo() {
        showLoaderHUD(strMessage: "")
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            hideLoaderHUD()
            if error == nil {
                if let dictResult = result as? NSDictionary {
                    print(dictResult)
                    let userId = dictResult.object(forKey: "id") as? String ?? ""
                    let givenName = dictResult.object(forKey: "first_name") as? String ?? ""
                    var email = ""
                    if let sEmail = dictResult.object(forKey: "email") as? String {
                        email = sEmail
                    }
                    let strProfilePic = String(format:"https://graph.facebook.com/%@/picture?type=large",userId)
                    
                    let dictToken: NSDictionary = ["token":getDeviceToken(), "device_type":DeviceType]
                    let arrToken: NSArray = NSArray.init(object: dictToken)
                    let dictParam: NSDictionary = ["auth_type": "provider", "provider": "facebook", "provider_id": userId , "email": email, "user_device_tokens_attributes":arrToken]
                    apiCheckRegistered(dictParam) { (isRegistered, responseObject) in
                        if isRegistered {
                            let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                            if obj.success == "1" {
                                UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                                if let dictUser = responseObject?["user"] as? NSDictionary {
                                    saveUserData(obj.user, dictUser)
                                }
                                let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                showMessage(obj.message ?? ErrorMSG)
                            }
                        } else {
                            let dictParam: NSMutableDictionary = ["access_token": AccessToken.current?.tokenString ?? "", "provider": "facebook", "provider_id": userId , "name": givenName , "avatar_url": strProfilePic, "email": email]
                            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "CompleteProfileVC") as! CompleteProfileVC
                            vc.dictParam = dictParam
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                } else {
                    showMessage("Facebook data not found for this user")
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        })
    }
}

//MARK:- GOOGLE LOGIN
extension LoginVC: GIDSignInDelegate {
    func GoogleLogin() {
        self.view.endEditing(true)
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        hideLoaderHUD()
        if let error = error {
            print("\(error.localizedDescription)")
            //showMessage(error.localizedDescription)
        } else {
            var strProfilePic = ""
            if user.profile.hasImage {
                strProfilePic = "\(user.profile.imageURL(withDimension: 200)!)"
            }
            let userId = user.userID
            let idToken = user.authentication.idToken
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            print("userId : ", userId ?? "")
            print("idToken : ", idToken ?? "")
            print("fullName : ", fullName ?? "")
            print("givenName : ", givenName ?? "")
            print("familyName : ", familyName ?? "")
            print("email : ", email ?? "")
            print("strProfilePic : ", strProfilePic)

            let dictToken: NSDictionary = ["token":getDeviceToken(), "device_type":DeviceType]
            let arrToken: NSArray = NSArray.init(object: dictToken)
            let dictParam: NSDictionary = ["auth_type": "provider", "provider": "google", "provider_id": userId ?? "" , "email": email ?? "", "user_device_tokens_attributes":arrToken]
            apiCheckRegistered(dictParam) { (isRegistered, responseObject) in
                if isRegistered {
                    let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                    if obj.success == "1" {
                        UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                        if let dictUser = responseObject?["user"] as? NSDictionary {
                            saveUserData(obj.user, dictUser)
                        }
                        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        showMessage(obj.message ?? ErrorMSG)
                    }
                } else {
                    let dictParam: NSMutableDictionary = ["access_token": idToken ?? "", "provider": "google", "provider_id": userId ?? "", "name": givenName ?? "", "avatar_url": strProfilePic, "email": email ?? ""]
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "CompleteProfileVC") as! CompleteProfileVC
                    vc.dictParam = dictParam
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Error:",error.localizedDescription)
        hideLoaderHUD()
    }
}

//MARK:- APPLE LOGIN
extension LoginVC : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func AppleLogin() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
        
            let userIdentifier = appleIDCredential.user
            
            let firstName = appleIDCredential.fullName?.familyName ?? ""
            let lastName = appleIDCredential.fullName?.givenName ?? ""
            
            let email = KeychainManager.sharedInstance.getValueFromKeychain("\(userIdentifier)_appleemail", appleIDCredential.email ?? "")
            let givenName = KeychainManager.sharedInstance.getValueFromKeychain("\(userIdentifier)_applename", TRIM(string: "\(firstName) \(lastName)"))
            
            print("Name: ", givenName)
            print("User Identifier: ", userIdentifier)
            print("ID Token String: ", idTokenString)
            print("Email: ", email)

            let dictToken: NSDictionary = ["token":getDeviceToken(), "device_type":DeviceType]
            let arrToken: NSArray = NSArray.init(object: dictToken)
            let dictParam: NSDictionary = ["auth_type": "provider", "provider": "apple", "provider_id": userIdentifier , "email": email, "user_device_tokens_attributes":arrToken]
            apiCheckRegistered(dictParam) { (isRegistered, responseObject) in
                if isRegistered {
                    let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                    if obj.success == "1" {
                        UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                        if let dictUser = responseObject?["user"] as? NSDictionary {
                            saveUserData(obj.user, dictUser)
                        }
                        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        showMessage(obj.message ?? ErrorMSG)
                    }
                } else {
                    
                    let dict: NSMutableDictionary = NSMutableDictionary()
                    dict.setObject(idTokenString, forKey: "access_token" as NSCopying)
                    dict.setObject("apple", forKey: "provider" as NSCopying)
                    dict.setObject("userIdentifier", forKey: "provider_id" as NSCopying)
                    dict.setObject("", forKey: "avatar_url" as NSCopying)
                    dict.setObject(givenName, forKey: "name" as NSCopying)
                    dict.setObject(email, forKey: "email" as NSCopying)
                    dict.setObject(self.prefectureId ?? 0, forKey: "prefecture_id" as NSCopying)
                    dict.setObject("", forKey: "address" as NSCopying)
                    let dictToken: NSDictionary = ["token":getDeviceToken(), "device_type":DeviceType]
                    let arrToken: NSArray = NSArray.init(object: dictToken)
                    dict.setObject(arrToken, forKey: "user_device_tokens_attributes" as NSCopying)
                    
                    DispatchQueue.main.async {
                        self.apiSocial(dict, true)
                    }
                    
                    /*
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "CompleteProfileVC") as! CompleteProfileVC
                    vc.dictParam = dictParam
                    self.navigationController?.pushViewController(vc, animated: true)*/
                }
            }
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Error \(error)")
    }
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    
    func apiSocial(_ dictParam: NSDictionary, _ showLoader: Bool) {
        self.view.endEditing(true)
        print("Social Param: ", dictParam)
        let sURL = SERVER_URL + API_SOCIAL
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_SOCIAL, parameters: dictParam, method: .post, isPassHeaderToken: false, showLoader: showLoader) { (error, responseObject) in
            hideLoaderHUD()
            print(responseObject ?? "")
            if error == nil {
                let obj: FMSignIn = FMSignIn.init(object: responseObject ?? "")
                if obj.success == "1" {
                    UserDefaults.standard.set(obj.token, forKey: UD_AUTHTOKEN)
                    if let dictUser = responseObject?["user"] as? NSDictionary {
                        saveUserData(obj.user, dictUser)
                    }
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "SignInConfirmVC")
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiGetPrefectures() {
        let sURL = SERVER_URL + API_GET_PREFECTURES
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: BLANK_HEADER) { (response, error) in
            print(response ?? "")
            let objPrefectures: FMPrefecturesList = FMPrefecturesList.init(object: response ?? "")
            var prefectures = [Prefectures]()
            prefectures = objPrefectures.prefectures ?? []
            if prefectures.count > 0 {
                self.prefectureId = prefectures[0].prefectureId
            }
        }
    }
}
