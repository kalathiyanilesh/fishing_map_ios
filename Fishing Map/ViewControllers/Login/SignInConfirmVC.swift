//
//  SignInConfirmVC.swift
//  Fishing Map
//
//  Created by iMac on 18/12/20.
//

import UIKit

class SignInConfirmVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLetsGo(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
