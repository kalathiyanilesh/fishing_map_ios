//
//  AppDelegate.swift
//  Fishing Map
//
//  Created by iMac on 27/10/20.
//

import UIKit
import Firebase
import GoogleMaps
import GooglePlaces
import CoreLocation
import GoogleSignIn
import FBSDKCoreKit
import IQKeyboardManagerSwift
import UserNotifications
import FirebaseMessaging
import GLNotificationBar

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var arrAllCategory = NSMutableArray()
    var objUser: User?
    var isHandlePush: Bool = false
    var objNotiPush: NotificationPush?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //self.window = UIWindow(frame: UIScreen.main.bounds)
        FishApp.mode = .live
        IQKeyboardManager.shared.enable = true
        
        GMSServices.provideAPIKey(GOOGLE_MAP_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_MAP_KEY)
        
        //FIREBASE
        let filePath = Bundle.main.path(forResource: "GoogleService-Info_FIREBASE", ofType: "plist")!
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
        
        //FACEBOOK LOGIN
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //GOOGLE LOGIN
        GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENTID
        
        //PUSH NOTIFICATION
        PushNotification()
        
        getCurrentLocation()
        
        if isUserLogin() {
            if UserDefaults.standard.object(forKey: UD_UserData) != nil {
                if let dict = UserDefaults.standard.object(forKey: UD_UserData) as? NSDictionary {
                    APP_DELEGATE.objUser = User.init(object: dict)
                }
            }
            apiGetCategory()
            apiGetUserInfo { (_) in }
            
            if let remoteNotification = launchOptions?[.remoteNotification] as?  [AnyHashable : Any] {
                print("Remote Notifcation: ", remoteNotification)
                let dictPushData: NSDictionary = remoteNotification as NSDictionary
                objNotiPush = NotificationPush.init(object: dictPushData)
                self.handlePushNotification(userInfo: dictPushData, application: application)
            }
            
            let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
            let nav = UINavigationController(rootViewController: vc)
            nav.setNavigationBarHidden(true, animated: false)
            self.window?.rootViewController = nav
            UIView.transition(with: self.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            }, completion: nil)
        } else {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "navlogin")
            self.window?.rootViewController = vc
            UIView.transition(with: self.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            }, completion: nil)
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let facebookDidHandle = ApplicationDelegate.shared.application(UIApplication.shared, open: url, sourceApplication: nil, annotation:[UIApplication.OpenURLOptionsKey.annotation])
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL)
        return facebookDidHandle || googleDidHandle
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        AppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}


//MARK:- GET CURRENT LOCATION
extension AppDelegate: CLLocationManagerDelegate {
    func getCurrentLocation()
    {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        locationManager.stopUpdatingLocation()
        let locValue:CLLocationCoordinate2D = manager.location?.coordinate ?? CLLocationCoordinate2D()
        lat_currnt = locValue.latitude
        long_currnt = locValue.longitude
        print("Current Lat: ",lat_currnt)
        print("Current Long: ",long_currnt)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        locationManager.startUpdatingLocation()
    }
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
        }
    }
}

//MARK:- PUSH NOTIFICATION
extension AppDelegate: UNUserNotificationCenterDelegate {
    func PushNotification() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UNNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print("Firebase registration token1: \(String(describing: deviceToken))")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        /*
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("Device Token: ", token)*/
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error Push: ", error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        //self.handlePushNotification(userInfo: userInfo as NSDictionary, application: application)
        print("UserInfo: ", userInfo)
        objNotiPush = NotificationPush.init(object: userInfo)
        let sText = "\((objNotiPush?.notifySubtype ?? "").firstUppercased)".replacingOccurrences(of: "_", with: " ")
        let notificationBar = GLNotificationBar(title: objNotiPush?.title, message: objNotiPush?.notificationText ?? "", preferredStyle: .detailedBanner, handler: nil)
        notificationBar.addAction(GLNotifyAction(title: "Go To \(sText)", style: .default) { (result) in
            self.handlePush()
        })
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if UIApplication.shared.applicationState == .active {
            print("UserInfo: ", userInfo)
            objNotiPush = NotificationPush.init(object: userInfo)
            let sText = "\((objNotiPush?.notifySubtype ?? "").firstUppercased)".replacingOccurrences(of: "_", with: " ")
            let notificationBar = GLNotificationBar(title: objNotiPush?.title, message: objNotiPush?.notificationText ?? "", preferredStyle: .detailedBanner, handler: nil)
            notificationBar.addAction(GLNotifyAction(title: "Go To \(sText)", style: .default) { (result) in
                self.handlePush()
            })
        } else {
            self.handlePushNotification(userInfo: userInfo as NSDictionary, application: application)
        }
    }
    
    func handlePushNotification(userInfo:NSDictionary,application: UIApplication)
    {
        print("UserInfo: ", userInfo)
        objNotiPush = NotificationPush.init(object: userInfo)
        handlePush()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}

//MARK:- HANDLE PUSH NOTIFICATION
extension AppDelegate {
    func handlePush() {
        if !isUserLogin() { return }
        guard APP_DELEGATE.objNotiPush != nil else { return }
        APP_DELEGATE.isHandlePush = true

        let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: false)
        self.window?.rootViewController = nav
        UIView.transition(with: self.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
        }, completion: nil)
    }
}
