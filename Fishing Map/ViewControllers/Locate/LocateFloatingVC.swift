//
//  LocateFloatingVC.swift
//  Fishing Map
//
//  Created by iMac on 04/11/20.
//

import UIKit

protocol delegateLocations {
    func getLocations(_ arrLocations: NSMutableArray)
}

class LocateFloatingVC: UIViewController {

    @IBOutlet var tblLocate: UITableView!
    var storedOffsets = [Int: CGFloat]()
    var selectedExploreLocation = 0
    
    var arrImgExplore = ["icn_fish_exoplore", "terrain", "ic-restaurant", "noun_Hotel"]
    var arrExploreNames = ["魚", "地形", "レストラン", "ホテル"]
    var arrLocateImageName = ["人気の魚", "人気の地形", "人気のレストラン", "人気のホテル"]
    
    var arrArroundLocations = NSMutableArray()
    var arrImages = NSMutableArray()
    //var refreshControl = UIRefreshControl()
    
    var delegateLocations: delegateLocations?
    
    //var objLocation: Locations?
    var lat_location: Double = 0
    var long_location: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lat_location = lat_currnt
        long_location = long_currnt
        tblLocate.isScrollEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectedLocation(notification:)), name: Notification.Name("selectedLocation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeLocation(notification:)), name: Notification.Name("changeLocation"), object: nil)
        //refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        //tblLocate.addSubview(refreshControl)
    }
    
    /*
    @objc func refresh(_ sender: AnyObject) {
       getData()
    }*/
    
    override func viewWillAppear(_ animated: Bool) {
        saveImageType()
        getData()
    }
    
    func getData() {
        let arrType = ["fish_locations", "fish_locations","restaurant_locations", "hotel_locations"]
        apiGetAround(arrType[selectedExploreLocation])
    }
    
    @objc func selectedLocation(notification: Notification) {
        if let index = notification.object as? Int {
            let objLocation = arrArroundLocations[index] as? Locations
            //manageImagesArray()
            guard let type = UserDefaults.standard.object(forKey: UD_Location_type) as? String else { return }
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
            vc.imgType = getPushImageType(type.lowercased())
            vc.sLocationID = objLocation?.locationId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func changeLocation(notification: Notification) {
        if let dictData = notification.userInfo as NSDictionary? {
            //["lat":place.coordinate.latitude, "long":place.coordinate.longitude]
            lat_location = dictData.object(forKey: "lat") as! Double
            long_location = dictData.object(forKey: "long") as! Double
            //objLocation = nil
            arrImages.removeAllObjects()
            tblLocate.reloadData()
            getData()
        }
    }
    
    @objc func clickTopLocationExplore(sender: UIButton) {
        selectedExploreLocation = sender.tag
        tblLocate.reloadData()
        getData()
    }
    
    func manageImagesArray() {
        //["魚", "地形", "レストラン", "ホテル"]
        arrImages.removeAllObjects()
        
        for i in 0..<self.arrArroundLocations.count {
            guard let objLoc = self.arrArroundLocations[i] as? Locations else { return }
            let sType = arrExploreNames[selectedExploreLocation]
            switch sType {
            case "魚":
                if let arr = objLoc.fishImages {
                    self.setupImages(arr, objLoc)
                }
            case "地形":
                if let arr = objLoc.terrainImages {
                    self.setupImages(arr, objLoc)
                }
            case "レストラン":
                if let arr = objLoc.restaurantImages {
                    self.setupImages(arr, objLoc)
                }
            case "ホテル":
                if let arr = objLoc.hotelImages {
                    self.setupImages(arr, objLoc)
                }
            default:
                break
            }
        }
    }
    
    func setupImages(_ arr: [Images], _ objLoc: Locations) {
        let arrData = NSMutableArray()
        for image in arr {
            let dict = image.dictionaryRepresentation()
            let objImage = Images.init(object: dict)
            objImage.imageLocationId = objLoc.locationId
            arrData.add(objImage)
        }
        arrImages.addObjects(from: arrData as! [Any])
        tblLocate.reloadData()
    }
}

//MARK:- UITableViewDelegate METHOD
extension LocateFloatingVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell : CellTopAroundFish = tableView.dequeueReusableCell(withIdentifier: "CellTopAroundFish", for: indexPath) as! CellTopAroundFish
            cell.collAroundFish.tag = indexPath.row
            cell.lblTitle.text = arrLocateImageName[selectedExploreLocation]
            cell.imgLocateType.image = UIImage(named: arrImgExplore[selectedExploreLocation])
            cell.lblNoRecords.alpha = arrImages.count > 0 ? 0 : 1
            cell.btnAdd.addTarget(self, action: #selector(btnAddImage), for: .touchUpInside)
            return cell
        default:
            let cell : CellExploreLoc = tableView.dequeueReusableCell(withIdentifier: "CellExploreLoc", for: indexPath) as! CellExploreLoc
            cell.colExploreLocations.tag = indexPath.row
            cell.lblTitle.text = "…のトップロケーションを探索する"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            guard let profileCell = cell as? CellTopAroundFish else { return }
            profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        case 1:
            guard let exploreCell = cell as? CellExploreLoc else { return }
            exploreCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            exploreCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 236
        default:
            return 180
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            guard let profileCell = cell as? CellTopAroundFish else { return }
            storedOffsets[indexPath.row] = profileCell.collectionViewOffset
        case 1:
            guard let exploreCell = cell as? CellExploreLoc else { return }
            storedOffsets[indexPath.row] = exploreCell.collectionViewOffset
        default:
            break
        }
    }

    @objc func btnAddImage() {
        let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "AddLocationVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UICollectionViewDelegate METHOD
extension LocateFloatingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return arrImages.count
        } else {
            return arrExploreNames.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
            let objImg: Images = arrImages[indexPath.row] as! Images
            cell.yImageName.constant = 0
            cell.heightImageName.constant = 0
            cell.heightImgFish.constant = 120
            cell.lblTitle.text = ""//objImg.imageName
            cell.lblTotalLikes.text = objImg.imageLikesCount
            cell.imgPost.sd_setImage(with: URL(string: objImg.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.viewComment.isHidden = true
            return cell
        } else {
            let cell: CellExploreLocations = collectionView.dequeueReusableCell(withReuseIdentifier: "CellExploreLocations", for: indexPath) as! CellExploreLocations
            cell.btnExploreForOption.layer.cornerRadius = 12
//            cell.widthLabel.constant = SCREENWIDTH()/6
            cell.btnExploreForOption.setImage(UIImage(named: arrImgExplore[indexPath.item]), for: .normal)
            cell.lblExploreForName.text = arrExploreNames[indexPath.item]
            if selectedExploreLocation == indexPath.item {
                cell.btnExploreForOption.layer.borderWidth = 2
                cell.btnExploreForOption.layer.borderColor = Color_Hex(hex: "#1DBBD1").cgColor
                cell.btnExploreForOption.backgroundColor = .white
                cell.btnExploreForOption.tintColor = Color_Hex(hex: "#1DBBD1")
                cell.lblExploreForName.textColor = Color_Hex(hex: "#056989")
                cell.btnExploreForOption.dropShadow(scale: true, color: Color_Hex(hex: "#1DBBD1"), opacity: 0.14, offset: CGSize(width: 0, height: 4), radius: 8)
                cell.lblExploreForName.font = UIFont(name: "Interstate-Bold", size: 12)
            } else {
                cell.btnExploreForOption.layer.borderWidth = 0
                cell.btnExploreForOption.layer.borderColor = UIColor.clear.cgColor
                cell.btnExploreForOption.backgroundColor = Color_Hex(hex: "#006677")
                cell.btnExploreForOption.tintColor = .white
                cell.lblExploreForName.textColor = Color_Hex(hex: "#101010")
                cell.btnExploreForOption.dropShadow(scale: true, color: .clear, opacity: 0.14, offset: CGSize(width: 0, height: 4), radius: 8)
                cell.lblExploreForName.font = UIFont(name: "Interstate-Regular", size: 12)
            }
            cell.imgType.isHidden = true
            if indexPath.item == 3 {
                cell.imgType.isHidden = false
                if selectedExploreLocation == 3 {
                    cell.imgType.image = UIImage(named: "noun_Hotel")
                } else {
                    cell.imgType.image = UIImage(named: "unselect_Hotel")
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
            let objImg: Images = arrImages[indexPath.row] as! Images
            if objImg.isLocation == "1" {
                guard let type = UserDefaults.standard.object(forKey: UD_Location_type) as? String else { return }
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
                vc.imgType = getPushImageType(type.lowercased())
                vc.sLocationID = objImg.imageLocationId
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                switch selectedExploreLocation {
                case 0,1:
                    let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                    vc.imageType = (selectedExploreLocation == 0) ? .Fish : .TopTerrain
                    vc.sImageID = objImg.imageId ?? "0"
                    self.navigationController?.pushViewController(vc, animated: true)
                case 2,3:
                    let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
                    vc.imageType = (selectedExploreLocation == 2) ? .Restaurant : .Hotels
                    vc.sImageID = objImg.imageId ?? "0"
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    return
                }
            }
        } else {
            selectedExploreLocation = indexPath.item
           // objLocation = nil
            arrImages.removeAllObjects()
            collectionView.reloadData()
            self.tblLocate.reloadData()
            saveImageType()
            getData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width:148, height:170)
        } else {
            return CGSize(width: (SCREENWIDTH()-110)/4, height: collectionView.frame.height)
        }
    }
    
    func saveImageType() {
        let userDefaults = UserDefaults.standard
        switch selectedExploreLocation {
        case 0:
            userDefaults.setValue("Fish", forKey: UD_Location_type)
        case 1:
            userDefaults.setValue("Terrain", forKey: UD_Location_type)
        case 2:
            userDefaults.setValue("Restaurant", forKey: UD_Location_type)
        default:
            userDefaults.setValue("Hotel", forKey: UD_Location_type)
        }
    }
}

//MARK:- API CALLING
extension LocateFloatingVC {
    func apiGetAround(_ sType: String) {
        let sURL = SERVER_URL + "\(API_LOCATION)/\(sType)?lat=\(lat_location)&lng=\(long_location)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOCATION, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            if error == nil {
                self.arrArroundLocations = NSMutableArray()
                let objLoc: FMLocateAround = FMLocateAround.init(object: responseObject ?? "")
                if objLoc.success ?? false {
                    for loc in objLoc.locations ?? [] {
                        self.arrArroundLocations.add(loc)
                        /*
                        if self.objLocation != nil {
                            if self.objLocation?.locationId == loc.locationId {
                                self.objLocation = loc
                            }
                        }*/
                    }
                    /*
                    if self.arrArroundLocations.count > 0 {
                        if self.objLocation == nil {
                            self.objLocation = self.arrArroundLocations[0] as? Locations
                        }
                        self.manageImagesArray()
                    }*/
                    self.manageImagesArray()
                    self.delegateLocations?.getLocations(self.arrArroundLocations)
                } else {
                    showMessage(objLoc.message ?? ErrorMSG)
                }
                self.tblLocate.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}


//MARK:- UITABLEVIEWCELL
class CellTopAroundFish: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var collAroundFish: UICollectionView!
    @IBOutlet weak var imgLocateType: UIImageView!
    @IBOutlet var lblNoRecords: UILabel!
    @IBOutlet var btnAdd: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collAroundFish.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
    }
}

extension CellTopAroundFish {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collAroundFish.delegate = dataSourceDelegate
        collAroundFish.dataSource = dataSourceDelegate
        collAroundFish.tag = row
        collAroundFish.setContentOffset(collAroundFish.contentOffset, animated:false)
        collAroundFish.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collAroundFish.contentOffset.x = newValue }
        get { return collAroundFish.contentOffset.x }
    }
}

class CellExploreLoc: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewBack: UIView!
    @IBOutlet weak var colExploreLocations: UICollectionView!
}

extension CellExploreLoc {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        colExploreLocations.delegate = dataSourceDelegate
        colExploreLocations.dataSource = dataSourceDelegate
        colExploreLocations.tag = row
        colExploreLocations.setContentOffset(colExploreLocations.contentOffset, animated:false)
        colExploreLocations.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { colExploreLocations.contentOffset.x = newValue }
        get { return colExploreLocations.contentOffset.x }
    }
}

class CellExploreLocations: UICollectionViewCell {
    
    @IBOutlet weak var btnExploreForOption: UIButton!
    @IBOutlet weak var lblExploreForName: UILabel!
    @IBOutlet var imgType: UIImageView!
}
