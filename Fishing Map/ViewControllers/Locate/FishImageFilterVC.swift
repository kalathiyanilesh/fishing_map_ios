//
//  FishImageFilterVC.swift
//  Fishing Map
//
//  Created by macOS on 05/11/20.
//

import UIKit

protocol UpdatelocateionType: class {
    func updateLocationTitle(title: String)
}

class CellFishImages: UITableViewCell {
    @IBOutlet weak var collFishImages: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet var lblNoRecord: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collFishImages.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
    }
}

extension CellFishImages {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collFishImages.delegate = dataSourceDelegate
        collFishImages.dataSource = dataSourceDelegate
        collFishImages.tag = row
        collFishImages.setContentOffset(collFishImages.contentOffset, animated:false)
        collFishImages.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collFishImages.contentOffset.x = newValue }
        get { return collFishImages.contentOffset.x }
    }
}


class FishImageFilterVC: UIViewController {
    
    @IBOutlet weak var tblFishImages: UITableView!
    @IBOutlet weak var viewContentDrawer: UIView!
    @IBOutlet weak var topOfContentView: NSLayoutConstraint!
    
    var storedOffsets = [Int: CGFloat]()
    var locateTitle = "魚の写真でフィルター"
    var arrArroundLocations = NSMutableArray()
    var arrImages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topOfContentView.constant = 0 - viewContentDrawer.frame.height
        guard var type = UserDefaults.standard.object(forKey: UD_Location_type) as? String else { return }
        locateTitle = "魚の写真でフィルター"
        if type == "Terrain" { type = "Fish" }
        apiGetAllImage(type.lowercased())
        apiGetAllLocation(type.lowercased())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewContentDrawer.layer.masksToBounds = true
        viewContentDrawer.layer.cornerRadius = 16
        if #available(iOS 11.0, *) {
            viewContentDrawer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        runAfterTime(time: 0.2) {
            UIView.animate(withDuration: 0.3) {
                self.topOfContentView.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }

    //MARK:- UIBUTTON ACTION
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.topOfContentView.constant = 0 - self.viewContentDrawer.frame.height
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func btnViewAllFishes() {
        guard let type = UserDefaults.standard.object(forKey: UD_Location_type) as? String else { return }
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
        vc.imageType = getPushImageType(type.lowercased())
        vc.arrAllImages = arrImages
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnViewAllLocations() {
        guard let type = UserDefaults.standard.object(forKey: UD_Location_type) as? String else { return }
        let vc = loadVC(strStoryboardId: SB_Select_Location, strVCId: "SelectLocationVC") as! SelectLocationVC
        vc.isFromLocate = true
        vc.sImageType = type.lowercased()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension FishImageFilterVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellFishImages = tableView.dequeueReusableCell(withIdentifier: "CellFishImages", for: indexPath) as! CellFishImages
        if indexPath.row == 0 {
            cell.lblTitle.text = locateTitle
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAllFishes), for: .touchUpInside)
            cell.lblNoRecord.alpha = arrImages.count > 0 ? 0 : 1
        } else {
            cell.lblTitle.text = "スポットでフィルター"
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAllLocations), for: .touchUpInside)
            cell.lblNoRecord.alpha = arrArroundLocations.count > 0 ? 0 : 1
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let profileCell = cell as? CellFishImages else { return }
        profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 236
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let profileCell = cell as? CellFishImages else { return }
        storedOffsets[indexPath.row] = profileCell.collectionViewOffset
    }
}

extension FishImageFilterVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return arrImages.count
        default:
            return arrArroundLocations.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.imgPost.image = UIImage(named: "temp1")
        switch collectionView.tag {
        case 0:
            cell.topOfLike.constant = 14
            cell.topOfComment.constant = 14
            cell.heightViewLike.constant = 16
            cell.heightViewComment.constant = 16
            
            let objImg: Images = arrImages[indexPath.row] as! Images
            cell.yImageName.constant = 0
            cell.heightImageName.constant = 0
            cell.heightImgFish.constant = 120
            cell.lblTitle.text = ""//objImg.imageName
            cell.lblTotalLikes.text = objImg.imageLikesCount
            cell.imgPost.sd_setImage(with: URL(string: objImg.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            
        default:
            cell.topOfLike.constant = 0
            cell.topOfComment.constant = 0
            cell.heightViewLike.constant = 0
            cell.heightViewComment.constant = 0
            cell.heightImgFish.constant = 124
            
            let objLoc = arrArroundLocations.object(at: indexPath.row) as! NewLocation
            cell.imgPost.sd_setImage(with: URL(string: objLoc.locationImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTitle.text = objLoc.locationName
        }
        cell.viewComment.isHidden = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let type = UserDefaults.standard.object(forKey: UD_Location_type) as? String else { return }
        let imageType = getPushImageType(type.lowercased())
        switch collectionView.tag {
        case 0:
            let objImg: Images = arrImages[indexPath.row] as! Images
            if objImg.isLocation == "1" {
                let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
                vc.imgType = imageType
                vc.sLocationID = objImg.imageLocationId
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                switch imageType {
                case .Restaurant, .Hotels:
                    let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
                    vc.imageType = imageType
                    vc.sImageID = objImg.imageId ?? "0"
                    self.navigationController?.pushViewController(vc, animated: true)
                default:
                    let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
                    vc.imageType = (imageType == .TopTerrain) ? .Fish : imageType
                    vc.sImageID = objImg.imageId ?? "0"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        default:
            let objLoc = arrArroundLocations.object(at: indexPath.row) as! NewLocation
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
            vc.imgType = imageType
            vc.sLocationID = objLoc.locationId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        /*
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        selectedImage = indexPath.row
        collectionView.reloadData()
        
        if collectionView.tag == 1 {
            let objLoc =  arrArroundLocations[selectedCollection] as! Locations
            self.manageImages(objLoc.locationId ?? "0")
        }*/
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:148, height:170)
    }
    
}

// MARK:- API CALLING
extension FishImageFilterVC {
    func apiGetAllLocation(_ sType: String) {
        let sURL = SERVER_URL + API_LOCATION
        let dictParam: NSDictionary = ["image_type": sType, "lat":lat_currnt, "lng": long_currnt]
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOCATION, parameters: dictParam, method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            if error == nil {
                self.arrArroundLocations = NSMutableArray()
                let objLoc: FMNewLocation = FMNewLocation.init(object: responseObject ?? "")
                if objLoc.success == "1" {
                    for loc in objLoc.locations ?? [] {
                        self.arrArroundLocations.add(loc)
                    }
                } else {
                    showMessage(objLoc.message ?? ErrorMSG)
                }
                self.tblFishImages.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiGetAllImage(_ sType: String) {
        let sURL = SERVER_URL + API_CREATE_IMAGES
        let dictParam: NSDictionary = ["image_type": sType]
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_CREATE_IMAGES, parameters: dictParam, method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                self.arrImages = NSMutableArray()
                let obj: FMAllImages = FMAllImages.init(object: responseObject ?? "")
                if obj.success == "1" {
                    if let arr = obj.images {
                        self.arrImages.addObjects(from: arr)
                    }
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
                self.tblFishImages.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}
