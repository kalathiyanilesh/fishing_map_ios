//
//  ViewAllFishImagesVC.swift
//  Fishing Map
//
//  Created by macOS on 05/11/20.
//

import UIKit

class ViewAllFishImagesVC: UIViewController {
    
    @IBOutlet weak var colAllFishImages: UICollectionView!
    @IBOutlet weak var lblNavigationTitle: UILabel!
    @IBOutlet var lblNoRecordFound: UILabel!
    @IBOutlet var widthAddImage: NSLayoutConstraint!
    @IBOutlet var btnAddImage: UIButton!
    
    var imageType = LocateImageType.Fish
    var objProduct: Products?
    var sLocationID: String?
    var arrAllImages = NSMutableArray()
    var isShowPosts: Bool = false
    var arrPosts: [Posts] = []
    var sUserID: String?
    var isProfileImages: Bool = false
    var isFromLocateFilter: Bool = false
    var isAddImage: Bool = false
    var objLocation: Locations?
    
    override func viewDidLoad() {
        colAllFishImages.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 25, bottom: 20, right: 25)
        switch imageType {
        case .Fish, .TopScene:
            layout.itemSize = CGSize(width: 148, height: 170)
        default:
            layout.itemSize = CGSize(width: 148, height: 179)
        }
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        colAllFishImages!.collectionViewLayout = layout
        
        super.viewDidLoad()
        
        setupUI()
        
        if !isAddImage {
            btnAddImage.isHidden = true
            widthAddImage.constant = 0
        }
        if sLocationID != nil {
            apiGetAllImages()
        }
        if objProduct != nil {
            apiGetProductImage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isShowPosts {
            apiGetUserPost()
        }
    }
    
    func setupUI() {
        if isShowPosts {
            lblNavigationTitle.text = "全ての投稿を見る"
            return
        }
        switch imageType {
        case .Fish:
            self.lblNavigationTitle.text = "全ての魚の写真を見る"
        case .Locations:
            self.lblNavigationTitle.text = "全てのスポットを見る"
        case .TopScene:
            self.lblNavigationTitle.text = "全ての景色を見る"
        case .AddImage:
            self.lblNavigationTitle.text = "全てを見る \(objProduct?.productName ?? "")"
        default:
            return
        }
    }
    
    @IBAction func btnAddImage(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        vc.objLocation = objLocation
        vc.addImgType = imageType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension ViewAllFishImagesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isShowPosts {
            lblNoRecordFound.alpha = arrPosts.count > 0 ? 0 : 1
            return arrPosts.count
        } else {
            lblNoRecordFound.alpha = arrAllImages.count > 0 ? 0 : 1
            return arrAllImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        if isShowPosts {
            cell.yImageName.constant = 0
            cell.heightImageName.constant = 0
            cell.heightImgFish.constant = 120
            
            let objPost = arrPosts[indexPath.row]
            cell.imgPost.sd_setImage(with: URL(string: objPost.postImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTitle.text = ""
            cell.lblTotalLikes.text = "\(objPost.postLikesCount ?? 0)"
            cell.lblTotalComments.text = "\(objPost.postCommentsCount ?? 0)"
            if objPost.postIsLiked ?? false {
                cell.imgLike.image = UIImage(named: "ic_like")
            } else {
                cell.imgLike.image = UIImage(named: "ic_like_gray")
            }
            cell.btnLikes.tag = indexPath.row
            cell.btnLikes.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
            return cell
        }
        switch imageType {
        case .Fish, .TopScene:
            cell.viewComment.isHidden = true
        case .AddImage:
            cell.viewLike.isHidden = true
            cell.viewComment.isHidden = true
            cell.heightImgFish.constant = 128
        default:
            if isFromLocateFilter {
                cell.viewComment.isHidden = true
            } else {
                cell.heightImgFish.constant = 128
                cell.topOfLike.constant = 0
                cell.topOfComment.constant = 0
                cell.heightViewLike.constant = 0
                cell.heightViewComment.constant = 0
            }
        }
        if imageType == .AddImage {
            let objImg = arrAllImages.object(at: indexPath.row) as? ProductImages
            cell.imgPost.sd_setImage(with: URL(string: objImg?.productImageImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTitle.text = objImg?.productName
        } else {
            cell.yImageName.constant = 0
            cell.heightImageName.constant = 0
            cell.heightImgFish.constant = 120
            
            if isProfileImages {
                let objImg = arrAllImages.object(at: indexPath.row) as? ProfileImages
                cell.imgPost.sd_setImage(with: URL(string: objImg?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                cell.lblTitle.text = ""//objImg?.imageName
                cell.lblTotalLikes.text = objImg?.imageLikesCount
            } else {
                let objImg = arrAllImages.object(at: indexPath.row) as? Images
                cell.imgPost.sd_setImage(with: URL(string: objImg?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                cell.lblTitle.text = ""//objImg?.imageName
                cell.lblTotalLikes.text = objImg?.imageLikesCount
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isShowPosts {
            return CGSize(width:148, height:170)
        }
        switch imageType {
        case .Fish, .TopScene, .AddImage:
            return CGSize(width: (SCREENWIDTH()-78)/2, height: 170)
        default:
            return CGSize(width: (SCREENWIDTH()-78)/2, height: 179)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isFromLocateFilter { return }
        if isShowPosts {
            let vc = loadVC(strStoryboardId: SB_FEED, strVCId: idFeedDetailsVC) as! FeedDetailsVC
            vc.post = arrPosts[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        if isProfileImages {
            let objImg = arrAllImages.object(at: indexPath.row) as? ProfileImages
            let sType = objImg?.imageType
            switch sType {
            case "fish":
                imageType = .Fish
            case "terrain":
                imageType = .TopTerrain
            case "parking":
                imageType = .ParkingSpots
            case "scene":
                imageType = .TopScene
            case "hotel":
                imageType = .Hotels
            case "restaurant":
                imageType = .Restaurant
            default:
                break
            }
        }
        switch imageType {
        case .Fish, .TopTerrain, .ParkingSpots, .TopScene:
            var sImageId = ""
            if isProfileImages {
                let objImg = arrAllImages.object(at: indexPath.row) as? ProfileImages
                sImageId = objImg?.imageId ?? "0"
            } else {
                let objImg = arrAllImages.object(at: indexPath.row) as? Images
                sImageId = objImg?.imageId ?? "0"
            }
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
            vc.imageType = imageType
            vc.sImageID = sImageId
            self.navigationController?.pushViewController(vc, animated: true)
        case .Restaurant, .Hotels:
            var sImageId = ""
            if isProfileImages {
                let objImg = arrAllImages.object(at: indexPath.row) as? ProfileImages
                sImageId = objImg?.imageId ?? "0"
            } else {
                let objImg = arrAllImages.object(at: indexPath.row) as? Images
                sImageId = objImg?.imageId ?? "0"
            }
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
            vc.imageType = imageType
            vc.sImageID = sImageId
            self.navigationController?.pushViewController(vc, animated: true)
        case .AddImage:
            let objProductImage = arrAllImages.object(at: indexPath.row) as? ProductImages
            let vc = loadVC(strStoryboardId: SB_Store, strVCId: "EquipmentDetailsVC") as! EquipmentDetailsVC
            vc.objProductImage = objProductImage
            vc.objProduct = objProduct
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    @objc func btnLike(_ sender: UIButton) {
        let index = sender.tag
        let post: Posts = arrPosts[index]
        if post.postIsLiked ?? false {
            apiUnLikePost(post.postId ?? 0, isUnlikeEvent: false)
            post.postIsLiked = false
            
            post.postLikesCount = (post.postLikesCount ?? 0) - 1
            if post.postLikesCount ?? 0 < 0 {
                post.postLikesCount = 0
            }
        } else {
            apiLikePost(post.postId ?? 0, isLikeEvent: false)
            post.postIsLiked = true
            post.postLikesCount = (post.postLikesCount ?? 0) + 1
        }
        colAllFishImages.reloadData()
    }
}

//MARK:- API CALLING
extension ViewAllFishImagesVC {
    func apiGetAllImages() {
        let sURL = SERVER_URL + "locations/\(sLocationID ?? "0")/" + API_GET_ALL_IMAGES
        let dictParam: NSDictionary = ["image_type": getImageType()]
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_GET_ALL_IMAGES, parameters: dictParam, method: .get, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                let obj: FMAllImages = FMAllImages.init(object: responseObject ?? "")
                if obj.success == "1" {
                    if let arr = obj.images {
                        self.arrAllImages.addObjects(from: arr)
                    }
                    self.colAllFishImages.reloadData()
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiGetProductImage() {
        self.arrAllImages.removeAllObjects()
        let sURL = SERVER_URL + API_PRODCTS + "/\(objProduct?.productId ?? "0")/\(API_PRODUCT_IMAGE)"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_PRODUCT_IMAGE, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                let objStore: FMProductImage = FMProductImage.init(object: responseObject ?? "")
                if objStore.success == "1" {
                    for product in objStore.productImages ?? [] {
                        self.arrAllImages.add(product)
                    }
                } else {
                    showMessage(objStore.message ?? ErrorMSG)
                }
                self.colAllFishImages.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func apiGetUserPost() {
        let sURL = SERVER_URL + API_USER + "/\(sUserID ?? "0")/" + API_USER_POST
        HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error)  in
            self.arrPosts.removeAll()
            let objFeedPosts: FMFeedPosts = FMFeedPosts.init(object: response ?? "")
            for post in objFeedPosts.posts ?? [] {
                self.arrPosts.append(post)
            }
            self.colAllFishImages.reloadData()
        }
    }
    
    func getImageType() -> String {
        switch imageType {
        case .Fish:
            return "fish"
        case .TopTerrain:
            return "terrain"
        case .Restaurant:
            return "restaurant"
        case .Hotels:
            return "hotel"
        case .TopScene:
            return "scene"
        case .ParkingSpots:
            return "parking"
        case .Post:
            return "post"
        case .Locations:
            return "location"
        default:
            return ""
        }
    }
}
