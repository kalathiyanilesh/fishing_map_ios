//
//  LocateVC.swift
//  Fishing Map
//
//  Created by iMac on 04/11/20.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FloatingPanel

class LocateVC: UIViewController {

    @IBOutlet var lblYourLocation: UILabel!
    @IBOutlet var txtSearchLocation: UITextField!
    @IBOutlet var mapView: GMSMapView!
    var fpc: FloatingPanelController!
    var lat_location: Double = 0
    var long_location: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lat_location = lat_currnt
        long_location = long_currnt
        SetupFloatingView()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        getAddress(lat_location, long_location) { (sAddress) in
            self.lblYourLocation.text = sAddress
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mapView.padding = UIEdgeInsets(top: 70, left: 0, bottom: 70, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*
        mapView.camera = GMSCameraPosition.camera(withLatitude: lat_currnt, longitude: long_currnt, zoom: 12.0)
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat_currnt, longitude: long_currnt))
        marker.map = mapView*/
    }
    
    @IBAction func btnCurrentLoc(_ sender: Any) {
        lat_location = lat_currnt
        long_location = long_currnt
        getAddress(lat_location, long_location) { (sAddress) in
            self.lblYourLocation.text = sAddress
        }
        let dictData: NSDictionary = ["lat":lat_location, "long":long_location]
        NotificationCenter.default.post(name: Notification.Name("changeLocation"), object: nil, userInfo: dictData as? [AnyHashable : Any])
    }
    
    @IBAction func btnSearchLoc(_ sender: Any) {
        addLocation()
    }
    
    @IBAction func btnSearchTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "FeedSearchVC") as! FeedSearchVC
        vc.isForLocation = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_SIDEMENU, strVCId: "navsidemenu")
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }
    
    @IBAction func btnSearchFishTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "navFishFiltervc")
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }
}

//MARK:- DELEGATE
extension LocateVC:delegateLocations, GMSMapViewDelegate {
    func getLocations(_ arrLocations: NSMutableArray) {
        mapView.clear()
        var bounds = GMSCoordinateBounds()
        
        let markerCurrent = GMSMarker()
        markerCurrent.position = CLLocationCoordinate2D(latitude: lat_location, longitude: long_location)
        markerCurrent.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        markerCurrent.map = self.mapView
        bounds = bounds.includingCoordinate(markerCurrent.position)
        var sPinName = "fish_orange_pin" //"fish_pin"
        if let type = UserDefaults.standard.object(forKey: UD_Location_type) as? String {
            switch type {
            case "Fish", "Terrain":
                sPinName = "fish_orange_pin"
            case "Restaurant":
                sPinName = "ic_pin_restaurant"
            case "Hotel":
                sPinName = "ic_pin_hotel"
            default:
                sPinName = "fish_orange_pin"
            }
        }
        
        for i in 0..<arrLocations.count {
            let objLoc: Locations = arrLocations.object(at: i) as! Locations
            let marker = GMSMarker()
            marker.title = objLoc.locationName
            marker.position = CLLocationCoordinate2D(latitude: Double(objLoc.locationLat ?? "0") ?? 0, longitude: Double(objLoc.locationLng ?? "0") ?? 0)
            marker.icon = UIImage(named: sPinName)
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.map = mapView
            marker.userData = ["index": i]
            marker.isTappable = true
            bounds = bounds.includingCoordinate(marker.position)
        }
        mapView.setMinZoom(1, maxZoom: 15)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        mapView.animate(with: update)
        mapView.setMinZoom(1, maxZoom: 20)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let dict = marker.userData as? [String:Int] {
            NotificationCenter.default.post(name: Notification.Name("selectedLocation"), object: dict["index"] ?? 0)
        }
        mapView.selectedMarker = marker
        return true
    }
}

//MARK:- SET FLOATING VIEW
extension LocateVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.white
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.layer.cornerRadius = 15.0
        fpc.surfaceView.clipsToBounds = true
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "LocateFloatingVC") as! LocateFloatingVC
        vc.delegateLocations = self
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
        fpc.track(scrollView: vc.tblLocate)
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return MyFloatingPanelLayout()
    }
}


// MARK:- PLACE PICKER
extension LocateVC: GMSAutocompleteViewControllerDelegate {
    
    func addLocation() {
        let placePickerController = GMSAutocompleteViewController()
        placePickerController.delegate = self
        placePickerController.modalPresentationStyle = .fullScreen
        present(placePickerController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place address: \(place.formattedAddress ?? "")")
        dismiss(animated: true) {
            self.lat_location = place.coordinate.latitude
            self.long_location = place.coordinate.longitude
            
            let dictData: NSDictionary = ["lat":self.lat_location, "long":self.long_location]
            NotificationCenter.default.post(name: Notification.Name("changeLocation"), object: nil, userInfo: dictData as? [AnyHashable : Any])
            
            self.lblYourLocation.text = place.name ?? ""
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
