//
//  LocateSearchVC.swift
//  Fishing Map
//
//  Created by iMac on 23/12/20.
//

import UIKit

class cellLocationSearch: UITableViewCell {
    @IBOutlet var imgLocation: UIImageView!
    @IBOutlet var lblLocationName: UILabel!
}

class LocateSearchVC: UIViewController {

    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tblResult: UITableView!
    var arrSearchLoc : [NewLocation] = []
    var sTag: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSearch.delegate = self
        txtSearch.text = sTag
        apiSearchLoc()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UITABLEVEW DELEGATE
extension LocateSearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchLoc.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: cellLocationSearch = tableView.dequeueReusableCell(withIdentifier: "cellLocationSearch", for: indexPath) as! cellLocationSearch
        let objLoc = arrSearchLoc[indexPath.row]
        cell.imgLocation.sd_setImage(with: URL(string: objLoc.locationImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        cell.lblLocationName.text = objLoc.locationName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objLoc = arrSearchLoc[indexPath.row]
        let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
        if objLoc.locationCategoryId == "1" {
            vc.imgType = .Fish
        } else if objLoc.locationCategoryId == "2" {
            vc.imgType = .Restaurant
        } else if objLoc.locationCategoryId == "3" {
            vc.imgType = .Hotels
        }
        vc.sLocationID = objLoc.locationId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 249
    }
}

//MARK:- API CALLING
extension LocateSearchVC {
    func apiSearchLoc() {
        //let dictParam: NSDictionary = ["term": TRIM(string: txtSearch.text ?? "")]
        let sURL = SERVER_URL + API_SEARCH_LOCATION + "?term=\(TRIM(string: txtSearch.text ?? ""))"
        let urlString = sURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: urlString, service: API_SEARCH_LOCATION, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: true) { (error, response) in
            if error == nil {
                print(response ?? "")
                if error == nil {
                    let objLoc: FMNewLocation = FMNewLocation.init(object: response ?? "")
                    if objLoc.success == "1" {
                        self.arrSearchLoc.removeAll()
                        self.arrSearchLoc = objLoc.locations ?? []
                        self.tblResult.reloadData()
                    }
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}

//MARK:- UITEXTFIELD DELEGATE
extension LocateSearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if TRIM(string: textField.text ?? "").count > 0 {
            self.apiSearchLoc()
        }
    }
}
