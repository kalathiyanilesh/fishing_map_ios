//
//  AddInformationVC.swift
//  Fishing Map
//
//  Created by macOS on 07/11/20.
//

import UIKit

enum LocateImageType {
    case TopScene
    case TopTerrain
    case Fish
    case ParkingSpots
    case Locations
    case Post
    case AddImage
    case Hotels
    case Restaurant
}

class CellAddInfo: UITableViewCell {
    @IBOutlet weak var lblInfoType: UILabel!
    @IBOutlet weak var collImages: UICollectionView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnAddImages: UIButton!
    @IBOutlet weak var btnViewAll: UIButton!
    
    @IBOutlet var viewBeFirst: UIView!
    @IBOutlet var lblBeFirstTitle: UILabel!
    @IBOutlet var btnAddImageSecond: UIButton!
    @IBOutlet var imgCat: UIImageView!
    @IBOutlet var viewDashed: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collImages.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
    }
}

extension CellAddInfo {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collImages.delegate = dataSourceDelegate
        collImages.dataSource = dataSourceDelegate
        collImages.tag = row
        collImages.setContentOffset(collImages.contentOffset, animated:false)
        collImages.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collImages.contentOffset.x = newValue }
        get { return collImages.contentOffset.x }
    }
}

class AddInformationVC: UIViewController {
    
    @IBOutlet weak var tblAddInfo: UITableView!
    
    var storedOffsets = [Int: CGFloat]()
    var imageType = LocateImageType.TopScene
    var arrImages = NSMutableArray()
    var sLocationID: String?
    var sLocationName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        apiGetAllImage()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Submission, strVCId: "SubmissionPopupVC")
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false)
    }*/
    
    @objc func btnViewAll(sender: UIButton) {
        switch sender.tag {
        case 0:
            let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
            vc.imageType = imageType
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
    }
    
    @objc func btnAddImage(sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        vc.sLocationID = sLocationID
        vc.sLocationName = sLocationName ?? ""
        /*
        if imageType == .Fish {
            switch sender.tag {
            case 0:
                vc.addImgType = .TopScene
            case 1:
                vc.addImgType = .TopTerrain
            case 2:
                vc.addImgType = .Fish
            case 3:
                vc.addImgType = .ParkingSpots
            default:
                return
            }
        } else {
            vc.addImgType = .Hotels
        }*/
        vc.addImgType = imageType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK:- UITableViewDelegate
extension AddInformationVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellAddInfo = tableView.dequeueReusableCell(withIdentifier: "CellAddInfo", for: indexPath) as! CellAddInfo
        cell.btnViewAll.tag = indexPath.row
        cell.btnAddImages.tag = indexPath.row
        cell.btnNext.isHidden = true
        cell.lblInfoType.text = "画像"
        if let layout = cell.collImages.collectionViewLayout as? UICollectionViewFlowLayout {
            //layout.scrollDirection = imageType == .Fish ? .horizontal : .vertical
            layout.scrollDirection = .vertical
        }
        cell.btnAddImages.setTitle("写真の投稿 +", for: .normal)
        cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(sender:)), for: .touchUpInside)
        cell.btnAddImages.addTarget(self, action: #selector(btnAddImage(sender:)), for: .touchUpInside)
        if imageType == .Restaurant {
            cell.imgCat.image = UIImage(named: "Restaurant_cat")
        } else if imageType == .Hotels {
            cell.imgCat.image = UIImage(named: "Hotel_cat")
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let profileCell = cell as? CellAddInfo else { return }
        profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let profileCell = cell as? CellTopAroundFish else { return }
        storedOffsets[indexPath.row] = profileCell.collectionViewOffset
        
    }
}

// MARK:- UICollectionViewDelegate
extension AddInformationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.viewComment.isHidden = true
        cell.heightViewComment.constant = 0
        cell.yImageName.constant = 0
        cell.heightImageName.constant = 0
        cell.heightImgFish.constant = 120
        
        let objImg: Images = arrImages[indexPath.row] as! Images
        cell.lblTitle.text = ""//objImg.imageName
        cell.lblTotalLikes.text = objImg.imageLikesCount
        cell.imgPost.sd_setImage(with: URL(string: objImg.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        if imageType == .ParkingSpots {
            cell.lblParkingCost.isHidden = false
            cell.viewSlots.isHidden = false
            cell.viewLike.isHidden = true
            cell.lblTotalSlots.text = "0 Slots" //"\(objImg?.spotSlots ?? "0") Slots"
            cell.lblParkingCost.text = "‎¥‎ 0" //"‎¥‎ \(objImg?.spotPrice ?? "0")"
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-78)/2, height:170)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objImg: Images = arrImages[indexPath.row] as! Images
        switch imageType {
        case .Restaurant, .Hotels:
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
            vc.imageType = imageType
            vc.sImageID = objImg.imageId ?? "0"
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
            vc.imageType = imageType
            vc.sImageID = objImg.imageId ?? "0"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}


// MARK:- API CALLING
extension AddInformationVC {
    func apiGetAllImage() {
        var sType = ""
        if imageType == .Fish || imageType == .TopTerrain {
            sType = "fish"
        } else if imageType == .Restaurant {
            sType = "restaurant"
        } else if imageType == .Hotels {
            sType = "hotel"
        } else if imageType == .ParkingSpots {
            sType = "parking"
        } else if imageType == .TopScene {
            sType = "scene"
        }
        let sURL = SERVER_URL + API_CREATE_IMAGES
        let dictParam: NSDictionary = ["image_type": sType]
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_CREATE_IMAGES, parameters: dictParam, method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                self.arrImages = NSMutableArray()
                let obj: FMAllImages = FMAllImages.init(object: responseObject ?? "")
                if obj.success == "1" {
                    if let arr = obj.images {
                        self.arrImages.addObjects(from: arr)
                    }
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
                self.tblAddInfo.reloadData()
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
}
