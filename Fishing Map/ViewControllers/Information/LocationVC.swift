//
//  LocationVC.swift
//  Fishing Map
//
//  Created by macOS on 08/11/20.
//

import UIKit

class CellLocationView: UITableViewCell {
    @IBOutlet weak var heightRentView: NSLayoutConstraint!
    @IBOutlet weak var topOfRentView: NSLayoutConstraint!
    @IBOutlet weak var btnLikes: UIButton!
    @IBOutlet weak var lblLocationDetails: UILabel!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblPerNight: UILabel!
    @IBOutlet var imgLocation: UIImageView!
}

class CellImagesTypeLocation: UITableViewCell {
    
    @IBOutlet weak var lblImagesType: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var collTypeImages: UICollectionView!
    @IBOutlet var btnAddImage: UIButton!
    @IBOutlet var viewNoRecord: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collTypeImages.register(UINib(nibName: "CellPost", bundle: nil), forCellWithReuseIdentifier: "CellPost")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
        layout.itemSize = CGSize(width: 148, height: 170)
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        collTypeImages!.collectionViewLayout = layout
    }
}

extension CellImagesTypeLocation {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collTypeImages.delegate = dataSourceDelegate
        collTypeImages.dataSource = dataSourceDelegate
        collTypeImages.tag = row
        collTypeImages.setContentOffset(collTypeImages.contentOffset, animated:false)
        collTypeImages.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collTypeImages.contentOffset.x = newValue }
        get { return collTypeImages.contentOffset.x }
    }
}

class LocationVC: UIViewController {
    
    @IBOutlet weak var tblLocationDetails: UITableView!
    @IBOutlet weak var lblSearchTitle: UILabel!
    @IBOutlet var viewBlank: UIView!
    
    var storedOffsets = [Int: CGFloat]()
    var arrImagesType = ["人気の風景", "人気の地形", "近くの駐車場", "人気の魚"]
    var arrImgType = ["img_lake", "", "", ""]
    var imgType = LocateImageType.TopScene
    var sLocationID: String?
    var objLocDetails: Location?
    
    var arrFIshImages = NSMutableArray()
    var arrTerrainImages = NSMutableArray()
    var arrParkingImages = NSMutableArray()
    var arrSceneImages = NSMutableArray()
    var arrRestaurantImages = NSMutableArray()
    var arrHotelImages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch imgType {
        case .Fish, .TopTerrain, .ParkingSpots, .TopScene:
            lblSearchTitle.text = "スポット"
        case .Restaurant:
            lblSearchTitle.text = "レストランの場所"
        case .Hotels:
            lblSearchTitle.text = "ホテルの場所"
        default:
            lblSearchTitle.text = ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if sLocationID != nil {
            apiGetLocationDetails()
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMore(_ sender: Any) {
        
        var sId = ""
        if sLocationID != nil {
            sId = sLocationID ?? ""
        } else {
            sId = objLocDetails?.locationId ?? ""
        }

        let actionSheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let reportActionButton = UIAlertAction(title: "報告書", style: .default)
        { _ in
            let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: "BlockReasonVC") as! BlockReasonVC
            vc.sType = "locations"
            vc.sID = sId
            vc.modalPresentationStyle = .overCurrentContext
            mostTopViewController?.present(vc, animated: false, completion: nil)
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            }, completion: nil)
        }
        actionSheet.addAction(reportActionButton)
        
        let cancelActionButton = UIAlertAction(title: "キャンセル", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheet.addAction(cancelActionButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func setLocDetails() {
        arrFIshImages = NSMutableArray()
        arrTerrainImages = NSMutableArray()
        arrParkingImages = NSMutableArray()
        arrSceneImages = NSMutableArray()
        arrRestaurantImages = NSMutableArray()
        arrHotelImages = NSMutableArray()
        
        if let arr = objLocDetails?.fishImages {
            arrFIshImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.terrainImages {
            arrTerrainImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.parkingSpots {
            arrParkingImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.sceneImages {
            arrSceneImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.restaurantImages {
            arrRestaurantImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.hotelImages {
            arrHotelImages.addObjects(from: arr)
        }
        tblLocationDetails.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.viewBlank.alpha = 0
        }
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension LocationVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
                return 4
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: CellLocationView = tableView.dequeueReusableCell(withIdentifier: "CellLocationView", for: indexPath) as! CellLocationView
            cell.lblLocationName.text = objLocDetails?.locationName
            cell.lblLocationDetails.text = objLocDetails?.locationDescription
            cell.lblLocationDetails.setLineSpacing(lineSpacing: 1.22, lineHeightMultiple: 1.22)
            cell.imgLocation.sd_setImage(with: URL(string: objLocDetails?.locationImageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            /*
            btnLikes.setTitle("\(objLocDetails?.imageLikesCount ?? 0)", for: .normal)
            btnLikes.isSelected = objLocDetails?.imageIsLiked ?? false*/
            if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
                cell.heightRentView.constant = 0
                cell.topOfRentView.constant = 0
            } else {
                cell.heightRentView.constant = 14
                cell.topOfRentView.constant = 14
                cell.lblPerNight.text  = (imgType == .Restaurant) ? " / ２人" : " / 夜"
                cell.lblPrice.text = "¥‎ \(objLocDetails?.locationPrice ?? "0")"
            }
            
            cell.btnLikes.addTarget(self, action: #selector(btnLikes(_:)), for: .touchUpInside)
            cell.btnLikes.setTitle("\(objLocDetails?.locationLikesCount ?? "0")", for: .normal)
            cell.btnLikes.isSelected = (objLocDetails?.isLiked ?? "0" == "1") ? true : false
            
            return cell
        default:
            let cell: CellImagesTypeLocation = tableView.dequeueReusableCell(withIdentifier: "CellImagesTypeLocation", for: indexPath) as! CellImagesTypeLocation
            if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
                cell.lblImagesType.text = arrImagesType[indexPath.row]
                switch cell.lblImagesType.text {
                case "人気の風景":
                    cell.viewNoRecord.alpha = arrSceneImages.count > 0 ? 0 : 1
                case "人気の地形":
                    cell.viewNoRecord.alpha = arrTerrainImages.count > 0 ? 0 : 1
                case "近くの駐車場":
                    cell.viewNoRecord.alpha = arrParkingImages.count > 0 ? 0 : 1
                case "人気の魚":
                    cell.viewNoRecord.alpha = arrFIshImages.count > 0 ? 0 : 1
                default:
                    break
                }
            } else if imgType == .Restaurant {
                cell.lblImagesType.text = "人気の写真"
                cell.viewNoRecord.alpha = arrRestaurantImages.count > 0 ? 0 : 1
            } else if imgType == .Hotels {
                cell.lblImagesType.text = "人気の写真"
                cell.viewNoRecord.alpha = arrHotelImages.count > 0 ? 0 : 1
            }
            cell.btnAddImage.addTarget(self, action: #selector(btnAddImage(_:)), for: .touchUpInside)
            cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            return
        default:
            guard let profileCell = cell as? CellImagesTypeLocation else { return }
            profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        default:
            if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
                var arrTmp = NSMutableArray()
                let sType = arrImagesType[indexPath.row]
                switch sType {
                case "人気の風景":
                    arrTmp = arrSceneImages
                case "人気の地形":
                    arrTmp = arrTerrainImages
                case "近くの駐車場":
                    arrTmp = arrParkingImages
                case "人気の魚":
                    arrTmp = arrFIshImages
                default:
                    return 0
                }
                if arrTmp.count > 2 {
                    return 435
                } else if arrTmp.count > 0 {
                    return 249
                } else {
                    return 185
                }
            } else {
                var arrData = NSMutableArray()
                if imgType == .Restaurant {
                    arrData = arrRestaurantImages
                } else if imgType == .Hotels {
                    arrData = arrHotelImages
                }
                if arrData.count > 0 {
                    return 628
                } else {
                    return 185
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            return
        default:
            guard let profileCell = cell as? CellFishImages else { return }
            storedOffsets[indexPath.row] = profileCell.collectionViewOffset
        }
    }
    
    @objc func btnLikes(_ sender: UIButton) {
        if objLocDetails?.isLiked ?? "0" == "1" {
            objLocDetails?.isLiked = "0"
            apiUnlikeLocation(objLocDetails?.locationId ?? "")
            objLocDetails?.locationLikesCount = "\((Int(objLocDetails?.locationLikesCount ?? "0") ?? 0) - 1)"
        } else {
            objLocDetails?.isLiked = "1"
            apiLikeLocation(objLocDetails?.locationId ?? "")
            objLocDetails?.locationLikesCount = "\((Int(objLocDetails?.locationLikesCount ?? "0") ?? 0) + 1)"
        }
        UIView.performWithoutAnimation {
            tblLocationDetails.reloadData()
        }
    }
    
    @objc func btnAddImage(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblLocationDetails)
        let indexPath: IndexPath = tblLocationDetails.indexPathForRow(at: buttonPosition)!
        
        let objLoc: Locations = Locations.init(object: objLocDetails?.dictionaryRepresentation() ?? NSDictionary())
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        vc.objLocation = objLoc
        
        if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
            let sType = arrImagesType[indexPath.row]
            switch sType {
            case "人気の風景":
                vc.addImgType = .TopScene
            case "人気の地形":
                vc.addImgType = .TopTerrain
            case "近くの駐車場":
                vc.addImgType = .ParkingSpots
            case "人気の魚":
                vc.addImgType = .Fish
            default:
                break
            }
        } else {
            vc.addImgType = imgType
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnViewAll(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblLocationDetails)
        let indexPath: IndexPath = tblLocationDetails.indexPathForRow(at: buttonPosition)!
        
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
        let objLoc: Locations = Locations.init(object: objLocDetails?.dictionaryRepresentation() ?? NSDictionary())
        vc.objLocation = objLoc
        vc.isAddImage = true
        
        if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
            let sType = arrImagesType[indexPath.row]
            switch sType {
            case "人気の風景":
                vc.imageType = .TopScene
            case "人気の地形":
                vc.imageType = .TopTerrain
            case "近くの駐車場":
                vc.imageType = .ParkingSpots
            case "人気の魚":
                vc.imageType = .Fish
            default:
                break
            }
        } else {
            vc.imageType = imgType
        }
        vc.sLocationID = objLocDetails?.locationId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UICOLLECTIONVIEW DELEGATE
extension LocationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
            switch collectionView.tag {
            case 0:
                return (arrSceneImages.count > 4) ? 4 : arrSceneImages.count
            case 1:
                return (arrTerrainImages.count > 4) ? 4 : arrTerrainImages.count
            case 2:
                return (arrParkingImages.count > 4) ? 4 : arrParkingImages.count
            case 3:
                return (arrFIshImages.count > 4) ? 4 : arrFIshImages.count
            default:
                return 0
            }
        } else if imgType == .Restaurant {
            return arrRestaurantImages.count
        } else if imgType == .Hotels {
            return arrHotelImages.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.viewComment.isHidden = true
        var objImg: Images?
        if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
            cell.lblParkingCost.isHidden = true
            cell.viewSlots.isHidden = true
            cell.viewLike.isHidden = false
            switch collectionView.tag {
            case 0:
                objImg = arrSceneImages.object(at: indexPath.row) as? Images
            case 1:
                objImg = arrTerrainImages.object(at: indexPath.row) as? Images
            case 2:
                let objPImg = arrParkingImages.object(at: indexPath.row) as? ImageParking
                cell.lblParkingCost.isHidden = false
                cell.viewSlots.isHidden = false
                cell.viewLike.isHidden = true
                cell.imgPost.sd_setImage(with: URL(string: objPImg?.spotUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
                //cell.lblTitle.text = objPImg?.spotName
                cell.yImageName.constant = 0
                cell.heightImageName.constant = 0
                cell.heightImgFish.constant = 120
                cell.lblTitle.text = ""//objImg?.imageName
                cell.lblTotalSlots.text = "\(objPImg?.spotSlots ?? "0") Slots"
                cell.lblParkingCost.text = "‎¥‎ \(objPImg?.spotPrice ?? "0")"
                return cell
            case 3:
                objImg = arrFIshImages.object(at: indexPath.row) as? Images
            default:
                break
            }
        } else if imgType == .Restaurant {
            objImg = arrRestaurantImages.object(at: indexPath.row) as? Images
        } else if imgType == .Hotels {
            objImg = arrHotelImages.object(at: indexPath.row) as? Images
        }

        cell.imgPost.sd_setImage(with: URL(string: objImg?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        cell.yImageName.constant = 0
        cell.heightImageName.constant = 0
        cell.heightImgFish.constant = 120
        cell.lblTitle.text = ""//objImg?.imageName
        cell.lblTotalLikes.text = objImg?.imageLikesCount
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-78)/2, height:170)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if imgType == .Fish || imgType == .TopTerrain || imgType == .ParkingSpots || imgType == .TopScene {
            var objImg: Images?
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
            switch collectionView.tag {
            case 0:
                vc.imageType = .TopScene
                objImg = arrSceneImages.object(at: indexPath.row) as? Images
            case 1:
                vc.imageType = .TopTerrain
                objImg = arrTerrainImages.object(at: indexPath.row) as? Images
            case 2:
                let objPImg = arrParkingImages.object(at: indexPath.row) as? ImageParking
                vc.imageType = .ParkingSpots
                vc.sImageID = objPImg?.spotId
                self.navigationController?.pushViewController(vc, animated: true)
                return
            case 3:
                vc.imageType = .Fish
                objImg = arrFIshImages.object(at: indexPath.row) as? Images
            default:
                break
            }
            vc.sImageID = objImg?.imageId
            self.navigationController?.pushViewController(vc, animated: true)
        } else if imgType == .Restaurant ||  imgType == .Hotels {
            var objImg: Images?
            if imgType == .Restaurant {
                objImg = arrRestaurantImages.object(at: indexPath.row) as? Images
            } else {
                objImg = arrHotelImages.object(at: indexPath.row) as? Images
            }
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "HotelImageDetailsVC") as! HotelImageDetailsVC
            vc.imageType = imgType
            vc.sImageID = objImg?.imageId ?? "0"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- API CALLING
extension LocationVC {
    func apiGetLocationDetails() {
        let showLoader: Bool = (viewBlank.alpha == 1) ? true : false
        let sURL = SERVER_URL + "locations/\(sLocationID ?? "0")/\(getLocType())"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: getLocType(), parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: showLoader) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                let obj: FMLocationDetails = FMLocationDetails.init(object: responseObject ?? "")
                if obj.success == "1" {
                    self.objLocDetails = obj.location
                    self.setLocDetails()
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func getLocType() -> String {
        switch imgType {
        case .Fish, .TopTerrain, .ParkingSpots, .TopScene:
            return "fish_location"
        case .Restaurant:
            return "restaurant_location"
        case .Hotels:
            return "hotel_location"
        default:
            return ""
        }
    }
}
