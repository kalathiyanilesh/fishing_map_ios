//
//  HotelImageDetailsVC.swift
//  Fishing Map
//
//  Created by macOS on 12/11/20.
//

import UIKit
import GoogleMaps

class HotelImageDetailsVC: UIViewController {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var imgCover: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblPerNight: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var viewBlank: UIView!
    
    var imageType = LocateImageType.Hotels
    var sImageID: String?
    var objImageDetails: Image?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "ホテルの写真"
        lblPerNight.text = " / 夜"
        if imageType == .Restaurant {
            lblTitle.text = "レストランの写真情報"
            lblPerNight.text = " / ２人"
        }
        if sImageID != nil {
            apiGetImageDetails()
        } else {
            viewBlank.alpha = 0
        }
    }
    
    func setImageDetails() {
        let lat: Double = Double(objImageDetails?.imageLat ?? "0") ?? 0
        let lng: Double = Double(objImageDetails?.imageLng ?? "0") ?? 0
        if lat == 0 && lng == 0 {
        } else {
            mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16.0)
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
            marker.icon = UIImage(named: getPinName(imageType))
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.map = mapView
        }
        mapView.padding = UIEdgeInsets(top: 25, left: 0, bottom: 25, right: 0)
        
        imgCover.sd_setImage(with: URL(string: objImageDetails?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblName.text = objImageDetails?.imageName
        lblDescription.text = objImageDetails?.imageDescription
        lblDescription.setLineSpacing(lineSpacing: 1.22, lineHeightMultiple: 1.22)
        btnLike.setTitle("\(objImageDetails?.imageLikesCount ?? "0")", for: .normal)
        btnLike.isSelected = ((objImageDetails?.imageIsLiked ?? "0") == "1") ? true : false
        lblPrice.text = "¥‎ \(objImageDetails?.locationPrice ?? "0")"
        if objImageDetails?.locationPrice?.count ?? 0 <= 0 {
            lblPrice.text = "¥‎ 0"
        }
        
        UIView.animate(withDuration: 0.3) {
            self.viewBlank.alpha = 0
        }
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLike(_ sender: Any) {
        guard let sID = sImageID else { return }
        if objImageDetails?.imageIsLiked ?? "0" == "1" {
            objImageDetails?.imageIsLiked = "0"
            apiUnlikeImage(sID, type: getImageType())
            objImageDetails?.imageLikesCount = "\((Int(objImageDetails?.imageLikesCount ?? "0") ?? 0) - 1)"
        } else {
            objImageDetails?.imageIsLiked = "1"
            apiLikeImage(sID, type: getImageType())
            objImageDetails?.imageLikesCount = "\((Int(objImageDetails?.imageLikesCount ?? "0") ?? 0) + 1)"
        }
        btnLike.setTitle("\(objImageDetails?.imageLikesCount ?? "0")", for: .normal)
        btnLike.isSelected = ((objImageDetails?.imageIsLiked ?? "0") == "1") ? true : false
    }
    
    @IBAction func btnViewDetailsTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
        vc.imgType = imageType
        vc.sLocationID = objImageDetails?.locationId ?? nil
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnAddInfoTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Information, strVCId: "AddInformationVC") as! AddInformationVC
        vc.imageType = imageType
        vc.sLocationID = objImageDetails?.locationId ?? nil
        vc.sLocationName = objImageDetails?.locationName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCoverLoc(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            if let url = URL(string: "comgooglemaps-x-callback://?saddr=\(lat_currnt),\(long_currnt)&daddr=\(objImageDetails?.imageLat ?? ""),\(objImageDetails?.imageLng ?? "")&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }}
        else {
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=\(lat_currnt),\(long_currnt)&daddr=\(objImageDetails?.imageLat ?? ""),\(objImageDetails?.imageLng ?? "")&directionsmode=driving") {
                UIApplication.shared.open(urlDestination)
            }
        }
    }
}

//MARK:- API CALLING
extension HotelImageDetailsVC {
    func apiGetImageDetails() {
        let dictParam: NSDictionary = ["image_type": getImageType()]
        let sURL = SERVER_URL + "images/\(sImageID ?? "0")/" + API_SHOW_IMAGE
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_SHOW_IMAGE, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                let obj: FMImageDetails = FMImageDetails.init(object: responseObject ?? "")
                if obj.success ?? false {
                    self.objImageDetails = obj.image
                    self.setImageDetails()
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func getImageType() -> String {
        switch imageType {
        case .Hotels:
            return "hotel"
        case .Restaurant:
            return "restaurant"
        default:
            return ""
        }
    }
}
