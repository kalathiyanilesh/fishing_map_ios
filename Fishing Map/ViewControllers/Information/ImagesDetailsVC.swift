//
//  ImagesDetailsVC.swift
//  Fishing Map
//
//  Created by macOS on 08/11/20.
//

import UIKit
import GoogleMaps

class ImagesDetailsVC: UIViewController {

    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var viewContentBottom: UIView!
    @IBOutlet weak var lblNavigationTitle: UILabel!
    @IBOutlet weak var lblImageSceneName: UILabel!
    @IBOutlet weak var viewForParking: UIView!
    @IBOutlet weak var btnLikes: UIButton!
    @IBOutlet weak var btnSlots: UIButton!
    
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblImageName: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgCover: UIImageView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblBeachParkingAvail: UILabel!
    
    @IBOutlet var viewBlank: UIView!
    @IBOutlet var lblParkingCounter: UILabel!
    @IBOutlet var viewParkingPin: UIView!
    
    var imageType = LocateImageType.Fish
    var sImageID: String?
    var objImageDetails: Image?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        self.viewContentBottom.clipsToBounds = true
        self.viewContentBottom.layer.cornerRadius = 16
        if #available(iOS 11.0, *) {
            self.viewContentBottom.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        switch imageType {
        case .Fish:
            self.lblNavigationTitle.text = "魚の写真の情報"
            self.lblImageSceneName.text = "Image 1"
        case .TopTerrain:
            self.lblNavigationTitle.text = "写真の詳細"
            self.lblImageSceneName.text = "Image 1"
        case .TopScene:
            self.lblNavigationTitle.text = "風景の写真情報"
            self.lblImageSceneName.text = "風景 1"
        case .ParkingSpots:
            self.lblNavigationTitle.text = "駐車場の情報"
            self.lblImageSceneName.text = "駐車場 1"
            self.viewForParking.isHidden = false
            self.btnSlots.isHidden = false
            self.btnLikes.isHidden = true
            self.btnSlots.setTitle("0 Slots", for: .normal)
        default:
            break
        }
        
        if sImageID != nil {
            apiGetImageDetails()
        } else {
            viewBlank.alpha = 0
        }
    }
    
    func setImageDetails() {
        let arrParkingSpots = NSMutableArray()
        for objPark in objImageDetails?.parkingSpots ?? [] {
            arrParkingSpots.add(objPark)
        }
        
        if Float(objImageDetails?.imageLat ?? "0") == 0 && Float(objImageDetails?.imageLng ?? "0") == 0 {
        } else {
            let dict: NSDictionary = ["lat":objImageDetails?.imageLat ?? "0", "lng":objImageDetails?.imageLng ?? "0", "spots_counter":"-1"]
            let objParking: ParkingSpot = ParkingSpot.init(object: dict)
            arrParkingSpots.add(objParking)
        }
        
        if arrParkingSpots.count > 0 {
            setParkingMarkers(arrParkingSpots)
        } else {
            let lat: Double = Double(objImageDetails?.imageLat ?? "0") ?? 0
            let lng: Double = Double(objImageDetails?.imageLng ?? "0") ?? 0
            if lat == 0 && lng == 0 {
            } else {
                viewMap.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16.0)
                let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
                marker.icon = UIImage(named: getPinName(imageType))
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                marker.map = viewMap
            }
        }

        viewMap.padding = UIEdgeInsets(top: 25, left: 0, bottom: 25, right: 0)
        
        lblLocation.text = objImageDetails?.locationName
        lblUserName.text = objImageDetails?.userName
        imgUser.sd_setImage(with: URL(string: objImageDetails?.userAvatar ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        imgCover.sd_setImage(with: URL(string: objImageDetails?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
        lblImageName.text = objImageDetails?.imageName
        lblDescription.text = objImageDetails?.imageDescription
        lblDescription.setLineSpacing(lineSpacing: 1.22, lineHeightMultiple: 1.22)
        btnLikes.setTitle("\(objImageDetails?.imageLikesCount ?? "0")", for: .normal)
        btnLikes.isSelected = ((objImageDetails?.imageIsLiked ?? "0") == "1") ? true : false
        lblDate.text = "にアップロード \(intToDate(miliseconds: Int(objImageDetails?.createdAt ?? "0") ?? 0, shortMonthName: false))"
        
        if imageType == .ParkingSpots {
            if objImageDetails?.hasBeachParking ?? "0" == "1" {
                lblBeachParkingAvail.text = "Available"
                lblBeachParkingAvail.textColor = Color_Hex(hex: "109080")
            } else {
                lblBeachParkingAvail.text = "Not Available"
                lblBeachParkingAvail.textColor = Color_Hex(hex: "E82828")
            }
            self.btnSlots.setTitle("\(objImageDetails?.parkingSpotsCounter ?? "0") Slots", for: .normal)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.viewBlank.alpha = 0
        }
        
        let imgG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        imgUser.isUserInteractionEnabled = true
        imgUser.addGestureRecognizer(imgG)
        
        let lblG = UITapGestureRecognizer(target: self, action: #selector(profileTapped(_:)))
        lblUserName.isUserInteractionEnabled = true
        lblUserName.addGestureRecognizer(lblG)
    }
    
    func setParkingMarkers(_ arrParkingSpots: NSMutableArray) {
        viewMap.clear()
        var bounds = GMSCoordinateBounds()
        for i in 0..<arrParkingSpots.count {
            let objLoc: ParkingSpot = arrParkingSpots.object(at: i) as! ParkingSpot
            var imgPin = UIImage()
            if objLoc.spotsCounter == "-1" {
                imgPin = UIImage(named: getPinName(imageType)) ?? UIImage()
            } else {
                lblParkingCounter.text = objLoc.spotsCounter
                imgPin = viewParkingPin.takeScreenshot()
            }
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: Double(objLoc.lat ?? "0") ?? 0, longitude: Double(objLoc.lng ?? "0") ?? 0)
            marker.icon = imgPin
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.map = viewMap
            bounds = bounds.includingCoordinate(marker.position)
        }
        viewMap.setMinZoom(1, maxZoom: 15)
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        viewMap.animate(with: update)
        viewMap.setMinZoom(1, maxZoom: 20)
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCoverLoc(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            if let url = URL(string: "comgooglemaps-x-callback://?saddr=\(lat_currnt),\(long_currnt)&daddr=\(objImageDetails?.imageLat ?? ""),\(objImageDetails?.imageLng ?? "")&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }}
        else {
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=\(lat_currnt),\(long_currnt)&daddr=\(objImageDetails?.imageLat ?? ""),\(objImageDetails?.imageLng ?? "")&directionsmode=driving") {
                UIApplication.shared.open(urlDestination)
            }
        }
    }
    
    @IBAction func btnLike(_ sender: UIButton) {
        guard let sID = sImageID else { return }
        if objImageDetails?.imageIsLiked ?? "0" == "1" {
            objImageDetails?.imageIsLiked = "0"
            apiUnlikeImage(sID, type: getImageType())
            objImageDetails?.imageLikesCount = "\((Int(objImageDetails?.imageLikesCount ?? "0") ?? 0) - 1)"
        } else {
            objImageDetails?.imageIsLiked = "1"
            apiLikeImage(sID, type: getImageType())
            objImageDetails?.imageLikesCount = "\((Int(objImageDetails?.imageLikesCount ?? "0") ?? 0) + 1)"
        }
        btnLikes.setTitle("\(objImageDetails?.imageLikesCount ?? "0")", for: .normal)
        btnLikes.isSelected = ((objImageDetails?.imageIsLiked ?? "0") == "1") ? true : false
    }
    
    @IBAction func btnViewMoreTapped(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Information, strVCId: "LocationVC") as! LocationVC
        vc.imgType = imageType
        vc.sLocationID = objImageDetails?.locationId ?? nil
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnAddInfoTapped(_ sender: UIButton) {
        //let vc = loadVC(strStoryboardId: SB_Information, strVCId: "AddInformationVC") as! AddInformationVC
        let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesListVC") as! ImagesListVC
        vc.imageType = imageType
        vc.sLocationID = objImageDetails?.locationId ?? nil
        vc.sLocationName = objImageDetails?.locationName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func profileTapped(_ gesture: UITapGestureRecognizer) {
        if objImageDetails == nil { return }
        let vc = loadVC(strStoryboardId: SB_PROFILE, strVCId: idProfileVC) as! ProfileVC
        vc.sUserID = objImageDetails?.userId
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API CALLING
extension ImagesDetailsVC {
    func apiGetImageDetails() {
        let dictParam: NSDictionary = ["image_type": getImageType()]
        let sURL = SERVER_URL + "images/\(sImageID ?? "0")/" + API_SHOW_IMAGE
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_SHOW_IMAGE, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                let obj: FMImageDetails = FMImageDetails.init(object: responseObject ?? "")
                if obj.success ?? false {
                    self.objImageDetails = obj.image
                    self.setImageDetails()
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func getImageType() -> String {
        switch imageType {
        case .Fish:
            return "fish"
        case .TopTerrain:
            return "terrain"
        case .ParkingSpots:
            return "parking"
        case .TopScene:
            return "scene"
        default:
            return ""
        }
    }
}
