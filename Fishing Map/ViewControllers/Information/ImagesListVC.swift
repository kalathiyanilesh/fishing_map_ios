//
//  ImagesListVC.swift
//  Fishing Map
//
//  Created by iMac on 04/05/21.
//

import UIKit

class ImagesListVC: UIViewController {
    
    @IBOutlet var tblImages: UITableView!
    var storedOffsets = [Int: CGFloat]()
    var imageType = LocateImageType.TopScene
    var sLocationID: String?
    var sLocationName: String?
    var objLocDetails: Location?
    
    var arrFIshImages = NSMutableArray()
    var arrTerrainImages = NSMutableArray()
    var arrParkingImages = NSMutableArray()
    var arrSceneImages = NSMutableArray()
    var isFishImages: Bool = false
//    var arrRestaurantImages = NSMutableArray()
//    var arrHotelImages = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        apiGetLocationDetails()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnViewAll(sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_LOCATE, strVCId: "ViewAllFishImagesVC") as! ViewAllFishImagesVC
        let objLoc: Locations = Locations.init(object: objLocDetails?.dictionaryRepresentation() ?? NSDictionary())
        vc.objLocation = objLoc
        vc.isAddImage = true
        vc.sLocationID = objLocDetails?.locationId
        switch sender.tag {
        case 0:
            vc.imageType = .Fish
        case 1:
            vc.imageType = .TopTerrain
        case 2:
            vc.imageType = .TopScene
        default:
            vc.imageType = .ParkingSpots
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnAddImage(sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_FEED, strVCId: "AddPostVC") as! AddPostVC
        vc.sLocationID = sLocationID
        vc.sLocationName = sLocationName ?? ""
        switch sender.tag {
        case 0:
            vc.addImgType = .Fish
        case 1:
            vc.addImgType = .TopTerrain
        case 2:
            vc.addImgType = .TopScene
        default:
            vc.addImgType = .ParkingSpots
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

// MARK:- UITableViewDelegate
extension ImagesListVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellAddInfo = tableView.dequeueReusableCell(withIdentifier: "CellAddInfo", for: indexPath) as! CellAddInfo
        
        cell.btnViewAll.tag = indexPath.row
        cell.btnAddImages.tag = indexPath.row
        cell.btnAddImageSecond.tag = indexPath.row
        if let layout = cell.collImages.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        cell.btnAddImages.setTitle("写真の投稿 +", for: .normal)
        cell.btnViewAll.addTarget(self, action: #selector(btnViewAll(sender:)), for: .touchUpInside)
        cell.btnAddImages.addTarget(self, action: #selector(btnAddImage(sender:)), for: .touchUpInside)
        cell.btnAddImageSecond.addTarget(self, action: #selector(btnAddImage(sender:)), for: .touchUpInside)
        cell.viewDashed.addDashedBorder()
        
        switch indexPath.row {
        case 0:
            cell.lblInfoType.text = "魚の写真"
            cell.lblBeFirstTitle.text = "魚画像の投稿"
            cell.imgCat.image = UIImage(named: "Fish_cat")
            cell.viewBeFirst.alpha = arrFIshImages.count > 0 ? 0 : 1
        case 1:
            cell.lblInfoType.text = "地形"
            cell.lblBeFirstTitle.text = "地形画像の投稿"
            cell.imgCat.image = UIImage(named: "Terrain_cat")
            cell.viewBeFirst.alpha = arrTerrainImages.count > 0 ? 0 : 1
        case 2:
            cell.lblInfoType.text = "風景"
            cell.lblBeFirstTitle.text = "風景画像の投稿"
            cell.imgCat.image = UIImage(named: "Scene_cat")
            cell.viewBeFirst.alpha = arrSceneImages.count > 0 ? 0 : 1
        default:
            cell.lblInfoType.text = "駐車場"
            cell.lblBeFirstTitle.text = "駐車場画像の投稿"
            cell.imgCat.image = UIImage(named: "Parking_cat")
            cell.viewBeFirst.alpha = arrParkingImages.count > 0 ? 0 : 1
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let profileCell = cell as? CellAddInfo else { return }
        profileCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        profileCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 259+15
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let profileCell = cell as? CellTopAroundFish else { return }
        storedOffsets[indexPath.row] = profileCell.collectionViewOffset
        
    }
}

// MARK:- UICollectionViewDelegate
extension ImagesListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return arrFIshImages.count
        case 1:
            return arrTerrainImages.count
        case 2:
            return arrSceneImages.count
        default:
            return arrParkingImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPost = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPost", for: indexPath) as! CellPost
        cell.viewComment.isHidden = true
        cell.heightViewComment.constant = 0
        cell.yImageName.constant = 0
        cell.heightImageName.constant = 0
        cell.heightImgFish.constant = (imageType == .ParkingSpots) ? 66+15 : 88+15
        cell.lblTitle.text = ""
        
        var objImg: Images?
        switch collectionView.tag {
        case 0:
            objImg = arrFIshImages.object(at: indexPath.row) as? Images
        case 1:
            objImg = arrTerrainImages.object(at: indexPath.row) as? Images
        case 2:
            objImg = arrSceneImages.object(at: indexPath.row) as? Images
        default:
            let objPImg = arrParkingImages.object(at: indexPath.row) as? ImageParking
            cell.lblParkingCost.isHidden = false
            cell.viewSlots.isHidden = false
            cell.viewLike.isHidden = true
            cell.imgPost.sd_setImage(with: URL(string: objPImg?.spotUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)
            cell.lblTotalSlots.text = "\(objPImg?.spotSlots ?? "0") Slots"
            cell.lblParkingCost.text = "" //"‎¥‎ \(objPImg?.spotPrice ?? "0")"
            return cell
        }
        
        cell.lblTotalLikes.text = objImg?.imageLikesCount
        cell.imgPost.sd_setImage(with: URL(string: objImg?.imageUrl ?? ""), placeholderImage: PLACE_HOLDER, options: .progressiveLoad, context: nil)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: (SCREENWIDTH()-78)/2, height:170)
        return CGSize(width: 101, height:130+15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 3 {
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
            let objPImg = arrParkingImages.object(at: indexPath.row) as? ImageParking
            vc.imageType = .ParkingSpots
            vc.sImageID = objPImg?.spotId
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            var objImg: Images?
            switch collectionView.tag {
            case 0:
                objImg = arrFIshImages.object(at: indexPath.row) as? Images
            case 1:
                objImg = arrTerrainImages.object(at: indexPath.row) as? Images
            case 2:
                objImg = arrSceneImages.object(at: indexPath.row) as? Images
            default:
                break
            }
            let vc = loadVC(strStoryboardId: SB_Information, strVCId: "ImagesDetailsVC") as! ImagesDetailsVC
            vc.imageType = imageType
            vc.sImageID = objImg?.imageId ?? "0"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}


// MARK:- API CALLING
extension ImagesListVC {
    func apiGetLocationDetails() {
        let sURL = SERVER_URL + "locations/\(sLocationID ?? "0")/\(getLocType())"
        HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: getLocType(), parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
            if error == nil {
                print(responseObject ?? "")
                let obj: FMLocationDetails = FMLocationDetails.init(object: responseObject ?? "")
                if obj.success == "1" {
                    self.objLocDetails = obj.location
                    self.setLocDetails()
                } else {
                    showMessage(obj.message ?? ErrorMSG)
                }
            } else {
                showMessage(error?.localizedDescription ?? ErrorMSG)
            }
        }
    }
    
    func getLocType() -> String {
        switch imageType {
        case .Fish, .TopTerrain, .ParkingSpots, .TopScene:
            return "fish_location"
        case .Restaurant:
            return "restaurant_location"
        case .Hotels:
            return "hotel_location"
        default:
            return ""
        }
    }
    
    func setLocDetails() {
        arrFIshImages = NSMutableArray()
        arrTerrainImages = NSMutableArray()
        arrParkingImages = NSMutableArray()
        arrSceneImages = NSMutableArray()
//        arrRestaurantImages = NSMutableArray()
//        arrHotelImages = NSMutableArray()
        
        if let arr = objLocDetails?.fishImages {
            arrFIshImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.terrainImages {
            arrTerrainImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.parkingSpots {
            arrParkingImages.addObjects(from: arr)
        }
        if let arr = objLocDetails?.sceneImages {
            arrSceneImages.addObjects(from: arr)
        }
//        if let arr = objLocDetails?.restaurantImages {
//            arrRestaurantImages.addObjects(from: arr)
//        }
//        if let arr = objLocDetails?.hotelImages {
//            arrHotelImages.addObjects(from: arr)
//        }
        tblImages.reloadData()
    }
}
