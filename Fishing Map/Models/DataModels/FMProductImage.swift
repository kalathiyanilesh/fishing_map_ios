//
//  FMProductImage.swift
//
//  Created by iMac on 16/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMProductImage: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pagination = "pagination"
    static let errorcode = "errorcode"
    static let message = "message"
    static let productImages = "product_images"
    static let success = "success"
  }

  // MARK: Properties
  public var pagination: Pagination?
  public var errorcode: String?
  public var message: String?
  public var productImages: [ProductImages]?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    pagination = Pagination(json: json[SerializationKeys.pagination])
    errorcode = json[SerializationKeys.errorcode].string
    message = json[SerializationKeys.message].string
    if let items = json[SerializationKeys.productImages].array { productImages = items.map { ProductImages(json: $0) } }
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = productImages { dictionary[SerializationKeys.productImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.productImages = aDecoder.decodeObject(forKey: SerializationKeys.productImages) as? [ProductImages]
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(productImages, forKey: SerializationKeys.productImages)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}

public final class ProductImages: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userAvatarUrl = "user_avatar_url"
    static let productId = "product_id"
    static let productImageId = "product_image_id"
    static let productImageDate = "product_image_date"
    static let productImageLng = "product_image_lng"
    static let productImageCreatedAt = "product_image_created_at"
    static let productImageLat = "product_image_lat"
    static let productShopUrl = "product_shop_url"
    static let productImageImageUrl = "product_image_image_url"
    static let productName = "product_name"
    static let userId = "user_id"
    static let userName = "user_name"
  }

  // MARK: Properties
  public var userAvatarUrl: String?
  public var productId: String?
  public var productImageId: String?
  public var productImageDate: String?
  public var productImageLng: String?
  public var productImageCreatedAt: String?
  public var productImageLat: String?
  public var productShopUrl: String?
  public var productImageImageUrl: String?
  public var productName: String?
  public var userId: String?
  public var userName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    userAvatarUrl = json[SerializationKeys.userAvatarUrl].string
    productId = json[SerializationKeys.productId].string
    productImageId = json[SerializationKeys.productImageId].string
    productImageDate = json[SerializationKeys.productImageDate].string
    productImageLng = json[SerializationKeys.productImageLng].string
    productImageCreatedAt = json[SerializationKeys.productImageCreatedAt].string
    productImageLat = json[SerializationKeys.productImageLat].string
    productShopUrl = json[SerializationKeys.productShopUrl].string
    productImageImageUrl = json[SerializationKeys.productImageImageUrl].string
    productName = json[SerializationKeys.productName].string
    userId = json[SerializationKeys.userId].string
    userName = json[SerializationKeys.userName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userAvatarUrl { dictionary[SerializationKeys.userAvatarUrl] = value }
    if let value = productId { dictionary[SerializationKeys.productId] = value }
    if let value = productImageId { dictionary[SerializationKeys.productImageId] = value }
    if let value = productImageDate { dictionary[SerializationKeys.productImageDate] = value }
    if let value = productImageLng { dictionary[SerializationKeys.productImageLng] = value }
    if let value = productImageCreatedAt { dictionary[SerializationKeys.productImageCreatedAt] = value }
    if let value = productImageLat { dictionary[SerializationKeys.productImageLat] = value }
    if let value = productShopUrl { dictionary[SerializationKeys.productShopUrl] = value }
    if let value = productImageImageUrl { dictionary[SerializationKeys.productImageImageUrl] = value }
    if let value = productName { dictionary[SerializationKeys.productName] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = userName { dictionary[SerializationKeys.userName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.userAvatarUrl = aDecoder.decodeObject(forKey: SerializationKeys.userAvatarUrl) as? String
    self.productId = aDecoder.decodeObject(forKey: SerializationKeys.productId) as? String
    self.productImageId = aDecoder.decodeObject(forKey: SerializationKeys.productImageId) as? String
    self.productImageDate = aDecoder.decodeObject(forKey: SerializationKeys.productImageDate) as? String
    self.productImageLng = aDecoder.decodeObject(forKey: SerializationKeys.productImageLng) as? String
    self.productImageCreatedAt = aDecoder.decodeObject(forKey: SerializationKeys.productImageCreatedAt) as? String
    self.productImageLat = aDecoder.decodeObject(forKey: SerializationKeys.productImageLat) as? String
    self.productShopUrl = aDecoder.decodeObject(forKey: SerializationKeys.productShopUrl) as? String
    self.productImageImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.productImageImageUrl) as? String
    self.productName = aDecoder.decodeObject(forKey: SerializationKeys.productName) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
    self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(userAvatarUrl, forKey: SerializationKeys.userAvatarUrl)
    aCoder.encode(productId, forKey: SerializationKeys.productId)
    aCoder.encode(productImageId, forKey: SerializationKeys.productImageId)
    aCoder.encode(productImageDate, forKey: SerializationKeys.productImageDate)
    aCoder.encode(productImageLng, forKey: SerializationKeys.productImageLng)
    aCoder.encode(productImageCreatedAt, forKey: SerializationKeys.productImageCreatedAt)
    aCoder.encode(productImageLat, forKey: SerializationKeys.productImageLat)
    aCoder.encode(productShopUrl, forKey: SerializationKeys.productShopUrl)
    aCoder.encode(productImageImageUrl, forKey: SerializationKeys.productImageImageUrl)
    aCoder.encode(productName, forKey: SerializationKeys.productName)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(userName, forKey: SerializationKeys.userName)
  }

}
