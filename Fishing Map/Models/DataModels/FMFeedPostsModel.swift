//
//  FMFeedPostsModel.swift
//  Fishing Map
//
//  Created by macOS on 26/11/20.
//

import Foundation
import SwiftyJSON

public final class FMFeedPosts: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let errorcode = "errorcode"
        static let posts = "posts"
        static let pagination = "pagination"
        static let message = "message"
        static let success = "success"
    }
    
    // MARK: Properties
    public var errorcode: Int?
    public var posts: [Posts]?
    public var pagination: Pagination?
    public var message: String?
    public var success: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        errorcode = json[SerializationKeys.errorcode].int
        if let items = json[SerializationKeys.posts].array { posts = items.map { Posts(json: $0) } }
        pagination = Pagination(json: json[SerializationKeys.pagination])
        message = json[SerializationKeys.message].string
        success = json[SerializationKeys.success].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
        if let value = posts { dictionary[SerializationKeys.posts] = value.map { $0.dictionaryRepresentation() } }
        if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
        if let value = message { dictionary[SerializationKeys.message] = value }
        dictionary[SerializationKeys.success] = success
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
        self.posts = aDecoder.decodeObject(forKey: SerializationKeys.posts) as? [Posts]
        self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
        self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
        self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
        aCoder.encode(posts, forKey: SerializationKeys.posts)
        aCoder.encode(pagination, forKey: SerializationKeys.pagination)
        aCoder.encode(message, forKey: SerializationKeys.message)
        aCoder.encode(success, forKey: SerializationKeys.success)
    }
    
}

public final class Pagination: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let lastPage = "last_page?"
        static let totalCount = "total_count"
        static let totalPages = "total_pages"
        static let outOfRange = "out_of_range?"
        static let currentPage = "current_page"
        static let nextPage = "next_page"
        static let firstPage = "first_page?"
    }
    
    // MARK: Properties
    public var lastPage: Bool? = false
    public var totalCount: Int?
    public var totalPages: Int?
    public var outOfRange: Bool? = false
    public var currentPage: Int?
    public var nextPage: Int?
    public var firstPage: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        lastPage? = json[SerializationKeys.lastPage].boolValue
        totalCount = json[SerializationKeys.totalCount].int
        totalPages = json[SerializationKeys.totalPages].int
        outOfRange? = json[SerializationKeys.outOfRange].boolValue
        currentPage = json[SerializationKeys.currentPage].int
        nextPage = json[SerializationKeys.nextPage].int
        firstPage? = json[SerializationKeys.firstPage].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.lastPage] = lastPage
        if let value = totalCount { dictionary[SerializationKeys.totalCount] = value }
        if let value = totalPages { dictionary[SerializationKeys.totalPages] = value }
        dictionary[SerializationKeys.outOfRange] = outOfRange
        if let value = currentPage { dictionary[SerializationKeys.currentPage] = value }
        if let value = nextPage { dictionary[SerializationKeys.nextPage] = value }
        dictionary[SerializationKeys.firstPage] = firstPage
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.lastPage? = aDecoder.decodeBool(forKey: SerializationKeys.lastPage)
        self.totalCount = aDecoder.decodeObject(forKey: SerializationKeys.totalCount) as? Int
        self.totalPages = aDecoder.decodeObject(forKey: SerializationKeys.totalPages) as? Int
        self.outOfRange? = aDecoder.decodeBool(forKey: SerializationKeys.outOfRange)
        self.currentPage = aDecoder.decodeObject(forKey: SerializationKeys.currentPage) as? Int
        self.nextPage = aDecoder.decodeObject(forKey: SerializationKeys.nextPage) as? Int
        self.firstPage? = aDecoder.decodeBool(forKey: SerializationKeys.firstPage)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(lastPage, forKey: SerializationKeys.lastPage)
        aCoder.encode(totalCount, forKey: SerializationKeys.totalCount)
        aCoder.encode(totalPages, forKey: SerializationKeys.totalPages)
        aCoder.encode(outOfRange, forKey: SerializationKeys.outOfRange)
        aCoder.encode(currentPage, forKey: SerializationKeys.currentPage)
        aCoder.encode(nextPage, forKey: SerializationKeys.nextPage)
        aCoder.encode(firstPage, forKey: SerializationKeys.firstPage)
    }
    
}

public final class PostComments: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let isViewerAuthor = "is_viewer_author"
        static let comment = "comment"
        static let userId = "user_id"
        static let userName = "user_name"
        static let userAvatar = "user_avatar"
    }
    
    // MARK: Properties
    public var isViewerAuthor: Bool? = false
    public var comment: String?
    public var userId: Int?
    public var userName: String?
    public var userAvatar: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        isViewerAuthor = json[SerializationKeys.isViewerAuthor].boolValue
        comment = json[SerializationKeys.comment].string
        userId = json[SerializationKeys.userId].int
        userName = json[SerializationKeys.userName].string
        userAvatar = json[SerializationKeys.userAvatar].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.isViewerAuthor] = isViewerAuthor
        if let value = comment { dictionary[SerializationKeys.comment] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = userAvatar { dictionary[SerializationKeys.userAvatar] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.isViewerAuthor = aDecoder.decodeBool(forKey: SerializationKeys.isViewerAuthor)
        self.comment = aDecoder.decodeObject(forKey: SerializationKeys.comment) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
        self.userAvatar = aDecoder.decodeObject(forKey: SerializationKeys.userAvatar) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(isViewerAuthor, forKey: SerializationKeys.isViewerAuthor)
        aCoder.encode(comment, forKey: SerializationKeys.comment)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(userName, forKey: SerializationKeys.userName)
        aCoder.encode(userAvatar, forKey: SerializationKeys.userAvatar)
    }
    
}

public final class Posts: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let postLat = "post_lat"
        static let postComments = "post_comments"
        static let postLikesCount = "post_likes_count"
        static let postLng = "post_lng"
        static let postCommentsCount = "post_comments_count"
        static let postIsLiked = "post_is_liked"
        static let postImageUrl = "post_image_url"
        static let postDescription = "post_description"
        static let postId = "post_id"
        static let userAvatar = "user_avatar"
        static let isViewerAuthor = "is_viewer_author"
        static let postCategoryIconUrl = "post_category_icon_url"
        static let postDate = "post_date"
        static let userId = "user_id"
        static let userName = "user_name"
        static let postMapAddress = "post_map_address"
        static let postCategoryId = "post_category_id"
    }
    
    // MARK: Properties
    public var postLat: String?
    public var postComments: [PostComments]?
    public var postLikesCount: Int?
    public var postLng: String?
    public var postCommentsCount: Int?
    public var postIsLiked: Bool? = false
    public var postImageUrl: String?
    public var postDescription: String?
    public var postId: Int?
    public var userAvatar: String?
    public var isViewerAuthor: Bool? = false
    public var postCategoryIconUrl: String?
    public var postDate: Int?
    public var userId: Int?
    public var userName: String?
    public var postMapAddress: String?
    public var postCategoryId: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        postLat = json[SerializationKeys.postLat].string
        if let items = json[SerializationKeys.postComments].array { postComments = items.map { PostComments(json: $0) } }
        postLikesCount = json[SerializationKeys.postLikesCount].int
        postLng = json[SerializationKeys.postLng].string
        postCommentsCount = json[SerializationKeys.postCommentsCount].int
        postIsLiked = json[SerializationKeys.postIsLiked].boolValue
        postImageUrl = json[SerializationKeys.postImageUrl].string
        postDescription = json[SerializationKeys.postDescription].string
        postId = json[SerializationKeys.postId].int
        userAvatar = json[SerializationKeys.userAvatar].string
        isViewerAuthor = json[SerializationKeys.isViewerAuthor].boolValue
        postCategoryIconUrl = json[SerializationKeys.postCategoryIconUrl].string
        postDate = json[SerializationKeys.postDate].int
        userId = json[SerializationKeys.userId].int
        userName = json[SerializationKeys.userName].string
        postMapAddress = json[SerializationKeys.postMapAddress].string
        postCategoryId = json[SerializationKeys.postCategoryId].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = postLat { dictionary[SerializationKeys.postLat] = value }
        if let value = postComments { dictionary[SerializationKeys.postComments] = value.map { $0.dictionaryRepresentation() } }
        if let value = postLikesCount { dictionary[SerializationKeys.postLikesCount] = value }
        if let value = postLng { dictionary[SerializationKeys.postLng] = value }
        if let value = postCommentsCount { dictionary[SerializationKeys.postCommentsCount] = value }
        dictionary[SerializationKeys.postIsLiked] = postIsLiked
        if let value = postImageUrl { dictionary[SerializationKeys.postImageUrl] = value }
        if let value = postDescription { dictionary[SerializationKeys.postDescription] = value }
        if let value = postId { dictionary[SerializationKeys.postId] = value }
        if let value = userAvatar { dictionary[SerializationKeys.userAvatar] = value }
        dictionary[SerializationKeys.isViewerAuthor] = isViewerAuthor
        if let value = postCategoryIconUrl { dictionary[SerializationKeys.postCategoryIconUrl] = value }
        if let value = postDate { dictionary[SerializationKeys.postDate] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = postMapAddress { dictionary[SerializationKeys.postMapAddress] = value }
        if let value = postCategoryId { dictionary[SerializationKeys.postCategoryId] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.postLat = aDecoder.decodeObject(forKey: SerializationKeys.postLat) as? String
        self.postComments = aDecoder.decodeObject(forKey: SerializationKeys.postComments) as? [PostComments]
        self.postLikesCount = aDecoder.decodeObject(forKey: SerializationKeys.postLikesCount) as? Int
        self.postLng = aDecoder.decodeObject(forKey: SerializationKeys.postLng) as? String
        self.postCommentsCount = aDecoder.decodeObject(forKey: SerializationKeys.postCommentsCount) as? Int
        self.postIsLiked = aDecoder.decodeBool(forKey: SerializationKeys.postIsLiked)
        self.postImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.postImageUrl) as? String
        self.postDescription = aDecoder.decodeObject(forKey: SerializationKeys.postDescription) as? String
        self.postId = aDecoder.decodeObject(forKey: SerializationKeys.postId) as? Int
        self.userAvatar = aDecoder.decodeObject(forKey: SerializationKeys.userAvatar) as? String
        self.isViewerAuthor = aDecoder.decodeBool(forKey: SerializationKeys.isViewerAuthor)
        self.postCategoryIconUrl = aDecoder.decodeObject(forKey: SerializationKeys.postCategoryIconUrl) as? String
        self.postDate = aDecoder.decodeObject(forKey: SerializationKeys.postDate) as? Int
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
        self.postMapAddress = aDecoder.decodeObject(forKey: SerializationKeys.postMapAddress) as? String
        self.postCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.postCategoryId) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(postLat, forKey: SerializationKeys.postLat)
        aCoder.encode(postComments, forKey: SerializationKeys.postComments)
        aCoder.encode(postLikesCount, forKey: SerializationKeys.postLikesCount)
        aCoder.encode(postLng, forKey: SerializationKeys.postLng)
        aCoder.encode(postCommentsCount, forKey: SerializationKeys.postCommentsCount)
        aCoder.encode(postIsLiked, forKey: SerializationKeys.postIsLiked)
        aCoder.encode(postImageUrl, forKey: SerializationKeys.postImageUrl)
        aCoder.encode(postDescription, forKey: SerializationKeys.postDescription)
        aCoder.encode(postId, forKey: SerializationKeys.postId)
        aCoder.encode(userAvatar, forKey: SerializationKeys.userAvatar)
        aCoder.encode(isViewerAuthor, forKey: SerializationKeys.isViewerAuthor)
        aCoder.encode(postCategoryIconUrl, forKey: SerializationKeys.postCategoryIconUrl)
        aCoder.encode(postDate, forKey: SerializationKeys.postDate)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(userName, forKey: SerializationKeys.userName)
        aCoder.encode(postMapAddress, forKey: SerializationKeys.postMapAddress)
        aCoder.encode(postCategoryId, forKey: SerializationKeys.postCategoryId)
    }
    
}
