//
//  FMNewLocation.swift
//
//  Created by iMac on 23/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMNewLocation: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let pagination = "pagination"
    static let message = "message"
    static let locations = "locations"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: String?
  public var pagination: Pagination?
  public var message: String?
  public var locations: [NewLocation]?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].string
    pagination = Pagination(json: json[SerializationKeys.pagination])
    message = json[SerializationKeys.message].string
    if let items = json[SerializationKeys.locations].array { locations = items.map { NewLocation(json: $0) } }
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = locations { dictionary[SerializationKeys.locations] = value.map { $0.dictionaryRepresentation() } }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.locations = aDecoder.decodeObject(forKey: SerializationKeys.locations) as? [NewLocation]
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(locations, forKey: SerializationKeys.locations)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}


public final class NewLocation: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locationCategoryId = "location_category_id"
    static let locationCategoryName = "location_category_name"
    static let locationImageUrl = "location_image_url"
    static let locationName = "location_name"
    static let locationId = "location_id"
  }

  // MARK: Properties
  public var locationCategoryId: String?
  public var locationCategoryName: String?
  public var locationImageUrl: String?
  public var locationName: String?
  public var locationId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    locationCategoryId = json[SerializationKeys.locationCategoryId].string
    locationCategoryName = json[SerializationKeys.locationCategoryName].string
    locationImageUrl = json[SerializationKeys.locationImageUrl].string
    locationName = json[SerializationKeys.locationName].string
    locationId = json[SerializationKeys.locationId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = locationCategoryId { dictionary[SerializationKeys.locationCategoryId] = value }
    if let value = locationCategoryName { dictionary[SerializationKeys.locationCategoryName] = value }
    if let value = locationImageUrl { dictionary[SerializationKeys.locationImageUrl] = value }
    if let value = locationName { dictionary[SerializationKeys.locationName] = value }
    if let value = locationId { dictionary[SerializationKeys.locationId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.locationCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.locationCategoryId) as? String
    self.locationCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.locationCategoryName) as? String
    self.locationImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.locationImageUrl) as? String
    self.locationName = aDecoder.decodeObject(forKey: SerializationKeys.locationName) as? String
    self.locationId = aDecoder.decodeObject(forKey: SerializationKeys.locationId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(locationCategoryId, forKey: SerializationKeys.locationCategoryId)
    aCoder.encode(locationCategoryName, forKey: SerializationKeys.locationCategoryName)
    aCoder.encode(locationImageUrl, forKey: SerializationKeys.locationImageUrl)
    aCoder.encode(locationName, forKey: SerializationKeys.locationName)
    aCoder.encode(locationId, forKey: SerializationKeys.locationId)
  }

}
