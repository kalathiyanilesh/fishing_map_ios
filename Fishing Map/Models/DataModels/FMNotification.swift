//
//  FMNotification.swift
//
//  Created by iMac on 24/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMNotification: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let pagination = "pagination"
    static let notifications = "notifications"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: String?
  public var pagination: Pagination?
  public var notifications: [Notifications]?
  public var message: String?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].string
    pagination = Pagination(json: json[SerializationKeys.pagination])
    if let items = json[SerializationKeys.notifications].array { notifications = items.map { Notifications(json: $0) } }
    message = json[SerializationKeys.message].string
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = notifications { dictionary[SerializationKeys.notifications] = value.map { $0.dictionaryRepresentation() } }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.notifications = aDecoder.decodeObject(forKey: SerializationKeys.notifications) as? [Notifications]
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(notifications, forKey: SerializationKeys.notifications)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}


public final class Notifications: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let notificationObjImageUrl = "notification_obj_image_url"
      static let notificationId = "notification_id"
      static let notificationAssociatedObjId = "notification_associated_obj_id"
      static let isCurrentDate = "is_current_date"
      static let notificationObjClass = "notification_obj_class"
      static let notificationNotifySubtype = "notification_notify_subtype"
      static let notificationNotifyType = "notification_notify_type"
      static let notificationIsRead = "notification_is_read"
      static let notificationObjId = "notification_obj_id"
      static let notificationText = "notification_text"
      static let notificationActorName = "notification_actor_name"
    }

    // MARK: Properties
    public var notificationObjImageUrl: String?
    public var notificationId: String?
    public var notificationAssociatedObjId: String?
    public var isCurrentDate: String?
    public var notificationObjClass: String?
    public var notificationNotifySubtype: String?
    public var notificationNotifyType: String?
    public var notificationIsRead: String?
    public var notificationObjId: String?
    public var notificationText: String?
    public var notificationActorName: String?

    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
      self.init(json: JSON(object))
    }

    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
      notificationObjImageUrl = json[SerializationKeys.notificationObjImageUrl].string
      notificationId = json[SerializationKeys.notificationId].string
      notificationAssociatedObjId = json[SerializationKeys.notificationAssociatedObjId].string
      isCurrentDate = json[SerializationKeys.isCurrentDate].string
      notificationObjClass = json[SerializationKeys.notificationObjClass].string
      notificationNotifySubtype = json[SerializationKeys.notificationNotifySubtype].string
      notificationNotifyType = json[SerializationKeys.notificationNotifyType].string
      notificationIsRead = json[SerializationKeys.notificationIsRead].string
      notificationObjId = json[SerializationKeys.notificationObjId].string
      notificationText = json[SerializationKeys.notificationText].string
      notificationActorName = json[SerializationKeys.notificationActorName].string
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = notificationObjImageUrl { dictionary[SerializationKeys.notificationObjImageUrl] = value }
      if let value = notificationId { dictionary[SerializationKeys.notificationId] = value }
      if let value = notificationAssociatedObjId { dictionary[SerializationKeys.notificationAssociatedObjId] = value }
      if let value = isCurrentDate { dictionary[SerializationKeys.isCurrentDate] = value }
      if let value = notificationObjClass { dictionary[SerializationKeys.notificationObjClass] = value }
      if let value = notificationNotifySubtype { dictionary[SerializationKeys.notificationNotifySubtype] = value }
      if let value = notificationNotifyType { dictionary[SerializationKeys.notificationNotifyType] = value }
      if let value = notificationIsRead { dictionary[SerializationKeys.notificationIsRead] = value }
      if let value = notificationObjId { dictionary[SerializationKeys.notificationObjId] = value }
      if let value = notificationText { dictionary[SerializationKeys.notificationText] = value }
      if let value = notificationActorName { dictionary[SerializationKeys.notificationActorName] = value }
      return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
      self.notificationObjImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.notificationObjImageUrl) as? String
      self.notificationId = aDecoder.decodeObject(forKey: SerializationKeys.notificationId) as? String
      self.notificationAssociatedObjId = aDecoder.decodeObject(forKey: SerializationKeys.notificationAssociatedObjId) as? String
      self.isCurrentDate = aDecoder.decodeObject(forKey: SerializationKeys.isCurrentDate) as? String
      self.notificationObjClass = aDecoder.decodeObject(forKey: SerializationKeys.notificationObjClass) as? String
      self.notificationNotifySubtype = aDecoder.decodeObject(forKey: SerializationKeys.notificationNotifySubtype) as? String
      self.notificationNotifyType = aDecoder.decodeObject(forKey: SerializationKeys.notificationNotifyType) as? String
      self.notificationIsRead = aDecoder.decodeObject(forKey: SerializationKeys.notificationIsRead) as? String
      self.notificationObjId = aDecoder.decodeObject(forKey: SerializationKeys.notificationObjId) as? String
      self.notificationText = aDecoder.decodeObject(forKey: SerializationKeys.notificationText) as? String
      self.notificationActorName = aDecoder.decodeObject(forKey: SerializationKeys.notificationActorName) as? String
    }

    public func encode(with aCoder: NSCoder) {
      aCoder.encode(notificationObjImageUrl, forKey: SerializationKeys.notificationObjImageUrl)
      aCoder.encode(notificationId, forKey: SerializationKeys.notificationId)
      aCoder.encode(notificationAssociatedObjId, forKey: SerializationKeys.notificationAssociatedObjId)
      aCoder.encode(isCurrentDate, forKey: SerializationKeys.isCurrentDate)
      aCoder.encode(notificationObjClass, forKey: SerializationKeys.notificationObjClass)
      aCoder.encode(notificationNotifySubtype, forKey: SerializationKeys.notificationNotifySubtype)
      aCoder.encode(notificationNotifyType, forKey: SerializationKeys.notificationNotifyType)
      aCoder.encode(notificationIsRead, forKey: SerializationKeys.notificationIsRead)
      aCoder.encode(notificationObjId, forKey: SerializationKeys.notificationObjId)
      aCoder.encode(notificationText, forKey: SerializationKeys.notificationText)
      aCoder.encode(notificationActorName, forKey: SerializationKeys.notificationActorName)
    }

  }
