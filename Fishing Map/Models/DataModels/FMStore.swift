//
//  FMStore.swift
//
//  Created by iMac on 16/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMStore: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pagination = "pagination"
    static let errorcode = "errorcode"
    static let products = "products"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var pagination: Pagination?
  public var errorcode: String?
  public var products: [Products]?
  public var message: String?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    pagination = Pagination(json: json[SerializationKeys.pagination])
    errorcode = json[SerializationKeys.errorcode].string
    if let items = json[SerializationKeys.products].array { products = items.map { Products(json: $0) } }
    message = json[SerializationKeys.message].string
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = products { dictionary[SerializationKeys.products] = value.map { $0.dictionaryRepresentation() } }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.products = aDecoder.decodeObject(forKey: SerializationKeys.products) as? [Products]
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(products, forKey: SerializationKeys.products)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}


public final class Products: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productId = "product_id"
    static let productName = "product_name"
    static let productImageUrl = "product_image_url"
    static let productPrice = "product_price"
    static let productDescription = "product_description"
    static let productShopUrl = "product_shop_url"
  }

  // MARK: Properties
  public var productId: String?
  public var productName: String?
  public var productImageUrl: String?
  public var productPrice: String?
  public var productDescription: String?
  public var productShopUrl: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    productId = json[SerializationKeys.productId].string
    productName = json[SerializationKeys.productName].string
    productImageUrl = json[SerializationKeys.productImageUrl].string
    productPrice = json[SerializationKeys.productPrice].string
    productDescription = json[SerializationKeys.productDescription].string
    productShopUrl = json[SerializationKeys.productShopUrl].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = productId { dictionary[SerializationKeys.productId] = value }
    if let value = productName { dictionary[SerializationKeys.productName] = value }
    if let value = productImageUrl { dictionary[SerializationKeys.productImageUrl] = value }
    if let value = productPrice { dictionary[SerializationKeys.productPrice] = value }
    if let value = productDescription { dictionary[SerializationKeys.productDescription] = value }
    if let value = productShopUrl { dictionary[SerializationKeys.productShopUrl] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.productId = aDecoder.decodeObject(forKey: SerializationKeys.productId) as? String
    self.productName = aDecoder.decodeObject(forKey: SerializationKeys.productName) as? String
    self.productImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.productImageUrl) as? String
    self.productPrice = aDecoder.decodeObject(forKey: SerializationKeys.productPrice) as? String
    self.productDescription = aDecoder.decodeObject(forKey: SerializationKeys.productDescription) as? String
    self.productShopUrl = aDecoder.decodeObject(forKey: SerializationKeys.productShopUrl) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(productId, forKey: SerializationKeys.productId)
    aCoder.encode(productName, forKey: SerializationKeys.productName)
    aCoder.encode(productImageUrl, forKey: SerializationKeys.productImageUrl)
    aCoder.encode(productPrice, forKey: SerializationKeys.productPrice)
    aCoder.encode(productDescription, forKey: SerializationKeys.productDescription)
    aCoder.encode(productShopUrl, forKey: SerializationKeys.productShopUrl)
  }

}
