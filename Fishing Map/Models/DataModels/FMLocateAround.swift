//
//  FMLocateAround.swift
//
//  Created by iMac on 10/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMLocateAround: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let pagination = "pagination"
    static let message = "message"
    static let locations = "locations"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: Int?
  public var pagination: Pagination?
  public var message: String?
  public var locations: [Locations]?
  public var success: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].int
    pagination = Pagination(json: json[SerializationKeys.pagination])
    message = json[SerializationKeys.message].string
    if let items = json[SerializationKeys.locations].array { locations = items.map { Locations(json: $0) } }
    success = json[SerializationKeys.success].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = locations { dictionary[SerializationKeys.locations] = value.map { $0.dictionaryRepresentation() } }
    dictionary[SerializationKeys.success] = success
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.locations = aDecoder.decodeObject(forKey: SerializationKeys.locations) as? [Locations]
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(locations, forKey: SerializationKeys.locations)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}

public final class Locations: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locationLng = "location_lng"
    static let locationLat = "location_lat"
    static let locationName = "location_name"
    static let terrainImages = "terrain_images"
    static let fishImages = "fish_images"
    static let restaurantImages = "restaurant_images"
    static let hotelImages = "hotel_images"
    static let locationId = "location_id"
  }

  // MARK: Properties
  public var locationLng: String?
  public var locationLat: String?
  public var locationName: String?
  public var terrainImages: [Images]?
  public var fishImages: [Images]?
  public var restaurantImages: [Images]?
  public var hotelImages: [Images]?
  public var locationId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    locationLng = json[SerializationKeys.locationLng].string
    locationLat = json[SerializationKeys.locationLat].string
    locationName = json[SerializationKeys.locationName].string
    if let items = json[SerializationKeys.terrainImages].array { terrainImages = items.map { Images(json: $0) } }
    if let items = json[SerializationKeys.fishImages].array { fishImages = items.map { Images(json: $0) } }
    if let items = json[SerializationKeys.restaurantImages].array { restaurantImages = items.map { Images(json: $0) } }
    if let items = json[SerializationKeys.hotelImages].array { hotelImages = items.map { Images(json: $0) } }
    locationId = json[SerializationKeys.locationId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = locationLng { dictionary[SerializationKeys.locationLng] = value }
    if let value = locationLat { dictionary[SerializationKeys.locationLat] = value }
    if let value = locationName { dictionary[SerializationKeys.locationName] = value }
    if let value = terrainImages { dictionary[SerializationKeys.terrainImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = fishImages { dictionary[SerializationKeys.fishImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = restaurantImages { dictionary[SerializationKeys.restaurantImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = hotelImages { dictionary[SerializationKeys.hotelImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = locationId { dictionary[SerializationKeys.locationId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.locationLng = aDecoder.decodeObject(forKey: SerializationKeys.locationLng) as? String
    self.locationLat = aDecoder.decodeObject(forKey: SerializationKeys.locationLat) as? String
    self.locationName = aDecoder.decodeObject(forKey: SerializationKeys.locationName) as? String
    self.terrainImages = aDecoder.decodeObject(forKey: SerializationKeys.terrainImages) as? [Images]
    self.fishImages = aDecoder.decodeObject(forKey: SerializationKeys.fishImages) as? [Images]
    self.restaurantImages = aDecoder.decodeObject(forKey: SerializationKeys.restaurantImages) as? [Images]
    self.hotelImages = aDecoder.decodeObject(forKey: SerializationKeys.hotelImages) as? [Images]
    self.locationId = aDecoder.decodeObject(forKey: SerializationKeys.locationId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(locationLng, forKey: SerializationKeys.locationLng)
    aCoder.encode(locationLat, forKey: SerializationKeys.locationLat)
    aCoder.encode(locationName, forKey: SerializationKeys.locationName)
    aCoder.encode(terrainImages, forKey: SerializationKeys.terrainImages)
    aCoder.encode(fishImages, forKey: SerializationKeys.fishImages)
    aCoder.encode(restaurantImages, forKey: SerializationKeys.restaurantImages)
    aCoder.encode(hotelImages, forKey: SerializationKeys.hotelImages)
    aCoder.encode(locationId, forKey: SerializationKeys.locationId)
  }

}

public final class Images: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let imageId = "image_id"
      static let imageUrl = "image_url"
      static let imageLikesCount = "image_likes_count"
      static let isLocation = "is_location"
      static let imageLocationId = "image_location_id"
    }

    // MARK: Properties
    public var imageId: String?
    public var imageUrl: String?
    public var imageLikesCount: String?
    public var isLocation: String?
    public var imageLocationId: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
      self.init(json: JSON(object))
    }

    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
      imageId = json[SerializationKeys.imageId].string
      imageUrl = json[SerializationKeys.imageUrl].string
      imageLikesCount = json[SerializationKeys.imageLikesCount].string
      isLocation = json[SerializationKeys.isLocation].string
      imageLocationId = json[SerializationKeys.imageLocationId].string
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = imageId { dictionary[SerializationKeys.imageId] = value }
      if let value = imageUrl { dictionary[SerializationKeys.imageUrl] = value }
      if let value = imageLikesCount { dictionary[SerializationKeys.imageLikesCount] = value }
      if let value = isLocation { dictionary[SerializationKeys.isLocation] = value }
      if let value = imageLocationId { dictionary[SerializationKeys.imageLocationId] = value }
      return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
      self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
      self.imageUrl = aDecoder.decodeObject(forKey: SerializationKeys.imageUrl) as? String
      self.imageLikesCount = aDecoder.decodeObject(forKey: SerializationKeys.imageLikesCount) as? String
      self.isLocation = aDecoder.decodeObject(forKey: SerializationKeys.isLocation) as? String
      self.imageLocationId = aDecoder.decodeObject(forKey: SerializationKeys.imageLocationId) as? String
    }

    public func encode(with aCoder: NSCoder) {
      aCoder.encode(imageId, forKey: SerializationKeys.imageId)
      aCoder.encode(imageUrl, forKey: SerializationKeys.imageUrl)
      aCoder.encode(imageLikesCount, forKey: SerializationKeys.imageLikesCount)
      aCoder.encode(isLocation, forKey: SerializationKeys.isLocation)
      aCoder.encode(imageLocationId, forKey: SerializationKeys.imageLocationId)
    }

  }
