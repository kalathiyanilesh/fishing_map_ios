//
//  FMInitialSearchesModel.swift
//  Fishing Map
//
//  Created by macOS on 06/12/20.
//

import Foundation
import SwiftyJSON

public final class FMInitialSearches: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let errorcode = "errorcode"
        static let posts = "posts"
        static let pagination = "pagination"
        static let message = "message"
        static let tags = "tags"
        static let success = "success"
    }
    
    // MARK: Properties
    public var errorcode: Int?
    public var posts: [Posts]?
    public var pagination: PaginationInitialSearch?
    public var message: String?
    public var tags: [Tags]?
    public var success: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        errorcode = json[SerializationKeys.errorcode].int
        if let items = json[SerializationKeys.posts].array { posts = items.map { Posts(json: $0) } }
        pagination = PaginationInitialSearch(json: json[SerializationKeys.pagination])
        message = json[SerializationKeys.message].string
        if let items = json[SerializationKeys.tags].array { tags = items.map { Tags(json: $0) } }
        success = json[SerializationKeys.success].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
        if let value = posts { dictionary[SerializationKeys.posts] = value.map { $0.dictionaryRepresentation() } }
        if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
        if let value = message { dictionary[SerializationKeys.message] = value }
        if let value = tags { dictionary[SerializationKeys.tags] = value.map { $0.dictionaryRepresentation() } }
        dictionary[SerializationKeys.success] = success
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
        self.posts = aDecoder.decodeObject(forKey: SerializationKeys.posts) as? [Posts]
        self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? PaginationInitialSearch
        self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
        self.tags = aDecoder.decodeObject(forKey: SerializationKeys.tags) as? [Tags]
        self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
        aCoder.encode(posts, forKey: SerializationKeys.posts)
        aCoder.encode(pagination, forKey: SerializationKeys.pagination)
        aCoder.encode(message, forKey: SerializationKeys.message)
        aCoder.encode(tags, forKey: SerializationKeys.tags)
        aCoder.encode(success, forKey: SerializationKeys.success)
    }
    
}

public final class PaginationInitialSearch: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let lastPage = "last_page?"
        static let totalCount = "total_count"
        static let totalPages = "total_pages"
        static let outOfRange = "out_of_range?"
        static let currentPage = "current_page"
        static let firstPage = "first_page?"
    }
    
    // MARK: Properties
    public var lastPage: Bool? = false
    public var totalCount: Int?
    public var totalPages: Int?
    public var outOfRange: Bool? = false
    public var currentPage: Int?
    public var firstPage: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        lastPage? = json[SerializationKeys.lastPage].boolValue
        totalCount = json[SerializationKeys.totalCount].int
        totalPages = json[SerializationKeys.totalPages].int
        outOfRange? = json[SerializationKeys.outOfRange].boolValue
        currentPage = json[SerializationKeys.currentPage].int
        firstPage? = json[SerializationKeys.firstPage].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.lastPage] = lastPage
        if let value = totalCount { dictionary[SerializationKeys.totalCount] = value }
        if let value = totalPages { dictionary[SerializationKeys.totalPages] = value }
        dictionary[SerializationKeys.outOfRange] = outOfRange
        if let value = currentPage { dictionary[SerializationKeys.currentPage] = value }
        dictionary[SerializationKeys.firstPage] = firstPage
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.lastPage? = aDecoder.decodeBool(forKey: SerializationKeys.lastPage)
        self.totalCount = aDecoder.decodeObject(forKey: SerializationKeys.totalCount) as? Int
        self.totalPages = aDecoder.decodeObject(forKey: SerializationKeys.totalPages) as? Int
        self.outOfRange? = aDecoder.decodeBool(forKey: SerializationKeys.outOfRange)
        self.currentPage = aDecoder.decodeObject(forKey: SerializationKeys.currentPage) as? Int
        self.firstPage? = aDecoder.decodeBool(forKey: SerializationKeys.firstPage)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(lastPage, forKey: SerializationKeys.lastPage)
        aCoder.encode(totalCount, forKey: SerializationKeys.totalCount)
        aCoder.encode(totalPages, forKey: SerializationKeys.totalPages)
        aCoder.encode(outOfRange, forKey: SerializationKeys.outOfRange)
        aCoder.encode(currentPage, forKey: SerializationKeys.currentPage)
        aCoder.encode(firstPage, forKey: SerializationKeys.firstPage)
    }
    
}

public final class Tags: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let tag = "tag"
    }
    
    // MARK: Properties
    public var id: Int?
    public var tag: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].int
        tag = json[SerializationKeys.tag].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = tag { dictionary[SerializationKeys.tag] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.tag = aDecoder.decodeObject(forKey: SerializationKeys.tag) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(tag, forKey: SerializationKeys.tag)
    }
    
}
