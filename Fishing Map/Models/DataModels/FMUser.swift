//
//  FMUser.swift
//
//  Created by iMac on 21/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMUser: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let user = "user"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: String?
  public var user: User?
  public var message: String?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].string
    user = User(json: json[SerializationKeys.user])
    message = json[SerializationKeys.message].string
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.user = aDecoder.decodeObject(forKey: SerializationKeys.user) as? User
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(user, forKey: SerializationKeys.user)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}

public final class UpcomingEvents: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let eventDate = "event_date"
    static let eventId = "event_id"
    static let eventTitle = "event_title"
  }

  // MARK: Properties
  public var eventDate: String?
  public var eventId: String?
  public var eventTitle: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    eventDate = json[SerializationKeys.eventDate].string
    eventId = json[SerializationKeys.eventId].string
    eventTitle = json[SerializationKeys.eventTitle].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = eventDate { dictionary[SerializationKeys.eventDate] = value }
    if let value = eventId { dictionary[SerializationKeys.eventId] = value }
    if let value = eventTitle { dictionary[SerializationKeys.eventTitle] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.eventDate = aDecoder.decodeObject(forKey: SerializationKeys.eventDate) as? String
    self.eventId = aDecoder.decodeObject(forKey: SerializationKeys.eventId) as? String
    self.eventTitle = aDecoder.decodeObject(forKey: SerializationKeys.eventTitle) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(eventDate, forKey: SerializationKeys.eventDate)
    aCoder.encode(eventId, forKey: SerializationKeys.eventId)
    aCoder.encode(eventTitle, forKey: SerializationKeys.eventTitle)
  }

}

public final class User: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let userEmail = "user_email"
      static let userPrefectureId = "user_prefecture_id"
      static let likesCount = "likes_count"
      static let userAddress = "user_address"
      static let userPrefectureName = "user_prefecture_name"
      static let unreadNotificationsCount = "unread_notifications_count"
      static let userAvatarUrl = "user_avatar_url"
      static let upcomingEvents = "upcoming_events"
      static let hasUnreadNotifications = "has_unread_notifications"
      static let postsCount = "posts_count"
      static let isCurrentUser = "is_current_user"
      static let userId = "user_id"
      static let userName = "user_name"
      static let imagesCount = "images_count"
    }

    // MARK: Properties
    public var userEmail: String?
    public var userPrefectureId: String?
    public var likesCount: String?
    public var userAddress: String?
    public var userPrefectureName: String?
    public var unreadNotificationsCount: String?
    public var userAvatarUrl: String?
    public var upcomingEvents: [UpcomingEvents]?
    public var hasUnreadNotifications: String?
    public var postsCount: String?
    public var isCurrentUser: String?
    public var userId: String?
    public var userName: String?
    public var imagesCount: String?

    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
      self.init(json: JSON(object))
    }

    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
      userEmail = json[SerializationKeys.userEmail].string
      userPrefectureId = json[SerializationKeys.userPrefectureId].string
      likesCount = json[SerializationKeys.likesCount].string
      userAddress = json[SerializationKeys.userAddress].string
      userPrefectureName = json[SerializationKeys.userPrefectureName].string
      unreadNotificationsCount = json[SerializationKeys.unreadNotificationsCount].string
      userAvatarUrl = json[SerializationKeys.userAvatarUrl].string
      if let items = json[SerializationKeys.upcomingEvents].array { upcomingEvents = items.map { UpcomingEvents(json: $0) } }
      hasUnreadNotifications = json[SerializationKeys.hasUnreadNotifications].string
      postsCount = json[SerializationKeys.postsCount].string
      isCurrentUser = json[SerializationKeys.isCurrentUser].string
      userId = json[SerializationKeys.userId].string
      userName = json[SerializationKeys.userName].string
      imagesCount = json[SerializationKeys.imagesCount].string
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = userEmail { dictionary[SerializationKeys.userEmail] = value }
      if let value = userPrefectureId { dictionary[SerializationKeys.userPrefectureId] = value }
      if let value = likesCount { dictionary[SerializationKeys.likesCount] = value }
      if let value = userAddress { dictionary[SerializationKeys.userAddress] = value }
      if let value = userPrefectureName { dictionary[SerializationKeys.userPrefectureName] = value }
      if let value = unreadNotificationsCount { dictionary[SerializationKeys.unreadNotificationsCount] = value }
      if let value = userAvatarUrl { dictionary[SerializationKeys.userAvatarUrl] = value }
      if let value = upcomingEvents { dictionary[SerializationKeys.upcomingEvents] = value }
      if let value = hasUnreadNotifications { dictionary[SerializationKeys.hasUnreadNotifications] = value }
      if let value = postsCount { dictionary[SerializationKeys.postsCount] = value }
      if let value = isCurrentUser { dictionary[SerializationKeys.isCurrentUser] = value }
      if let value = userId { dictionary[SerializationKeys.userId] = value }
      if let value = userName { dictionary[SerializationKeys.userName] = value }
      if let value = imagesCount { dictionary[SerializationKeys.imagesCount] = value }
      return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
      self.userEmail = aDecoder.decodeObject(forKey: SerializationKeys.userEmail) as? String
      self.userPrefectureId = aDecoder.decodeObject(forKey: SerializationKeys.userPrefectureId) as? String
      self.likesCount = aDecoder.decodeObject(forKey: SerializationKeys.likesCount) as? String
      self.userAddress = aDecoder.decodeObject(forKey: SerializationKeys.userAddress) as? String
      self.userPrefectureName = aDecoder.decodeObject(forKey: SerializationKeys.userPrefectureName) as? String
      self.unreadNotificationsCount = aDecoder.decodeObject(forKey: SerializationKeys.unreadNotificationsCount) as? String
      self.userAvatarUrl = aDecoder.decodeObject(forKey: SerializationKeys.userAvatarUrl) as? String
      self.upcomingEvents = aDecoder.decodeObject(forKey: SerializationKeys.upcomingEvents) as? [UpcomingEvents]
      self.hasUnreadNotifications = aDecoder.decodeObject(forKey: SerializationKeys.hasUnreadNotifications) as? String
      self.postsCount = aDecoder.decodeObject(forKey: SerializationKeys.postsCount) as? String
      self.isCurrentUser = aDecoder.decodeObject(forKey: SerializationKeys.isCurrentUser) as? String
      self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
      self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
      self.imagesCount = aDecoder.decodeObject(forKey: SerializationKeys.imagesCount) as? String
    }

    public func encode(with aCoder: NSCoder) {
      aCoder.encode(userEmail, forKey: SerializationKeys.userEmail)
      aCoder.encode(userPrefectureId, forKey: SerializationKeys.userPrefectureId)
      aCoder.encode(likesCount, forKey: SerializationKeys.likesCount)
      aCoder.encode(userAddress, forKey: SerializationKeys.userAddress)
      aCoder.encode(userPrefectureName, forKey: SerializationKeys.userPrefectureName)
      aCoder.encode(unreadNotificationsCount, forKey: SerializationKeys.unreadNotificationsCount)
      aCoder.encode(userAvatarUrl, forKey: SerializationKeys.userAvatarUrl)
      aCoder.encode(upcomingEvents, forKey: SerializationKeys.upcomingEvents)
      aCoder.encode(hasUnreadNotifications, forKey: SerializationKeys.hasUnreadNotifications)
      aCoder.encode(postsCount, forKey: SerializationKeys.postsCount)
      aCoder.encode(isCurrentUser, forKey: SerializationKeys.isCurrentUser)
      aCoder.encode(userId, forKey: SerializationKeys.userId)
      aCoder.encode(userName, forKey: SerializationKeys.userName)
      aCoder.encode(imagesCount, forKey: SerializationKeys.imagesCount)
    }

  }
