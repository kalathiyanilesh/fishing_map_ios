//
//  FMProfileUpload.swift
//
//  Created by iMac on 08/01/21
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMProfileUpload: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let pagination = "pagination"
    static let message = "message"
    static let images = "images"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: String?
  public var pagination: Pagination?
  public var message: String?
  public var images: [ProfileImages]?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].string
    pagination = Pagination(json: json[SerializationKeys.pagination])
    message = json[SerializationKeys.message].string
    if let items = json[SerializationKeys.images].array { images = items.map { ProfileImages(json: $0) } }
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = images { dictionary[SerializationKeys.images] = value.map { $0.dictionaryRepresentation() } }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.images = aDecoder.decodeObject(forKey: SerializationKeys.images) as? [ProfileImages]
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(images, forKey: SerializationKeys.images)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}

public final class ProfileImages: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let imageSpotPrice = "image_spot_price"
    static let imageId = "image_id"
    static let imageSpotSlots = "image_spot_slots"
    static let imageName = "image_name"
    static let imageType = "image_type"
    static let imageLikesCount = "image_likes_count"
    static let userAvatar = "user_avatar"
    static let imageLocationId = "image_location_id"
    static let imageUrl = "image_url"
    static let userId = "user_id"
    static let userName = "user_name"
  }

  // MARK: Properties
  public var imageSpotPrice: String?
  public var imageId: String?
  public var imageSpotSlots: String?
  public var imageName: String?
  public var imageType: String?
  public var imageLikesCount: String?
  public var userAvatar: String?
  public var imageLocationId: String?
  public var imageUrl: String?
  public var userId: String?
  public var userName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    imageSpotPrice = json[SerializationKeys.imageSpotPrice].string
    imageId = json[SerializationKeys.imageId].string
    imageSpotSlots = json[SerializationKeys.imageSpotSlots].string
    imageName = json[SerializationKeys.imageName].string
    imageType = json[SerializationKeys.imageType].string
    imageLikesCount = json[SerializationKeys.imageLikesCount].string
    userAvatar = json[SerializationKeys.userAvatar].string
    imageLocationId = json[SerializationKeys.imageLocationId].string
    imageUrl = json[SerializationKeys.imageUrl].string
    userId = json[SerializationKeys.userId].string
    userName = json[SerializationKeys.userName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = imageSpotPrice { dictionary[SerializationKeys.imageSpotPrice] = value }
    if let value = imageId { dictionary[SerializationKeys.imageId] = value }
    if let value = imageSpotSlots { dictionary[SerializationKeys.imageSpotSlots] = value }
    if let value = imageName { dictionary[SerializationKeys.imageName] = value }
    if let value = imageType { dictionary[SerializationKeys.imageType] = value }
    if let value = imageLikesCount { dictionary[SerializationKeys.imageLikesCount] = value }
    if let value = userAvatar { dictionary[SerializationKeys.userAvatar] = value }
    if let value = imageLocationId { dictionary[SerializationKeys.imageLocationId] = value }
    if let value = imageUrl { dictionary[SerializationKeys.imageUrl] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = userName { dictionary[SerializationKeys.userName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.imageSpotPrice = aDecoder.decodeObject(forKey: SerializationKeys.imageSpotPrice) as? String
    self.imageId = aDecoder.decodeObject(forKey: SerializationKeys.imageId) as? String
    self.imageSpotSlots = aDecoder.decodeObject(forKey: SerializationKeys.imageSpotSlots) as? String
    self.imageName = aDecoder.decodeObject(forKey: SerializationKeys.imageName) as? String
    self.imageType = aDecoder.decodeObject(forKey: SerializationKeys.imageType) as? String
    self.imageLikesCount = aDecoder.decodeObject(forKey: SerializationKeys.imageLikesCount) as? String
    self.userAvatar = aDecoder.decodeObject(forKey: SerializationKeys.userAvatar) as? String
    self.imageLocationId = aDecoder.decodeObject(forKey: SerializationKeys.imageLocationId) as? String
    self.imageUrl = aDecoder.decodeObject(forKey: SerializationKeys.imageUrl) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
    self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(imageSpotPrice, forKey: SerializationKeys.imageSpotPrice)
    aCoder.encode(imageId, forKey: SerializationKeys.imageId)
    aCoder.encode(imageSpotSlots, forKey: SerializationKeys.imageSpotSlots)
    aCoder.encode(imageName, forKey: SerializationKeys.imageName)
    aCoder.encode(imageType, forKey: SerializationKeys.imageType)
    aCoder.encode(imageLikesCount, forKey: SerializationKeys.imageLikesCount)
    aCoder.encode(userAvatar, forKey: SerializationKeys.userAvatar)
    aCoder.encode(imageLocationId, forKey: SerializationKeys.imageLocationId)
    aCoder.encode(imageUrl, forKey: SerializationKeys.imageUrl)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(userName, forKey: SerializationKeys.userName)
  }

}
