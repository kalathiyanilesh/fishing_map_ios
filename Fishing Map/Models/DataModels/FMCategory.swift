//
//  FMCategory.swift
//
//  Created by iMac on 10/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMCategory: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let categoryIconUrl = "category_icon_url"
    static let categoryId = "category_id"
    static let categoryName = "category_name"
  }

  // MARK: Properties
  public var categoryIconUrl: String?
  public var categoryId: String?
  public var categoryName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    categoryIconUrl = json[SerializationKeys.categoryIconUrl].string
    categoryId = json[SerializationKeys.categoryId].string
    categoryName = json[SerializationKeys.categoryName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = categoryIconUrl { dictionary[SerializationKeys.categoryIconUrl] = value }
    if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
    if let value = categoryName { dictionary[SerializationKeys.categoryName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.categoryIconUrl = aDecoder.decodeObject(forKey: SerializationKeys.categoryIconUrl) as? String
    self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? String
    self.categoryName = aDecoder.decodeObject(forKey: SerializationKeys.categoryName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(categoryIconUrl, forKey: SerializationKeys.categoryIconUrl)
    aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
    aCoder.encode(categoryName, forKey: SerializationKeys.categoryName)
  }

}
