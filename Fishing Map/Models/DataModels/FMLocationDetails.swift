//
//  FMLocationDetails.swift
//
//  Created by iMac on 12/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMLocationDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let location = "location"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: String?
  public var location: Location?
  public var message: String?
  public var success: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].string
    location = Location(json: json[SerializationKeys.location])
    message = json[SerializationKeys.message].string
    success = json[SerializationKeys.success].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = location { dictionary[SerializationKeys.location] = value.dictionaryRepresentation() }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = success { dictionary[SerializationKeys.success] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? String
    self.location = aDecoder.decodeObject(forKey: SerializationKeys.location) as? Location
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(location, forKey: SerializationKeys.location)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}


public final class Location: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locationId = "location_id"
    static let locationDescription = "location_description"
    static let locationName = "location_name"
    static let locationImageUrl = "location_image_url"
    static let sceneImages = "scene_images"
    static let parkingSpots = "parking_spots"
    static let locationLat = "location_lat"
    static let fishImages = "fish_images"
    static let terrainImages = "terrain_images"
    static let locationLng = "location_lng"
    static let hotelImages = "hotel_images"
    static let restaurantImages = "restaurant_images"
    static let locationPrice = "location_price"
    static let isLiked = "is_liked"
    static let locationLikesCount = "location_likes_count"
  }

  // MARK: Properties
  public var locationId: String?
  public var locationDescription: String?
  public var locationName: String?
  public var locationImageUrl: String?
  public var sceneImages: [Images]?
  public var parkingSpots: [ImageParking]?
  public var locationLat: String?
  public var fishImages: [Images]?
  public var terrainImages: [Images]?
  public var locationLng: String?
  public var hotelImages: [Images]?
  public var restaurantImages: [Images]?
  public var locationPrice: String?
  public var isLiked: String?
  public var locationLikesCount: String?
    
  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    locationId = json[SerializationKeys.locationId].string
    locationDescription = json[SerializationKeys.locationDescription].string
    locationName = json[SerializationKeys.locationName].string
    locationImageUrl = json[SerializationKeys.locationImageUrl].string
    if let items = json[SerializationKeys.sceneImages].array { sceneImages = items.map { Images(json: $0) } }
    if let items = json[SerializationKeys.parkingSpots].array { parkingSpots = items.map { ImageParking(json: $0) } }
    locationLat = json[SerializationKeys.locationLat].string
    if let items = json[SerializationKeys.fishImages].array { fishImages = items.map { Images(json: $0) } }
    if let items = json[SerializationKeys.terrainImages].array { terrainImages = items.map { Images(json: $0) } }
    locationLng = json[SerializationKeys.locationLng].string
    if let items = json[SerializationKeys.hotelImages].array { hotelImages = items.map { Images(json: $0) } }
    if let items = json[SerializationKeys.restaurantImages].array { restaurantImages = items.map { Images(json: $0) } }
    locationPrice = json[SerializationKeys.locationPrice].string
    isLiked = json[SerializationKeys.isLiked].string
    locationLikesCount = json[SerializationKeys.locationLikesCount].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = locationId { dictionary[SerializationKeys.locationId] = value }
    if let value = locationDescription { dictionary[SerializationKeys.locationDescription] = value }
    if let value = locationName { dictionary[SerializationKeys.locationName] = value }
    if let value = locationImageUrl { dictionary[SerializationKeys.locationImageUrl] = value }
    if let value = sceneImages { dictionary[SerializationKeys.sceneImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = parkingSpots { dictionary[SerializationKeys.parkingSpots] = value.map { $0.dictionaryRepresentation() } }
    if let value = locationLat { dictionary[SerializationKeys.locationLat] = value }
    if let value = fishImages { dictionary[SerializationKeys.fishImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = terrainImages { dictionary[SerializationKeys.terrainImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = locationLng { dictionary[SerializationKeys.locationLng] = value }
    if let value = hotelImages { dictionary[SerializationKeys.hotelImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = restaurantImages { dictionary[SerializationKeys.restaurantImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = locationPrice { dictionary[SerializationKeys.locationPrice] = value }
    if let value = isLiked { dictionary[SerializationKeys.isLiked] = value }
    if let value = locationLikesCount { dictionary[SerializationKeys.locationLikesCount] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.locationId = aDecoder.decodeObject(forKey: SerializationKeys.locationId) as? String
    self.locationDescription = aDecoder.decodeObject(forKey: SerializationKeys.locationDescription) as? String
    self.locationName = aDecoder.decodeObject(forKey: SerializationKeys.locationName) as? String
    self.locationImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.locationImageUrl) as? String
    self.sceneImages = aDecoder.decodeObject(forKey: SerializationKeys.sceneImages) as? [Images]
    self.parkingSpots = aDecoder.decodeObject(forKey: SerializationKeys.parkingSpots) as? [ImageParking]
    self.locationLat = aDecoder.decodeObject(forKey: SerializationKeys.locationLat) as? String
    self.fishImages = aDecoder.decodeObject(forKey: SerializationKeys.fishImages) as? [Images]
    self.terrainImages = aDecoder.decodeObject(forKey: SerializationKeys.terrainImages) as? [Images]
    self.locationLng = aDecoder.decodeObject(forKey: SerializationKeys.locationLng) as? String
    self.hotelImages = aDecoder.decodeObject(forKey: SerializationKeys.hotelImages) as? [Images]
    self.restaurantImages = aDecoder.decodeObject(forKey: SerializationKeys.restaurantImages) as? [Images]
    self.locationPrice = aDecoder.decodeObject(forKey: SerializationKeys.locationPrice) as? String
    self.isLiked = aDecoder.decodeObject(forKey: SerializationKeys.isLiked) as? String
    self.locationLikesCount = aDecoder.decodeObject(forKey: SerializationKeys.locationLikesCount) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(locationId, forKey: SerializationKeys.locationId)
    aCoder.encode(locationDescription, forKey: SerializationKeys.locationDescription)
    aCoder.encode(locationName, forKey: SerializationKeys.locationName)
    aCoder.encode(locationImageUrl, forKey: SerializationKeys.locationImageUrl)
    aCoder.encode(sceneImages, forKey: SerializationKeys.sceneImages)
    aCoder.encode(parkingSpots, forKey: SerializationKeys.parkingSpots)
    aCoder.encode(locationLat, forKey: SerializationKeys.locationLat)
    aCoder.encode(fishImages, forKey: SerializationKeys.fishImages)
    aCoder.encode(terrainImages, forKey: SerializationKeys.terrainImages)
    aCoder.encode(locationLng, forKey: SerializationKeys.locationLng)
    aCoder.encode(hotelImages, forKey: SerializationKeys.hotelImages)
    aCoder.encode(restaurantImages, forKey: SerializationKeys.restaurantImages)
    aCoder.encode(locationPrice, forKey: SerializationKeys.locationPrice)
    aCoder.encode(isLiked, forKey: SerializationKeys.isLiked)
    aCoder.encode(locationLikesCount, forKey: SerializationKeys.locationLikesCount)
  }

}

public final class ImageParking: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let spotId = "spot_id"
    static let spotPrice = "spot_price"
    static let spotUrl = "spot_url"
    static let spotSlots = "spot_slots"
    static let spotName = "spot_name"
  }

  // MARK: Properties
  public var spotId: String?
  public var spotPrice: String?
  public var spotUrl: String?
  public var spotSlots: String?
  public var spotName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    spotId = json[SerializationKeys.spotId].string
    spotPrice = json[SerializationKeys.spotPrice].string
    spotUrl = json[SerializationKeys.spotUrl].string
    spotSlots = json[SerializationKeys.spotSlots].string
    spotName = json[SerializationKeys.spotName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = spotId { dictionary[SerializationKeys.spotId] = value }
    if let value = spotPrice { dictionary[SerializationKeys.spotPrice] = value }
    if let value = spotUrl { dictionary[SerializationKeys.spotUrl] = value }
    if let value = spotSlots { dictionary[SerializationKeys.spotSlots] = value }
    if let value = spotName { dictionary[SerializationKeys.spotName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.spotId = aDecoder.decodeObject(forKey: SerializationKeys.spotId) as? String
    self.spotPrice = aDecoder.decodeObject(forKey: SerializationKeys.spotPrice) as? String
    self.spotUrl = aDecoder.decodeObject(forKey: SerializationKeys.spotUrl) as? String
    self.spotSlots = aDecoder.decodeObject(forKey: SerializationKeys.spotSlots) as? String
    self.spotName = aDecoder.decodeObject(forKey: SerializationKeys.spotName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(spotId, forKey: SerializationKeys.spotId)
    aCoder.encode(spotPrice, forKey: SerializationKeys.spotPrice)
    aCoder.encode(spotUrl, forKey: SerializationKeys.spotUrl)
    aCoder.encode(spotSlots, forKey: SerializationKeys.spotSlots)
    aCoder.encode(spotName, forKey: SerializationKeys.spotName)
  }

}
