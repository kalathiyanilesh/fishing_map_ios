//
//  NotificationPush.swift
//
//  Created by iMac on 12/01/21
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class NotificationPush: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let clickAction = "click_action"
      static let googleCSenderId = "google_c_sender_id"
      static let googleCAE = "google_c_a_e"
      static let objClass = "obj_class"
      static let notifyType = "notify_type"
      static let objId = "obj_id"
      static let notifySubtype = "notify_subtype"
      static let title = "title"
      static let objImageUrl = "obj_image_url"
      static let aps = "aps"
      static let gcmMessageId = "gcm_message_id"
      static let notificationText = "notification_text"
    }

    // MARK: Properties
    public var clickAction: String?
    public var googleCSenderId: String?
    public var googleCAE: String?
    public var objClass: String?
    public var notifyType: String?
    public var objId: String?
    public var notifySubtype: String?
    public var title: String?
    public var objImageUrl: String?
    public var aps: Aps?
    public var gcmMessageId: String?
    public var notificationText: String?

    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
      self.init(json: JSON(object))
    }

    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
      clickAction = json[SerializationKeys.clickAction].string
      googleCSenderId = json[SerializationKeys.googleCSenderId].string
      googleCAE = json[SerializationKeys.googleCAE].string
      objClass = json[SerializationKeys.objClass].string
      notifyType = json[SerializationKeys.notifyType].string
      objId = json[SerializationKeys.objId].string
      notifySubtype = json[SerializationKeys.notifySubtype].string
      title = json[SerializationKeys.title].string
      objImageUrl = json[SerializationKeys.objImageUrl].string
      aps = Aps(json: json[SerializationKeys.aps])
      gcmMessageId = json[SerializationKeys.gcmMessageId].string
      notificationText = json[SerializationKeys.notificationText].string
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = clickAction { dictionary[SerializationKeys.clickAction] = value }
      if let value = googleCSenderId { dictionary[SerializationKeys.googleCSenderId] = value }
      if let value = googleCAE { dictionary[SerializationKeys.googleCAE] = value }
      if let value = objClass { dictionary[SerializationKeys.objClass] = value }
      if let value = notifyType { dictionary[SerializationKeys.notifyType] = value }
      if let value = objId { dictionary[SerializationKeys.objId] = value }
      if let value = notifySubtype { dictionary[SerializationKeys.notifySubtype] = value }
      if let value = title { dictionary[SerializationKeys.title] = value }
      if let value = objImageUrl { dictionary[SerializationKeys.objImageUrl] = value }
      if let value = aps { dictionary[SerializationKeys.aps] = value.dictionaryRepresentation() }
      if let value = gcmMessageId { dictionary[SerializationKeys.gcmMessageId] = value }
      if let value = notificationText { dictionary[SerializationKeys.notificationText] = value }
      return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
      self.clickAction = aDecoder.decodeObject(forKey: SerializationKeys.clickAction) as? String
      self.googleCSenderId = aDecoder.decodeObject(forKey: SerializationKeys.googleCSenderId) as? String
      self.googleCAE = aDecoder.decodeObject(forKey: SerializationKeys.googleCAE) as? String
      self.objClass = aDecoder.decodeObject(forKey: SerializationKeys.objClass) as? String
      self.notifyType = aDecoder.decodeObject(forKey: SerializationKeys.notifyType) as? String
      self.objId = aDecoder.decodeObject(forKey: SerializationKeys.objId) as? String
      self.notifySubtype = aDecoder.decodeObject(forKey: SerializationKeys.notifySubtype) as? String
      self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
      self.objImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.objImageUrl) as? String
      self.aps = aDecoder.decodeObject(forKey: SerializationKeys.aps) as? Aps
      self.gcmMessageId = aDecoder.decodeObject(forKey: SerializationKeys.gcmMessageId) as? String
      self.notificationText = aDecoder.decodeObject(forKey: SerializationKeys.notificationText) as? String
    }

    public func encode(with aCoder: NSCoder) {
      aCoder.encode(clickAction, forKey: SerializationKeys.clickAction)
      aCoder.encode(googleCSenderId, forKey: SerializationKeys.googleCSenderId)
      aCoder.encode(googleCAE, forKey: SerializationKeys.googleCAE)
      aCoder.encode(objClass, forKey: SerializationKeys.objClass)
      aCoder.encode(notifyType, forKey: SerializationKeys.notifyType)
      aCoder.encode(objId, forKey: SerializationKeys.objId)
      aCoder.encode(notifySubtype, forKey: SerializationKeys.notifySubtype)
      aCoder.encode(title, forKey: SerializationKeys.title)
      aCoder.encode(objImageUrl, forKey: SerializationKeys.objImageUrl)
      aCoder.encode(aps, forKey: SerializationKeys.aps)
      aCoder.encode(gcmMessageId, forKey: SerializationKeys.gcmMessageId)
      aCoder.encode(notificationText, forKey: SerializationKeys.notificationText)
    }

  }

public final class Aps: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let alert = "alert"
  }

  // MARK: Properties
  public var alert: Alert?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    alert = Alert(json: json[SerializationKeys.alert])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = alert { dictionary[SerializationKeys.alert] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.alert = aDecoder.decodeObject(forKey: SerializationKeys.alert) as? Alert
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(alert, forKey: SerializationKeys.alert)
  }

}

public final class Alert: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let body = "body"
    static let title = "title"
  }

  // MARK: Properties
  public var body: String?
  public var title: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    body = json[SerializationKeys.body].string
    title = json[SerializationKeys.title].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = body { dictionary[SerializationKeys.body] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.body = aDecoder.decodeObject(forKey: SerializationKeys.body) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(body, forKey: SerializationKeys.body)
    aCoder.encode(title, forKey: SerializationKeys.title)
  }

}
