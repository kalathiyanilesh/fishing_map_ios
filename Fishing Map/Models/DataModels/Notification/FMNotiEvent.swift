//
//  FMNotiEvent.swift
//
//  Created by iMac on 11/01/21
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMNotiEvent: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let event = "event"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: Int?
  public var event: Events?
  public var message: String?
  public var success: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].int
    event = Events(json: json[SerializationKeys.event])
    message = json[SerializationKeys.message].string
    success = json[SerializationKeys.success].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = event { dictionary[SerializationKeys.event] = value.dictionaryRepresentation() }
    if let value = message { dictionary[SerializationKeys.message] = value }
    dictionary[SerializationKeys.success] = success
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
    self.event = aDecoder.decodeObject(forKey: SerializationKeys.event) as? Events
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(event, forKey: SerializationKeys.event)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}
