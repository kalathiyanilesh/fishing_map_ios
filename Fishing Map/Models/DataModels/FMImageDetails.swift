//
//  FMImageDetails.swift
//
//  Created by iMac on 12/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMImageDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let message = "message"
    static let image = "image"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: Int?
  public var message: String?
  public var image: Image?
  public var success: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].int
    message = json[SerializationKeys.message].string
    image = Image(json: json[SerializationKeys.image])
    success = json[SerializationKeys.success].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    dictionary[SerializationKeys.success] = success
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? Image
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}

public final class Image: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
      static let imageIsLiked = "image_is_liked"
      static let imageDescription = "image_description"
      static let imageName = "image_name"
      static let hasBeachParking = "has_beach_parking"
      static let imageLng = "image_lng"
      static let locationId = "location_id"
      static let locationDescription = "location_description"
      static let locationName = "location_name"
      static let imageLikesCount = "image_likes_count"
      static let createdAt = "created_at"
      static let parkingSpots = "parking_spots"
      static let imageLat = "image_lat"
      static let userAvatar = "user_avatar"
      static let parkingSpotsCounter = "parking_spots_counter"
      static let imageUrl = "image_url"
      static let userId = "user_id"
      static let userName = "user_name"
      static let pricePerHour = "price_per_hour"
      static let locationPrice = "location_price"
    }

    // MARK: Properties
    public var imageIsLiked: String?
    public var imageDescription: String?
    public var imageName: String?
    public var hasBeachParking: String?
    public var imageLng: String?
    public var locationId: String?
    public var locationDescription: String?
    public var locationName: String?
    public var imageLikesCount: String?
    public var createdAt: String?
    public var parkingSpots: [ParkingSpot]?
    public var imageLat: String?
    public var userAvatar: String?
    public var parkingSpotsCounter: String?
    public var imageUrl: String?
    public var userId: String?
    public var userName: String?
    public var pricePerHour: String?
    public var locationPrice: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
      self.init(json: JSON(object))
    }

    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
      imageIsLiked = json[SerializationKeys.imageIsLiked].string
      imageDescription = json[SerializationKeys.imageDescription].string
      imageName = json[SerializationKeys.imageName].string
      hasBeachParking = json[SerializationKeys.hasBeachParking].string
      imageLng = json[SerializationKeys.imageLng].string
      locationId = json[SerializationKeys.locationId].string
      locationDescription = json[SerializationKeys.locationDescription].string
      locationName = json[SerializationKeys.locationName].string
      imageLikesCount = json[SerializationKeys.imageLikesCount].string
      createdAt = json[SerializationKeys.createdAt].string
      if let items = json[SerializationKeys.parkingSpots].array { parkingSpots = items.map { ParkingSpot(json: $0) } }
      imageLat = json[SerializationKeys.imageLat].string
      userAvatar = json[SerializationKeys.userAvatar].string
      parkingSpotsCounter = json[SerializationKeys.parkingSpotsCounter].string
      imageUrl = json[SerializationKeys.imageUrl].string
      userId = json[SerializationKeys.userId].string
      userName = json[SerializationKeys.userName].string
      pricePerHour = json[SerializationKeys.pricePerHour].string
      locationPrice = json[SerializationKeys.locationPrice].string
    }

    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
      var dictionary: [String: Any] = [:]
      if let value = imageIsLiked { dictionary[SerializationKeys.imageIsLiked] = value }
      if let value = imageDescription { dictionary[SerializationKeys.imageDescription] = value }
      if let value = imageName { dictionary[SerializationKeys.imageName] = value }
      if let value = hasBeachParking { dictionary[SerializationKeys.hasBeachParking] = value }
      if let value = imageLng { dictionary[SerializationKeys.imageLng] = value }
      if let value = locationId { dictionary[SerializationKeys.locationId] = value }
      if let value = locationDescription { dictionary[SerializationKeys.locationDescription] = value }
      if let value = locationName { dictionary[SerializationKeys.locationName] = value }
      if let value = imageLikesCount { dictionary[SerializationKeys.imageLikesCount] = value }
      if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
      if let value = parkingSpots { dictionary[SerializationKeys.parkingSpots] = value }
      if let value = imageLat { dictionary[SerializationKeys.imageLat] = value }
      if let value = userAvatar { dictionary[SerializationKeys.userAvatar] = value }
      if let value = parkingSpotsCounter { dictionary[SerializationKeys.parkingSpotsCounter] = value }
      if let value = imageUrl { dictionary[SerializationKeys.imageUrl] = value }
      if let value = userId { dictionary[SerializationKeys.userId] = value }
      if let value = userName { dictionary[SerializationKeys.userName] = value }
      if let value = pricePerHour { dictionary[SerializationKeys.pricePerHour] = value }
      if let value = locationPrice { dictionary[SerializationKeys.locationPrice] = value }
      return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
      self.imageIsLiked = aDecoder.decodeObject(forKey: SerializationKeys.imageIsLiked) as? String
      self.imageDescription = aDecoder.decodeObject(forKey: SerializationKeys.imageDescription) as? String
      self.imageName = aDecoder.decodeObject(forKey: SerializationKeys.imageName) as? String
      self.hasBeachParking = aDecoder.decodeObject(forKey: SerializationKeys.hasBeachParking) as? String
      self.imageLng = aDecoder.decodeObject(forKey: SerializationKeys.imageLng) as? String
      self.locationId = aDecoder.decodeObject(forKey: SerializationKeys.locationId) as? String
      self.locationDescription = aDecoder.decodeObject(forKey: SerializationKeys.locationDescription) as? String
      self.locationName = aDecoder.decodeObject(forKey: SerializationKeys.locationName) as? String
      self.imageLikesCount = aDecoder.decodeObject(forKey: SerializationKeys.imageLikesCount) as? String
      self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
      self.parkingSpots = aDecoder.decodeObject(forKey: SerializationKeys.parkingSpots) as? [ParkingSpot]
      self.imageLat = aDecoder.decodeObject(forKey: SerializationKeys.imageLat) as? String
      self.userAvatar = aDecoder.decodeObject(forKey: SerializationKeys.userAvatar) as? String
      self.parkingSpotsCounter = aDecoder.decodeObject(forKey: SerializationKeys.parkingSpotsCounter) as? String
      self.imageUrl = aDecoder.decodeObject(forKey: SerializationKeys.imageUrl) as? String
      self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
      self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
      self.pricePerHour = aDecoder.decodeObject(forKey: SerializationKeys.pricePerHour) as? String
      self.locationPrice = aDecoder.decodeObject(forKey: SerializationKeys.locationPrice) as? String
    }

    public func encode(with aCoder: NSCoder) {
      aCoder.encode(imageIsLiked, forKey: SerializationKeys.imageIsLiked)
      aCoder.encode(imageDescription, forKey: SerializationKeys.imageDescription)
      aCoder.encode(imageName, forKey: SerializationKeys.imageName)
      aCoder.encode(hasBeachParking, forKey: SerializationKeys.hasBeachParking)
      aCoder.encode(imageLng, forKey: SerializationKeys.imageLng)
      aCoder.encode(locationId, forKey: SerializationKeys.locationId)
      aCoder.encode(locationDescription, forKey: SerializationKeys.locationDescription)
      aCoder.encode(locationName, forKey: SerializationKeys.locationName)
      aCoder.encode(imageLikesCount, forKey: SerializationKeys.imageLikesCount)
      aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
      aCoder.encode(parkingSpots, forKey: SerializationKeys.parkingSpots)
      aCoder.encode(imageLat, forKey: SerializationKeys.imageLat)
      aCoder.encode(userAvatar, forKey: SerializationKeys.userAvatar)
      aCoder.encode(parkingSpotsCounter, forKey: SerializationKeys.parkingSpotsCounter)
      aCoder.encode(imageUrl, forKey: SerializationKeys.imageUrl)
      aCoder.encode(userId, forKey: SerializationKeys.userId)
      aCoder.encode(userName, forKey: SerializationKeys.userName)
      aCoder.encode(pricePerHour, forKey: SerializationKeys.pricePerHour)
      aCoder.encode(locationPrice, forKey: SerializationKeys.locationPrice)
    }

  }

public final class ParkingSpot: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lat = "lat"
    static let lng = "lng"
    static let spotsCounter = "spots_counter"
  }

  // MARK: Properties
  public var lat: String?
  public var lng: String?
  public var spotsCounter: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    lat = json[SerializationKeys.lat].string
    lng = json[SerializationKeys.lng].string
    spotsCounter = json[SerializationKeys.spotsCounter].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = spotsCounter { dictionary[SerializationKeys.spotsCounter] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? String
    self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? String
    self.spotsCounter = aDecoder.decodeObject(forKey: SerializationKeys.spotsCounter) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lat, forKey: SerializationKeys.lat)
    aCoder.encode(lng, forKey: SerializationKeys.lng)
    aCoder.encode(spotsCounter, forKey: SerializationKeys.spotsCounter)
  }

}
