//
//  FMPrefecturesList.swift
//  Fishing Map
//
//  Created by macOS on 10/12/20.
//

import Foundation
import SwiftyJSON

public final class FMPrefecturesList: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let errorcode = "errorcode"
        static let pagination = "pagination"
        static let prefectures = "prefectures"
        static let message = "message"
        static let success = "success"
    }
    
    // MARK: Properties
    public var errorcode: Int?
    public var pagination: PaginationPrefectures?
    public var prefectures: [Prefectures]?
    public var message: String?
    public var success: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        errorcode = json[SerializationKeys.errorcode].int
        pagination = PaginationPrefectures(json: json[SerializationKeys.pagination])
        if let items = json[SerializationKeys.prefectures].array { prefectures = items.map { Prefectures(json: $0) } }
        message = json[SerializationKeys.message].string
        success = json[SerializationKeys.success].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
        if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
        if let value = prefectures { dictionary[SerializationKeys.prefectures] = value.map { $0.dictionaryRepresentation() } }
        if let value = message { dictionary[SerializationKeys.message] = value }
        dictionary[SerializationKeys.success] = success
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
        self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? PaginationPrefectures
        self.prefectures = aDecoder.decodeObject(forKey: SerializationKeys.prefectures) as? [Prefectures]
        self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
        self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
        aCoder.encode(pagination, forKey: SerializationKeys.pagination)
        aCoder.encode(prefectures, forKey: SerializationKeys.prefectures)
        aCoder.encode(message, forKey: SerializationKeys.message)
        aCoder.encode(success, forKey: SerializationKeys.success)
    }
    
}

public final class Prefectures: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let prefectureId = "prefecture_id"
        static let prefectureImageUrl = "prefecture_image_url"
        static let prefectureName = "prefecture_name"
    }
    
    // MARK: Properties
    public var prefectureId: Int?
    public var prefectureImageUrl: String?
    public var prefectureName: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        prefectureId = json[SerializationKeys.prefectureId].int
        prefectureImageUrl = json[SerializationKeys.prefectureImageUrl].string
        prefectureName = json[SerializationKeys.prefectureName].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = prefectureId { dictionary[SerializationKeys.prefectureId] = value }
        if let value = prefectureImageUrl { dictionary[SerializationKeys.prefectureImageUrl] = value }
        if let value = prefectureName { dictionary[SerializationKeys.prefectureName] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.prefectureId = aDecoder.decodeObject(forKey: SerializationKeys.prefectureId) as? Int
        self.prefectureImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.prefectureImageUrl) as? String
        self.prefectureName = aDecoder.decodeObject(forKey: SerializationKeys.prefectureName) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(prefectureId, forKey: SerializationKeys.prefectureId)
        aCoder.encode(prefectureImageUrl, forKey: SerializationKeys.prefectureImageUrl)
        aCoder.encode(prefectureName, forKey: SerializationKeys.prefectureName)
    }
    
}

public final class PaginationPrefectures: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let lastPage = "last_page?"
        static let totalCount = "total_count"
        static let totalPages = "total_pages"
        static let outOfRange = "out_of_range?"
        static let currentPage = "current_page"
        static let firstPage = "first_page?"
    }
    
    // MARK: Properties
    public var lastPage: Bool? = false
    public var totalCount: Int?
    public var totalPages: Int?
    public var outOfRange: Bool? = false
    public var currentPage: Int?
    public var firstPage: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        lastPage? = json[SerializationKeys.lastPage].boolValue
        totalCount = json[SerializationKeys.totalCount].int
        totalPages = json[SerializationKeys.totalPages].int
        outOfRange? = json[SerializationKeys.outOfRange].boolValue
        currentPage = json[SerializationKeys.currentPage].int
        firstPage? = json[SerializationKeys.firstPage].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.lastPage] = lastPage
        if let value = totalCount { dictionary[SerializationKeys.totalCount] = value }
        if let value = totalPages { dictionary[SerializationKeys.totalPages] = value }
        dictionary[SerializationKeys.outOfRange] = outOfRange
        if let value = currentPage { dictionary[SerializationKeys.currentPage] = value }
        dictionary[SerializationKeys.firstPage] = firstPage
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.lastPage? = aDecoder.decodeBool(forKey: SerializationKeys.lastPage)
        self.totalCount = aDecoder.decodeObject(forKey: SerializationKeys.totalCount) as? Int
        self.totalPages = aDecoder.decodeObject(forKey: SerializationKeys.totalPages) as? Int
        self.outOfRange? = aDecoder.decodeBool(forKey: SerializationKeys.outOfRange)
        self.currentPage = aDecoder.decodeObject(forKey: SerializationKeys.currentPage) as? Int
        self.firstPage? = aDecoder.decodeBool(forKey: SerializationKeys.firstPage)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(lastPage, forKey: SerializationKeys.lastPage)
        aCoder.encode(totalCount, forKey: SerializationKeys.totalCount)
        aCoder.encode(totalPages, forKey: SerializationKeys.totalPages)
        aCoder.encode(outOfRange, forKey: SerializationKeys.outOfRange)
        aCoder.encode(currentPage, forKey: SerializationKeys.currentPage)
        aCoder.encode(firstPage, forKey: SerializationKeys.firstPage)
    }
    
}
