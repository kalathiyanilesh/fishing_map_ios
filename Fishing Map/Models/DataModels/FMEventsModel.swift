//
//  FMEventsModel.swift
//  Fishing Map
//
//  Created by macOS on 08/12/20.
//

import Foundation
import SwiftyJSON

public final class FMEvents: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let errorcode = "errorcode"
        static let pagination = "pagination"
        static let message = "message"
        static let events = "events"
        static let success = "success"
    }
    
    // MARK: Properties
    public var errorcode: Int?
    public var pagination: PaginationEvents?
    public var message: String?
    public var events: [Events]?
    public var success: Bool? = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        errorcode = json[SerializationKeys.errorcode].int
        pagination = PaginationEvents(json: json[SerializationKeys.pagination])
        message = json[SerializationKeys.message].string
        if let items = json[SerializationKeys.events].array { events = items.map { Events(json: $0) } }
        success = json[SerializationKeys.success].boolValue
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
        if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
        if let value = message { dictionary[SerializationKeys.message] = value }
        if let value = events { dictionary[SerializationKeys.events] = value.map { $0.dictionaryRepresentation() } }
        dictionary[SerializationKeys.success] = success
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
        self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? PaginationEvents
        self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
        self.events = aDecoder.decodeObject(forKey: SerializationKeys.events) as? [Events]
        self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
        aCoder.encode(pagination, forKey: SerializationKeys.pagination)
        aCoder.encode(message, forKey: SerializationKeys.message)
        aCoder.encode(events, forKey: SerializationKeys.events)
        aCoder.encode(success, forKey: SerializationKeys.success)
    }
    
}

public final class PaginationEvents: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let lastPage = "last_page?"
        static let totalCount = "total_count"
        static let totalPages = "total_pages"
        static let outOfRange = "out_of_range?"
        static let currentPage = "current_page"
        static let firstPage = "first_page?"
        static let nextPage = "next_page"
    }
    
    // MARK: Properties
    public var lastPage: Bool? = false
    public var totalCount: Int?
    public var totalPages: Int?
    public var outOfRange: Bool? = false
    public var currentPage: Int?
    public var firstPage: Bool? = false
    public var nextPage: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        lastPage? = json[SerializationKeys.lastPage].boolValue
        totalCount = json[SerializationKeys.totalCount].int
        totalPages = json[SerializationKeys.totalPages].int
        outOfRange? = json[SerializationKeys.outOfRange].boolValue
        currentPage = json[SerializationKeys.currentPage].int
        firstPage? = json[SerializationKeys.firstPage].boolValue
        nextPage = json[SerializationKeys.nextPage].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.lastPage] = lastPage
        if let value = totalCount { dictionary[SerializationKeys.totalCount] = value }
        if let value = totalPages { dictionary[SerializationKeys.totalPages] = value }
        dictionary[SerializationKeys.outOfRange] = outOfRange
        if let value = currentPage { dictionary[SerializationKeys.currentPage] = value }
        if let value = nextPage { dictionary[SerializationKeys.nextPage] = value }
        dictionary[SerializationKeys.firstPage] = firstPage
        
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.lastPage? = aDecoder.decodeBool(forKey: SerializationKeys.lastPage)
        self.totalCount = aDecoder.decodeObject(forKey: SerializationKeys.totalCount) as? Int
        self.totalPages = aDecoder.decodeObject(forKey: SerializationKeys.totalPages) as? Int
        self.outOfRange? = aDecoder.decodeBool(forKey: SerializationKeys.outOfRange)
        self.currentPage = aDecoder.decodeObject(forKey: SerializationKeys.currentPage) as? Int
        self.nextPage = aDecoder.decodeObject(forKey: SerializationKeys.nextPage) as? Int
        self.firstPage? = aDecoder.decodeBool(forKey: SerializationKeys.firstPage)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(lastPage, forKey: SerializationKeys.lastPage)
        aCoder.encode(totalCount, forKey: SerializationKeys.totalCount)
        aCoder.encode(totalPages, forKey: SerializationKeys.totalPages)
        aCoder.encode(outOfRange, forKey: SerializationKeys.outOfRange)
        aCoder.encode(currentPage, forKey: SerializationKeys.currentPage)
        aCoder.encode(nextPage, forKey: SerializationKeys.nextPage)
        aCoder.encode(firstPage, forKey: SerializationKeys.firstPage)
    }
    
}

public final class Events: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let eventLat = "event_lat"
        static let eventPrefectureId = "event_prefecture_id"
        static let eventAddress = "event_address"
        static let eventTitle = "event_title"
        static let eventOceanCleaning = "event_ocean_cleaning"
        static let eventRegistrationLink = "event_registration_link"
        static let eventImageUrl = "event_image_url"
        static let eventMapAddress = "event_map_address"
        static let eventPrefectureName = "event_prefecture_name"
        static let eventAttendeesCount = "event_attendees_count"
        static let eventLikesCount = "event_likes_count"
        static let eventIsLiked = "event_is_liked"
        static let eventIsRegistered = "event_is_registered"
        static let eventDescription = "event_description"
        static let eventAttendessAvatarUrls = "event_attendess_avatar_urls"
        static let userAvatar = "user_avatar"
        static let eventDate = "event_date"
        static let eventCommentsCount = "event_comments_count"
        static let createdAt = "created_at"
        static let userId = "user_id"
        static let userName = "user_name"
        static let eventComments = "event_comments"
        static let eventLng = "event_lng"
        static let eventId = "event_id"
    }
    
    // MARK: Properties
    public var eventLat: String?
    public var eventPrefectureId: Int?
    public var eventAddress: String?
    public var eventTitle: String?
    public var eventOceanCleaning: Bool? = false
    public var eventRegistrationLink: String?
    public var eventImageUrl: String?
    public var eventMapAddress: String?
    public var eventPrefectureName: String?
    public var eventAttendeesCount: Int?
    public var eventLikesCount: Int?
    public var eventIsLiked: Bool? = false
    public var eventIsRegistered: Bool? = false
    public var eventDescription: String?
    public var eventAttendessAvatarUrls: [EventAttendessAvatarUrls]?
    public var userAvatar: String?
    public var eventDate: Int?
    public var eventCommentsCount: Int?
    public var createdAt: Int?
    public var userId: Int?
    public var userName: String?
    public var eventComments: [EventComments]?
    public var eventLng: String?
    public var eventId: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        eventLat = json[SerializationKeys.eventLat].string
        eventPrefectureId = json[SerializationKeys.eventPrefectureId].int
        eventAddress = json[SerializationKeys.eventAddress].string
        eventTitle = json[SerializationKeys.eventTitle].string
        eventOceanCleaning = json[SerializationKeys.eventOceanCleaning].boolValue
        eventRegistrationLink = json[SerializationKeys.eventRegistrationLink].string
        eventImageUrl = json[SerializationKeys.eventImageUrl].string
        eventMapAddress = json[SerializationKeys.eventMapAddress].string
        eventPrefectureName = json[SerializationKeys.eventPrefectureName].string
        eventAttendeesCount = json[SerializationKeys.eventAttendeesCount].int
        eventLikesCount = json[SerializationKeys.eventLikesCount].int
        eventIsLiked = json[SerializationKeys.eventIsLiked].boolValue
        eventIsRegistered = json[SerializationKeys.eventIsRegistered].boolValue
        eventDescription = json[SerializationKeys.eventDescription].string
        if let items = json[SerializationKeys.eventAttendessAvatarUrls].array { eventAttendessAvatarUrls = items.map { EventAttendessAvatarUrls(json: $0) } }
        userAvatar = json[SerializationKeys.userAvatar].string
        eventDate = json[SerializationKeys.eventDate].int
        eventCommentsCount = json[SerializationKeys.eventCommentsCount].int
        createdAt = json[SerializationKeys.createdAt].int
        userId = json[SerializationKeys.userId].int
        userName = json[SerializationKeys.userName].string
        if let items = json[SerializationKeys.eventComments].array { eventComments = items.map { EventComments(json: $0) } }
        eventLng = json[SerializationKeys.eventLng].string
        eventId = json[SerializationKeys.eventId].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = eventLat { dictionary[SerializationKeys.eventLat] = value }
        if let value = eventPrefectureId { dictionary[SerializationKeys.eventPrefectureId] = value }
        if let value = eventAddress { dictionary[SerializationKeys.eventAddress] = value }
        if let value = eventTitle { dictionary[SerializationKeys.eventTitle] = value }
        dictionary[SerializationKeys.eventOceanCleaning] = eventOceanCleaning
        if let value = eventRegistrationLink { dictionary[SerializationKeys.eventRegistrationLink] = value }
        if let value = eventImageUrl { dictionary[SerializationKeys.eventImageUrl] = value }
        if let value = eventMapAddress { dictionary[SerializationKeys.eventMapAddress] = value }
        if let value = eventPrefectureName { dictionary[SerializationKeys.eventPrefectureName] = value }
        if let value = eventAttendeesCount { dictionary[SerializationKeys.eventAttendeesCount] = value }
        if let value = eventLikesCount { dictionary[SerializationKeys.eventLikesCount] = value }
        dictionary[SerializationKeys.eventIsLiked] = eventIsLiked
        dictionary[SerializationKeys.eventIsRegistered] = eventIsRegistered
        if let value = eventDescription { dictionary[SerializationKeys.eventDescription] = value }
        if let value = eventAttendessAvatarUrls { dictionary[SerializationKeys.eventAttendessAvatarUrls] = value.map { $0.dictionaryRepresentation() } }
        if let value = userAvatar { dictionary[SerializationKeys.userAvatar] = value }
        if let value = eventDate { dictionary[SerializationKeys.eventDate] = value }
        if let value = eventCommentsCount { dictionary[SerializationKeys.eventCommentsCount] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = eventComments { dictionary[SerializationKeys.eventComments] = value.map { $0.dictionaryRepresentation() } }
        if let value = eventLng { dictionary[SerializationKeys.eventLng] = value }
        if let value = eventId { dictionary[SerializationKeys.eventId] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.eventLat = aDecoder.decodeObject(forKey: SerializationKeys.eventLat) as? String
        self.eventPrefectureId = aDecoder.decodeObject(forKey: SerializationKeys.eventPrefectureId) as? Int
        self.eventAddress = aDecoder.decodeObject(forKey: SerializationKeys.eventAddress) as? String
        self.eventTitle = aDecoder.decodeObject(forKey: SerializationKeys.eventTitle) as? String
        self.eventOceanCleaning = aDecoder.decodeBool(forKey: SerializationKeys.eventOceanCleaning)
        self.eventRegistrationLink = aDecoder.decodeObject(forKey: SerializationKeys.eventRegistrationLink) as? String
        self.eventImageUrl = aDecoder.decodeObject(forKey: SerializationKeys.eventImageUrl) as? String
        self.eventMapAddress = aDecoder.decodeObject(forKey: SerializationKeys.eventMapAddress) as? String
        self.eventPrefectureName = aDecoder.decodeObject(forKey: SerializationKeys.eventPrefectureName) as? String
        self.eventAttendeesCount = aDecoder.decodeObject(forKey: SerializationKeys.eventAttendeesCount) as? Int
        self.eventLikesCount = aDecoder.decodeObject(forKey: SerializationKeys.eventLikesCount) as? Int
        self.eventIsLiked = aDecoder.decodeBool(forKey: SerializationKeys.eventIsLiked)
        self.eventIsRegistered = aDecoder.decodeBool(forKey: SerializationKeys.eventIsRegistered)
        self.eventDescription = aDecoder.decodeObject(forKey: SerializationKeys.eventDescription) as? String
        self.eventAttendessAvatarUrls = aDecoder.decodeObject(forKey: SerializationKeys.eventAttendessAvatarUrls) as? [EventAttendessAvatarUrls]
        self.userAvatar = aDecoder.decodeObject(forKey: SerializationKeys.userAvatar) as? String
        self.eventDate = aDecoder.decodeObject(forKey: SerializationKeys.eventDate) as? Int
        self.eventCommentsCount = aDecoder.decodeObject(forKey: SerializationKeys.eventCommentsCount) as? Int
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? Int
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
        self.eventComments = aDecoder.decodeObject(forKey: SerializationKeys.eventComments) as? [EventComments]
        self.eventLng = aDecoder.decodeObject(forKey: SerializationKeys.eventLng) as? String
        self.eventId = aDecoder.decodeObject(forKey: SerializationKeys.eventId) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(eventLat, forKey: SerializationKeys.eventLat)
        aCoder.encode(eventPrefectureId, forKey: SerializationKeys.eventPrefectureId)
        aCoder.encode(eventAddress, forKey: SerializationKeys.eventAddress)
        aCoder.encode(eventTitle, forKey: SerializationKeys.eventTitle)
        aCoder.encode(eventOceanCleaning, forKey: SerializationKeys.eventOceanCleaning)
        aCoder.encode(eventRegistrationLink, forKey: SerializationKeys.eventRegistrationLink)
        aCoder.encode(eventImageUrl, forKey: SerializationKeys.eventImageUrl)
        aCoder.encode(eventMapAddress, forKey: SerializationKeys.eventMapAddress)
        aCoder.encode(eventPrefectureName, forKey: SerializationKeys.eventPrefectureName)
        aCoder.encode(eventAttendeesCount, forKey: SerializationKeys.eventAttendeesCount)
        aCoder.encode(eventLikesCount, forKey: SerializationKeys.eventLikesCount)
        aCoder.encode(eventIsLiked, forKey: SerializationKeys.eventIsLiked)
        aCoder.encode(eventIsRegistered, forKey: SerializationKeys.eventIsRegistered)
        aCoder.encode(eventDescription, forKey: SerializationKeys.eventDescription)
        aCoder.encode(eventAttendessAvatarUrls, forKey: SerializationKeys.eventAttendessAvatarUrls)
        aCoder.encode(userAvatar, forKey: SerializationKeys.userAvatar)
        aCoder.encode(eventDate, forKey: SerializationKeys.eventDate)
        aCoder.encode(eventCommentsCount, forKey: SerializationKeys.eventCommentsCount)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(userName, forKey: SerializationKeys.userName)
        aCoder.encode(eventComments, forKey: SerializationKeys.eventComments)
        aCoder.encode(eventLng, forKey: SerializationKeys.eventLng)
        aCoder.encode(eventId, forKey: SerializationKeys.eventId)
    }
    
}

public final class EventComments: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let isViewerAuthor = "is_viewer_author"
        static let comment = "comment"
        static let userId = "user_id"
        static let userName = "user_name"
        static let userAvatar = "user_avatar"
    }
    
    // MARK: Properties
    public var isViewerAuthor: Bool? = false
    public var comment: String?
    public var userId: Int?
    public var userName: String?
    public var userAvatar: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        isViewerAuthor = json[SerializationKeys.isViewerAuthor].boolValue
        comment = json[SerializationKeys.comment].string
        userId = json[SerializationKeys.userId].int
        userName = json[SerializationKeys.userName].string
        userAvatar = json[SerializationKeys.userAvatar].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[SerializationKeys.isViewerAuthor] = isViewerAuthor
        if let value = comment { dictionary[SerializationKeys.comment] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = userName { dictionary[SerializationKeys.userName] = value }
        if let value = userAvatar { dictionary[SerializationKeys.userAvatar] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.isViewerAuthor = aDecoder.decodeBool(forKey: SerializationKeys.isViewerAuthor)
        self.comment = aDecoder.decodeObject(forKey: SerializationKeys.comment) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.userName = aDecoder.decodeObject(forKey: SerializationKeys.userName) as? String
        self.userAvatar = aDecoder.decodeObject(forKey: SerializationKeys.userAvatar) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(isViewerAuthor, forKey: SerializationKeys.isViewerAuthor)
        aCoder.encode(comment, forKey: SerializationKeys.comment)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(userName, forKey: SerializationKeys.userName)
        aCoder.encode(userAvatar, forKey: SerializationKeys.userAvatar)
    }
    
}

public final class EventAttendessAvatarUrls: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let avatarUrl = "avatar_url"
    }
    
    // MARK: Properties
    public var avatarUrl: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        avatarUrl = json[SerializationKeys.avatarUrl].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = avatarUrl { dictionary[SerializationKeys.avatarUrl] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.avatarUrl = aDecoder.decodeObject(forKey: SerializationKeys.avatarUrl) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(avatarUrl, forKey: SerializationKeys.avatarUrl)
    }
    
}
