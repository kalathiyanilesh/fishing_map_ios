//
//  FMComments.swift
//
//  Created by iMac on 08/12/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FMComments: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let errorcode = "errorcode"
    static let pagination = "pagination"
    static let comments = "comments"
    static let message = "message"
    static let success = "success"
  }

  // MARK: Properties
  public var errorcode: Int?
  public var pagination: Pagination?
  public var comments: [PostComments]?
  public var message: String?
  public var success: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    errorcode = json[SerializationKeys.errorcode].int
    pagination = Pagination(json: json[SerializationKeys.pagination])
    if let items = json[SerializationKeys.comments].array { comments = items.map { PostComments(json: $0) } }
    message = json[SerializationKeys.message].string
    success = json[SerializationKeys.success].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = errorcode { dictionary[SerializationKeys.errorcode] = value }
    if let value = pagination { dictionary[SerializationKeys.pagination] = value.dictionaryRepresentation() }
    if let value = comments { dictionary[SerializationKeys.comments] = value.map { $0.dictionaryRepresentation() } }
    if let value = message { dictionary[SerializationKeys.message] = value }
    dictionary[SerializationKeys.success] = success
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.errorcode = aDecoder.decodeObject(forKey: SerializationKeys.errorcode) as? Int
    self.pagination = aDecoder.decodeObject(forKey: SerializationKeys.pagination) as? Pagination
    self.comments = aDecoder.decodeObject(forKey: SerializationKeys.comments) as? [PostComments]
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.success = aDecoder.decodeBool(forKey: SerializationKeys.success)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(errorcode, forKey: SerializationKeys.errorcode)
    aCoder.encode(pagination, forKey: SerializationKeys.pagination)
    aCoder.encode(comments, forKey: SerializationKeys.comments)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(success, forKey: SerializationKeys.success)
  }

}
