//
//  Constants.swift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

enum Mode {
    case development
    case live
}

struct FishApp {
    
    static var mode:Mode = .development
    
    static var ImageFolder : String {
        if mode == .development {
            return "testing_fishingmap_images"
        }else {
            return "fishingmap_images"
        }
    }
}

let kDATA = "data"
let kSUCCESS = "success"
let kMESSAGE = "message"
let kRESPONSE_STATUS = "ResponseStatus"

let SERVER_URL = "https://api.fishmap.jp/api/v1/" // production
//let SERVER_URL = "http://54.95.171.72:3000/api/v1/" //devlopement

//MARK:- API end points
let API_CHECK_REGISTERED = "auth/is_registered"
let API_LOGIN = "auth/manual"
let API_SIGNIN = "registrations"
let API_SOCIAL = "auth/social"
let API_LOGOUT = "auth/logout"
let API_GET_FEED_POSTS = "feed_posts"
let API_GET_SEARCH_INITIAL = "feed_posts/search_initial"
let API_GET_SEARCH_RESULTS = "feed_posts/search_results"
let API_LIKE = "like"
let API_UNLIKE = "unlike"
let API_ADD_COMMENT = "comment"
let API_GET_COMMENT = "comments"
let API_GET_EVENTS = "events"
let API_FISH_LOCATIONS = "locations/fish_locations"
let API_LOCATION = "locations"
let API_CATEGORY = "categories"
let API_CREATE_IMAGES = "images"
let API_GET_PREFECTURES = "prefectures"
let API_SHOW_IMAGE = "show_image"
let API_GET_ALL_IMAGES = "images"
let API_REGISTER = "register"
let API_UNREGISTER = "unregister"
let API_TRENDING_EVENT = "events/trending"
let API_SEARCH_EVENT = "events/search"
let API_FILTER_EVENT = "events/filter"
let API_PRODCTS = "products"
let API_PRODUCT_IMAGE = "product_images"
let API_UPLOAD_IMAGE = "upload_image"
let API_USER = "users"
let API_USER_POST = "posts"
let API_USER_IMAGES = "images"
let API_EDIT_PROFILE = "edit_profile"
let API_NOTIFICATION = "notifications"
let API_READ_NOTIFICATION = "read_notifications"
let API_NEW_LOCATIONS = "locations/new_locations"
let API_SEARCH_LOCATION = "locations/search_location"
let API_Report = "reports"
let API_DELETE_FEED =  "feed_posts"

let TERMS_CONDITION = ""

//device_type values: {"android"=>0, "ios"=>1, "web"=>2}
let DeviceType = 1

