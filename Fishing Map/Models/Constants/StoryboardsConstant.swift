//
//  StoryboardsConstant.swift
//  PriceScale
//
//  Created by Pinkesh on 08/10/19.
//  Copyright © 2019 Pinkesh. All rights reserved.
//

import Foundation

struct Storyboards {
    static let LaunchScreen = "LaunchScreen"
    //static let Main = "Main"
}

struct ViewControllers {
    static let HomeVC = "HomeVC"
    static let OptionOneVC = "OptionOneVC"
    static let OptionTwoVC = "OptionTwoVC"
}
