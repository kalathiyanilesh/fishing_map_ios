//
//  constant.swift

import Foundation
import UIKit
import CoreLocation
import SystemConfiguration
import SwiftMessages
import AVFoundation
import FloatingPanel
import Toaster
import Photos
import UPCarouselFlowLayout
import Firebase
import GoogleMaps
import FirebaseMessaging

var spinner = RPLoadingAnimationView.init(frame: CGRect.zero)
var overlayView = UIView()
var lat_currnt: Double = 0
var long_currnt: Double = 0

/*
notify_type: [:generic, :event, :post, :location, :image]
notify_subtype: [:approval, :like, :comment, :register]*/
enum NOTIFICATION_TYPE: String {
    case GENERIC = "generic"
    case EVENT = "event"
    case POST = "post"
    case LOCATION = "location"
    case IMAGE = "image"
}

public let isSimulator: Bool = {
    var isSim = false
    #if arch(i386) || arch(x86_64)
        isSim = true
    #endif
    return isSim
}()

public func FONT_DISPLAY_PRO_LIGHT(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFProDisplay-Light", size: fontSize)!
}

public func FONT_PRO_BOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFProText-Bold", size: fontSize)!
}

public func FONT_SFUI_REG(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFUIText-Regular", size: fontSize)!
}

public func FONT_SFUI_SEMIBOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "SFUIText-Semibold", size: fontSize)!
}

public func FONT_INTERSTATE_BOLD(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "Interstate-bold", size: fontSize)!
}

public func FONT_INTERSTATE_LIGHT(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "Interstate-light", size: fontSize)!
}

public func FONT_INTERSTATE_REGULAR(fontSize: CGFloat) -> UIFont{
    return UIFont(name: "Interstate-regular", size: fontSize)!
}

/*
func isSystemInDarkMode() -> Bool {
    return APP_DELEGATE.window!.traitCollection.userInterfaceStyle == .dark ? true : false
}*/

//MARK:-  Get VC for navigation
//var banner = NotificationBanner(title: "", subtitle: "", style: .success)
public func getStoryboard(storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: storyboardName, bundle: nil)
}

public func loadVC(strStoryboardId: String, strVCId: String) -> UIViewController {
    let vc = getStoryboard(storyboardName: strStoryboardId).instantiateViewController(withIdentifier: strVCId)
    return vc
}

public var CurrentTimeStamp: String
{
    return "\(NSDate().timeIntervalSince1970 * 1000)"
}

func convertTextToQRCode(text: String) -> UIImage {
    
    let size : CGSize = CGSize(width: 500, height: 500)
    let data = text.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
    
    let filter = CIFilter(name: "CIQRCodeGenerator")!
    
    filter.setValue(data, forKey: "inputMessage")
    filter.setValue("L", forKey: "inputCorrectionLevel")
    
    let qrcodeCIImage = filter.outputImage!
    
    let cgImage = CIContext(options:nil).createCGImage(qrcodeCIImage, from: qrcodeCIImage.extent)
    UIGraphicsBeginImageContext(CGSize(width: size.width * UIScreen.main.scale, height:size.height * UIScreen.main.scale))
    let context = UIGraphicsGetCurrentContext()
    context!.interpolationQuality = .none
    
    context?.draw(cgImage!, in: CGRect(x: 0.0,y: 0.0,width: context!.boundingBoxOfClipPath.width,height: context!.boundingBoxOfClipPath.height))
    
    let preImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    let qrCodeImage = UIImage(cgImage: (preImage?.cgImage!)!, scale: 1.0/UIScreen.main.scale, orientation: .downMirrored)
    
    return qrCodeImage
}

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func getUserInfo() -> NSDictionary {
    var dictUser: NSDictionary = NSDictionary()
    if let dictData = UserDefaults.standard.value(forKey: UD_UserData) as? NSDictionary {
        dictUser = dictData
    }
    return dictUser
}

func getUserID() -> String {
    if(UserDefaults.standard.string(forKey: UD_UserId) == nil) {
        return ""
    } else {
        return UserDefaults.standard.string(forKey: UD_UserId)!
    }
}

func getDeviceToken() -> String {
    return Messaging.messaging().fcmToken != nil ? Messaging.messaging().fcmToken! : "SIMULATOR"
}

func compressImage(image:UIImage) -> NSData
{
    var compression:CGFloat!
    let maxCompression:CGFloat!
    compression = 0.9
    maxCompression = 0.1
    var imageData = image.jpegData(compressionQuality: compression)! as NSData
    while (imageData.length > 10 && compression > maxCompression)
    {
        compression = compression - 0.10
        imageData = image.jpegData(compressionQuality: compression)! as NSData
    }
    return imageData
}

func SetCursorColor() {
    UITextField.appearance().tintColor = APP_COLOR
    UITextView.appearance().tintColor = APP_COLOR
}

func setNoDataLabel(tableView:UITableView, array:NSMutableArray, text:String) -> Int
{
    var numOfSections = 0
    if array.count != 0 {
        tableView.backgroundView = nil
        numOfSections = 1
    } else {
        let noDataLabel = UILabel()
        noDataLabel.frame = CGRect(x: 10, y: 0, width: tableView.frame.size.width-20, height: tableView.frame.size.height)
        noDataLabel.text = text
        noDataLabel.numberOfLines = 0
        noDataLabel.font = FontWithSize("Montserrat-Regular", 12)
        noDataLabel.textColor = UIColor.lightGray
        noDataLabel.textAlignment = NSTextAlignment.center
        tableView.backgroundView = noDataLabel
        tableView.separatorStyle = .none
    }
    
    return numOfSections
}

func changeDateFormat(strDate: String,FormatFrom: String, FormateTo: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = FormatFrom
    let date = dateFormat.date(from: strDate)
    dateFormat.dateFormat = FormateTo
    if date == nil {
        return strDate
    }
    return dateFormat.string(from: date!)
}

public func convert(dicData:NSDictionary,toString:(_ strData:String)->())
{
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dicData)
        if let json = String(data: jsonData, encoding: .utf8) {
            toString(json)
        }
    } catch {
        print("something went wrong with parsing json")
    }
}

public func convert(JSONstring:String,toDictionary:(_ strData:NSDictionary)->())
{
    var strJSON:String = JSONstring
    strJSON = strJSON.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    let arr = strJSON.components(separatedBy: "\n")
    var dict : [String:Any]?
    for jsonString in arr{
        if let jsonDataToVerify = jsonString.data(using: String.Encoding.utf8)
        {
            do {
                dict = try JSONSerialization.jsonObject(with: jsonDataToVerify) as? [String : Any]
                print("JSON is valid.")
                toDictionary(dict! as NSDictionary)
            } catch {
                //print("Error deserializing JSON: \(error.localizedDescription)")
            }
        }
    }
}

public func getMainBaseCoin(strCoin:String,to:(_ strMain:String, _ strBase:String)->())
{
    if strCoin.count > 0 {
        let arrCoins = strCoin.components(separatedBy: "/")
        if arrCoins.count == 2 {
            to(arrCoins[0],arrCoins[1])
        }
    }
}

func dictionaryOfFilteredBy(dict: NSDictionary) -> NSDictionary {
    let replaced: NSMutableDictionary = NSMutableDictionary(dictionary : dict)
    let blank: String = ""
    for (key, _) in dict
    {
        let object = dict[key] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[key] = blank as AnyObject?
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[key] = dictionaryOfFilteredBy(dict: object as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[key] = arrayOfFilteredBy(arr: object as! NSArray)
        }
        else
        {
            replaced[key] = "\(object)" as AnyObject?
        }
    }
    return replaced
}

func arrayOfFilteredBy(arr: NSArray) -> NSArray {
    
    let replaced: NSMutableArray = NSMutableArray(array: arr)
    let blank: String = ""
    
    for i in 0..<arr.count
    {
        let object = arr[i] as AnyObject
        
        if (object.isKind(of: NSNull.self))
        {
            replaced[i] = blank as AnyObject
        }
        else if (object is [AnyHashable: AnyObject])
        {
            replaced[i] = dictionaryOfFilteredBy(dict: arr[i] as! NSDictionary)
        }
        else if (object is [AnyObject])
        {
            replaced[i] = arrayOfFilteredBy(arr: arr[i] as! NSArray)
        }
        else
        {
            replaced[i] = "\(object)" as AnyObject
        }
        
    }
    return replaced
}

public var mostTopViewController: UIViewController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    set {
        UIApplication.shared.keyWindow?.rootViewController = newValue
    }
}

//MARK:- iOS version checking Functions
public var appDisplayName: String? {
    return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
}
public var appBundleID: String? {
    return Bundle.main.bundleIdentifier
}
public func IOS_VERSION() -> String {
    return UIDevice.current.systemVersion
}
public var statusBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
}
public var topSafeAreaHeight: CGFloat {
    return APP_DELEGATE.window?.safeAreaInsets.top ?? 0
}
public var applicationIconBadgeNumber: Int {
    get {
        return UIApplication.shared.applicationIconBadgeNumber
    }
    set {
        UIApplication.shared.applicationIconBadgeNumber = newValue
    }
}
public var appVersion: String? {
    return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
}
public func SCREENWIDTH() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.width
}

public func SCREENHEIGHT() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.height
}

func isIPhoneX()-> Bool {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            return false
            
        case 1334:
            return false
            
        case 1920, 2208:
            return false
            
        case 2436:
            return true
            
        case 2688:
            return true
            
        case 1792:
            return true
            
        default:
            return false
        }
    }
    return false
}


//MARK:- SwiftLoader
public func showLoaderHUD(strMessage:String)
{
    let activityData = ActivityData()
    NVActivityIndicatorView.DEFAULT_TYPE = .ballPulse
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = ""
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}

public func hideLoaderHUD()
{
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    spinner.removeFromSuperview()
    overlayView.removeFromSuperview()
}

public func showMessage(_ bodymsg:String)
{
    ToastView.appearance().backgroundColor = UIColor.black //Color_Hex(hex: "3CBCB4")
    ToastView.appearance().font = FontWithSize("Interstate-Regular", 12)
    Toast(text: bodymsg).show()
    /*
    let view = MessageView.viewFromNib(layout: .cardView)
    //view.configureDropShadow()
    view.configureContent(body: bodymsg)
    view.layoutMarginAdditions = UIEdgeInsets(top: 25, left: 10, bottom: 20, right: 10)
    view.configureTheme(.warning, iconStyle: .light)
    
    let backgroundColor = Color_Hex(hex: "404040")
    let foregroundColor = UIColor.white
    
    view.configureTheme(backgroundColor: backgroundColor, foregroundColor: foregroundColor)
    view.titleLabel?.isHidden = true
    view.button?.isHidden = true
    view.iconImageView?.isHidden = true
    view.iconLabel?.isHidden = true
    SwiftMessages.show(view: view)*/
}

func getAddress(_ selectedLat: Double, _ selectedLon: Double, handler: @escaping (String) -> Void)
{
    APP_DELEGATE.getCurrentLocation()
    let address: String = ""
    let geocoder = GMSGeocoder()
    let position: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:selectedLat, longitude: selectedLon)
    geocoder.reverseGeocodeCoordinate(position) { response, error in
        if let location = response?.firstResult() {
            let lines = location.lines! as [String]
            let address = lines.joined(separator: "\n")
            print("Current Address:", address)
            handler(address)
        } else {
            handler(address)
            print("reverse geodcode fail: \(error?.localizedDescription ?? "Not Found")")
        }
    }
    
    /*
    let geoCoder = CLGeocoder()
    let location = CLLocation(latitude: selectedLat, longitude: selectedLon)
    //selectedLat and selectedLon are double values set by the app in a previous process
    
    geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
        
        // Place details
        var placeMark: CLPlacemark?
        placeMark = placemarks?[0]
        
        // Address dictionary
        //print(placeMark.addressDictionary ?? "")
        
        // Location name
        if let locationName = placeMark?.addressDictionary?["Name"] as? String {
            address += locationName + ", "
        }
        
        // Street address
        if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
            address += street + ", "
        }
        
        // City
        if let city = placeMark?.addressDictionary?["City"] as? String {
            address += city + ", "
        }
        
        // Zip code
        if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
            address += zip + ", "
        }
        
        // Country
        if let country = placeMark?.addressDictionary?["Country"] as? String {
            address += country
        }
        
       // Passing address back
       handler(address)
    })*/
}

//MARK:- Network indicator
public func ShowNetworkIndicator(xx :Bool)
{
    runOnMainThreadWithoutDeadlock {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
}

//MARK : Length validation
public func TRIM(string: Any) -> String
{
    return (string as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
}

public func validateTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTxtViewLength(_ txtVal: UITextView, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTextLength(_ strVal: String, withMessage msg: String) -> Bool {
    if TRIM(string: strVal).count == 0
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateAmount(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    var amount : Double = 0
    if TRIM(string: txtVal.text!).count > 0{
        amount = Double(TRIM(string: txtVal.text ?? ""))!
    }
    if amount <= 0 {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateMinTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count != 10
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    print("!=10 length")
    return true
}

public func validateMaxTxtFieldLength(_ txtVal: UITextField, lenght:Int,msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "").count > lenght
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validatePasswordLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count < 6
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}
public func passwordMismatch(_ txtVal: UITextField, _ txtVal1: UITextField, withMessage msg: String) -> Bool
{
    if TRIM(string: txtVal.text ?? "") != TRIM(string: txtVal1.text ?? "")
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateEmailAddress(_ txtVal: UITextField ,withMessage msg: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    if(emailTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validatePhoneNo(_ txtVal: UITextField ,withMessage msg: String) -> Bool
{
    let PHONE_REGEX = "^[0-9]{6,}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    if(phoneTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validateTermsCondition(_ termsSelected: Bool, withMessage msg: String) -> Bool {
    if !termsSelected
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateBool(_ isSelected: Bool, withMessage msg: String) -> Bool {
    if !isSelected
    {
        showMessage(msg)
        return false
    }
    return true
}

public func isBase64(stringBase64:String) -> Bool
{
    let regex = "([A-Za-z0-9+/]{4})*" + "([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)"
    let test = NSPredicate(format:"SELF MATCHES %@", regex)
    if(test.evaluate(with: stringBase64) != true)
    {
        return false
    }
    return true
}

public func getDateString(_ date: Date,format: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = format
    return dateFormat.string(from: date)
}

public func isUserLogin() -> Bool {
    if UserDefaults.standard.value(forKey: IS_LOGIN) == nil{
        return false
    }
    return true
}

public func getAuthToken() -> [String:String] {
    if UserDefaults.standard.value(forKey: UD_AUTHTOKEN) != nil {
        return ["Authorization":UserDefaults.standard.value(forKey: UD_AUTHTOKEN) as? String ?? ""]
    }
    return ["Authorization":""]
}

//Convert Int to Date
public func intToDate(miliseconds: Int, shortMonthName: Bool) -> String {
    //Convert to Date
    let date = NSDate(timeIntervalSince1970: Double(miliseconds))
    
    //Date formatting
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = shortMonthName ? "dd\nMMM" : "dd MMMM" // "dd MMMM yyyy HH:mm:a"
    //dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
    let dateString = dateFormatter.string(from: date as Date)
    
    return dateString
}

func getThumbnailImage(forUrl url: URL) -> UIImage? {
    let asset: AVAsset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)

    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 60) , actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }

    return nil
}

func saveUserData(_ obj: User?, _ dictUser: NSDictionary?) {
    APP_DELEGATE.objUser = obj
    UserDefaults.standard.set(obj?.userId, forKey: UD_UserId)
    if let dictdata = dictUser {
        UserDefaults.standard.set(dictdata, forKey: UD_UserData)
    }
    UserDefaults.standard.set("1", forKey: IS_LOGIN)
    UserDefaults.standard.synchronize()
    apiGetCategory()
}

func Logoutuser() {
    if !isUserLogin() { return }
    apiLogout()
    UserDefaults.standard.removeObject(forKey: UD_UserData)
    UserDefaults.standard.removeObject(forKey: UD_AUTHTOKEN)
    UserDefaults.standard.removeObject(forKey: UD_UserData)
    UserDefaults.standard.removeObject(forKey: IS_LOGIN)
    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "navlogin")
    APP_DELEGATE.window?.rootViewController = vc
    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
    }, completion: nil)
}

//MARK:- - Get image from image name
public func Set_Local_Image(imageName :String) -> UIImage
{
    return UIImage(named:imageName)!
}

//MARK:- FONT
public func FontWithSize(_ fname: String,_ fsize: Int) -> UIFont
{
    return UIFont(name: fname, size: CGFloat(fsize))!
}

public func randomColor() -> UIColor {
    let r: UInt32 = arc4random_uniform(255)
    let g: UInt32 = arc4random_uniform(255)
    let b: UInt32 = arc4random_uniform(255)
    
    return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0)
}

//Mark : string to dictionary
public func convertStringToDictionary(str:String) -> [String: Any]? {
    if let data = str.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

//MARK: dictionary to string
public func convertDictionaryToJSONString(dic:NSDictionary) -> String? {
    do{
        let jsonData: Data? = try JSONSerialization.data(withJSONObject: dic, options: [])
        var myString: String? = nil
        if let aData = jsonData {
            myString = String(data: aData, encoding: .utf8)
        }
        return myString
    }catch{
        print(error)
    }
    return ""
}

//MARK:-Check Internet connection
func isConnectedToNetwork() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    })
    else
    {
        return false
    }
    
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    let available =  (isReachable && !needsConnection)
    if(available)
    {
        return true
    }
    else
    {
        showMessage(InternetNotAvail)
        return false
    }
}

func Color_Hex(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func ConvertDateInFormate(d: String, _ from: String, _ to: String) -> String {
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = from
    let ReminderDate = dateFormat.date(from: d)
    dateFormat.dateFormat = to
    return dateFormat.string(from: ReminderDate!)
}

func popBack<T: UIViewController>(_ navController: UINavigationController?, toControllerType: T.Type) {
    if var viewControllers: [UIViewController] = navController?.viewControllers {
        viewControllers = viewControllers.reversed()
        for currentViewController in viewControllers {
            if currentViewController .isKind(of: toControllerType) {
                navController?.popToViewController(currentViewController, animated: true)
                break
            }
        }
    }
}

func SetCollLayOut(_ collCard: UICollectionView) {
    let layout = UPCarouselFlowLayout()
    layout.itemSize = CGSize(width: SCREENWIDTH(), height: 225)
    layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 100)
    layout.scrollDirection = .horizontal
    collCard.collectionViewLayout = layout
}

func GoToHomePage() {
    let vc = loadVC(strStoryboardId: SB_HOME, strVCId: "navrecipenew")
    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
        APP_DELEGATE.window?.rootViewController = vc
    }, completion: nil)
}

//MARK:- FIND HEIGHT OR WIDTH
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension Date {
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd yyyy"
        return formatter
    }()
    
    
    var formatted: String {
        return Date.formatter.string(from: self)
    }
    
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

extension String {
    func Localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    var firstCapitalized: String {
        return String(prefix(1)).capitalized + dropFirst()
    }
    
    func RemoveSpace() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? self
    }
}

extension TimeInterval {
    var minuteSecond: String {
        return String(format: "%dm %ds", minute, second)
    }
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}
extension UIViewController {
    func pushViewController(strStoryboardId:String,strVCId:String, animation:Bool) {
        let vc = loadVC(strStoryboardId: strStoryboardId, strVCId: strVCId)
        self.navigationController?.pushViewController(vc, animated: animation)
    }
}


extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

//MARK: FloatingPanelLayout
class MyFloatingPanelLayout: FloatingPanelLayout {
    public var initialPosition: FloatingPanelPosition {
        return .tip
    }

    public var supportedPositions: Set<FloatingPanelPosition> {
        return [.tip, .half]
    }
    
    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .half:
            return 420
        case .tip:
            return 308
        default: return nil
        }
    }
}

//MARK: FloatingPanelLayout
class EquipmentFloatingPanelLayout: FloatingPanelLayout {
    public var initialPosition: FloatingPanelPosition {
        return .tip
    }

    public var supportedPositions: Set<FloatingPanelPosition> {
        return [.tip, .half]
    }
    
    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .half:
            return 580
        case .tip:
            return 65
        default: return nil
        }
    }
}

func AddGradient(_ view: UIView) {
    let colorTop =  Color_Hex(hex: "#78FFD6").withAlphaComponent(0.9).cgColor
    let colorBottom = Color_Hex(hex: "#007991").withAlphaComponent(0.9).cgColor
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [colorTop, colorBottom]
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
    gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
    gradientLayer.frame = view.bounds
    view.layer.insertSublayer(gradientLayer, at:0)
}

func AddBlurEffect(_ view: UIView) {
    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.frame = view.bounds
    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view.addSubview(blurEffectView)
}

extension String {

    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

@IBDesignable class BigSwitch: UISwitch {

    @IBInspectable var scale : CGFloat = 1{
        didSet{
            setup()
        }
    }

    //from storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    //from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    private func setup(){
        self.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

    override func prepareForInterfaceBuilder() {
        setup()
        super.prepareForInterfaceBuilder()
    }
}

extension UserDefaults {
    func setJsonObject<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func getJsonObject<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

func getAssetThumbnail(asset: PHAsset, _ size: CGSize) -> UIImage {
       let manager = PHImageManager.default()
       let option = PHImageRequestOptions()
       var thumbnail = UIImage()
       option.isSynchronous = true
       manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
           thumbnail = result!
       })
       return thumbnail
   }

extension UIView {

    func dropBorder(color: UIColor) {
        layer.borderColor = color.cgColor
        layer.borderWidth = 1
        clipsToBounds = true
    }
    
    func removeBorder() {
        layer.borderColor = UIColor.clear.cgColor
        layer.borderWidth = 0
    }
    
    func dropShadow(scale: Bool = true,color: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.16), opacity: Float = 0.6, offset: CGSize = CGSize(width: 0, height: 2), radius: CGFloat = 4) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

}

func getUniqID() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "ddMMyyyyhhmmssSSS"
    let uniqid = formatter.string(from: Date())
    return uniqid
}

func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
    let widthOffset = downloadedImage.size.width - cellImageFrame.width
    let widthOffsetPercentage = (widthOffset*100)/downloadedImage.size.width
    let heightOffset = (widthOffsetPercentage * downloadedImage.size.height)/100
    let effectiveHeight = downloadedImage.size.height - heightOffset
    return(effectiveHeight)
}

extension UILabel {

    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

        guard let labelText = self.text else { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple

        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }

        // (Swift 4.2 and above) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))


        // (Swift 4.1 and 4.0) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        self.attributedText = attributedString
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

// MARK:- PRIVATE METHOD
func apiCheckRegistered(_ dictData: NSDictionary, completion:@escaping (_ isRegistered: Bool,_ responseDict: NSDictionary?) -> Void) {
    let sURL = SERVER_URL + API_CHECK_REGISTERED
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_CHECK_REGISTERED, parameters: dictData, method: .post, isPassHeaderToken: false , showLoader: true) { (error, responseObject) in
        print("User Info: ", responseObject ?? "")
        if error == nil {
            if let isRegistered = responseObject?.object(forKey: "is_registered_user") as? String {
                if isRegistered == "1" {
                    completion(true, responseObject)
                } else {
                    completion(false, nil)
                }
            } else {
                completion(false, nil)
            }
        } else {
            print(error?.localizedDescription ?? ErrorMSG)
            completion(false, nil)
        }
    }
}

func apiGetUserInfo(compilation:@escaping  (_ isSuccess: Bool) -> Void) {
    let sURL = SERVER_URL + API_USER + "/\(getUserID())"
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_USER, parameters: NSDictionary(), method: .get, isPassHeaderToken: true , showLoader: false) { (error, responseObject) in
        print("User Info: ", responseObject ?? "")
        if error == nil {
            let obj: FMUser = FMUser.init(object: responseObject ?? "")
            if obj.success == "1" {
                APP_DELEGATE.objUser = obj.user
                compilation(true)
            } else {
                compilation(false)
            }
        } else {
            print(error?.localizedDescription ?? ErrorMSG)
            compilation(false)
        }
    }
}

func apiLogout() {
    let sURL = SERVER_URL + API_LOGOUT
    let dictParam: NSDictionary = ["token":getDeviceToken()]
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LOGOUT, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: false) { (error, response) in
        print(response ?? "")
    }
}

func apiLikePost(_ postOrEventID: Int, isLikeEvent: Bool) {
    let dictComment: NSDictionary = ["comment":""]
    let dictParam: NSDictionary = ["comment":dictComment]
    var sURL = String()
    if isLikeEvent {
        sURL = SERVER_URL + "events/\(postOrEventID)/" + API_LIKE
    } else {
        sURL = SERVER_URL + "feed_posts/\(postOrEventID)/" + API_LIKE
    }
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LIKE, parameters: dictParam, method: .post, isPassHeaderToken: true , showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        }
    }
}

func apiUnLikePost(_ postOrEventID: Int, isUnlikeEvent: Bool) {
    let dictComment: NSDictionary = ["comment":""]
    let dictParam: NSDictionary = ["comment":dictComment]
    var sURL = String()
    if isUnlikeEvent {
        sURL = SERVER_URL + "events/\(postOrEventID)/" + API_UNLIKE
    } else {
        sURL = SERVER_URL + "feed_posts/\(postOrEventID)/" + API_UNLIKE
    }
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_UNLIKE, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        }
    }
}

func apiRegisterEvent(_ eventId: Int, completion:@escaping (_ success: Bool) -> Void) {
    let dictComment: NSDictionary = ["comment":""]
    let dictParam: NSDictionary = ["comment":dictComment]
    let sURL = SERVER_URL + "events/\(eventId)/" + API_REGISTER
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_REGISTER, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        } else {
            completion(true)
        }
    }
}

func apiDeleteFeed(_ feedId: Int, completion:@escaping (_ success: Bool) -> Void) {
    let sURL = SERVER_URL + "\(API_DELETE_FEED)/\(feedId)"
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_DELETE_FEED, parameters: [:], method: .delete, isPassHeaderToken: true, showLoader: true) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        } else {
            completion(true)
        }
    }
}

func apiUnRegisterEvent(_ eventId: Int, completion:@escaping (_ success: Bool) -> Void) {
    let dictComment: NSDictionary = ["comment":""]
    let dictParam: NSDictionary = ["comment":dictComment]
    let sURL = SERVER_URL + "events/\(eventId)/" + API_UNREGISTER
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_UNREGISTER, parameters: dictParam, method: .post, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        } else {
            completion(true)
        }
    }
}

func apiLikeImage(_ sImageID: String, type: String) {
    let dictParam: NSDictionary = ["image_type":type]
    let sURL = SERVER_URL + "images/\(sImageID)/" + API_LIKE
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LIKE, parameters: dictParam, method: .post, isPassHeaderToken: true , showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        }
    }
}

func apiUnlikeImage(_ sImageID: String, type: String) {
    let dictParam: NSDictionary = ["image_type":type]
    let sURL = SERVER_URL + "images/\(sImageID)/" + API_UNLIKE
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_UNLIKE, parameters: dictParam, method: .post, isPassHeaderToken: true , showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        }
    }
}

func apiLikeLocation(_ sLID: String) {
    let sURL = SERVER_URL + "locations/\(sLID)/" + API_LIKE
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_LIKE, parameters: NSDictionary(), method: .post, isPassHeaderToken: true , showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        }
    }
}

func apiUnlikeLocation(_ sLID: String) {
    let sURL = SERVER_URL + "locations/\(sLID)/" + API_UNLIKE
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_UNLIKE, parameters: NSDictionary(), method: .post, isPassHeaderToken: true , showLoader: false) { (error, responseObject) in
        print(responseObject ?? "")
        if error != nil {
            print(error?.localizedDescription ?? ErrorMSG)
        }
    }
}

func apiGetFeedPost(_ sPostId: String, completion:@escaping (_ objPost: Posts?) -> Void) {
    showLoaderHUD(strMessage: "")
    let sURL = SERVER_URL + API_GET_FEED_POSTS + "/\(sPostId)"
    HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error)  in
        print(response ?? "")
        hideLoaderHUD()
        let objFeedPosts: FMNotiFeedPost = FMNotiFeedPost.init(object: response ?? "")
        if objFeedPosts.success ?? false {
            completion(objFeedPosts.post)
        } else {
            showMessage(objFeedPosts.message ?? ErrorMSG)
        }
    }
}

func apiGetEventDetails(_ sEventId: String, _ showLoader: Bool = true, completion:@escaping (_ objPost: Events?) -> Void) {
    if showLoader { showLoaderHUD(strMessage: "") }
    let sURL = SERVER_URL + API_GET_EVENTS + "/\(sEventId)"
    HttpRequestManager.sharedInstance.getRequestWithoutParams(endpointurl: sURL, httpHeader: getAuthToken()) { (response, error)  in
        print(response ?? "")
        if showLoader { hideLoaderHUD() }
        let objEvent: FMNotiEvent = FMNotiEvent.init(object: response ?? "")
        if objEvent.success ?? false {
            completion(objEvent.event)
        } else {
            showMessage(objEvent.message ?? ErrorMSG)
        }
    }
}

func apiGetCategory() {
    manageCategory()
    let sURL = SERVER_URL + API_CATEGORY
    HttpRequestManager.sharedInstance.requestWithJsonParam(endpointurl: sURL, service: API_CATEGORY, parameters: NSDictionary(), method: .get, isPassHeaderToken: true, showLoader: false) { (error, responseObject) in
        if error == nil {
            print(responseObject ?? "")
            if responseObject?["success"] as? String == "1" {
                if let arrCat = responseObject?["categories"] as? NSArray {
                    UserDefaults.standard.setValue(arrCat, forKey: "allcategory")
                    manageCategory()
                }
            }
        }
    }
}

func manageCategory() {
    if UserDefaults.standard.object(forKey: "allcategory") != nil {
        APP_DELEGATE.arrAllCategory = NSMutableArray()
        let arrCat = UserDefaults.standard.object(forKey: "allcategory") as! NSArray
        for i in 0..<arrCat.count {
            let dictCat = arrCat.object(at: i) as! NSDictionary
            let objCat: FMCategory = FMCategory.init(object: dictCat)
            APP_DELEGATE.arrAllCategory.add(objCat)
        }
    }
}

func getArrCatName() -> [String] {
    var arrCatName: [String] = [String]()
    for i in 0..<APP_DELEGATE.arrAllCategory.count {
        let objCat: FMCategory = APP_DELEGATE.arrAllCategory[i] as! FMCategory
        let name = objCat.categoryName ?? ""
        arrCatName.append(name)
    }
    return arrCatName
}

func getArrCatIDs() -> [String] {
    var arrCatId: [String] = [String]()
    for i in 0..<APP_DELEGATE.arrAllCategory.count {
        let objCat: FMCategory = APP_DELEGATE.arrAllCategory[i] as! FMCategory
        let name = objCat.categoryId ?? ""
        arrCatId.append(name)
    }
    return arrCatId
}

func getCatImage(_ sCateName: String) -> URL? {
    var sCatImage = ""
    for i in 0..<APP_DELEGATE.arrAllCategory.count {
        let objCat = APP_DELEGATE.arrAllCategory.object(at: i) as! FMCategory
        if objCat.categoryName?.lowercased() == sCateName.lowercased() {
            sCatImage = objCat.categoryIconUrl ?? ""
            break
        }
    }
    return URL(string: sCatImage)
}

func getCategoryID(_ sCateName: String) -> String {
    var sCatID = "0"
    for i in 0..<APP_DELEGATE.arrAllCategory.count {
        let objCat = APP_DELEGATE.arrAllCategory.object(at: i) as! FMCategory
        if objCat.categoryName?.lowercased() == sCateName.lowercased() {
            sCatID = objCat.categoryId ?? "0"
            break
        }
    }
    return sCatID
}

func getCategoryName(_ catID: String) -> String {
    var sCatName = "0"
    for i in 0..<APP_DELEGATE.arrAllCategory.count {
        let objCat = APP_DELEGATE.arrAllCategory.object(at: i) as! FMCategory
        if objCat.categoryId == catID {
            sCatName = objCat.categoryName ?? ""
            break
        }
    }
    return sCatName
}

func UploadImageToCloud(_ img: UIImage, handler: @escaping (String) -> Void) {
    showLoaderHUD(strMessage: "")
    let storage = Storage.storage()
    let storageRef = storage.reference()
    let data = compressImage(image: img)
    let metaData = StorageMetadata()
    metaData.contentType = "image/jpg"
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "ddMMyyyyHH:mm:ss.SSSS"
    let sImageName = "IMG-" + "\(dateFormatter.string(from: Date()))" + "\(GetRandomIntValue())" + "\(GetRandomIntValue())" + "\(GetRandomIntValue())" + "\(GetRandomIntValue())"
    print("imageName: ",sImageName)
    
    //let PRODUCTION_IMAGE_FOLDER = "fishingmap_images"

    let riversRef = storageRef.child("\(FishApp.ImageFolder)/\(sImageName).jpg")
    riversRef.putData(data as Data, metadata: metaData) { (metadata, error) in
        if let error = error {
            print(error.localizedDescription)
            showMessage(error.localizedDescription)
            hideLoaderHUD()
        } else {
            riversRef.downloadURL { (url, error) in
                if error == nil {
                    handler("\(url!)")
                } else {
                    hideLoaderHUD()
                    showMessage(error?.localizedDescription ?? ErrorMSG)
                }
            }
        }
    }
}

func deleteImage(_ sURL: String) {
    let storage = Storage.storage()
    let storageRef = storage.reference(forURL: sURL)
    storageRef.delete { error in
        if let error = error {
            print(error)
        } else {
            print("File deleted successfully")
        }
    }
}

func GetRandomIntValue() -> Int {
    return Int(arc4random_uniform(UInt32(10)))
}

func convertStringToTimeStamp(_ sDate: String) -> Int {
    let dfmatter = DateFormatter()
    dfmatter.dateFormat="dd-MM-yyyy"
    let date = dfmatter.date(from: sDate)
    let dateStamp:TimeInterval = date!.timeIntervalSince1970
    let dateSt:Int = Int(dateStamp)
    return dateSt
}

func convertHashtags(text:String) -> NSAttributedString {
    let attrString = NSMutableAttributedString(string: text)
    attrString.beginEditing()
    do {
        // Find all the hashtags in our string
        let regex = try NSRegularExpression(pattern: "(?:\\s|^)(#(?:[a-zA-Z].*?|\\d+[a-zA-Z]+.*?))\\b", options: NSRegularExpression.Options.anchorsMatchLines)
        let results = regex.matches(in: text, options: NSRegularExpression.MatchingOptions.withoutAnchoringBounds, range: NSMakeRange(0, text.count))
        let array = results.map { (text as NSString).substring(with: $0.range) }
        for hashtag in array {
            // get range of the hashtag in the main string
            let range = (attrString.string as NSString).range(of: hashtag)
            // add a colour to the hashtag
            attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: Color_Hex(hex: "056989") , range: range)
        }
        attrString.endEditing()
    }
    catch {
        attrString.endEditing()
    }
    return attrString
}

func getPinName(_ imageType: LocateImageType) -> String {
    var sImage = ""
    if imageType == .Fish {
        sImage = "fish_orange_pin"
    } else if imageType == .ParkingSpots {
        sImage = "ic_pin_parking"
    } else if imageType == .TopScene {
        sImage = "ic_pin_scene"
    } else if imageType == .TopTerrain {
        sImage = "ic_pin_terrain"
    } else if imageType == .Restaurant {
        sImage = "ic_pin_restaurant"
    } else if imageType == .Hotels {
        sImage = "ic_pin_hotel"
    }
    return sImage
}

func getPushImageType(_ sText: String) -> LocateImageType {
    var imageType = LocateImageType.Fish
    if sText == "fish" || sText == "FishLocation" {
        imageType = .Fish
    } else if sText == "terrain" {
        imageType = .TopTerrain
    } else if sText == "scene" {
        imageType = .TopScene
    } else if sText == "parking" {
        imageType = .ParkingSpots
    } else if sText == "restaurant" || sText == "RestaurantLocation" {
        imageType = .Restaurant
    } else if sText == "hotel" || sText == "HotelLocation" {
        imageType = .Hotels
    }
    return imageType
}

extension String {

  func CGFloatValue() -> CGFloat? {
    guard let doubleValue = Double(self) else {
      return nil
    }

    return CGFloat(doubleValue)
  }
}

func getImageTypeNameInJapan(_ sText: String) -> String {
//    var arrImageType = ["Fish", "Terrain", "Scene", "Parking", "Restaurant", "Hotel"]
//    var arrJapaneseImageType = ["魚", "地形", "シーン", "パーキング", "レストラン", "ホテル"]
    if sText == "Fish" {
        return "魚"
    } else if sText == "Terrain" {
        return "地形"
    } else if sText == "Scene" {
        return "シーン"
    } else if sText == "Parking" {
        return "パーキング"
    } else if sText == "Restaurant" {
        return "レストラン"
    } else if sText == "Hotel" {
        return "ホテル"
    } else {
        return ""
    }
}

func gotoTabBar(_ selectedIndex: Int = 0) {
    let vc = loadVC(strStoryboardId: SB_HOME, strVCId: idTabBarVC) as! TabBarVC
    vc.index = selectedIndex
    let nav = UINavigationController(rootViewController: vc)
    nav.setNavigationBarHidden(true, animated: false)
    APP_DELEGATE.window?.rootViewController = nav
    UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
    }, completion: nil)
}
