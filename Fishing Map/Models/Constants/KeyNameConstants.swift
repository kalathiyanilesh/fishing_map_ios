
import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

public let GOOGLE_MAP_KEY: String = "AIzaSyAP2ejsgHsQfh7xmAV25DW8hrmPnA-u1SY" // for google map
public let GOOGLE_CLIENTID: String = "861688223888-f7ususqg1t1n7sqjv98ac2cvofc8toe4.apps.googleusercontent.com" //for google signin

//MARK:- HTTPHEADER
let BLANK_HEADER = [String:String]()

//MARK:- APPNAME
public let APPNAME :String = "釣りマップ"

//MARK:- LOADER COLOR
let loadercolor = [Color_Hex(hex: "1C2244"), Color_Hex(hex: "131835")]

//MARK:- PLACEHOLDER IMAGES
let PLACE_HOLDER_USER = UIImage(named: "temp1")
let PLACE_HOLDER = UIImage(named: "placeholder")
let PLACE_HOLDER_CATEGORY = UIImage(named: "Select_cat")

//MARK:- HEADER KEY
public let kHEADER = "token"
public let kTIMEZONE = "timezone"

//MARK:- PAGELIMIT (PAGINATION)
public let PAGE_LIMIT = 10

//MARK:- COLORDS
let APP_COLOR = Color_Hex(hex: "8F6BFF")

//MARK:- STORY BOARD NAMES
public let SB_LOGIN:String = "Login"
public let SB_MAIN:String = "Main"
public let SB_HOME:String = "Home"
public let SB_FEED:String = "Feed"
public let SB_SIDEMENU:String = "SideMenu"
public let SB_PROFILE:String = "Profile"
public let SB_LOCATE:String = "Locate"
public let SB_Information = "Information"
public let SB_Submission = "Submission"
public let SB_Events = "Events"
public let SB_Store = "Store"
public let SB_Select_Location = "SelectLocation"

//MARK:- USER DEFAULTS KEY
let IS_LOGIN:String = "islogin"
let UD_AUTHTOKEN:String = "UDAUTHTOKEN"
let UD_UserId:String = "UDUserId"
let UD_UserData:String = "UDUserData"
let UD_DeviceToken:String = "UDDeviceToken"

//MARK:- CONTROLLERS ID
public let idNavnBoarding = "navOnboarding"
public let idTabBarVC = "TabBarVC"
public let idFeedDetailsVC = "FeedDetailsVC"
public let idFeedSearchVC = "FeedSearchVC"
public let idSideMenuVC = "SideMenuVC"
public let idProfileVC = "ProfileVC"

//MARK - MESSAGES
let ServerResponseError = "サーバーが応答していません"
let DontHaveCamera = "You don't have camera."
let InternetNotAvail = "インターネットがありません"
let ErrorMSG = "問題が発生しました。もう一度操作してください。"

//MARK:- UserDefaults
let UD_Location_type = "location_type"
