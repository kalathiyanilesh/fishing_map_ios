//
//  HttpRequestManager.swift
//  Copyright © 2020 Nilesh. All rights reserved.

import UIKit
import SwiftyJSON
import Alamofire

class HttpRequestManager
{
    static let sharedInstance = HttpRequestManager()
    var alamoFireManager = Alamofire.SessionManager.default
    let additionalHeader = ["User-Agent": "iOS"]
    var resObjects:AnyObject!
    
    //MARK:- GET & POST
    func requestWithJsonParam(endpointurl:String,
                              service:String,
                              parameters:NSDictionary,
                              method: HTTPMethod,
                              isPassHeaderToken: Bool,
                              showLoader:Bool,
                              isObjInString: Bool = true,
                              responseData:@escaping  (_ error: NSError?,_ responseDict: NSDictionary?) -> Void)
    {
        if isConnectedToNetwork()
        {
            if(showLoader) { showLoaderHUD(strMessage: "") }
            print("URL : \(endpointurl) \nParam :\(parameters)")
            print("Response Json String: ", convertDictionaryToJSONString(dic: parameters) ?? "")
            ShowNetworkIndicator(xx: true)
            var headers: HTTPHeaders = HTTPHeaders()
            if isPassHeaderToken {
                headers = getAuthToken()
                print("Headers:", headers)
            }
            alamoFireManager.request(endpointurl, method: method, parameters: parameters as? Parameters, encoding: getEncoding(method), headers: headers)
                .responseString(completionHandler: { (responseString) in
                    ShowNetworkIndicator(xx: false)
                    if(showLoader) { hideLoaderHUD() }
                    if(responseString.value == nil)
                    {
                        responseData(responseString.error as NSError?, nil)
                    }
                    else
                    {
                        let strResponse = "\(responseString.value!)"
                        //let arr = strResponse.components(separatedBy: "\n")
                        //let dict =  convertStringToDictionary(str:(arr.last  ?? "")!)
                        let dict =  convertStringToDictionary(str:strResponse)
                        if dict != nil {
                            var dictResponse : NSDictionary = dict! as NSDictionary
                            if isObjInString {
                                dictResponse = dictionaryOfFilteredBy(dict: dict! as NSDictionary)
                            }
                            if service == API_ADD_COMMENT { dictResponse = dict! as NSDictionary }
                            print("Response Json String: ", convertDictionaryToJSONString(dic: dictResponse) ?? "")
                            if let success = dictResponse["success"] as? String {
                                if success == "1" {
                                    responseData(nil,dictResponse)
                                } else {
                                    if let msg = dictResponse["message"] as? String {
                                        showMessage(msg)
                                        let sErrorCode = dictResponse["errorcode"] as? String ?? ""
                                        if (msg == "Authorization token not provided" || msg == "Permission denied for this account type" || msg == "There is some problem with the token. Please issue a new token." || sErrorCode == "411") && isUserLogin() {
                                            Logoutuser()
                                        }
                                    }
                                }
                            } else {
                                responseData(nil,dictResponse)
                            }
                        }
                    }
                })
        }
    }
    
    func getRequestWithoutParams(endpointurl:String, httpHeader:[String: String], responseData:@escaping (_ data:AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        let header = httpHeader
        print("URL :", endpointurl)
        print(httpHeader)
        alamoFireManager.request(endpointurl, method: .get, headers: header).responseJSON { (response:DataResponse<Any>) in
            ShowNetworkIndicator(xx: false)
            
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?)
            }
            else
            {
                switch(response.result)
                {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil)
                    }
                    break
                case .failure(_):
                    responseData(nil, response.result.error as NSError?)
                    break
                }
            }
        }
    }
    
    func getRequestWithParams(endpointurl:String, httpHeader:[String: String], parameters: [String:String], responseData:@escaping (_ data:AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        let header = httpHeader
        print("URL :", endpointurl)
        print(httpHeader)
        alamoFireManager.request(endpointurl, method: .get, parameters: parameters, headers: header).responseJSON { (response:DataResponse<Any>) in
            ShowNetworkIndicator(xx: false)
            
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?)
            }
            else
            {
                switch(response.result)
                {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil)
                    }
                    break
                case .failure(_):
                    responseData(nil, response.result.error as NSError?)
                    break
                }
            }
        }
    }
    
    func getEncoding(_ method: HTTPMethod) -> ParameterEncoding {
        switch method {
        case .post:
            return JSONEncoding.default
        case .get:
            return URLEncoding.queryString
        default:
            return JSONEncoding.default
        }
    }
}
