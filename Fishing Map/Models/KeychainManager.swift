//
//  KeychainManager.swift
//  vMedico
//
//  Created by iMac on 20/03/21.
//  Copyright © 2021 skycap. All rights reserved.
//

import UIKit
import JNKeychain

class KeychainManager: NSObject {
    static let sharedInstance = KeychainManager()
    
    func getValueFromKeychain(_ key: String, _ value: String?) -> String {
        var email = self.keychain_valueForKey(key) as? String
        if email == nil {
            email = value
            self.keychain_setObject(email! as AnyObject, forKey: key)
        }
        return email!
    }
    
    // MARK: - Keychain
    
    func keychain_setObject(_ object: AnyObject, forKey: String) {
        let result = JNKeychain.saveValue(object, forKey: forKey)
        if !result {
            print("keychain saving: smth went wrong")
        }
    }
    
    func keychain_deleteObjectForKey(_ key: String) -> Bool {
        let result = JNKeychain.deleteValue(forKey: key)
        return result
    }
    
    func keychain_valueForKey(_ key: String) -> AnyObject? {
        let value = JNKeychain.loadValue(forKey: key)
        return value as AnyObject?
    }
}
